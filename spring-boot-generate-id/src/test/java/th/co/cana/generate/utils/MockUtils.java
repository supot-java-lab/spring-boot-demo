package th.co.cana.generate.utils;

import th.co.cana.generate.models.TransactionOrderReq;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */
public final class MockUtils {
    private static final String[] CODES = {"PC-1001", "PC-1002", "PC-1003"};
    public static List<TransactionOrderReq> createOrderReqs(int total, int qty) {
        List<TransactionOrderReq> orderReqs = new ArrayList<>();
        for (int i = 1; i <= total; i++) {
            int index = 0;
            if (i % 2 == 0) {
                index = 1;
            } else if (i % 3 == 0) {
                index = 2;
            }
            TransactionOrderReq orderReq = new TransactionOrderReq();
            orderReq.setProductCode(CODES[index]);
            orderReq.setProductName("product" + i);
            orderReq.setProductQty(qty);
            orderReq.setProductAmt(BigDecimal.valueOf((long) i * i));
            orderReqs.add(orderReq);
        }
        return orderReqs;
    }
}
