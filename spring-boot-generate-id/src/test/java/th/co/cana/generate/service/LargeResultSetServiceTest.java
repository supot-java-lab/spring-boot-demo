package th.co.cana.generate.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import th.co.cana.generate.BaseUnitTest;

import java.util.concurrent.TimeUnit;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class LargeResultSetServiceTest extends BaseUnitTest {

    @Autowired
    private LargeResultSetService service;

    @DisplayName("[LargeResultSetService]: exportCustomer")
    @Test
    @Order(1)
    void exportCustomer() throws InterruptedException {
        int start = 0;
        int limit = 5000;
        for (int i = 0; i <= 450; i++) {
            service.exportCustomer(start, limit);
            start = start + limit;
            TimeUnit.SECONDS.sleep(4);
        }
        Assertions.assertTrue(Boolean.TRUE);
    }
}
