/*
 * -------------------------------------------------------------------------------
 * Copyright (c) 2023. The Siam Commercial Bank Public Company Limited. All rights reserved.
 * -------------------------------------------------------------------------------
 */

package th.co.cana.generate;

import org.junit.jupiter.api.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;

/**
 * @author supot.jdev
 * @version 1.0
 */

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Tag("skip")
public class BaseIntegrationTest extends SpringUnitTest {
    @Value(value="${local.server.port}")
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    public TestRestTemplate restTemplate() {
        return restTemplate;
    }

    public String baseUrl() {
        return "http://localhost:" + port;
    }
}
