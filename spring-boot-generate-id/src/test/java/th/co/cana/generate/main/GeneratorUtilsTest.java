package th.co.cana.generate.main;

import io.github.jdevlibs.utils.GeneratorUtils;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class GeneratorUtilsTest {
    public static void main(String[] args) {
        Long id = GeneratorUtils.getSnowFlakeId();
        System.out.println(id);

        String idKey = "0099-" + GeneratorUtils.getSnowFlakeIdString();
        System.out.println(idKey);
    }
}
