package th.co.cana.generate.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import th.co.cana.generate.BaseUnitTest;
import th.co.cana.generate.entities.RunningNo;
import th.co.cana.generate.models.RunningNumberInfo;
import th.co.cana.generate.models.TransactionOrderReq;
import th.co.cana.generate.models.TransactionOrderResp;
import th.co.cana.generate.utils.MockUtils;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Slf4j
public class OrderServiceTest extends BaseUnitTest {

    @Autowired
    private TransactionOrderService service;
    @Autowired
    private GenerateRunningService generateRunningService;
    @Autowired
    private GenRunningService genRunningService;

    @DisplayName("[PurchaseInfoService]: processPurchase")
    @Disabled
    @Test
    @Order(1)
    void processOrder() {
        final List<TransactionOrderReq> items = MockUtils.createOrderReqs(1, 1);
        TransactionOrderResp resp = service.processOrder(items.get(0));
        log.info("{} -> {}", Thread.currentThread().getName(), resp);
    }

    @DisplayName("[PurchaseInfoService]: processPurchase")
    @Disabled
    @Test
    @Order(2)
    void processPurchase() throws InterruptedException {
        final List<TransactionOrderReq> items = MockUtils.createOrderReqs(50, 1);
        ExecutorService executorService = Executors.newFixedThreadPool(items.size());
        int numberOfThreads = items.size();
        CountDownLatch latch = new CountDownLatch(numberOfThreads);
        for (int i = 0; i < numberOfThreads; i++) {
            int finalI = i;
            executorService.execute(() -> {
                TransactionOrderReq req = items.get(finalI);
                TransactionOrderResp resp = service.processOrder(req);
                log.info("{} -> {}", Thread.currentThread().getName(), resp);
                latch.countDown();
            });
        }
        latch.await();
    }

    @DisplayName("[PurchaseInfoService]: generateRunningNo")
    @Disabled
    @Test
    @Order(3)
    void generateRunningNo() throws InterruptedException {
        int numberOfThreads = 100;
        ExecutorService executorService = Executors.newFixedThreadPool(numberOfThreads + 3);
        CountDownLatch latch = new CountDownLatch(numberOfThreads);
        for (int i = 0; i < numberOfThreads; i++) {
            executorService.execute(() -> {
                RunningNumberInfo resp = generateRunningService.generateRunningNo(RunningNo.RunningType.DBT);
                log.info("{} -> {}", Thread.currentThread().getName(), resp.runningNumber());
                latch.countDown();
            });
        }
        latch.await();
    }

    @DisplayName("[PurchaseInfoService]: generateRunningNo_map")
    //@Disabled
    @Test
    @Order(4)
    void generateRunningNo_map() throws InterruptedException {
        int numberOfThreads = 100;
        ExecutorService executorService = Executors.newFixedThreadPool(numberOfThreads + 3);
        CountDownLatch latch = new CountDownLatch(numberOfThreads);
        for (int i = 0; i < numberOfThreads; i++) {
            executorService.execute(() -> {
                Long number;
                try {
                    number = genRunningService.generateRunningNo("DBT");
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
                log.info("{} -> {}", Thread.currentThread().getName(), number);
                latch.countDown();
            });
        }
        latch.await();
    }
}
