package th.co.cana.generate;

import io.github.jdevlibs.utils.Systems;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
* @author supot.jdev
* @version 1.0
*/

@ExtendWith(SpringExtension.class)
@TestMethodOrder(OrderAnnotation.class)
public abstract class SpringUnitTest {

	@BeforeAll
	static void initialConfiguration() {
		Systems.initial();
	}
}
