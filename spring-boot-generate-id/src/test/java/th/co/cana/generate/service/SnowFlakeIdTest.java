package th.co.cana.generate.service;

import io.github.jdevlibs.utils.GeneratorUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import th.co.cana.generate.BaseUnitTest;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Slf4j
public class SnowFlakeIdTest extends BaseUnitTest {

    @DisplayName("[GeneratorUtils]: getSnowFlakeIdExecutor")
    @Disabled
    @Test
    @Order(1)
    void getSnowFlakeIdExecutor() throws InterruptedException {
        Map<String, Object> params = new HashMap<>();
        int numberOfThreads = 10000;
        ExecutorService executorService = Executors.newFixedThreadPool(1000);
        CountDownLatch latch = new CountDownLatch(numberOfThreads);
        for (int i = 0; i < numberOfThreads; i++) {
            executorService.execute(() -> {
                String id = GeneratorUtils.getSnowFlakeIdString();
                params.put(id,null);
                log.info("{} -> {}", Thread.currentThread().getName(), id);
                latch.countDown();
            });
        }
        latch.await();
        log.info("Run of : {} -> {}", numberOfThreads, params.size());
        Assertions.assertEquals(numberOfThreads, params.size());
    }

    @DisplayName("[GeneratorUtils]: getSnowFlakeId")
    @Disabled
    @Test
    @Order(2)
    void getSnowFlakeId() {
        Map<Long, Object> params = new HashMap<>();
        int total = 2_000_000;
        for (int i = 0; i < total; i++) {
            Long id = GeneratorUtils.getSnowFlakeId();
            params.put(id,null);
        }
        log.info("Run of : {} -> {}", total, params.size());
        //log.info("Initial Memory Size: {}", GraphLayout.parseInstance(params).totalSize());
        Assertions.assertEquals(total, params.size());
    }

    @DisplayName("[GeneratorUtils]: getSnowFlakeIdString")
    @Test
    @Order(3)
    void getSnowFlakeIdString() {
        Map<String, Object> params = new HashMap<>();
        int total = 2_000_000;
        for (int i = 0; i < total; i++) {
            String id = GeneratorUtils.getSnowFlakeIdString();
            params.put(id,null);
        }
        log.info("Run of : {} -> {}", total, params.size());
        //log.info("Initial Memory Size: {}", GraphLayout.parseInstance(params).totalSize());
        Assertions.assertEquals(total, params.size());
    }
}
