package th.co.cana.generate.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import th.co.cana.generate.utils.CacheUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class CacheUtilsTest {

    @Test
    void generateOrderNumber() throws InterruptedException {
        int numberOfThreads = 700;
        ExecutorService executorService = Executors.newFixedThreadPool(numberOfThreads + 3);
        CountDownLatch latch = new CountDownLatch(numberOfThreads);
        Map<Long, Long> keys = new HashMap<>();
        for (int i = 0; i < numberOfThreads; i++) {
            executorService.execute(() -> {
                long number = CacheUtils.generateOrderNumber("DBT");
                keys.put(number, null);
                System.out.println(number);
                latch.countDown();
            });
        }
        latch.await();
        Assertions.assertEquals(numberOfThreads, keys.size());
    }
}
