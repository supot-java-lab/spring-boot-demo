package th.co.cana.generate.repository.dao;

public interface LargeResultSetDao {

    void exportCustomer(int start, int limit);
}
