package th.co.cana.generate.models;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
public class TransactionOrderResp implements Serializable {

    private String transactionId;
    private Long orderId;
    private LocalDateTime transactionDate;
}
