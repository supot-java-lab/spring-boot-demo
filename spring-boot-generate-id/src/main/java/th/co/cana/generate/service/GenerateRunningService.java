package th.co.cana.generate.service;

import io.github.jdevlibs.utils.Utils;
import io.github.jdevlibs.utils.Validators;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import th.co.cana.generate.entities.RunningNo;
import th.co.cana.generate.excption.ServiceException;
import th.co.cana.generate.models.RunningNumberInfo;
import th.co.cana.generate.repository.RunningNoRepository;

/**
 * @author supot.jdev
 * @version 1.0
 */

@RequiredArgsConstructor
@Service
public class GenerateRunningService extends AbstractService {

    private final RunningNoRepository runningNoRepository;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public RunningNumberInfo generateRunningNo(RunningNo.RunningType type) {
        logger.info("Starting generating running no");
        try {
            RunningNo entity = runningNoRepository.findFirstByType(RunningNo.RunningType.DBT);
            if (Validators.isNull(entity)) {
                entity = new RunningNo();
                entity.setType(RunningNo.RunningType.DBT);
                entity.setRunningNo(1L);
                entity.setPaddingNo(0);
                entity.setRunningDoc(Utils.padLeft(entity.getRunningNo().intValue(), 10, "0"));
            } else {
                Long nextNumber = entity.getRunningNo() + 1;
                entity.setRunningNo(nextNumber);
                entity.setRunningDoc(Utils.padLeft(entity.getRunningNo().intValue(), 10, "0"));
            }
            runningNoRepository.saveAndFlush(entity);

            RunningNumberInfo runningNumberInfo = new RunningNumberInfo();
            runningNumberInfo.setLastRunningNo(entity.getRunningNo());
            runningNumberInfo.setRunningDoc(entity.getRunningDoc());
            logger.info("{}", runningNumberInfo);
            return runningNumberInfo;
        } catch (Exception ex) {
            throw new ServiceException(ex);
        } finally {
            logger.info("Finished generating running no");
        }
    }
}
