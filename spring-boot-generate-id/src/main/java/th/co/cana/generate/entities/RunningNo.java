package th.co.cana.generate.entities;

import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import th.co.cana.generate.spring.SnowFlakeLongGenerated;

@Getter
@Setter
@Entity
@Table(name = "running_no")
public class RunningNo {
    @Id
    @SnowFlakeLongGenerated
    @Column(name = "id", nullable = false)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "type")
    private RunningType type;

    @Column(name = "running_no")
    private Long runningNo;

    @Size(max = 50)
    @Column(name = "running_doc", length = 50)
    private String runningDoc;

    @Column(name = "padding_no")
    private Integer paddingNo;

    @Size(max = 4)
    @Column(name = "branch_code", length = 4)
    private String branchCode;


    public enum RunningType {
        DBT("DBT"),
        SRS("SRS");

        final String desc;
        RunningType(String desc){
            this.desc = desc;
        }

        public String getDesc() {
            return desc;
        }
    }
}