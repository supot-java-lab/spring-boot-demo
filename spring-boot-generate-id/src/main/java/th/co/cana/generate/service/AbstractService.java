package th.co.cana.generate.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author supot.jdev
 * @version 1.0
 */
public abstract class AbstractService {
    protected static final Logger logger = LoggerFactory.getLogger(AbstractService.class);
}