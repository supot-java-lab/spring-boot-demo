package th.co.cana.generate.utils;

import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Slf4j
public class CacheUtils {
    private static final Map<String, String> customerMaps = new HashMap<>();
    private static final Map<String, Long> KEY_ORDER_NUMBERS = new ConcurrentHashMap<>();

    public static void clearCustomerCache() {
        log.info("clear customer cache size : {}", customerMaps.size());
        customerMaps.clear();
    }

    public static void addCustomer(String customerId) {
        customerMaps.put(customerId, null);
    }

    public static boolean isExitsCustomer(String customerId) {
        return customerMaps.containsKey(customerId);
    }

    public static Long generateOrderNumber(String keyName) {
        return KEY_ORDER_NUMBERS.compute(keyName, (key, value) -> (value == null) ? 1 : value + 1);
    }
}
