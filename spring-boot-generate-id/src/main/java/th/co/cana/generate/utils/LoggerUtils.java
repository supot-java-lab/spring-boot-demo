package th.co.cana.generate.utils;

import org.slf4j.MDC;

/**
 * @author supot.jdev
 * @version 1.0
 */
public final class LoggerUtils {
    public static final String LOG_MDC_TRACKING_ID = "trackingId";

    public static String getTrackingId() {
        return MDC.get(LOG_MDC_TRACKING_ID);
    }
}
