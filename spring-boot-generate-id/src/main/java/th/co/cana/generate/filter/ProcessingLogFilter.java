package th.co.cana.generate.filter;

import io.github.jdevlibs.utils.GeneratorUtils;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.lang.NonNull;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import th.co.cana.generate.utils.LoggerUtils;

import java.io.IOException;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Slf4j
public class ProcessingLogFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(@NonNull HttpServletRequest request, @NonNull HttpServletResponse response
            , @NonNull FilterChain filterChain) throws ServletException, IOException {

        String path = getRequestPath();
        try {
            MDC.put(LoggerUtils.LOG_MDC_TRACKING_ID, GeneratorUtils.getSnowFlakeIdString());
            log.info("Starting call api : {}", path);
            filterChain.doFilter(request, response);
        } finally {
            log.info("Finished call api : {}", path);
            MDC.remove(LoggerUtils.LOG_MDC_TRACKING_ID);
        }
    }

    private String getRequestPath() {
        try {
            return ServletUriComponentsBuilder.fromCurrentRequestUri().build().getPath();
        } catch (Exception ex) {
            return null;
        }
    }
}