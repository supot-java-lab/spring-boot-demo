package th.co.cana.generate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import th.co.cana.generate.entities.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
}