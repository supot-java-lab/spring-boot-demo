/*
 * -----------------------------------------------------------------------------------
 * Copyright (c) 2024.  Tourism Authority of Thailand (TAT) All rights reserved
 * -----------------------------------------------------------------------------------
 */

package th.co.cana.generate.spring;

import org.hibernate.annotations.IdGeneratorType;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @author supot.jdev
 * @version 1.0
 */
@IdGeneratorType( SnowFlakeLongGenerator.class )
@Target({METHOD, FIELD})
@Retention(RUNTIME)
public @interface SnowFlakeLongGenerated {
    String name() default "";
}