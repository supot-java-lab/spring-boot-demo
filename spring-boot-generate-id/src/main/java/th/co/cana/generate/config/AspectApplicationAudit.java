package th.co.cana.generate.config;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;
import th.co.cana.generate.utils.AppConstants;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Aspect
@Component
@ConditionalOnProperty(value = "application.performance-audit-enabled", havingValue = "true")
@Slf4j
public class AspectApplicationAudit {

    @Around(AppConstants.Aspects.REPOSITORY)
    public Object executeRepositoryProcess(final ProceedingJoinPoint joinPoint) throws Throwable {
        return execute(joinPoint);
    }

    @Around(AppConstants.Aspects.DAO)
    public Object executeDaoProcess(final ProceedingJoinPoint joinPoint) throws Throwable {
        return execute(joinPoint);
    }

    @Around(AppConstants.Aspects.SERVICE)
    public Object executeServiceProcess(final ProceedingJoinPoint joinPoint) throws Throwable {
        return execute(joinPoint);
    }

    private Object execute(final ProceedingJoinPoint joinPoint) throws Throwable {
        StopWatch stopWatch = new StopWatch();
        try {
            stopWatch.start();
            return joinPoint.proceed();
        } finally {
            stopWatch.stop();
            logs(stopWatch, joinPoint);
        }
    }

    private void logs(final StopWatch stopWatch, final ProceedingJoinPoint joinPoint) {
        try {
            log.info("Performance Audit[{}.{}] execution time : {} ms",
                    joinPoint.getTarget().getClass().getName(),
                    joinPoint.getSignature().getName(),
                    stopWatch.getTotalTimeMillis());
        } catch (Exception ex) {
            //Ignore
        }
    }
}