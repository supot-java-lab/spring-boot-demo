package th.co.cana.generate.csv;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class CsvException extends Exception {
    public CsvException(String message) {
        super(message);
    }

    public CsvException(String message, Throwable cause) {
        super(message, cause);
    }

    public CsvException(Throwable cause) {
        super(cause);
    }
}
