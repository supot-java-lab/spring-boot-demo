package th.co.cana.generate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import th.co.cana.generate.entities.TransactionOrder;

public interface TransactionOrderRepository extends JpaRepository<TransactionOrder, Long> {
}