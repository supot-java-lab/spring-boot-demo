package th.co.cana.generate.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import th.co.cana.generate.repository.dao.LargeResultSetDao;

/**
 * @author supot.jdev
 * @version 1.0
 */

@RequiredArgsConstructor
@Service
public class LargeResultSetService {
    private final LargeResultSetDao largeResultSetDao;

    public void exportCustomer(int start, int limit) {
        largeResultSetDao.exportCustomer(start, limit);
    }
}
