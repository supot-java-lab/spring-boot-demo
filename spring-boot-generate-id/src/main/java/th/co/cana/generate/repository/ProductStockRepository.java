package th.co.cana.generate.repository;

import jakarta.persistence.LockModeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.transaction.annotation.Transactional;
import th.co.cana.generate.entities.ProductStock;

public interface ProductStockRepository extends JpaRepository<ProductStock, String> {

    @Transactional
    @Lock(LockModeType.PESSIMISTIC_WRITE)
    ProductStock findByProductCode(String productCode);
}