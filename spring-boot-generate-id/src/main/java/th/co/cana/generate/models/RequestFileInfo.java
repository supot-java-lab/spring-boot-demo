package th.co.cana.generate.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Getter
@Setter
@ToString
public class RequestFileInfo implements Serializable {
    private String name;
    private String originalFilename;
    private String contentType;
    private long size;
}
