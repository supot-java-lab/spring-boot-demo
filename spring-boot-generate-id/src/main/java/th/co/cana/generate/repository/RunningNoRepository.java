package th.co.cana.generate.repository;

import jakarta.persistence.LockModeType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;
import org.springframework.transaction.annotation.Transactional;
import th.co.cana.generate.entities.RunningNo;

public interface RunningNoRepository extends JpaRepository<RunningNo, Long> {

  @Transactional
  @Lock(LockModeType.PESSIMISTIC_WRITE)
  RunningNo findFirstByType(RunningNo.RunningType type);
}