/*
 * -----------------------------------------------------------------------------------
 * Copyright (c) 2024.  Tourism Authority of Thailand (TAT) All rights reserved
 * -----------------------------------------------------------------------------------
 */

package th.co.cana.generate.spring;

import io.github.jdevlibs.utils.GeneratorUtils;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.id.IdentifierGenerator;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class SnowFlakeLongGenerator implements IdentifierGenerator {

    @Override
    public Long generate(SharedSessionContractImplementor session, Object object) {
        return GeneratorUtils.getSnowFlakeId();
    }
}
