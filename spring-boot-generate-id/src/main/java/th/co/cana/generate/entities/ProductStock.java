package th.co.cana.generate.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "product_stock")
public class ProductStock {
    @Id
    @Column(name = "product_code", nullable = false, length = 15)
    private String productCode;

    @Size(max = 100)
    @Column(name = "product_name", length = 100)
    private String productName;

    @Column(name = "product_qty")
    private Integer productQty;

}