package th.co.cana.generate.models;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
public class TransactionOrderReq implements Serializable {

    @NotBlank
    private String productCode;
    @NotBlank
    private String productName;
    @NotNull
    private Integer productQty;
    @NotNull
    private BigDecimal productAmt;
}
