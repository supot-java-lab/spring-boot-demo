package th.co.cana.generate.service;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import th.co.cana.generate.entities.ProductStock;
import th.co.cana.generate.entities.TransactionOrder;
import th.co.cana.generate.excption.ServiceException;
import th.co.cana.generate.models.RunningNumberInfo;
import th.co.cana.generate.models.TransactionOrderReq;
import th.co.cana.generate.repository.ProductStockRepository;
import th.co.cana.generate.repository.TransactionOrderRepository;

import java.time.LocalDateTime;

/**
 * @author supot.jdev
 * @version 1.0
 */
@RequiredArgsConstructor
@Service
public class ProcessOrderService extends AbstractService {
    private final TransactionOrderRepository transactionOrderRepository;
    private final ProductStockRepository stockRepository;

    @Transactional
    public TransactionOrder createOrder(final TransactionOrderReq req, RunningNumberInfo numberInfo) {

        logger.info("\tStep 2.1 Create order item");
        TransactionOrder order = new TransactionOrder();
        BeanUtils.copyProperties(req, order);
        order.setTransactionDate(LocalDateTime.now());
        order.setTransactionNo(numberInfo.getRunningDoc());
        transactionOrderRepository.saveAndFlush(order);

        logger.info("\tStep 2.2 Update stock item");
        ProductStock stock = stockRepository.findByProductCode(req.getProductCode());
        if (stock == null) {
            throw new ServiceException("Can't find stock");
        }
        if (stock.getProductQty() < req.getProductQty()) {
            throw new ServiceException("Out of stock");
        }

        int qty = stock.getProductQty() - req.getProductQty();
        stock.setProductQty(qty);
        stockRepository.saveAndFlush(stock);

        return order;
    }
}
