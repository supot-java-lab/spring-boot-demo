package th.co.cana.generate.config;

import io.github.jdevlibs.spring.utils.HttpUtils;
import io.github.jdevlibs.spring.utils.JsonUtils;
import io.github.jdevlibs.utils.Validators;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import th.co.cana.generate.config.annotation.ApiAuditEventLog;
import th.co.cana.generate.entities.ApiAuditLog;
import th.co.cana.generate.models.RequestLog;
import th.co.cana.generate.service.AuditLogService;
import th.co.cana.generate.utils.AppConstants;
import th.co.cana.generate.utils.AspectUtils;

import java.time.LocalDateTime;

/**
 * @author supot.jdev
 * @version 1.0
 */

@RequiredArgsConstructor
@Slf4j
@Aspect
@Component
@ConditionalOnProperty(value = "application.request-api-audit-enabled", havingValue = "true")
public class AspectApiRequestAudit {
    private static final int TEXT_LEN = 5000;

    private final AuditLogService auditLogService;

    @Around(AppConstants.Aspects.AUDIT_LOG)
    public Object executeAuditLog(ProceedingJoinPoint joinPoint) throws Throwable {
        ApiAuditEventLog auditLog = AspectUtils.getAuditLogger(joinPoint);
        return execute(joinPoint, auditLog);
    }

    private Object execute(final ProceedingJoinPoint joinPoint, ApiAuditEventLog eventLog) throws Throwable {

        Object respObj;
        ApiAuditLog auditLog = null;
        String apiUrl = HttpUtils.getRequestPath();
        String respValue = null;
        try {
            RequestLog apiEventLog = AspectUtils.toRequestLog(joinPoint);
            if (Validators.isNotNull(eventLog)) {
                auditLog = AspectUtils.toApiAuditLog(apiEventLog, eventLog);
            }
            respObj = joinPoint.proceed();

            if (eventLog != null && !eventLog.ignoreResponse()) {
                respValue = JsonUtils.json(respObj);
            }
        } catch (Throwable ex) {
            respValue = ex.toString();
            throw ex;
        } finally {
            saveAuditLog(auditLog, respValue);
        }
        return respObj;
    }

    private void saveAuditLog(final ApiAuditLog auditLog, String respValue) {
        try {
            if (auditLog != null) {
                if (Validators.isNotEmpty(respValue) && respValue.length() > TEXT_LEN) {
                    auditLog.setApiResponse(respValue.substring(0, TEXT_LEN));
                } else {
                    auditLog.setApiResponse(respValue);
                }
                auditLog.setProcessEnd(LocalDateTime.now());
                auditLogService.save(auditLog);
            }
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
    }
}
