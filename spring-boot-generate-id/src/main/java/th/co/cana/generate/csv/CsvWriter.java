package th.co.cana.generate.csv;

import com.opencsv.CSVWriter;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import io.github.jdevlibs.utils.FileUtils;
import io.github.jdevlibs.utils.Validators;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CsvWriter {
    private Character separator;
    private String encode;

    /**
     * Direct write JDBC ResultSet to csv file.
     *
     * @param rs       JDBC ResultSet
     * @param filename Output file name
     * @throws CsvException When exception
     */
    public void write(ResultSet rs, String filename) throws CsvException {
        write(rs, filename, true);
    }

    /**
     * Direct write JDBC ResultSet to csv file.
     *
     * @param rs                JDBC ResultSet
     * @param filename          Output file name
     * @param includeColumnName Include result column.
     * @throws CsvException When exception
     */
    public void write(ResultSet rs, String filename, boolean includeColumnName) throws CsvException {
        try (Writer writer = craeteWriter(filename);
             CSVWriter csvWriter = createCSVWriter(writer)
        ) {
            csvWriter.writeAll(rs, includeColumnName, true);
        } catch (Exception ex) {
            throw new CsvException(ex);
        }
    }

    /**
     * Direct write JDBC ResultSet to csv file.
     *
     * @param rs  JDBC ResultSet
     * @param out OutputStream
     * @throws CsvException When exception
     */
    public void write(ResultSet rs, OutputStream out) throws CsvException {
        write(rs, out, true);
    }

    /**
     * Direct write JDBC ResultSet to csv file.
     *
     * @param rs                JDBC ResultSet
     * @param out               OutputStream
     * @param includeColumnName Include result column.
     * @throws CsvException When exception
     */
    public void write(ResultSet rs, OutputStream out, boolean includeColumnName) throws CsvException {
        try (OutputStreamWriter writer = new OutputStreamWriter(out, csvEncode());
             CSVWriter csvWriter = createCSVWriter(writer)
        ) {
            csvWriter.writeAll(rs, includeColumnName, true);
        } catch (Exception ex) {
            throw new CsvException(ex);
        }
    }

    /**
     * Write values csv file.
     *
     * @param values   The values
     * @param filename csv file name
     * @throws CsvException When exception
     */
    public void write(List<String[]> values, String filename) throws CsvException {
        try (Writer writer = craeteWriter(filename);
             CSVWriter csvWriter = createCSVWriter(writer)
        ) {
            csvWriter.writeAll(values);
        } catch (Exception ex) {
            throw new CsvException(ex);
        }
    }

    /**
     * Write values csv file.
     *
     * @param values The values
     * @param out    OutputStream
     * @throws CsvException When exception
     */
    public void write(List<String[]> values, OutputStream out) throws CsvException {
        try (OutputStreamWriter writer = new OutputStreamWriter(out, csvEncode());
             CSVWriter csvWriter = createCSVWriter(writer)
        ) {
            csvWriter.writeAll(values);
        } catch (Exception ex) {
            throw new CsvException(ex);
        }
    }

    /**
     * Write values of bean to csv file without csv header.
     * @param values The collection value of Java bean.
     * @param out OutputStream
     * @param clazz Java bean class
     * @param <T> Generic java Type class
     */
    public <T> void write(List<T> values, OutputStream out, Class<T> clazz) throws CsvException {
        write(values, null, out, clazz);
    }

    /**
     * Write values of bean to csv file.
     * @param values The collection value of Java bean.
     * @param columns The csv data header.
     * @param out OutputStream
     * @param clazz Java bean class
     * @param <T> Generic java Type class
     */
    public <T> void write(List<T> values, List<String> columns, OutputStream out, Class<T> clazz) throws CsvException {
        try (OutputStreamWriter writer = new OutputStreamWriter(out, csvEncode());
             CSVWriter csvWriter = createCSVWriter(writer)
        ) {
            ColumnPositionMappingStrategy<T> mappingStrategy= new ColumnPositionMappingStrategy<>();
            mappingStrategy.setType(clazz);

            if (Validators.isNotEmpty(columns)) {
                List<String[]> headers = new ArrayList<>();
                headers.add(columns.toArray(new String[0]));
                csvWriter.writeAll(headers);
            }

            StatefulBeanToCsvBuilder<T> builder = new StatefulBeanToCsvBuilder<>(csvWriter);
            StatefulBeanToCsv<T> beanToCsv = builder.withMappingStrategy(mappingStrategy).build();
            beanToCsv.write(values);
        } catch (Exception ex) {
            throw new CsvException(ex);
        }
    }

    private Writer craeteWriter(String filename) throws IOException {
        FileUtils.mkdirByFile(filename);
        return Files.newBufferedWriter(Paths.get(filename), getCharset());
    }

    private CSVWriter createCSVWriter(Writer writer) {
        return new CSVCustomWriter(writer, separatorChar(),
                CSVWriter.NO_QUOTE_CHARACTER,
                CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                CSVWriter.DEFAULT_LINE_END);
    }

    private Charset getCharset() {
        return Charset.forName(csvEncode());
    }

    private char separatorChar() {
        return (separator == null ? ',' : separator);
    }

    private String csvEncode() {
        return (encode == null || encode.isEmpty()) ? "UTF-8" : encode;
    }
}