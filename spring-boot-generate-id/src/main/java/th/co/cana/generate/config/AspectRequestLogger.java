package th.co.cana.generate.config;

import io.github.jdevlibs.spring.utils.HttpUtils;
import io.github.jdevlibs.spring.utils.JsonUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;
import th.co.cana.generate.config.properties.ApplicationProperties;
import th.co.cana.generate.models.RequestLog;
import th.co.cana.generate.utils.AppConstants;
import th.co.cana.generate.utils.AspectUtils;

/**
 * @author supot.jdev
 * @version 1.0
 */

@RequiredArgsConstructor
@Slf4j
@Aspect
@Component
@ConditionalOnProperty(value = "application.request-api-logger-enabled", havingValue = "true")
public class AspectRequestLogger {
    private static final int TEXT_LEN = 5000;

    private final ApplicationProperties properties;

    @Around(AppConstants.Aspects.HTTP_LOG)
    public Object executeLogger(final ProceedingJoinPoint joinPoint) throws Throwable {
        return execute(joinPoint);
    }

    private Object execute(final ProceedingJoinPoint joinPoint) throws Throwable {

        StopWatch stopWatch = new StopWatch();

        Object respObj;
        String apiUrl = HttpUtils.getRequestPath();
        String methodName = null;
        try {
            stopWatch.start();

            methodName = joinPoint.getSignature().getName();
            log.info("Starting call API : {} execute method name : {}", apiUrl, methodName);
            RequestLog apiEventLog = AspectUtils.toRequestLog(joinPoint);
            log.info("Request Information : {}", JsonUtils.json(apiEventLog, isPrettyOutput()));

            respObj = joinPoint.proceed();

            log.info("Response Information : {}", JsonUtils.json(respObj, isPrettyOutput()));
        } finally {
            stopWatch.stop();
            logPerformance(stopWatch, joinPoint);
            log.info("Finished call API : {} execute method name : {}", apiUrl, methodName);
        }
        return respObj;
    }

    private void logPerformance(final StopWatch stopWatch, final ProceedingJoinPoint joinPoint) {
        try {
            log.info("API Performance [{}.{}] execution time : {} ms",
                    joinPoint.getTarget().getClass().getName(),
                    joinPoint.getSignature().getName(),
                    stopWatch.getTotalTimeMillis());
        } catch (Exception ex) {
            //Ignore
        }
    }

    private boolean isPrettyOutput() {
        return properties != null && properties.isRequestLoggerPrettyOutput();
    }
}
