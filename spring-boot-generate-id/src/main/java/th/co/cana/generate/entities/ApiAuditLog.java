package th.co.cana.generate.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import th.co.cana.generate.spring.SnowFlakeLongGenerated;

import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "api_audit_log")
public class ApiAuditLog {
    @Id
    @SnowFlakeLongGenerated
    @Column(name = "id", nullable = false)
    private Long id;

    @Size(max = 150)
    @Column(name = "api_name", length = 150)
    private String apiName;

    @Size(max = 500)
    @Column(name = "api_url", length = 500)
    private String apiUrl;

    @Size(max = 25)
    @Column(name = "api_method", length = 25)
    private String apiMethod;

    @Size(max = 5000)
    @Column(name = "api_request", length = 5000)
    private String apiRequest;

    @Size(max = 5000)
    @Column(name = "api_response", length = 5000)
    private String apiResponse;

    @Size(max = 100)
    @Column(name = "client", length = 100)
    private String client;

    @Column(name = "process_start")
    private LocalDateTime processStart;

    @Column(name = "process_end")
    private LocalDateTime processEnd;

}