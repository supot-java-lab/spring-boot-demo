package th.co.cana.generate.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import th.co.cana.generate.repository.RunningNoRepository;

/**
 * @author supot.jdev
 * @version 1.0
 */
@RequiredArgsConstructor
@Service
public class ShutdownService extends AbstractService {
    private final RunningNoRepository runningNoRepository;

    public void shutdown() {
        runningNoRepository.deleteAll();
    }
}
