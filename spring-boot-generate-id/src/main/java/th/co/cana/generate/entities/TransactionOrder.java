package th.co.cana.generate.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import th.co.cana.generate.spring.SnowFlakeLongGenerated;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "transaction_order")
public class TransactionOrder {
    @Id
    @SnowFlakeLongGenerated
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "transaction_date")
    private LocalDateTime transactionDate;

    @Size(max = 100)
    @Column(name = "transaction_no", length = 100)
    private String transactionNo;

    @Column(name = "product_code")
    private String productCode;

    @Column(name = "product_name")
    private String productName;

    @Column(name = "product_qty")
    private Integer productQty;

    @Column(name = "product_amt")
    private BigDecimal productAmt;

}