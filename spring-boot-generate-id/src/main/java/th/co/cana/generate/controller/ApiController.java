package th.co.cana.generate.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * @author supot.jdev
 * @version 1.0
 */
public abstract class ApiController {
    protected Logger logger = LoggerFactory.getLogger(getClass());

    public <T> ResponseEntity<T> ok(T body) {
        return ResponseEntity.ok(body);
    }

    public <T> ResponseEntity<T> badRequest(T body) {
        return ResponseEntity.badRequest().body(body);
    }

    public ResponseEntity<Object> notFound() {
        return ResponseEntity.notFound().build();
    }

    public ResponseEntity<Object> internalServerError(Object body) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(body);
    }
}
