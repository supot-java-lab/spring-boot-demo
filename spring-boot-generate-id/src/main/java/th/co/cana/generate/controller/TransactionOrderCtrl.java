package th.co.cana.generate.controller;

import io.github.jdevlibs.utils.GeneratorUtils;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import th.co.cana.generate.config.annotation.ApiAuditEventLog;
import th.co.cana.generate.entities.TransactionOrder;
import th.co.cana.generate.excption.ServiceException;
import th.co.cana.generate.models.TransactionOrderReq;
import th.co.cana.generate.service.TransactionOrderService;
import th.co.cana.generate.utils.AppConstants;
import th.co.cana.generate.utils.CacheUtils;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author supot.jdev
 * @version 1.0
 */
@RestController
@RequestMapping(AppConstants.Apis.ORDER)
@RequiredArgsConstructor
public class TransactionOrderCtrl extends ApiController {

    private final TransactionOrderService transactionOrderService;

    @GetMapping()
    public ResponseEntity<List<TransactionOrder>> search() {
        return ok(transactionOrderService.search());
    }

    @GetMapping("/{id}")
    public ResponseEntity<TransactionOrder> getById(@PathVariable Long id) {
        return ok(transactionOrderService.getById(id));
    }

    @ApiAuditEventLog(apiName = "create order")
    @PostMapping()
    public ResponseEntity<Object> save(@Valid @RequestBody TransactionOrderReq req) {
        try {
            return ok(transactionOrderService.processOrder(req));
        } catch (ServiceException ex) {
            return internalServerError(ex);
        }
    }

    @PostMapping("/cache")
    public ResponseEntity<Map<String, Object>> cache() {
        Map<String, Object> result = new HashMap<>();
        result.put("status", 200);
        result.put("message", "success");
        result.put("date", LocalDateTime.now());

        for (int i = 1; i <= 100; i++) {
            CacheUtils.addCustomer(GeneratorUtils.getSnowFlakeIdString());
        }

        return ok(result);
    }

    @GetMapping("/order-number")
    public ResponseEntity<Long> getOrderNumber() {
        long orderNumber = CacheUtils.generateOrderNumber("DBT");
        logger.info("orderNumber: {}", orderNumber);
        return ok(orderNumber);
    }
}
