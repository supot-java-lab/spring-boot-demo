package th.co.cana.generate.models;

import io.github.jdevlibs.utils.Convertors;
import lombok.Data;

import java.io.Serializable;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
public class RunningNumberInfo implements Serializable {
    private String runningDoc;
    private Long lastRunningNo;

    public Integer runningNumber() {
        return Convertors.toInteger(lastRunningNo, 0);
    }
}
