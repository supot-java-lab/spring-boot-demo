package th.co.cana.generate.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import th.co.cana.generate.entities.RunningNo;
import th.co.cana.generate.repository.RunningNoRepository;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @author supot.jdev
 * @version 1.0
 */
@RequiredArgsConstructor
@Service
public class GenRunningService extends AbstractService {
    private final ExecutorService executorService = Executors.newSingleThreadExecutor();

    private final RunningNoRepository runningNoRepository;
    private final Map<String, Long> runMap = new HashMap<>();

    public Long generateRunningNo(String pmkSymbol) throws Exception {
        Callable<Long> callableTask = () -> getRunning(pmkSymbol);
        Future<Long> future = executorService.submit(callableTask);
        return future.get();
    }


    private Long getRunning(String pmkSymbol) {
        if (this.runMap.get(pmkSymbol) != null) {
            Long number = this.runMap.get(pmkSymbol);
            number = number + 1;
            this.runMap.put(pmkSymbol, number);
        } else {
            RunningNo entity = runningNoRepository.findFirstByType(RunningNo.RunningType.DBT);
            if (entity == null) {
                this.runMap.put(pmkSymbol, 90000001L);
            } else {
                this.runMap.put(pmkSymbol, entity.getRunningNo() + 1);
                return this.runMap.get(pmkSymbol);
            }
        }
        return this.runMap.get(pmkSymbol);
    }
}
