package th.co.cana.generate.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import th.co.cana.generate.entities.RunningNo;
import th.co.cana.generate.entities.TransactionOrder;
import th.co.cana.generate.models.RunningNumberInfo;
import th.co.cana.generate.models.TransactionOrderReq;
import th.co.cana.generate.models.TransactionOrderResp;
import th.co.cana.generate.repository.TransactionOrderRepository;

import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */
@RequiredArgsConstructor
@Service
public class TransactionOrderService extends AbstractService {

    private final TransactionOrderRepository transactionOrderRepository;
    private final GenerateRunningService generateRunningService;
    private final ProcessOrderService processOrderService;

    public List<TransactionOrder> search() {
        return transactionOrderRepository.findAll();
    }

    public TransactionOrder getById(Long id) {
        return transactionOrderRepository.findById(id).orElse(null);
    }

    public TransactionOrderResp processOrder(final TransactionOrderReq req) {
        TransactionOrderResp resp = new TransactionOrderResp();
        try {
            logger.info("Step 1. Generate order ID");
            RunningNumberInfo numberInfo = generateRunningService.generateRunningNo(RunningNo.RunningType.DBT);

            logger.info("Step 2. Create order");
            TransactionOrder order = processOrderService.createOrder(req, numberInfo);

            logger.info("Step 3. Generate response");
            resp.setTransactionId(order.getTransactionNo());
            resp.setTransactionDate(order.getTransactionDate());
            resp.setOrderId(order.getId());
        } catch (Exception ex) {
            logger.error("processOrder", ex);
            throw new RuntimeException("processOrder", ex);
        }

        return resp;
    }
}
