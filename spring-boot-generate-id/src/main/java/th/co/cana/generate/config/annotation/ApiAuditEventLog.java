package th.co.cana.generate.config.annotation;

import java.lang.annotation.*;

/**
 * @author Supot Saelao
 * @version 1.0
 */

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface ApiAuditEventLog {

    String apiName() default "";

    String apiMethod() default "POST";

    boolean ignoreHeader() default false;

    boolean ignoreRequest() default false;

    boolean ignoreResponse() default false;
}
