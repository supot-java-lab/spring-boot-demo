package th.co.cana.generate.models;

import io.github.jdevlibs.spring.models.RequestInfo;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.HashMap;
import java.util.Map;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Getter
@Setter
@ToString
public class RequestLog extends RequestInfo {
    private String methodName;
    private Map<String, Object> payload;

    public void addPayload(String name, Object value) {
        if (payload == null) {
            payload = new HashMap<>();
        }
        payload.put(name, value);
    }
}
