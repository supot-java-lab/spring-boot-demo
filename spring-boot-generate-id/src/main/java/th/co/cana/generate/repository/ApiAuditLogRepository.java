package th.co.cana.generate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import th.co.cana.generate.entities.ApiAuditLog;

public interface ApiAuditLogRepository extends JpaRepository<ApiAuditLog, Long> {
}