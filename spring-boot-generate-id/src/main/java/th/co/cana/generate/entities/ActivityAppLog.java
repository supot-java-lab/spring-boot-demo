package th.co.cana.generate.entities;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import th.co.cana.generate.spring.SnowFlakeStringGenerated;

import java.time.Instant;

@Getter
@Setter
@Entity
@Table(name = "activity_app_log")
public class ActivityAppLog {
    @Id
    @SnowFlakeStringGenerated
    @Size(max = 36)
    @Column(name = "id", nullable = false, length = 36)
    private String id;

    @Size(max = 150)
    @Column(name = "username", length = 150)
    private String username;

    @Size(max = 50)
    @Column(name = "client_source", length = 50)
    private String clientSource;

    @Size(max = 150)
    @Column(name = "process_name", length = 150)
    private String processName;

    @Size(max = 150)
    @Column(name = "activity_name", length = 150)
    private String activityName;

    @Column(name = "activity_date")
    private Instant activityDate;

    @Size(max = 1000)
    @Column(name = "activity_desc", length = 1000)
    private String activityDesc;

}