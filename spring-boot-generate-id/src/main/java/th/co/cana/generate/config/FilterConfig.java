package th.co.cana.generate.config;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import th.co.cana.generate.filter.ProcessingLogFilter;
import th.co.cana.generate.utils.AppConstants;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Configuration
public class FilterConfig {

    @Bean
    public FilterRegistrationBean<ProcessingLogFilter> processingLogFilter() {
        FilterRegistrationBean<ProcessingLogFilter> filter = new FilterRegistrationBean<>();
        filter.setFilter(new ProcessingLogFilter());
        filter.addUrlPatterns(AppConstants.Apis.APP + "/*", "/api/*", "/error");
        filter.setOrder(0);
        return filter;
    }
}
