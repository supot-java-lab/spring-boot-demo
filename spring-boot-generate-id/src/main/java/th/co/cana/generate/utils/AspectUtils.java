package th.co.cana.generate.utils;

import io.github.jdevlibs.spring.models.RequestInfo;
import io.github.jdevlibs.spring.utils.HttpUtils;
import io.github.jdevlibs.spring.utils.JsonUtils;
import io.github.jdevlibs.utils.CsvUtils;
import io.github.jdevlibs.utils.Validators;
import jakarta.servlet.http.HttpServletRequest;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.BeanUtils;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;
import th.co.cana.generate.config.annotation.ApiAuditEventLog;
import th.co.cana.generate.entities.ApiAuditLog;
import th.co.cana.generate.models.RequestFileInfo;
import th.co.cana.generate.models.RequestLog;

import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */
public final class AspectUtils {
    private AspectUtils() {}

    public static ApiAuditEventLog getAuditLogger(final ProceedingJoinPoint joinPoint) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        return method.getAnnotation(ApiAuditEventLog.class);
    }

    public static RequestLog toRequestLog(final ProceedingJoinPoint joinPoint) {
        RequestLog requestLog = new RequestLog();
        RequestInfo requestInfo = getRequestInfo();
        if (Validators.isNotNull(requestInfo)) {
            BeanUtils.copyProperties(requestInfo, requestLog);
        } else {
            requestLog.setUrl(HttpUtils.getRequestPath());
            requestLog.setClient(HttpUtils.getRequestIPAddress());
            requestLog.setMethod(getHttpMethod());
            setHeader(requestLog);
        }
        requestLog.setMethodName(joinPoint.getSignature().getName());

        MethodSignature methodSig = (MethodSignature) joinPoint.getSignature();
        String[] names = methodSig.getParameterNames();
        Object[] args = joinPoint.getArgs();

        if (Validators.isNotEmpty(args)) {
            for (int i = 0; i < args.length; i++) {
                String paramName = CsvUtils.getValue(names, i);
                if (Validators.isEmpty(paramName)) {
                    continue;
                }

                Object obj = args[i];
                if (obj instanceof HttpServletRequest) {
                    continue;
                }

                if (obj instanceof MultipartFile) {
                    requestLog.addPayload(paramName, convertFileToJson((MultipartFile) obj));
                } else {
                    requestLog.addPayload(paramName, obj);
                }
            }
        }
        return requestLog;
    }

    public static RequestInfo getRequestInfo() {
        return HttpUtils.getRequestInfo();
    }

    public static String toResponseJson(final Object resp, boolean prettyOutput) {
        if (resp instanceof ResponseEntity<?> respEntity) {
            if (respEntity.getStatusCode().is2xxSuccessful()) {
                return getResponseEntityJson(respEntity, prettyOutput);
            } else {
                return JsonUtils.json(resp, prettyOutput);
            }
        } else {
            if (AspectUtils.isNotFileResource(resp)) {
                return JsonUtils.json(resp, prettyOutput);
            }
        }

        return null;
    }

    public static boolean isNotFileResource(Object body) {
        if (Validators.isNull(body)) {
            return true;
        }
        return !((body instanceof Resource) || (body instanceof byte[]) || (body instanceof Byte[]));
    }

    public static String getResponseEntityJson(ResponseEntity<?> respEntity, boolean prettyOutput) {
        if (respEntity.getStatusCode().is2xxSuccessful()) {
            Object body = respEntity.getBody();
            if (isNotFileResource(body)) {
                return JsonUtils.json(respEntity, prettyOutput);
            } else {
                return "Object Resource/byte[]";
            }
        } else {
            return JsonUtils.json(respEntity, prettyOutput);
        }
    }

    public static ApiAuditLog toApiAuditLog(final RequestLog log, ApiAuditEventLog eventLog) {
        ApiAuditLog entity = new ApiAuditLog();
        if (Validators.isNotEmpty(eventLog.apiName())) {
            entity.setApiName(eventLog.apiName());
        } else {
            entity.setApiName(log.getMethodName());
        }
        entity.setApiUrl(log.getUrl());
        entity.setApiMethod(log.getMethod());
        entity.setClient(log.getClient());
        entity.setProcessStart(LocalDateTime.now());
        if (Validators.isNotNull(eventLog)) {
            if (eventLog.ignoreHeader()) {
                log.setHeaders(null);
                log.setCookies(null);
            }
            if (eventLog.ignoreRequest()) {
                log.setPayload(null);
            }
        }
        entity.setApiRequest(JsonUtils.json(log));
        return entity;
    }

    private static void setHeader(RequestLog requestLog) {
        List<String> headerNames = HttpUtils.getHeaderNames();
        if (Validators.isNotEmpty(headerNames)) {
            for (String header : headerNames) {
                requestLog.addHeader(header, HttpUtils.getHeader(header));
            }
        }
    }

    private static RequestFileInfo convertFileToJson(final MultipartFile file) {
        RequestFileInfo requestFileInfo = new RequestFileInfo();
        requestFileInfo.setName(file.getName());
        requestFileInfo.setSize(file.getSize());
        requestFileInfo.setContentType(file.getContentType());
        requestFileInfo.setOriginalFilename(file.getOriginalFilename());
        return requestFileInfo;
    }

    private static String getHttpMethod() {
        try {
            return ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest().getMethod();
        } catch (Exception ex) {
            return null;
        }
    }
}
