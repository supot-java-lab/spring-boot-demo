package th.co.cana.generate.entities;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import th.co.cana.generate.spring.SnowFlakeLongGenerated;

import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@Entity
@Table(name = "customer")
public class Customer {
    @Id
    @SnowFlakeLongGenerated
    @Column(name = "id", nullable = false)
    private Long id;

    @Size(max = 255)
    @NotNull
    @Column(name = "first_name", nullable = false)
    private String firstName;

    @Size(max = 255)
    @Column(name = "last_name")
    private String lastName;

    @Column(name = "credit_amt", precision = 38, scale = 2)
    private BigDecimal creditAmt;

    @Column(name = "register_date")
    private LocalDate registerDate;

    @Size(max = 255)
    @Column(name = "address")
    private String address;

}