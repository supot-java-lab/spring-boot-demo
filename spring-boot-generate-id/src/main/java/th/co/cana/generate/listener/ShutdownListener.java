package th.co.cana.generate.listener;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import th.co.cana.generate.service.ShutdownService;
import th.co.cana.generate.utils.CacheUtils;

import java.time.LocalDateTime;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class ShutdownListener implements ApplicationListener<ApplicationEvent> {

    private final ShutdownService shutdownService;

    @Override
    public void onApplicationEvent(@NonNull ApplicationEvent event) {
        if (event instanceof ContextClosedEvent) {
            CacheUtils.clearCustomerCache();
            shutdownService.shutdown();
            log.info("Application Shutting down... at : {}", LocalDateTime.now());
        }
    }
}
