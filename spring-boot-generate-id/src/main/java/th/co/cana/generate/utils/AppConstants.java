package th.co.cana.generate.utils;

/**
 * @author supot.jdev
 * @version 1.0
 */
public final class AppConstants {

    private AppConstants() {
    }

    public static final String Y = "Y";
    public static final String N = "N";
    public static final String LANG_TH = "TH";
    public static final String LANG_EN = "EN";

    public static final String UTF8 = "UTF-8";
    public static final String TIS620 = "TIS-620";

    public static final class Aspects {
        private Aspects() {
        }

        public static final String SERVICE = "execution(public * th.co.cana.generate.service..*.*(..))";
        public static final String REPOSITORY = "execution(public * th.co.cana.generate.repository..*.*(..))";
        public static final String DAO = "execution(public * th.co.cana.generate.dao..*.*(..))";
        public static final String HTTP_LOG     = "within(@org.springframework.web.bind.annotation.RestController *)";
        public static final String AUDIT_LOG    = "execution(@th.co.cana.generate.config.annotation.ApiAuditEventLog * *(..))";
    }

    public static final class Apis {
        private Apis() {
        }

        public static final String ERROR = "/error";

        public static final String APP      = "/v1";
        public static final String ORDER    = "/v1/order";
    }
}
