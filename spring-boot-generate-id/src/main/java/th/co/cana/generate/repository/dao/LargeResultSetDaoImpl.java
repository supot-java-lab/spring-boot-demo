package th.co.cana.generate.repository.dao;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import th.co.cana.generate.csv.CsvWriter;
import th.co.cana.generate.excption.ServiceException;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Slf4j
@RequiredArgsConstructor
@Repository
public class LargeResultSetDaoImpl implements LargeResultSetDao {
    private final JdbcTemplate jdbcTemplate;

    @Override
    public void exportCustomer(int start, int limit) {
        try (Connection conn = jdbcTemplate.getDataSource().getConnection();
             Statement stmt = conn.createStatement()) {

            conn.setAutoCommit(false);
            ResultSet rs = null;
            Map<String, String> customerIds = new HashMap<>();
            try {
                log.info("Starting execute data");
                String sql = " SELECT * FROM (" +
                        "    SELECT ROW_NUMBER() OVER(ORDER BY C.user_id) AS SEQ_NO" +
                        "     , C.user_id AS USER_ID" +
                        "     , SUBSTR(IF(C.profile_citizen_id IS NULL OR C.profile_citizen_id = '', C.reference_type_number, C.profile_citizen_id), 1, 15) AS CUST_ID" +
                        "     , (CASE WHEN C.holder_type_code = '0' THEN 'B'" +
                        "        WHEN C.holder_type_code = '1' THEN 'P'" +
                        "        WHEN C.holder_type_code = '2' THEN 'Z'" +
                        "        WHEN C.holder_type_code = '3' THEN 'F'" +
                        "        ELSE 'P' END" +
                        "     ) AS CUST_TYPE" +
                        "     , IF(COALESCE(C.title_aa10_code, C.profile_title_code) = '199', '099', COALESCE(C.title_aa10_code, C.profile_title_code)) AS TITLE_CODE" +
                        "     , SUBSTR(COALESCE(C.title_aa10_name, C.profile_title_name), 1, 30) AS TITLE_NAME" +
                        "     , SUBSTR(IF(C.holder_type_code IN ('0', '2'), C.profile_thai_last_name, C.profile_thai_first_name), 1, 100) AS FIRST_NAME" +
                        "     , SUBSTR(IF(C.holder_type_code IN ('0', '2'), NULL, C.profile_thai_last_name), 1, 50) AS LAST_NAME" +
                        "     , DATE_FORMAT(C.profile_birth_date, '%Y-%m-%d') AS BIRTH_DATE" +
                        "     , DATE_FORMAT(C.create_date, '%Y-%m-%d') AS ADD_DATE" +
                        "     , NULL AS ENG_NAME" +
                        "     , (CASE WHEN C.profile_gender_code = 'M' THEN '0'" +
                        "        WHEN C.profile_gender_code = 'F' THEN '1'" +
                        "        WHEN C.profile_gender_code = 'J' THEN '2'" +
                        "        WHEN C.profile_gender_code = 'U' THEN '3'" +
                        "        ELSE '' END" +
                        "     ) AS SEX" +
                        "     , C.profile_country_code AS NATIONALITY" +
                        "     , (CASE WHEN C.reference_type = '0' THEN '1'" +
                        "        WHEN C.reference_type IN ('4', '5', '6', '7', '8', '9', 'B') THEN '-'" +
                        "        WHEN C.reference_type = '3' THEN '3'" +
                        "        WHEN C.reference_type = 'A' THEN '4'" +
                        "        ELSE C.reference_type END" +
                        "     ) AS TYPE_CARD" +
                        "     , SUBSTR(COALESCE(C.reference_type_number, C.profile_citizen_id), 1, 15) AS CARD_NO" +
                        "     , SUBSTR(C.tax_id, 1, 15) AS TAX_NO" +
                        "     , NULL AS MARITAL_STATUS" +
                        "     , NULL AS NO_OF_CHILD" +
                        "     , NULL AS OCCUPATION" +
                        "     , 'C' AS STATUS_CUST" +
                        "     , NULL AS AGENT_NO_12" +
                        "     , NULL AS USERID_KEY4" +
                        "     , NULL AS USERID_APP4" +
                        "     , NULL AS REF_PERSON" +
                        "     , C.profile_mobile AS TEL_PERSON" +
                        "     , NULL AS CHANGE_BY" +
                        "     , DATE_FORMAT(C.create_date, '%Y-%m-%d') AS CHANGE_DATE" +
                        "     , NULL AS CHANGE_APPROVE" +
                        "     , COALESCE(C.a10_occupation_code, C.pmk_occupation_code) AS OCCUPATION_CODE" +
                        "     , NULL AS STATUS" +
                        "     FROM dbt_mst_cust_profile C" +
                        " ) T" +
                        " WHERE SEQ_NO > " + start + " LIMIT " + limit;

                stmt.setFetchSize(1000);
                rs = stmt.executeQuery(sql);
                log.info("Finished execute data");
                try {
                    log.info("Starting write data");
                    CsvWriter csvWriter = CsvWriter.builder().separator('|').build();
                    csvWriter.write(rs, "src/test/resources/demo.csv");
                    log.info("Finished write data");
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                }
            } finally {
                if(rs != null) {
                    rs.close();
                }
            }
        } catch (SQLException ex) {
            throw new ServiceException(ex);
        }
    }
}
