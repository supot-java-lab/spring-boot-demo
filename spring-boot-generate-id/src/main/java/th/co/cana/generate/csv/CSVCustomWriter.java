package th.co.cana.generate.csv;

import com.opencsv.CSVWriter;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.Writer;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Slf4j
public class CSVCustomWriter extends CSVWriter {
    private boolean firstItem;

    public CSVCustomWriter(Writer writer) {
        super(writer);
        firstItem = true;
    }

    public CSVCustomWriter(Writer writer, char separator, char quotechar, char escapechar, String lineEnd) {
        super(writer, separator, quotechar, escapechar, lineEnd);
        firstItem = true;
    }

    @Override
    public void writeAll(Iterable<String[]> allLines, boolean applyQuotesToAll) {
        firstItem = true;
        log.info("String write csv with String[]");
        super.writeAll(allLines, applyQuotesToAll);
    }

    @Override
    public int writeAll(ResultSet rs, boolean includeColumnNames, boolean trim, boolean applyQuotesToAll) throws SQLException, IOException {
        firstItem = true;
        log.info("String write csv with ResultSet");
        return super.writeAll(rs, includeColumnNames, trim, applyQuotesToAll);
    }

    @Override
    protected void writeNext(String[] nextLine, boolean applyQuotesToAll, Appendable appendable) throws IOException {
        if (nextLine == null) {
            return;
        }

        if (firstItem) {
            firstItem = false;
        } else {
            appendable.append(lineEnd);
        }

        for (int i = 0; i < nextLine.length; i++) {
            if (i != 0) {
                appendable.append(separator);
            }

            String nextElement = nextLine[i];
            if (nextElement == null) {
                continue;
            }

            Boolean stringContainsSpecialCharacters = stringContainsSpecialCharacters(nextElement);
            appendQuoteCharacterIfNeeded(applyQuotesToAll, appendable, stringContainsSpecialCharacters);
            if (stringContainsSpecialCharacters) {
                processLine(nextElement, appendable);
            } else {
                appendable.append(nextElement);
            }
            appendQuoteCharacterIfNeeded(applyQuotesToAll, appendable, stringContainsSpecialCharacters);
        }

        writer.write(appendable.toString());
    }

    private void appendQuoteCharacterIfNeeded(boolean applyQuotesToAll, Appendable appendable, Boolean stringContainsSpecialCharacters) throws IOException {
        if ((applyQuotesToAll || stringContainsSpecialCharacters) && quotechar != NO_QUOTE_CHARACTER) {
            appendable.append(quotechar);
        }
    }
}
