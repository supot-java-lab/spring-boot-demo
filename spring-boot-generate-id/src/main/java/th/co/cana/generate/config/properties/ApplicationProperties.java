package th.co.cana.generate.config.properties;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author supot.jdev
 * @version 1.0
 */

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "application")
public class ApplicationProperties {
    private String name;
    private String version;
    private String buildTime;
    private String javaBuildVersion;

    private boolean enabledCache;
    private boolean performanceAuditEnabled;
    private boolean requestApiAuditEnabled;
    private boolean requestApiLoggerEnabled;
    private boolean requestLoggerPrettyOutput;

    private LogConfig logConfig = new LogConfig();

    @Data
    public static final class LogConfig {
        private String level;
        private LogLevel logback = new LogLevel();
        private LogLevel logstash = new LogLevel();
    }

    @Data
    public static final class LogLevel {
        private String level;
        private String destination;
        private int port;
    }
}
