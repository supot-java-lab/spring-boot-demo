package th.co.cana.generate.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import th.co.cana.generate.entities.ActivityAppLog;

public interface ActivityAppLogRepository extends JpaRepository<ActivityAppLog, String> {
}