package th.co.cana.generate.service;

import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import th.co.cana.generate.entities.ApiAuditLog;
import th.co.cana.generate.excption.ServiceException;
import th.co.cana.generate.repository.ApiAuditLogRepository;

/**
 * @author supot.jdev
 * @version 1.0
 */

@RequiredArgsConstructor
@Service
public class AuditLogService {

    private final ApiAuditLogRepository auditLogRepository;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Async
    public void save(final ApiAuditLog entity) {
        try {
            auditLogRepository.saveAndFlush(entity);
        } catch (Exception ex) {
            throw new ServiceException(ex);
        }
    }
}
