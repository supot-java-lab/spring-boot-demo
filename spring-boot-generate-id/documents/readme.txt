Managing Concurrency in a Java Microservice: Ensuring Thread-Safe Order — Stock Management
https://medium.com/@thm.harsh.mu/managing-concurrency-in-a-java-microservice-ensuring-thread-safe-ostock-management-4aa5cfc42721
1. Synchronized Blocks
2. Reentrant Locks
3. Atomic Variables
4. Higher-Level Concurrency Utilities
5. Database Transactions

How to handle incoming requests in Java with Spring Boot
https://medium.com/@burakkocakeu/how-to-handle-incoming-requests-in-java-with-spring-boot-b46cb35ed520

How Many Concurrent Requests Can a Single Server Application Handle?
https://haril.dev/en/blog/2023/11/10/Spring-MVC-Traffic-Testing

Different ways to write concurrency safe database operations in spring boot
https://rahulraj.io/different-ways-to-write-concurrency-safe-database-operations-in-spring-boot

Best Practices for Using @Transactional in Spring Boot
https://medium.com/thefreshwrites/best-practices-for-using-transactional-in-spring-boot-7baafba397ad


Java Memory Consumption with Collections
https://medium.com/@yureshcs/java-memory-management-with-collections-2e3c925edc07
https://dzone.com/articles/preventing-your-java-collections-from-wasting-memo
https://medium.com/@alxkm/choosing-the-right-collection-in-java-c59784fdc9c8

https://www.baeldung.com/java-reduce-memory-footprint