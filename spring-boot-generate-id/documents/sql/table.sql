CREATE TABLE `activity_app_log` (
  `id` varchar(36)  NOT NULL,
  `username` varchar(150)  DEFAULT NULL,
  `client_source` varchar(50)  DEFAULT NULL,
  `process_name` varchar(150)  DEFAULT NULL,
  `activity_name` varchar(150)  DEFAULT NULL,
  `activity_date` timestamp NULL DEFAULT NULL,
  `activity_desc` varchar(1000)  DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB ;

CREATE TABLE `api_audit_log` (
  `id` BIGINT NOT NULL,
  `api_name` varchar(150) DEFAULT NULL,
  `api_url` varchar(500) DEFAULT NULL,
  `api_method` varchar(25) DEFAULT NULL,
  `api_request` varchar(5000) DEFAULT NULL,
  `api_response` varchar(5000) DEFAULT NULL,
  `client` varchar(100) DEFAULT NULL,
  `process_start` timestamp(6) DEFAULT NULL,
  `process_end` timestamp(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB;

CREATE TABLE `customer` (
  `id` bigint NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `credit_amt` decimal(38,2) DEFAULT NULL,
  `register_date` date DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB ;

CREATE TABLE `running_no` (
  `id` bigint NOT NULL,
  `type` varchar(20) DEFAULT NULL,
  `running_no` bigint DEFAULT NULL,
  `running_doc` varchar(50) DEFAULT NULL,
  `padding_no` int DEFAULT NULL,
  `branch_code` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB ;


CREATE TABLE `transaction_order` (
  `id` bigint NOT NULL,
  `transaction_date` timestamp(6) NULL DEFAULT NULL,
  `transaction_no` varchar(100) DEFAULT NULL,
  `product_name` varchar(100) DEFAULT NULL,
  `product_qty` int DEFAULT NULL,
  `product_amt` decimal(10,0) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB ;