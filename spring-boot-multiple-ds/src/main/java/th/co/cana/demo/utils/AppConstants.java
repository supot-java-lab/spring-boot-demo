/*
 * -----------------------------------------------------------------------------------
 * Copyright (c) 2024.  Cana Enterprise Co.,Ltd. All rights reserved
 * -----------------------------------------------------------------------------------
 */

package th.co.cana.demo.utils;

import java.util.Locale;

/**
 * @author supot.jdev
 * @version 1.0
 */
public final class AppConstants {

    private AppConstants() {}

    public static final String Y = "Y";
    public static final String N = "N";

    public static final String UTF8 = "UTF-8";
    public static final Locale US = Locale.US;
    public static final Locale TH = new Locale("th", "TH");
    public static final String LANG_TH = "TH";
    public static final String LANG_US = "US";
    public static final String HTML_NEW_LINE = "<br/>";
    public static final String HTML_SPACE 	= "&nbsp;";
    public static final String HTML_SPACE_2 = "&ensp;";
    public static final String HTML_SPACE_4 = "&emsp;";
    public static final String SESSION_USER_ATT 	= "userInfo";
    public static final String DATE_FORMAT      = "dd/MM/yyyy";
    public static final String DATE_TIME_FORMAT = "dd/MM/yyyy HH:mm:ss";
    public static final String LOG_MDC_NAME = "client-id";

    /**
     * AOP : Aspect Oriented Programming package
     */
    public static final class Aspects {
        private Aspects() {}

        public static final String SERVICE = "execution(public * th.co.cana.demo.service..*.*(..))";
        public static final String REPOSITORY = "execution(public * th.co.cana.demo.repository..*.*(..))";
        public static final String DAO      = "execution(public * th.co.cana.demo.dao..*.*(..))";
    }

    public static final class DataSourceConfig {
        private DataSourceConfig() {}
        public static final String BASE_ENTITY_PACKAGE      = "th.co.cana.demo.entity.app";
        public static final String BASE_REPOSITORY_PACKAGE  = "th.co.cana.demo.repository.app";

        public static final String BASE_ENTITY_PACKAGE_CDR      = "th.co.cana.demo.entity.cdr";
        public static final String BASE_REPOSITORY_PACKAGE_CDR  = "th.co.cana.demo.repository.cdr";
    }

    public static final class Apis {
        private Apis() {
        }

        public static final String BASE_URL = "/v1/spring-boot";
    }
}
