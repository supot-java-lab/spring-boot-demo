/*
 * -----------------------------------------------------------------------------------
 * Copyright (c) 2024.  Cana Enterprise Co.,Ltd. All rights reserved
 * -----------------------------------------------------------------------------------
 */

package th.co.cana.demo.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import th.co.cana.demo.utils.AppConstants;
import th.co.cana.demo.utils.DatasourceUtils;

import javax.sql.DataSource;
import java.util.Objects;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Slf4j
@Configuration
@EnableJpaRepositories(
        basePackages = AppConstants.DataSourceConfig.BASE_REPOSITORY_PACKAGE_CDR,
        entityManagerFactoryRef = "cdrEntityManagerFactory",
        transactionManagerRef = "cdrTransactionManager"
)
@EnableTransactionManagement
public class DataSourceCdrConfig {

    @Bean("cdrDataSourceProperties")
    @ConfigurationProperties("spring.datasource.cdr")
    public DataSourceProperties cdrDataSourceProperties() {
        log.info("Starting CDR DataSource configuration loading....");
        return new DataSourceProperties();
    }

    @Bean("cdrDataSource")
    @ConfigurationProperties("spring.datasource.cdr.hikari")
    public DataSource cdrDataSource() {
        log.info("Starting CDR DataSource creating....");
        return cdrDataSourceProperties()
                .initializeDataSourceBuilder()
                .build();
    }

    @Bean("cdrEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean cdrEntityManagerFactory(
            @Qualifier("cdrDataSource") DataSource dataSource,
            EntityManagerFactoryBuilder builder) {
        log.info("Starting CDR EntityManagerFactory creating....");
        return builder.dataSource(dataSource)
                .packages(AppConstants.DataSourceConfig.BASE_ENTITY_PACKAGE_CDR)
                .properties(DatasourceUtils.defaultHibernateConfig())
                .build();
    }

    @Bean("cdrTransactionManager")
    public PlatformTransactionManager cdrTransactionManager(
            @Qualifier("cdrEntityManagerFactory") LocalContainerEntityManagerFactoryBean entityManagerFactory
    ) {
        log.info("Starting CDR TransactionManager creating....");
        return new JpaTransactionManager(Objects.requireNonNull(entityManagerFactory.getObject()));
    }

    @Bean("cdrJdbcTemplate")
    public JdbcTemplate cdrJdbcTemplate(@Qualifier("cdrDataSource") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }
}
