/*
 * -----------------------------------------------------------------------------------
 * Copyright (c) 2024.  Cana Enterprise Co.,Ltd. All rights reserved
 * -----------------------------------------------------------------------------------
 */

package th.co.cana.demo.entity;

import jakarta.persistence.Transient;
import lombok.Data;
import th.co.cana.demo.enums.RowModes;

import java.io.Serializable;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
public class DefaultEntity implements Serializable {
    @Transient
    protected boolean selected = false;
    @Transient
    protected boolean disabled = false;
    @Transient
    protected boolean readonly = false;
    @Transient
    protected RowModes rowMode;

    public boolean isRowNew() {
        return (RowModes.NEW == rowMode);
    }

    public boolean isRowEdit() {
        return (RowModes.EDIT == rowMode);
    }

    public boolean isRowView() {
        return (RowModes.VIEW == rowMode);
    }

    public boolean isRowDelete() {
        return (RowModes.DELETE == rowMode);
    }

    public boolean isRowInitial() {
        return (RowModes.INITIAL == rowMode);
    }
}