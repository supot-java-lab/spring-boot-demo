/*
 * -----------------------------------------------------------------------------------
 * Copyright (c) 2024.  Cana Enterprise Co.,Ltd. All rights reserved
 * -----------------------------------------------------------------------------------
 */

package th.co.cana.demo.filter;

import io.github.jdevlibs.utils.Utils;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;
import th.co.cana.demo.utils.AppConstants;
import th.co.cana.demo.utils.HttpUtils;

import java.io.IOException;

/**
 * @author supot.jdev
 * @version 1.0
 */

@Component
@Slf4j
public class Log4jFilter extends GenericFilterBean {
    @Override
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain filterChain) throws IOException, ServletException {
        String path = HttpUtils.getRequestPath();
        try {
            MDC.put(AppConstants.LOG_MDC_NAME, Utils.getUUID());
            log.info("Starting call api : {}", path);
            filterChain.doFilter(req, resp);
        } finally {
            log.info("Finished call api : {}", path);
            MDC.remove(AppConstants.LOG_MDC_NAME);
        }
    }
}