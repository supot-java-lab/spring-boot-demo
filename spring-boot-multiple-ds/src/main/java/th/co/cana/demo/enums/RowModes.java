/*
 * -----------------------------------------------------------------------------------
 * Copyright (c) 2024.  Cana Enterprise Co.,Ltd. All rights reserved
 * -----------------------------------------------------------------------------------
 */

package th.co.cana.demo.enums;

/**
 * @author supot.jdev
 * @version 1.0
 */
public enum RowModes {
    INITIAL, NEW, EDIT, VIEW, DELETE
}
