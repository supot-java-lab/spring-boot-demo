/*
 * -----------------------------------------------------------------------------------
 * Copyright (c) 2024.  Cana Enterprise Co.,Ltd. All rights reserved
 * -----------------------------------------------------------------------------------
 */

package th.co.cana.demo.controller;

import io.github.jdevlibs.utils.Utils;
import io.github.jdevlibs.utils.Validators;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import th.co.cana.demo.entity.app.ActivityAppLog;
import th.co.cana.demo.entity.cdr.ActivityCdrLog;
import th.co.cana.demo.service.ActivityLogService;
import th.co.cana.demo.utils.AppConstants;

import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */

@RequiredArgsConstructor
@RestController
@RequestMapping(AppConstants.Apis.BASE_URL)
public class ActivityLogCtrl extends ApiController {

    private final ActivityLogService activityLogService;

    @PostMapping("/app/log")
    public ResponseEntity<Boolean> saveAppLog(@Valid @RequestBody ActivityAppLog req) {

        if (Validators.isEmpty(req.getId())) {
            req.setId(Utils.getUUID());
        }
        activityLogService.save(req);
        return ok(Boolean.TRUE);
    }

    @GetMapping("/app/log")
    public ResponseEntity<List<ActivityAppLog>> getAppLog() {
        return ok(activityLogService.getActivityAppLogs());
    }

    @PostMapping("/cdr/log")
    public ResponseEntity<Boolean> saveCdrLog(@Valid @RequestBody ActivityCdrLog req) {
        if (Validators.isEmpty(req.getId())) {
            req.setId(Utils.getUUID());
        }
        activityLogService.save(req);
        return ok(Boolean.TRUE);
    }

    @GetMapping("/cdr/log")
    public ResponseEntity<List<ActivityCdrLog>> getCdrLog() {
        return ok(activityLogService.getActivityCdrLogs());
    }
}
