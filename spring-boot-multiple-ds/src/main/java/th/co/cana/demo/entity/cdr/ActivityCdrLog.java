/*
 * -----------------------------------------------------------------------------------
 * Copyright (c) 2024.  Cana Enterprise Co.,Ltd. All rights reserved
 * -----------------------------------------------------------------------------------
 */

package th.co.cana.demo.entity.cdr;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import th.co.cana.demo.entity.DefaultEntity;

import java.time.LocalDateTime;

/**
 * @author supot.jdev
 * @version 1.0
 */
@EqualsAndHashCode(callSuper = false, of = {"id"})
@Data
@Table(name = "activity_cdr_log")
@Entity
@NoArgsConstructor
public class ActivityCdrLog extends DefaultEntity {

    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "username")
    private String username;

    @Column(name = "client_source")
    private String clientSource;

    @Column(name = "process_name")
    private String processName;

    @Column(name = "activity_name")
    private String activityName;

    @Column(name = "activity_date")
    private LocalDateTime activityDate;

    @Column(name = "activity_desc")
    private String activityDesc;
}
