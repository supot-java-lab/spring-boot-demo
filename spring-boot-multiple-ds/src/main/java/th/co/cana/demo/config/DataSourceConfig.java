/*
 * -----------------------------------------------------------------------------------
 * Copyright (c) 2024.  Cana Enterprise Co.,Ltd. All rights reserved
 * -----------------------------------------------------------------------------------
 */

package th.co.cana.demo.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import th.co.cana.demo.utils.AppConstants;
import th.co.cana.demo.utils.DatasourceUtils;

import javax.sql.DataSource;
import java.util.Objects;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Slf4j
@Configuration
@EnableJpaRepositories(basePackages = AppConstants.DataSourceConfig.BASE_REPOSITORY_PACKAGE)
@EnableTransactionManagement
public class DataSourceConfig {

    @Primary
    @Bean("dataSourceProperties")
    @ConfigurationProperties("spring.datasource")
    public DataSourceProperties dataSourceProperties() {
        log.info("Starting APP DataSource configuration loading....");
        return new DataSourceProperties();
    }

    @Primary
    @Bean("dataSource")
    public DataSource dataSource() {
        log.info("Starting APP DataSource creating....");
        return dataSourceProperties()
                .initializeDataSourceBuilder()
                .build();
    }

    @Primary
    @Bean("entityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            @Qualifier("dataSource") DataSource dataSource,
            EntityManagerFactoryBuilder builder) {
        log.info("Starting APP entityManagerFactory creating....");
        return builder.dataSource(dataSource)
                .packages(AppConstants.DataSourceConfig.BASE_ENTITY_PACKAGE)
                .properties(DatasourceUtils.defaultHibernateConfig())
                .build();
    }

    @Primary
    @Bean("transactionManager")
    public PlatformTransactionManager transactionManager(
            @Qualifier("entityManagerFactory") LocalContainerEntityManagerFactoryBean entityManagerFactory
    ) {
        log.info("Starting APP transactionManager creating....");
        return new JpaTransactionManager(Objects.requireNonNull(entityManagerFactory.getObject()));
    }

    @Primary
    @Bean("jdbcTemplate")
    public JdbcTemplate jdbcTemplate(@Qualifier("dataSource") DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }
}
