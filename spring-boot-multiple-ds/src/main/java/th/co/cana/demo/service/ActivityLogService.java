/*
 * -----------------------------------------------------------------------------------
 * Copyright (c) 2024.  Cana Enterprise Co.,Ltd. All rights reserved
 * -----------------------------------------------------------------------------------
 */

package th.co.cana.demo.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import th.co.cana.demo.entity.app.ActivityAppLog;
import th.co.cana.demo.entity.cdr.ActivityCdrLog;
import th.co.cana.demo.repository.app.ActivityAppLogRepository;
import th.co.cana.demo.repository.cdr.ActivityCdrLogRepository;

import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */
@RequiredArgsConstructor
@Service
public class ActivityLogService extends AbstractService {

    private final ActivityAppLogRepository appLogRepository;
    private final ActivityCdrLogRepository cdrLogRepository;

    public void save(ActivityAppLog appLog) {
        appLogRepository.save(appLog);
    }

    public void save(ActivityCdrLog appLog) {
        cdrLogRepository.save(appLog);
    }

    public List<ActivityAppLog> getActivityAppLogs() {
        return appLogRepository.findAll();
    }

    public List<ActivityCdrLog> getActivityCdrLogs() {
        return cdrLogRepository.findAll();
    }
}
