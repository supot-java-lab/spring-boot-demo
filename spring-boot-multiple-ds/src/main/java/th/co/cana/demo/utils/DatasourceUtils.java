/*
 * -----------------------------------------------------------------------------------
 * Copyright (c) 2024.  Cana Enterprise Co.,Ltd. All rights reserved
 * -----------------------------------------------------------------------------------
 */

package th.co.cana.demo.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author supot.jdev
 * @version 1.0
 */
public final class DatasourceUtils {
    private DatasourceUtils() {}

    public static Map<String, String>  defaultHibernateConfig() {
        Map<String, String> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.auto", "none");

        return properties;
    }
}
