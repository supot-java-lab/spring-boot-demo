/*
 * -----------------------------------------------------------------------------------
 * Copyright (c) 2024.  Cana Enterprise Co.,Ltd. All rights reserved
 * -----------------------------------------------------------------------------------
 */

package th.co.cana.demo.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author supot.jdev
 * @version 1.0
 */
public abstract class AbstractService {
    protected Logger logger = LoggerFactory.getLogger(getClass());
}
