/*
 * -----------------------------------------------------------------------------------
 * Copyright (c) 2024.  Cana Enterprise Co.,Ltd. All rights reserved
 * -----------------------------------------------------------------------------------
 */

package th.co.cana.demo.repository.app;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import th.co.cana.demo.entity.app.ActivityAppLog;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Repository
public interface ActivityAppLogRepository extends JpaRepository<ActivityAppLog, String> {
}
