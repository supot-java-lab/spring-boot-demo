create table activity_cdr_log (
    id varchar(36) not null,
    username varchar(150),
    client_source varchar(50),
    process_name varchar(150),
    activity_name varchar(150),
    activity_date timestamp,
    activity_desc varchar(1000),
    constraint pk_activity_cdr_log primary key (id)
);

create table activity_app_log (
    id varchar(36) not null,
    username varchar(150),
    client_source varchar(50),
    process_name varchar(150),
    activity_name varchar(150),
    activity_date timestamp,
    activity_desc varchar(1000),
    constraint pk_activity_app_log primary key (id)
);