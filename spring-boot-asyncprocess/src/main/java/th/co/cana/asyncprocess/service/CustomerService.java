package th.co.cana.asyncprocess.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import th.co.cana.asyncprocess.model.Customer;

import java.util.concurrent.*;

@Service
@Slf4j
public class CustomerService {

    @Async
    public CompletableFuture<Customer> fineCustomer(String custId) {
        log.info("Looking up {}", custId);
        CompletableFuture<Customer> task = new CompletableFuture<>();
        try {
            Customer cust = new Customer();
            cust.setCustId(custId);
            cust.setFirstName("Supot");
            cust.setLastName("Saelao");

            log.info("Customer create : {}", cust);

            Thread.sleep(20000L);
            task.complete(cust);
            log.info("Task complete..");
        } catch (InterruptedException ex) {
            log.error("fineCustomer", ex);
            task.completeExceptionally(ex);
        }

        return task;
    }

    @Async
    public void processCustomer(Customer cutoCustomer) {
        log.info("Starting process {}", cutoCustomer);
        try {
            log.info("proces is long time");
            Thread.sleep(20000L);
            log.info("Process is success");
        } catch (Exception ex) {
            log.error("processCustomer", ex);
        }
    }
}
