package th.co.cana.asyncprocess.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import th.co.cana.asyncprocess.model.Customer;
import th.co.cana.asyncprocess.service.CustomerService;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/demo")
@Slf4j
public class AsyncDemoController {

    @Autowired
    private CustomerService customerService;

    @GetMapping("/customers")
    public ResponseEntity<Customer> getCustomer() throws ExecutionException, InterruptedException {

        CompletableFuture.supplyAsync(() -> customerService.fineCustomer("10002"));
        Customer cust = new Customer();
        cust.setCustId("1000");
        cust.setFirstName("Supot");
        cust.setLastName("Saelao");

        return ResponseEntity.ok(cust);
    }

    @GetMapping("/customers/{custId}")
    public  ResponseEntity<Customer> processCustomer( @PathVariable String custId) {
        Customer cust = new Customer();
        cust.setCustId(custId);
        cust.setFirstName("Supot");
        cust.setLastName("Saelao");

        CompletableFuture.runAsync(() -> customerService.processCustomer(cust));

        return ResponseEntity.ok(cust);
    }
}
