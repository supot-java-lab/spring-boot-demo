package th.co.cana.asyncprocess.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class Customer implements Serializable {
    private String custId;
    private String firstName;
    private String lastName;
}
