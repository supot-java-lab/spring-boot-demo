/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.eurekaclient.controller;

import java.time.LocalDateTime;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
* @author supot
* @version 1.0
*/

@RestController
@RequestMapping("/v1/hello")
public class HellCtrl extends Controller {

	@GetMapping
	public String hello() {
		return "Hello : " + LocalDateTime.now();
	}
}
