package th.co.cana.framework.api.models;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
public class CustomerModel implements Serializable {
    private Long customerId;
    @NotBlank
    private String firstName;
    private String lastName;
    private LocalDate birthDate;
    @NotNull
    private BigDecimal creditAmt;
    private String createdBy;
    private LocalDateTime createdDate;
    private String updatedBy;
    private LocalDateTime updatedDate;
}
