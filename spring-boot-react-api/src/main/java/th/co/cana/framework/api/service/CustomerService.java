package th.co.cana.framework.api.service;

import io.github.jdevlibs.utils.BeanUtils;
import io.github.jdevlibs.utils.Validators;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import th.co.cana.framework.api.entity.Customer;
import th.co.cana.framework.api.exception.ErrorCodes;
import th.co.cana.framework.api.models.CustomerModel;
import th.co.cana.framework.api.repository.CustomerRepository;
import th.co.cana.framework.api.utils.Exceptions;
import th.co.cana.framework.api.utils.GeneratorUtils;

import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */
@RequiredArgsConstructor
@Service
public class CustomerService extends AbstractService {
    private final CustomerRepository customerRepository;

    public List<Customer> findAll() {
        try {
            return customerRepository.findAll();
        } catch (Exception ex) {
            throw Exceptions.invokeException(ex, ErrorCodes.CODE_DB_QUERY);
        }
    }

    public Customer getCustomer(Long id) {
        try {
            return customerRepository.findById(id).orElse(null);
        } catch (Exception ex) {
            throw Exceptions.invokeException(ex, ErrorCodes.CODE_DB_QUERY);
        }
    }

    public void save(final CustomerModel customer) {
        try {
            customer.setCustomerId(GeneratorUtils.getSnowFlakeId());
            Customer entity = BeanUtils.copyProperties(customer, Customer.class);
            customerRepository.save(entity);
        } catch (Exception ex) {
            throw Exceptions.invokeException(ex, ErrorCodes.CODE_DB_INSERT);
        }
    }

    public void update(final CustomerModel customer) {
        try {
            if (Validators.isNull(customer)) {
                customer.setCustomerId(GeneratorUtils.getSnowFlakeId());
            }

            Customer entity = BeanUtils.copyProperties(customer, Customer.class, "createdBy", "createdDate");
            customerRepository.save(entity);
        } catch (Exception ex) {
            throw Exceptions.invokeException(ex, ErrorCodes.CODE_DB_UPDATE);
        }
    }

    public void delete(Long id) {
        try {
            customerRepository.deleteById(id);
        } catch (Exception ex) {
            throw Exceptions.invokeException(ex, ErrorCodes.CODE_DB_DELETE);
        }
    }
}
