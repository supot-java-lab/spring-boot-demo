package th.co.cana.framework.api.exception;

import java.util.List;

public class ValidationException extends RuntimeException {
    private List<String> errors;

    public ValidationException(Throwable cause) {
        super(cause);
    }

    public ValidationException(Throwable cause, List<String> errors) {
        super(cause);
        this.errors = errors;
    }

    public ValidationException(String message) {
        super(message);
    }

    public ValidationException(List<String> errors) {
        this.errors = errors;
    }

    public ValidationException(String message, List<String> errors) {
        super(message);
        this.errors = errors;
    }

    public List<String> getErrors() {
        return errors;
    }

}
