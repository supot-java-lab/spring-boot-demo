/*
 * -----------------------------------------------------------------------------------
 * Copyright (c) 2024.  Tourism Authority of Thailand (TAT) All rights reserved
 * -----------------------------------------------------------------------------------
 */

package th.co.cana.framework.api.utils;

import io.github.jdevlibs.utils.Validators;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.UncategorizedSQLException;
import th.co.cana.framework.api.exception.ErrorCodes;
import th.co.cana.framework.api.exception.ServiceException;

/**
 * @author supot.jdev
 * @version 1.0
 */
public final class Exceptions {
    private static final String LARGER 	= "String or binary data would be truncated";
    private static final String UNIQUE 	= "ORA-01400";
    private static final String NULL 	= "Cannot insert the value NULL into column";

    private Exceptions() {}

    public static RuntimeException toRuntimeException(Throwable throwable) {
        return throwable instanceof RuntimeException ? (RuntimeException) throwable
                : new RuntimeException(throwable);
    }

    public static RuntimeException toRuntimeException(String message, Throwable throwable) {
        return message == null ? toRuntimeException(throwable)
                : new RuntimeException(message, throwable);
    }

    public static RuntimeException toRuntimeException(String message) {
        return new RuntimeException(message);
    }

    public static ServiceException invokeException(Exception ex) {
        return invokeException(ex, ErrorCodes.CODE_DB_OTHER);
    }

    public static ServiceException invokeException(DataAccessException ex) {
        return invokeException(ex, ErrorCodes.CODE_DB_OTHER);
    }

    public static ServiceException invokeException(DataAccessException ex, ErrorCodes errorCode) {
        return getServiceException(ex, errorCode);
    }

    public static ServiceException invokeException(Exception ex, ErrorCodes errorCode) {
        return getServiceException(ex, errorCode);
    }

    private static ServiceException getServiceException(Exception ex, ErrorCodes errorCode) {
        String msg = ex.getMessage();
        if (ex instanceof DuplicateKeyException || ex.getCause() instanceof DuplicateKeyException) {
            return new ServiceException(ErrorCodes.CODE_DB_UNIQUE, ex);
        } else if (ex instanceof DataIntegrityViolationException || ex.getCause() instanceof DataIntegrityViolationException) {
            return checkException(msg, ex);
        } else if (ex instanceof UncategorizedSQLException || ex.getCause() instanceof UncategorizedSQLException) {
            return checkException(msg, ex);
        } else {
            return Validators.isNull(errorCode)
                    ? new ServiceException(ErrorCodes.CODE_DB_OTHER, ex)
                    : new ServiceException(errorCode, ex);
        }
    }

    private static ServiceException checkException(String msg, Exception ex) {
        if (Validators.isEmpty(msg)) {
            return new ServiceException(ErrorCodes.CODE_DB_OTHER, ex);
        }

        if (msg.contains(LARGER)) {
            return new ServiceException(ErrorCodes.CODE_DB_LARGER, ex);
        } else if (msg.contains(NULL)) {
            return new ServiceException(ErrorCodes.CODE_DB_NULL, ex);
        } else if (msg.contains(UNIQUE)) {
            return new ServiceException(ErrorCodes.CODE_DB_UNIQUE, ex);
        } else {
            return new ServiceException(ErrorCodes.CODE_DB_OTHER, ex);
        }
    }
}
