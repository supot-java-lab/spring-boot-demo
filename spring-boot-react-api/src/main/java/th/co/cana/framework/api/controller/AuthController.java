package th.co.cana.framework.api.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import th.co.cana.framework.api.models.LoginReq;
import th.co.cana.framework.api.models.LoginResp;
import th.co.cana.framework.api.models.RefreshTokenReq;
import th.co.cana.framework.api.service.UserService;
import th.co.cana.framework.api.utils.AppConstants;

/**
 * @author supot.jdev
 * @version 1.0
 */

@RequiredArgsConstructor
@RestController
@RequestMapping(AppConstants.Apis.AUTH)
public class AuthController extends ApiController {

    private final UserService userService;

    @PostMapping("/token")
    public ResponseEntity<LoginResp> getToken(@Valid @RequestBody LoginReq model) {
        return ok(userService.login(model));
    }

    @PostMapping("/token/refresh")
    public ResponseEntity<LoginResp> refreshToken(@Valid @RequestBody RefreshTokenReq model) {
        return ok(userService.refreshToken(model));
    }
}
