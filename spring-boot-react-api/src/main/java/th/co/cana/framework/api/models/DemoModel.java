package th.co.cana.framework.api.models;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
public class DemoModel implements Serializable {
    private String id;
    private Date date;
    private LocalDate localDate;
    private LocalDateTime localDateTime;
    private BigDecimal amount;
    private Timestamp timestamp;
    private Timestamp timestamp1;
}
