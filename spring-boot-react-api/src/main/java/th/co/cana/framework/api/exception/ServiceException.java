package th.co.cana.framework.api.exception;

import java.util.ArrayList;
import java.util.List;

public class ServiceException extends RuntimeException {
    private final ErrorCodes errorCode;
    private List<String> errors;

    public ServiceException(Throwable cause) {
        super(cause);
        this.errorCode = ErrorCodes.CODE_500;
    }

    public ServiceException(Throwable cause, List<String> errors) {
        super(cause);
        this.errorCode = ErrorCodes.CODE_500;
        this.errors = errors;
    }

    public ServiceException(String message) {
        super(message);
        this.errorCode = ErrorCodes.CODE_500;
        addError(message);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
        this.errorCode = ErrorCodes.CODE_500;
        addError(message);
    }

    public ServiceException(ErrorCodes errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
        addError(message);
    }

    public ServiceException(ErrorCodes errorCode, List<String> errors) {
        this.errorCode = errorCode;
        this.errors = errors;
    }

    public ServiceException(ErrorCodes errorCode, Throwable cause) {
        super(cause);
        this.errorCode = errorCode;
    }

    public ServiceException(ErrorCodes errorCode, String message, Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
        addError(message);
    }

    public ErrorCodes getErrorCode() {
        return errorCode;
    }

    public String getErrorMessageCode() {
        if (errorCode == null) {
            return null;
        }

        return errorCode.getMessage();
    }

    public List<String> getErrors() {
        return errors;
    }

    private void addError(String error) {
        if (error != null && !error.isEmpty()) {
            if (errors == null) {
                errors = new ArrayList<>();
            }
            errors.add(error);
        }
    }

    public boolean isConnectionTimeout() {
        return ErrorCodes.CODE_API_TIMEOUT_CONNECTION == errorCode;
    }

    public boolean isReadTimeout() {
        return ErrorCodes.CODE_API_TIMEOUT_READ == errorCode;
    }

    public boolean isWriteTimeout() {
        return ErrorCodes.CODE_API_TIMEOUT_WRITE == errorCode;
    }

    public boolean isApiTimeout() {
        return isConnectionTimeout() || isReadTimeout() || isWriteTimeout();
    }

    public boolean isApiError() {
        return isApiTimeout() || isUnknownHost();
    }

    public boolean isUnknownHost() {
        return ErrorCodes.CODE_API_UNKNOWN_HOST == errorCode;
    }
}
