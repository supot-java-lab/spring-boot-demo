/*
 * -----------------------------------------------------------------------------------
 * Copyright (c) 2024.  Tourism Authority of Thailand (TAT) All rights reserved
 * -----------------------------------------------------------------------------------
 */

package th.co.cana.framework.api.config;

import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import th.co.cana.framework.api.config.jackson.TimestampDeserializer;
import th.co.cana.framework.api.config.jackson.TimestampSerializer;
import th.co.cana.framework.api.config.properties.ApplicationProperties;
import th.co.cana.framework.api.filter.AuthTokenFilter;
import th.co.cana.framework.api.utils.AppConstants;
import th.co.cana.framework.api.utils.MessageUtils;

import java.time.format.DateTimeFormatter;
import java.util.concurrent.Executor;

/**
 * @author supot.jdev
 * @version 1.0
 */

@Configuration
@EnableAsync
public class ApplicationConfig {

    @Autowired
    public ApplicationConfig(MessageSource messageSource, ApplicationProperties properties) {
        MessageUtils.setMessageSource(messageSource);
    }

    @Bean
    public Jackson2ObjectMapperBuilderCustomizer jsonCustomizer() {
        return builder -> {
            builder.simpleDateFormat(AppConstants.JSON_DATE_TIME_FORMAT);
            builder.serializers(new LocalDateSerializer(DateTimeFormatter.ofPattern(AppConstants.JSON_DATE_FORMAT)));
            builder.serializers(new LocalDateTimeSerializer(DateTimeFormatter.ofPattern(AppConstants.JSON_TIME_STAMP_FORMAT)));
            builder.serializers(new TimestampSerializer(AppConstants.JSON_TIME_STAMP_FORMAT));

            builder.deserializers(new LocalDateDeserializer(DateTimeFormatter.ofPattern(AppConstants.JSON_DATE_FORMAT)));
            builder.deserializers(new LocalDateTimeDeserializer(DateTimeFormatter.ofPattern(AppConstants.JSON_TIME_STAMP_FORMAT)));
            builder.deserializers(new TimestampDeserializer(AppConstants.JSON_TIME_STAMP_FORMAT));
        };
    }

    @Bean
    public FilterRegistrationBean<AuthTokenFilter> authTokenFilter() {
        FilterRegistrationBean<AuthTokenFilter> filter = new FilterRegistrationBean<>();
        filter.setFilter(new AuthTokenFilter());
        filter.addUrlPatterns(AppConstants.Apis.getAuthUrlPatterns());
        filter.setOrder(2);
        return filter;
    }

    @Bean
    public Executor taskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(2);
        executor.setMaxPoolSize(2);
        executor.setQueueCapacity(500);
        executor.setThreadNamePrefix("application-task-");
        executor.initialize();

        return executor;
    }
}