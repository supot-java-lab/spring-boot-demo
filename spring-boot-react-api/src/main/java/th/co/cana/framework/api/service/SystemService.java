package th.co.cana.framework.api.service;

import io.github.jdevlibs.utils.Validators;
import lombok.RequiredArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import th.co.cana.framework.api.config.properties.ApplicationProperties;
import th.co.cana.framework.api.models.DemoModel;
import th.co.cana.framework.api.models.PropertyInfo;
import th.co.cana.framework.api.utils.GeneratorUtils;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Service
@RequiredArgsConstructor
public class SystemService  extends AbstractService{
    private final JdbcTemplate jdbcTemplate;
    private final ApplicationProperties properties;

    public String testConnection() {
        try {
            jdbcTemplate.queryForObject("SELECT 1", Integer.class);
            return "Database connection is working!";
        } catch (Exception ex) {
            return "Database connection is not working: " + ex.getMessage();
        }
    }

    public List<PropertyInfo> loadSystemPropertyAction() {
        List<PropertyInfo> propertyItems = new ArrayList<>();
        try {
            Map<String, String> maps = System.getenv();
            if (Validators.isNotEmpty(maps)) {
                for (Map.Entry<String, String> map : maps.entrySet()) {
                    if ("java.class.path".equalsIgnoreCase(map.getKey())) {
                        continue;
                    }
                    PropertyInfo proValue = new PropertyInfo();
                    proValue.setType("ENV");
                    proValue.setKey(map.getKey());
                    proValue.setValue(map.getValue());
                    propertyItems.add(proValue);
                }
            }

            Properties pro = System.getProperties();
            for (Map.Entry<Object, Object> map : pro.entrySet()) {
                if ("java.class.path".equalsIgnoreCase(map.getKey().toString())) {
                    continue;
                }
                PropertyInfo proValue = new PropertyInfo();
                proValue.setType("PROP");
                proValue.setKey(map.getKey().toString());
                proValue.setValue(map.getValue().toString());
                propertyItems.add(proValue);
            }
        } catch (Exception ex) {
            logger.error("loadSystemPropertyAction", ex);
        }

        return propertyItems;
    }

    public Map<String, Object> getVersionInfo() {
        Map<String, Object> values = new LinkedHashMap<>();
        values.put("name", properties.getName());
        values.put("version", properties.getVersion());
        values.put("buildTime", properties.getBuildTime());
        values.put("javaBuildVersion", properties.getJavaBuildVersion());

        return values;
    }

    public DemoModel getDemoModel() {
        DemoModel demoModel = new DemoModel();
        demoModel.setId(GeneratorUtils.getSnowFlakeIdString());
        demoModel.setDate(new Date());
        demoModel.setLocalDate(LocalDate.now());
        demoModel.setLocalDateTime(LocalDateTime.now());
        demoModel.setAmount(BigDecimal.valueOf(1230000D));
        demoModel.setTimestamp(Timestamp.from(Instant.now()));
        demoModel.setTimestamp1(demoModel.getTimestamp());

        return demoModel;
    }
}
