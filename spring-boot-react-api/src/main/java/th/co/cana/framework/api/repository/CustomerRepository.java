package th.co.cana.framework.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import th.co.cana.framework.api.entity.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
