package th.co.cana.framework.api.filter;

import io.github.jdevlibs.spring.utils.JsonUtils;
import io.github.jdevlibs.utils.Validators;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.lang.NonNull;
import org.springframework.web.filter.OncePerRequestFilter;
import th.co.cana.framework.api.models.Response;
import th.co.cana.framework.api.utils.JwtUtils;
import th.co.cana.framework.api.utils.ResponseUtils;

import java.io.IOException;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Slf4j
public class AuthTokenFilter extends OncePerRequestFilter {
    @Override
    protected void doFilterInternal(@NonNull HttpServletRequest request, @NonNull HttpServletResponse response
            , @NonNull FilterChain filterChain) throws ServletException, IOException {

        String token = JwtUtils.getToken(request);
        log.info("Step 1. Validate token");
        log.info("Token: {}", token);
        if (Validators.isEmpty(token)) {
            log.warn("In case unauthorized to access API");
            unauthorized(response, "HTTP Header[Authorization] is null.");
            return;
        }

        log.info("Step 1. Verify token");
        JwtUtils.JwtVerifyResult result = JwtUtils.verify(token);
        if (result.isError()) {
            log.warn("In case Validate token fail.");
            unauthorized(response, "Validate token error.");
            return;
        }
        if (result.isExpired()) {
            log.warn("In case Token is expired.");
            unauthorized(response, "Token is expired.");
            return;
        }

        filterChain.doFilter(request, response);
    }

    private void unauthorized(HttpServletResponse response, String msg) throws IOException {
        Response resp = ResponseUtils.unauthorized(msg);
        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        response.getOutputStream().flush();
        response.getOutputStream().write(JsonUtils.jsonAsBytes(resp));
    }
}
