package th.co.cana.framework.api.models.rest;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.HashMap;
import java.util.Map;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = true)
public class GetRequest extends Request {

    private Map<String, String> parameters;

    public void addParameter(String name, String value) {
        if (name == null || name.isEmpty()) {
            return;
        }
        if (parameters == null) {
            parameters = new HashMap<>();
        }
        parameters.put(name, value);
    }

    public void clearParameter() {
        parameters = null;
        clearHeader();
    }
}
