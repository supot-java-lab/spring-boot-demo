package th.co.cana.framework.api.utils;

import io.github.jdevlibs.spring.utils.JsonUtils;
import io.github.jdevlibs.utils.DateUtils;
import io.github.jdevlibs.utils.Encryptions;
import io.github.jdevlibs.utils.Validators;
import io.jsonwebtoken.*;
import jakarta.servlet.http.HttpServletRequest;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import javax.crypto.SecretKey;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Map;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Slf4j
public final class JwtUtils {
    private static final String SYSTEM 	        = "react-api";
    private static final String TOKEN_HEADER 	= "Authorization";
    private static final String TOKEN_PARAM 	= "token";
    private static final String HEADER_TOKEN 	= "Bearer ";
    private static final int EXP_TIME		    = 15; //15 Minutes
    private static final SecretKey KEY = Jwts.SIG.HS256.key().build();

    private JwtUtils() {}

    public static String sign(final JwtSignInfo info) {
        log.info("Sign api with : {}", info);

        // Let's set the JWT Claims
        JwtBuilder builder = Jwts.builder().signWith(KEY)
                .header().type("JWT")
                .add("app", Validators.isEmpty(info.getSystem()) ? SYSTEM : info.getSystem())
                .and()
                .issuer(info.user.id)
                .subject(info.user.name);
        if (Validators.isNotNull(info.getIssueDt())) {
            builder.issuedAt(info.getIssueDt());
        } else {
            builder.issuedAt(new Date());
        }

        // if it has specified, let's add the expiration
        Date exp;
        if (info.getExpMinute() > 0) {
            exp = DateUtils.addMinute(info.getIssueDt(), info.getExpMinute());
        } else {
            exp = DateUtils.addMinute(info.getIssueDt(), EXP_TIME);
        }
        builder.expiration(exp);

        if (Validators.isNotNull(info.getUser())) {
            builder.claim("user", info.getUser());
        }
        return builder.compact();
    }

    public static JwtVerifyResult verify(String token) {
        JwtVerifyResult result = new JwtVerifyResult();
        try {
            Jws<Claims> claim = Jwts.parser()
                    .verifyWith(KEY)
                    .build()
                    .parseSignedClaims(token);
            log.info("{}", claim);

            if (Validators.isNotNull(claim)) {
                result.setIssuer(claim.getPayload().getIssuer());
                result.setSubject(claim.getPayload().getSubject());
                result.setMessage("Verify token success.");
                result.setUser(getUserInfo(claim));
            }
        } catch (ExpiredJwtException ex) {
            result.expired(ex.getMessage());
        } catch (SecurityException ex) {
            result.fail(ex.getMessage());
        } catch (Exception ex) {
            result.error(ex.getMessage());
        }

        return result;
    }

    public static String getToken(HttpServletRequest req) {
        if (Validators.isNull(req)) {
            return null;
        }

        String token = req.getHeader(TOKEN_HEADER);
        if (token != null && token.startsWith(HEADER_TOKEN)) {
            token = token.substring(7);
        } else {
            token = req.getHeader(TOKEN_PARAM);
            if (Validators.isEmpty(token)) {
                token = req.getParameter(TOKEN_PARAM);
            }
        }
        return token;
    }

    public static String refreshToken(JwtSignInfo info) {
        LocalDateTime exp = LocalDateTime.now();
        int expMinute = exp.getMinute();
        if (expMinute == 0) {
            expMinute = EXP_TIME;
        }
        JwtRefreshToken token = new JwtRefreshToken();
        token.setId(info.user.id);
        token.setExp(exp.plusMinutes(expMinute * 2));

        String json = JsonUtils.json(token);
        return Encryptions.encodeBase64String(json.getBytes());
    }

    public static JwtRefreshToken getJwtRefreshToken(String token) {
        String json = Encryptions.decodeBase64String(token);
        return JsonUtils.model(json, JwtRefreshToken.class);
    }

    @SuppressWarnings("unchecked")
    private static JwtUserInfo getUserInfo(Jws<Claims> claim) {
        Map<String, Object> userMaps = claim.getPayload().get("user", Map.class);
        if (Validators.isEmpty(userMaps)) {
            return null;
        }

        JwtUserInfo user = new JwtUserInfo();
        user.setId(String.valueOf(userMaps.get("id")));
        user.setName(String.valueOf(userMaps.get("name")));
        return user;
    }

    @Data
    public static final class JwtSignInfo implements Serializable {
        private String system;
        private Date issueDt;
        private int expMinute;
        private JwtUserInfo user;
    }

    @Data
    public static final class JwtUserInfo implements Serializable {
        private String id;
        private String name;
    }

    @Data
    public static final class JwtRefreshToken implements Serializable {
        private String id;
        private LocalDateTime exp;
    }

    @Data
    public static final class JwtVerifyResult implements Serializable {
        private static final String SUCCESS = "00";
        private static final String EXPIRED = "01";
        private static final String FAIL = "02";
        private static final String ERROR = "99";

        private String code;
        private String message;
        private String desc;
        private String issuer;
        private String subject;
        private JwtUserInfo user;

        public JwtVerifyResult() {
            super();
            this.code = SUCCESS;
        }

        public JwtVerifyResult(String code) {
            super();
            this.code = code;
        }

        public JwtVerifyResult(String code, String message) {
            super();
            this.code = code;
            this.message = message;
        }

        public JwtVerifyResult(String code, String message, String desc) {
            super();
            this.code = code;
            this.message = message;
            this.desc = desc;
        }

        public JwtVerifyResult expired(String msg) {
            this.code = EXPIRED;
            this.message = "Verify token was expired.";
            this.desc = msg;
            return this;
        }

        public JwtVerifyResult fail(String msg) {
            this.code = FAIL;
            this.message = "Verify token was fail.";
            this.desc = msg;
            return this;
        }

        public JwtVerifyResult error(String msg) {
            this.code = ERROR;
            this.message = "Verify token was error.";
            this.desc = msg;
            return this;
        }

        public boolean isSuccess() {
            return SUCCESS.equals(code);
        }

        public boolean isExpired() {
            return EXPIRED.equals(code);
        }

        public boolean isError() {
            return (ERROR.equals(code) || FAIL.equals(code));
        }

    }
}