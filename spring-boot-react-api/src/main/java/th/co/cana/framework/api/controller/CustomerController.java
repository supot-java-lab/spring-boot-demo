package th.co.cana.framework.api.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import th.co.cana.framework.api.entity.Customer;
import th.co.cana.framework.api.models.CustomerModel;
import th.co.cana.framework.api.models.Response;
import th.co.cana.framework.api.service.CustomerService;
import th.co.cana.framework.api.utils.AppConstants;
import th.co.cana.framework.api.utils.ResponseUtils;

import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */

@RequiredArgsConstructor
@RestController
@RequestMapping(AppConstants.Apis.CUSTOMER)
public class CustomerController extends ApiController {

    private final CustomerService customerService;

    @GetMapping()
    public ResponseEntity<List<Customer>> getCustomers() {
        return ok(customerService.findAll());
    }

    @GetMapping("/{customerId}")
    public ResponseEntity<Customer> getCustomer(@PathVariable Long customerId) {
        return ok(customerService.getCustomer(customerId));
    }

    @PostMapping()
    public ResponseEntity<Response> save(@Valid @RequestBody CustomerModel model) {
        customerService.save(model);
        return ok(ResponseUtils.response());
    }

    @PutMapping("/{customerId}")
    public ResponseEntity<Response> update(@PathVariable Long customerId, @Valid @RequestBody CustomerModel model) {
        customerService.save(model);
        return ok(ResponseUtils.response());
    }

    @DeleteMapping("/{customerId}")
    public ResponseEntity<Response> delete(@PathVariable Long customerId) {
        customerService.delete(customerId);
        return ok(ResponseUtils.response());
    }
}
