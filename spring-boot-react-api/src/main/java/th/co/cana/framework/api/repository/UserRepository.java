package th.co.cana.framework.api.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import th.co.cana.framework.api.entity.Users;

@Repository
public interface UserRepository extends JpaRepository<Users, Long> {

    Users findByUserNameAndPassword(String userName, String password);

    Users findByRefreshToken(String refreshToken);
}
