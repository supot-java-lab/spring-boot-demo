package th.co.cana.framework.api.controller;

import io.github.jdevlibs.utils.MimeTypes;
import io.github.jdevlibs.utils.Validators;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import th.co.cana.framework.api.models.Response;
import th.co.cana.framework.api.utils.ResponseUtils;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public abstract class ApiController {
    protected Logger logger = LoggerFactory.getLogger(getClass());

    public <T> ResponseEntity<T> ok(T body) {
        return ResponseEntity.ok(body);
    }

    public <T> ResponseEntity<T> badRequest(T body) {
        return ResponseEntity.badRequest().body(body);
    }

    public ResponseEntity<Object> notFound() {
        return ResponseEntity.notFound().build();
    }

    public ResponseEntity<Object> noContent(String msg) {
        return ResponseEntity.status(HttpStatus.NO_CONTENT).body(ResponseUtils.notContent(msg));
    }

    public ResponseEntity<Object> internalServerError(Exception ex) {
        return internalServerError(ResponseUtils.processError(ex));
    }

    public ResponseEntity<Object> internalServerError(Object body) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(body);
    }

    public ResponseEntity<Response> internalError(Exception ex) {
        return internalError(ResponseUtils.processError(ex));
    }

    public ResponseEntity<Response> internalError(Response body) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(body);
    }

    public ResponseEntity<Object> unauthorized() {
        return unauthorized(ResponseUtils.unauthorized());
    }

    public ResponseEntity<Object> unauthorized(Object body) {
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(body);
    }

    public ResponseEntity<Object> download(Resource resource) {
        return download(resource, resource.getFilename(), true);
    }

    public ResponseEntity<Object> download(Resource resource, boolean attachment) {
        return download(resource, resource.getFilename(), attachment);
    }

    public ResponseEntity<Object> download(Resource resource, String fileName) {
        return download(resource, fileName, true);
    }

    public ResponseEntity<Object> download(Resource resource, String fileName, boolean attachment) {
        if (Validators.isNull(resource)) {
            return notFound();
        }
        if (Validators.isEmpty(fileName)) {
            fileName = resource.getFilename();
        }

        logger.info("Download file with name : {}", fileName);
        return getFileResponseEntity(fileName, attachment, resource);
    }

    public ResponseEntity<Object> download(String file) {
        return download(new File(file), null, true);
    }

    public ResponseEntity<Object> download(String file, boolean attachment) {
        return download(new File(file), null, attachment);
    }

    public ResponseEntity<Object> download(String file, String fileName) {
        return download(new File(file), fileName, true);
    }

    public ResponseEntity<Object> download(String file, String fileName, boolean attachment) {
        return download(new File(file), fileName, attachment);
    }

    public ResponseEntity<Object> download(File file) {
        return download(file, file.getName(), true);
    }

    public ResponseEntity<Object> download(File file, boolean attachment) {
        return download(file, file.getName(), attachment);
    }

    public ResponseEntity<Object> download(File file, String fileName) {
        return download(file, fileName, true);
    }

    public ResponseEntity<Object> download(File file, String fileName, boolean attachment) {
        if (Validators.isNull(file) || !file.exists() || !file.isFile()) {
            logger.error("Download File not found");
            return notFound();
        }
        if (Validators.isEmpty(fileName)) {
            fileName = file.getName();
        }

        logger.info("Download file {} with name : {}", file.getPath(), fileName);
        Resource resource = loadFileAsResource(file);
        if (Validators.isNull(resource)) {
            return notFound();
        }

        return getFileResponseEntity(fileName, attachment, resource);
    }

    private ResponseEntity<Object> getFileResponseEntity(String fileName, boolean attachment, Resource resource) {
        ResponseEntity.BodyBuilder builder = ResponseEntity.ok();
        if (attachment) {
            if (Validators.isNotEmpty(fileName)) {
                String filenameEncode = encode(fileName);
                builder.header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename*='UTF-8'" + filenameEncode);
            } else {
                builder.header(HttpHeaders.CONTENT_DISPOSITION, "attachment");
            }
        } else {
            builder.header(HttpHeaders.CONTENT_DISPOSITION, "inline");
        }
        if (Validators.isNotEmpty(fileName)) {
            String mimeType = MimeTypes.getMimeType(fileName);
            builder.header(HttpHeaders.CONTENT_TYPE, mimeType);
        } else {
            builder.header(HttpHeaders.CONTENT_TYPE, "application/octet-stream");
        }
        return builder.body(resource);
    }

    private Resource loadFileAsResource(File fileName) {
        try {
            Path filePath = fileName.toPath().normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                return resource;
            }

            logger.error("File {} not found", fileName.getPath());
        } catch (MalformedURLException ex) {
            logger.error("loadFileAsResource not found", ex);
        }

        return null;
    }

    /**
     * Copy Entity to DTO or DTO to entity
     *
     * @param sources The List<T> of source objects
     * @param clazz   Target copy class
     * @param ignores Ignore properties for copy
     * @return Target collection of class
     */
    public <T> List<T> copyProperties(List<?> sources, Class<T> clazz, String... ignores) {
        if (sources == null || sources.isEmpty() || clazz == null) {
            return Collections.emptyList();
        }

        List<T> items = new ArrayList<>();
        for (Object source : sources) {
            if (source == null) {
                continue;
            }

            T target = copyProperties(source, clazz, ignores);
            if (target != null) {
                items.add(target);
            }
        }

        return items;
    }

    /**
     * Copy Entity to DTO or DTO to entity
     *
     * @param source  The source object
     * @param clazz   Target copy class
     * @param ignores Ignore properties for copy
     * @return Target class
     */
    public <T> T copyProperties(Object source, Class<T> clazz, String... ignores) {
        if (source == null || clazz == null) {
            return null;
        }

        T target = BeanUtils.instantiateClass(clazz);
        BeanUtils.copyProperties(source, target, ignores);
        return target;
    }

    /**
     * Copy Entity to DTO or DTO to entity
     *
     * @param source  Source object
     * @param target  Target object
     * @param ignores Ignore properties for copy
     */
    public void copyProperties(Object source, Object target, String... ignores) {
        if (source == null || target == null) {
            return;
        }
        BeanUtils.copyProperties(source, target, ignores);
    }

    private String encode(String value) {
        try {
            return URLEncoder.encode(value, StandardCharsets.UTF_8);
        } catch (Exception ex) {
            return value;
        }
    }
}
