package th.co.cana.framework.api.config.jackson;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import io.github.jdevlibs.utils.DateFormats;
import io.github.jdevlibs.utils.Validators;
import th.co.cana.framework.api.utils.AppConstants;

import java.io.IOException;
import java.sql.Timestamp;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class TimestampDeserializer extends StdDeserializer<Timestamp> {
    private final String format;

    public TimestampDeserializer() {
        super(Timestamp.class);
        this.format = AppConstants.JSON_TIME_STAMP_FORMAT;
    }

    public TimestampDeserializer(String format) {
        super(Timestamp.class);
        if (Validators.isEmpty(format)) {
            format = AppConstants.JSON_TIME_STAMP_FORMAT;
        }
        this.format = format;
    }

    @Override
    public Timestamp deserialize(JsonParser jsonParser, DeserializationContext context) throws IOException {
        String value = jsonParser.getValueAsString();
        if (Validators.isEmpty(value)) {
            return null;
        }
        return DateFormats.sqlTimestamp(value, format);
    }
}
