package th.co.cana.framework.api.service;

import io.github.jdevlibs.utils.Encryptions;
import io.github.jdevlibs.utils.Validators;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import th.co.cana.framework.api.entity.Users;
import th.co.cana.framework.api.exception.ErrorCodes;
import th.co.cana.framework.api.exception.ValidationException;
import th.co.cana.framework.api.models.LoginReq;
import th.co.cana.framework.api.models.LoginResp;
import th.co.cana.framework.api.models.RefreshTokenReq;
import th.co.cana.framework.api.repository.UserRepository;
import th.co.cana.framework.api.utils.AuthUtils;
import th.co.cana.framework.api.utils.Exceptions;
import th.co.cana.framework.api.utils.GeneratorUtils;
import th.co.cana.framework.api.utils.MessageUtils;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */

@RequiredArgsConstructor
@Service
public class UserService extends AbstractService {
    private final UserRepository userRepository;

    public List<Users> getUsers() {
        try {
            return userRepository.findAll();
        } catch (Exception ex) {
            throw Exceptions.invokeException(ex, ErrorCodes.CODE_DB_QUERY);
        }
    }

    public Users getUser(Long id) {
        try {
            return userRepository.findById(id).orElse(null);
        } catch (Exception ex) {
            throw Exceptions.invokeException(ex, ErrorCodes.CODE_DB_QUERY);
        }
    }

    public LoginResp login(final LoginReq model) {
        try {
            String password = Encryptions.encodeBase64String(Encryptions.md5(model.getUserName(), model.getPassword()));
            Users user = userRepository.findByUserNameAndPassword(model.getUserName(), password);
            if (Validators.isNull(user) || user.isInvalid()) {
                throw new ValidationException(MessageUtils.getValue("resp.auth.invalid.user"));
            }

            LoginResp resp = AuthUtils.generateUserToken(user);
            this.updateUser(resp, user);

            return resp;
        } catch (ValidationException ex) {
            throw ex;
        } catch (Exception ex) {
            throw Exceptions.invokeException(ex, ErrorCodes.CODE_DB_QUERY);
        }
    }

    public LoginResp refreshToken(final RefreshTokenReq model) {
        try {
            Users user = userRepository.findByRefreshToken(model.getRefreshToken());
            if (Validators.isNull(user) || user.isInvalid()) {
                throw new ValidationException(MessageUtils.getValue("resp.auth.invalid.refresh.token"));
            }

            LoginResp resp = AuthUtils.generateUserToken(user);
            this.updateUser(resp, user);

            return resp;
        } catch (ValidationException ex) {
            throw ex;
        } catch (Exception ex) {
            throw Exceptions.invokeException(ex, ErrorCodes.CODE_DB_QUERY);
        }
    }

    public Users getRefreshToken(String refreshToken) {
        try {
            return userRepository.findByRefreshToken(refreshToken);
        } catch (Exception ex) {
            throw Exceptions.invokeException(ex, ErrorCodes.CODE_DB_QUERY);
        }
    }

    public void save(final Users user) {
        try {
            user.setId(GeneratorUtils.getSnowFlakeId());
            userRepository.save(user);
        } catch (Exception ex) {
            throw Exceptions.invokeException(ex, ErrorCodes.CODE_DB_INSERT);
        }
    }

    public void update(final Users user) {
        try {
            if (Validators.isNull(user.getId())) {
                user.setId(GeneratorUtils.getSnowFlakeId());
            }
            userRepository.save(user);
        } catch (Exception ex) {
            throw Exceptions.invokeException(ex, ErrorCodes.CODE_DB_INSERT);
        }
    }

    private void updateUser(final LoginResp resp, Users user) {
        if (Validators.isNotNull(resp)) {
            if (Validators.isNull(resp.getLastAccess())) {
                resp.setLastAccess(LocalDateTime.now());
            }
            user.setLastAccess(LocalDateTime.now());
            user.setAccessToken(resp.getAccessToken());
            user.setRefreshToken(resp.getRefreshToken());
            update(user);
        }
    }
}
