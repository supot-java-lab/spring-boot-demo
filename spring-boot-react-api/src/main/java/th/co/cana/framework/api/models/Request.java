package th.co.cana.framework.api.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Getter
@Setter
@ToString
public class Request implements Serializable {
    private String requestId;
    private String clientId;
    private String userId;
}
