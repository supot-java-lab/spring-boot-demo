package th.co.cana.framework.api.models;

import jakarta.validation.constraints.NotBlank;
import lombok.*;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Getter
@Setter
@ToString(callSuper = true)
public class RefreshTokenReq extends Request {
    @NotBlank
    private String userName;
    @NotBlank
    private String refreshToken;
}
