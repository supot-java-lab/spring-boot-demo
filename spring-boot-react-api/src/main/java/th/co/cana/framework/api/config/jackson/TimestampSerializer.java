package th.co.cana.framework.api.config.jackson;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import io.github.jdevlibs.utils.DateFormats;
import io.github.jdevlibs.utils.Validators;
import th.co.cana.framework.api.utils.AppConstants;

import java.io.IOException;
import java.sql.Timestamp;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class TimestampSerializer extends StdSerializer<Timestamp> {
    private final String format;

    public TimestampSerializer() {
        super(Timestamp.class);
        this.format = AppConstants.JSON_TIME_STAMP_FORMAT;
    }

    public TimestampSerializer(String format) {
        super(Timestamp.class);
        if (Validators.isEmpty(format)) {
            format = AppConstants.JSON_TIME_STAMP_FORMAT;
        }
        this.format = format;
    }

    @Override
    public void serialize(Timestamp value, JsonGenerator generator, SerializerProvider provider) throws IOException {
        if (value == null) {
            generator.writeNull();
        } else {
            generator.writeString(DateFormats.format(value.toLocalDateTime(), format));
        }
    }
}
