package th.co.cana.framework.api.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import th.co.cana.framework.api.models.DemoModel;
import th.co.cana.framework.api.models.PropertyInfo;
import th.co.cana.framework.api.service.SystemService;
import th.co.cana.framework.api.utils.AppConstants;

import java.util.List;
import java.util.Map;

/**
 * @author supot.jdev
 * @version 1.0
 */

@RequiredArgsConstructor
@RestController
@RequestMapping(AppConstants.Apis.SYSTEM)
public class SystemController extends ApiController {

    private final SystemService systemService;

    @GetMapping("/health")
    public String health() {
        return "up";
    }

    @GetMapping("/connection")
    public String testConnection() {
        return systemService.testConnection();
    }

    @GetMapping("/info")
    public ResponseEntity<List<PropertyInfo>> systemProperty() {
        return ok(systemService.loadSystemPropertyAction());
    }

    @GetMapping("/version")
    public ResponseEntity<Map<String, Object>> systemVersion() {
        return ok(systemService.getVersionInfo());
    }

    @GetMapping("/jackson")
    public ResponseEntity<DemoModel> getDemoModel() {
        return ok(systemService.getDemoModel());
    }
}
