package th.co.cana.framework.api.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import th.co.cana.framework.api.entity.Users;
import th.co.cana.framework.api.models.LoginResp;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Slf4j
public final class AuthUtils {
    private AuthUtils() {}

    public static LoginResp generateUserToken(final Users user) {
        log.info("Step 1. Generate user token");
        JwtUtils.JwtSignInfo signInfo = new JwtUtils.JwtSignInfo();
        signInfo.setUser(new JwtUtils.JwtUserInfo());
        signInfo.getUser().setId(user.getId().toString());
        signInfo.getUser().setName(user.getFullName());

        String token = JwtUtils.sign(signInfo);
        log.info("Token : {}", token);

        String refreshToken = JwtUtils.refreshToken(signInfo);
        log.info("Refresh Token : {}", refreshToken);

        log.info("Step 3. Update user token");
        LoginResp resp = new LoginResp();
        BeanUtils.copyProperties(user, resp);

        resp.setAccessToken(token);
        resp.setRefreshToken(refreshToken);

        return resp;
    }
}
