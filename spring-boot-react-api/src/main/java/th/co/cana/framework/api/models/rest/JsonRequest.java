package th.co.cana.framework.api.models.rest;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = true)
public class JsonRequest<T> extends Request {

    private T model;
}
