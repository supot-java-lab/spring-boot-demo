package th.co.cana.framework.api.models;

import lombok.Data;

import java.io.Serializable;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
public class AppBuildInfo implements Serializable {
    private String name;
    private String version;
    private String buildTime;
    private String javaBuildVersion;
    private String systemDate;
    private String systemLocale;
}
