/*
 * -----------------------------------------------------------------------------------
 * Copyright (c) 2024.  Tourism Authority of Thailand (TAT) All rights reserved
 * -----------------------------------------------------------------------------------
 */

package th.co.cana.framework.api.repository.jdbc;

import io.github.jdevlibs.spring.jdbc.JdbcDao;
import io.github.jdevlibs.spring.jdbc.criteria.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import th.co.cana.framework.api.exception.ErrorCodes;
import th.co.cana.framework.api.utils.Exceptions;

import java.sql.SQLException;
import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */

@Service
public class BaseJdbcDao extends JdbcDao implements BaseDao {

    @Autowired
    @Override
    protected void autowiredJdbcTemplate(JdbcTemplate jdbcTemplate) {
        setJdbcTemplate(jdbcTemplate);
    }

    @Override
    public  <T> List<T> queryToPaging(String sql, Parameter params,
                                      Criteria criteria, Class<T> clazz) {

        StringBuilder pageSql = new StringBuilder(sql);
        if (!criteria.isEmptySort()) {
            setOrderByOption(pageSql, criteria);
        }
        setPagingOption(pageSql, params, criteria);

        return queryToList(pageSql.toString(), params, clazz);
    }

    @Override
    protected void setPagingOption(StringBuilder sql, Parameter params, Criteria paging) {
        String normalSql = sql.toString();
        sql.setLength(0);
        sql.append("SELECT T.* FROM (");
        sql.append(" SELECT ROWNUM AS PAGE_ROW_NUM, TB.*  FROM (").append(normalSql);
        sql.append(" ) TB");
        sql.append(" ) T");
        if (params instanceof NameParameter name) {
            sql.append(" WHERE PAGE_ROW_NUM BETWEEN :P_ROW_START AND :P_ROW_END");
            name.add("P_ROW_START", paging.getOracleRowStart());
            name.add("P_ROW_END", paging.getOracleRowEnd());
        } else {
            IndexParameter inx = (IndexParameter) params;
            sql.append(" WHERE PAGE_ROW_NUM BETWEEN ? AND ?");
            inx.add(paging.getOracleRowStart());
            inx.add(paging.getOracleRowEnd());
        }
    }

    @Override
    public String sqlLikeContain(String value) {
        return "%" + value + "%";
    }

    @Override
    public String sqlLikeStart(String value) {
        return value + "%";
    }

    @Override
    public String sqlLikeEnd(String value) {
        return "%" + value;
    }

    @Override
    public void callDynamicProcedure(ProcedureCriteria criteria) {
        try {
            executeProcedure(criteria);
        } catch (SQLException ex) {
            throw Exceptions.invokeException(ex, ErrorCodes.CODE_DB_QUERY);
        }
    }
}
