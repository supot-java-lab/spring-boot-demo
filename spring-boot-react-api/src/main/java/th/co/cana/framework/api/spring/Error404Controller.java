package th.co.cana.framework.api.spring;

import io.github.jdevlibs.utils.Validators;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.web.servlet.error.AbstractErrorController;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import th.co.cana.framework.api.models.Response;
import th.co.cana.framework.api.utils.AppConstants;
import th.co.cana.framework.api.utils.MessageUtils;
import th.co.cana.framework.api.utils.ResponseUtils;

import java.util.Collections;
import java.util.Map;

/**
 * @author supot.jdev
 * @version 1.0
 */
@RestController
@RequestMapping({AppConstants.Apis.ERROR})
@Slf4j
public class Error404Controller extends AbstractErrorController {

    public Error404Controller(ErrorAttributes errorAttributes) {
        super(errorAttributes, Collections.emptyList());
    }

    @RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Response> error(HttpServletRequest request) {
        log.error("Application API error");
        Map<String, Object> errors = getErrorAttributes(request, ErrorAttributeOptions.defaults());
        HttpStatus status = this.getStatus(request);
        Response resp = ResponseUtils.response(status);

        Object error = errors.get("error");
        if (Validators.isNotNull(error)) {
            resp.addError(String.valueOf(error));
        }
        if (Validators.isNotEmpty(errors)) {
            resp.setPath(String.valueOf(errors.get("path")));
        }

        if (HttpStatus.NOT_FOUND == status && Validators.isEmpty(resp.getMessage())) {
            resp.setMessage(MessageUtils.getValue("resp.error.404"));
        }

        return new ResponseEntity<>(resp, status);
    }

}