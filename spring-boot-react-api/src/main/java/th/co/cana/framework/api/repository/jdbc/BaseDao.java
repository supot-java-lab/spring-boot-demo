/*
 * -----------------------------------------------------------------------------------
 * Copyright (c) 2024.  Tourism Authority of Thailand (TAT) All rights reserved
 * -----------------------------------------------------------------------------------
 */

package th.co.cana.framework.api.repository.jdbc;

import io.github.jdevlibs.spring.jdbc.criteria.ProcedureCriteria;

/**
 * @author supot.jdev
 * @version 1.0
 */
public interface BaseDao {

    void callDynamicProcedure(final ProcedureCriteria criteria);
}
