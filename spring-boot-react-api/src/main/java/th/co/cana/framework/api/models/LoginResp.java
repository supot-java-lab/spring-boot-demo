package th.co.cana.framework.api.models;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
public class LoginResp {
    private Long id;
    private String firstName;
    private String lastName;
    private String userName;
    private String accessToken;
    private String refreshToken;
    private LocalDateTime lastAccess;
}
