package th.co.cana.framework.api.utils;

import java.util.Locale;

/**
 * @author supot.jdev
 * @version 1.0
 */
public final class AppConstants {
    private AppConstants() {
    }

    public static final String BASE_PACKAGE_NAME = AppConstants.class.getPackageName().replaceFirst(".utils", "");

    public static final String Y = "Y";
    public static final String N = "N";

    public static final String UTF8 = "UTF-8";
    public static final Locale US = Locale.US;
    public static final Locale TH = new Locale("th", "TH");
    public static final String LANG_TH = "TH";
    public static final String LANG_US = "US";
    public static final String HTML_NEW_LINE = "<br/>";
    public static final String HTML_SPACE = "&nbsp;";
    public static final String HTML_SPACE_2 = "&ensp;";
    public static final String HTML_SPACE_4 = "&emsp;";

    public static final String DATE_FORMAT_FULL = "dd MMMM yyyy";
    public static final String DATE_FORMAT = "dd/MM/yyyy";
    public static final String DATE_TIME_FORMAT = "dd/MM/yyyy HH:mm:ss";

    public static final String JSON_DATE_FORMAT = "yyyy-MM-dd";
    public static final String JSON_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final String JSON_TIME_STAMP_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSSSS";

    /**
     * AOP: Aspect Oriented Programming package
     */
    public static final class Aspects {
        private Aspects() {
        }

        public static final String SERVICE = "execution(public * " + BASE_PACKAGE_NAME + ".service..*.*(..))";
        public static final String REPOSITORY = "execution(public * " + BASE_PACKAGE_NAME + ".repository..*.*(..))";
        public static final String DAO = "execution(public * " + BASE_PACKAGE_NAME + ".dao..*.*(..))";
    }

    public static final class Apis {
        private Apis() {
        }

        public static final String APP = "/v1/react-api";
        public static final String ERROR = "/error";
        public static final String AUTH = "/v1/auth";
        public static final String SYSTEM = APP + "/system";
        public static final String CUSTOMER = APP + "/customers";

        public static String[] getAuthUrlPatterns() {
            return new String[]{
                    APP + "/*"
            };
        }
    }
}