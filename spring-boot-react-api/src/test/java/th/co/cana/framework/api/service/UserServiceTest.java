package th.co.cana.framework.api.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import th.co.cana.framework.api.BaseUnitTest;
import th.co.cana.framework.api.models.LoginReq;
import th.co.cana.framework.api.models.LoginResp;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class UserServiceTest extends BaseUnitTest {
    @Autowired
    private UserService service;

    @DisplayName("[UserService]: login_ok")
    @Test
    @Order(1)
    void login_ok() {
        LoginReq loginReq = new LoginReq();
        loginReq.setUserName("admin");
        loginReq.setPassword("password");
        LoginResp resp = service.login(loginReq);
        Assertions.assertNotNull(resp);
        logger.info("{}", resp);
    }
}
