package th.co.cana.framework.api.main;

import io.github.jdevlibs.utils.DateFormats;
import th.co.cana.framework.api.utils.GeneratorUtils;
import th.co.cana.framework.api.utils.JwtUtils;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class TestTokenUtils {
    public static void main(String[] args) {
        JwtUtils.JwtSignInfo object = new JwtUtils.JwtSignInfo();
        object.setExpMinute(15);
        object.setIssueDt(new Date());
        object.setUser(new JwtUtils.JwtUserInfo());
        object.getUser().setId(GeneratorUtils.getSnowFlakeIdString());
        object.getUser().setName("Administrator");
        String token = JwtUtils.sign(object);
        System.out.println("token : " + token);

        String refreshToken = JwtUtils.refreshToken(object);
        System.out.println("refreshToken : " + refreshToken);

        JwtUtils.JwtVerifyResult result = JwtUtils.verify(token);
        System.out.println("value : " + result);

        JwtUtils.JwtRefreshToken refToken = JwtUtils.getJwtRefreshToken(refreshToken);
        System.out.println("refToken : " + refToken);

        String dateStr = DateFormats.format(LocalDateTime.now(), "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        System.out.println("dateStr : " + dateStr);
        System.out.println("LocalDateTime : " + LocalDateTime.now());
    }
}
