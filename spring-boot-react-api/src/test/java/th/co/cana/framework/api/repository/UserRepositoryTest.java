package th.co.cana.framework.api.repository;

import io.github.jdevlibs.utils.Encryptions;
import io.github.jdevlibs.utils.exception.SystemException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import th.co.cana.framework.api.BaseUnitTest;
import th.co.cana.framework.api.entity.Users;
import th.co.cana.framework.api.utils.GeneratorUtils;

import java.time.LocalDateTime;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class UserRepositoryTest extends BaseUnitTest {
    @Autowired
    private UserRepository repository;

    @DisplayName("[UserRepository]: save_ok")
    @Test
    @Order(1)
    void save_ok() throws SystemException {
        Users user = new Users();
        user.setId(GeneratorUtils.getSnowFlakeId());
        user.setFirstName("Admin");
        user.setUserName("admin");

        String password = Encryptions.encodeBase64String(Encryptions.md5(user.getUserName(), "password"));
        user.setPassword(password);
        user.setCreatedBy("SYSTEM");
        user.setCreatedDate(LocalDateTime.now());
        repository.save(user);

        Assertions.assertNotNull(user);
        logger.info("{}", user);
    }
}
