CREATE TABLE `customer` (
    `customer_id` bigint NOT NULL,
    `first_name` varchar(100) NOT NULL,
    `last_name` varchar(100) DEFAULT NULL,
    `birth_date` date DEFAULT NULL,
    `credit_amt` decimal(20,2) DEFAULT NULL,
    `created_by` varchar(50)  DEFAULT NULL,
    `created_date` timestamp NULL DEFAULT NULL,
    `updated_by` varchar(50)  DEFAULT NULL,
    `updated_date` timestamp NULL DEFAULT NULL,
    PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB;

CREATE TABLE `users` (
     `id` bigint NOT NULL,
     `first_name` varchar(150) NOT NULL,
     `last_name` varchar(150)  DEFAULT NULL,
     `user_name` varchar(50)  NOT NULL,
     `password` varchar(150) NOT NULL,
     `access_token` varchar(500) DEFAULT NULL,
     `refresh_token` varchar(500) DEFAULT NULL,
     `last_access` timestamp NULL DEFAULT NULL,
     `created_by` varchar(50) DEFAULT NULL,
     `created_date` timestamp NULL DEFAULT NULL,
     `updated_by` varchar(50)DEFAULT NULL,
     `updated_date` timestamp NULL DEFAULT NULL,
     PRIMARY KEY (`id`),
     UNIQUE KEY `users_unique` (`user_name`)
) ENGINE=InnoDB;