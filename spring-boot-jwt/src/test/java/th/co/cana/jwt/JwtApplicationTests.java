package th.co.cana.jwt;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

import th.co.cana.jwt.utils.JwtUtils;
import th.co.cana.jwt.utils.JwtUtils.SignInfo;
import th.co.cana.jwt.utils.JwtUtils.VerifyResult;
import th.co.cana.utils.Utils;

@SpringBootTest
public class JwtApplicationTests {
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Test
	public void testVerifyToken() {
		SignInfo info = this.createSignInfo();		
		String token = JwtUtils.sign(info);
		logger.info("Token genrate : {}", token);
		
		try {
			//Thread.sleep(1 * 60 * 1000L);
		} catch (Exception e) {

		}
		
		VerifyResult result = JwtUtils.verify(token);
		logger.info("Token verify : {}", result);
		
		assertTrue(result.isSuccess());
	}
	
	private SignInfo createSignInfo() {
		SignInfo info = new SignInfo(); 
		info.setIssuer("spring-jwt");
		info.setSubject("admin");
		info.setExpMinute(3);
		info.setId(Utils.getUUID());
		
		return info;
	}
}
