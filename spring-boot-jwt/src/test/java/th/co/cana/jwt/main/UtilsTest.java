/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.jwt.main;

import java.time.Instant;
import java.util.Date;

import th.co.cana.jwt.utils.JwtUtils;
import th.co.cana.jwt.utils.JwtUtils.VerifyResult;

/**
* @author supot
* @version 1.0
*/
public class UtilsTest {

	public static void main(String[] args) {
		Date date = new Date();
		System.out.println(date.getTime());
		System.out.println(date);
		
		date = new Date(1593071752L);
		System.out.println(date);
		date = new Date(1593071872L);
		System.out.println(date);
		
		System.out.println(Instant.now().toEpochMilli());
		
		String token = "eyJhbGciOiJIUzUxMiJ9.eyJqdGkiOiJGMUY0OUMwNS1FMjMyLTQwQzctOTZCMC0zMzYwREYyNjg5NzIiLCJpYXQiOjE1OTMwNzIxMjMsInN1YiI6ImFkbWluIiwiaXNzIjoic3ByaW5nLWp3dCIsImV4cCI6MTU5MzA3MjMwM30.aDccK8KtQ7yfOOzDrFJmscGFgbAztHwQkZaksb-8tz6EOLaFlOXop6B7nSiKLU-SKeMuX1SMpoUhdGT6BK3VmA";
		VerifyResult result = JwtUtils.verify(token);
		System.out.println(result);
	}
}
