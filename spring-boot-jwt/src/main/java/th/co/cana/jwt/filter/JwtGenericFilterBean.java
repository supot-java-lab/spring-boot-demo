/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.jwt.filter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import lombok.extern.slf4j.Slf4j;
import th.co.cana.jwt.utils.JwtUtils;
import th.co.cana.utils.Validators;

/**
 * @author supot
 * @version 1.0
 */

@Slf4j
public class JwtGenericFilterBean extends GenericFilterBean {
	
	private List<String> skipUrls;
	
	public JwtGenericFilterBean() {
		this.skipUrls = new ArrayList<>(0);
	}
	
	public JwtGenericFilterBean(List<String> skipUrls) {
		this.skipUrls = skipUrls;
	}
	
	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
			throws IOException, ServletException {

		log.info("Starting filter");
		HttpServletRequest httpReq = (HttpServletRequest) req;
		
		//Step 1. Validate URL pattern
		String reqURI = JwtUtils.getPath(httpReq);
		log.debug("Request path : {}", reqURI);
		if (isSkipUrl(reqURI)) {
			chain.doFilter(req, resp);
			return;
		}
		
		Authentication authen = JwtUtils.getAuthentication(httpReq);		
		SecurityContextHolder.getContext().setAuthentication(authen);
		chain.doFilter(req, resp);
	}

	private boolean isSkipUrl(String url) {
		if (Validators.isEmpty(skipUrls) || Validators.isEmpty(url)) {
			return false;
		}

		for (String str : skipUrls) {
			if (Validators.isEmpty(str)) {
				continue;
			}

			if (url.contains(str)) {
				return true;
			}
		}

		return false;
	}
}
