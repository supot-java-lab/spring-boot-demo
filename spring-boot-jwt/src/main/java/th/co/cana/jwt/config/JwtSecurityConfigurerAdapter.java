/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.jwt.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import th.co.cana.jwt.filter.JwtAuthenticationProcessingFilter;
import th.co.cana.jwt.filter.JwtGenericFilterBean;

/**
 * @author supot
 * @version 1.0
 */

@Configuration
public class JwtSecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication()
			.withUser("admin")
			.password(passwordEncoder().encode("password"))
			.authorities("ADMIN");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {	
	    http.csrf().disable()
	    	.authorizeRequests()
	        .antMatchers("/").permitAll()
	        .antMatchers(HttpMethod.POST, "/jwt/v1/authen").permitAll()
	        .anyRequest().authenticated()
	        .and()
	        .addFilterBefore(authenProcessingFilter(), UsernamePasswordAuthenticationFilter.class)
	        .addFilterBefore(authenFilterBean(), UsernamePasswordAuthenticationFilter.class);
	}
	
	private JwtAuthenticationProcessingFilter authenProcessingFilter() throws Exception {
		return new JwtAuthenticationProcessingFilter("/jwt/v1/authen", authenticationManager());
	}
	
	private JwtGenericFilterBean authenFilterBean() {
		return new JwtGenericFilterBean();
	}
}
