/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.jwt.model;

import java.io.Serializable;

import lombok.Data;

/**
* @author supot
* @version 1.0
*/

@Data
public class UserAuthen implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String username;
    private String password;
}
