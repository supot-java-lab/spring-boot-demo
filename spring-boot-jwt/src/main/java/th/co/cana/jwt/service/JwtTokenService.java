/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.jwt.service;

import java.util.Collections;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import th.co.cana.jwt.utils.JwtUtils;
import th.co.cana.jwt.utils.JwtUtils.VerifyResult;
import th.co.cana.utils.Validators;

/**
 * @author supot
 * @version 1.0
 */

@Slf4j
@Component
public class JwtTokenService {
	public Authentication getAuthentication(HttpServletRequest req) {
		String token = JwtUtils.getToken(req);
		log.info("Token parameter : {}", token);
		if (Validators.isEmpty(token)) {
			return null;
		}

		VerifyResult result = JwtUtils.verify(token);
		log.info("Verify result : {}", result);
		if (result.isSuccess()) {
			return new UsernamePasswordAuthenticationToken(result.getIssuer(), null, Collections.emptyList());
		}

		return null;
	}
}
