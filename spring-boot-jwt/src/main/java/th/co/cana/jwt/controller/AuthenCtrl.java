/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.jwt.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import th.co.cana.jwt.model.UserAuthen;

/**
* @author supot
* @version 1.0
*/

@RestController
@RequestMapping("/jwt/v1/authen")
public class AuthenCtrl extends Controller {

	@PostMapping
	public void login(@RequestBody UserAuthen user) {
		logger.info("{}", user);
	}
}
