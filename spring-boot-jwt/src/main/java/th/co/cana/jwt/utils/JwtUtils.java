/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by PTG Energy Public Company Limited. All rights reserved.
* Project Name : SemCommRest
* -----------------------------------------------------------------------------------
*/
package th.co.cana.jwt.utils;

import java.io.Serializable;
import java.security.Key;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.Date;

import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.SignatureException;
import th.co.cana.utils.Utils;
import th.co.cana.utils.Validators;

/**
* @author supot
* @version 1.0
*/
public class JwtUtils {
	private static final byte[] API_KEY 		= "4DEFF15F-78F3-49C3-8B7C-08A689301068-98098983-34202-SWERD-ERTT09".getBytes();
	private static final String TOKEN_HEADER 	= "Authorization";
	private static final String TOKEN_PARAM 	= "token";
	private static final String TOKEN_PREFIX 	= "Bearer";
	private static final Key SECRET_KEY 		= new SecretKeySpec(API_KEY, SignatureAlgorithm.HS512.getJcaName());
	
	private static Logger logger = LoggerFactory.getLogger(JwtUtils.class);
	
	private JwtUtils() {}

	public static String sign(SignInfo info) {
		logger.info("Sign api with : {}", info);
		
	    Instant issuedAt = Instant.now().truncatedTo(ChronoUnit.SECONDS);
		// Let's set the JWT Claims
		JwtBuilder builder = Jwts.builder()
				.setId(info.getId())
				.setIssuedAt(Date.from(issuedAt))
				.setSubject(info.getSubject())
				.setIssuer(info.getIssuer())
				.signWith(SECRET_KEY);

		// if it has been specified, let's add the expiration
		if (info.getExpMinute() > 0) {
			logger.info("Starting time : {}", issuedAt);
			Instant exp = issuedAt.plus(info.getExpMinute(), ChronoUnit.MINUTES);			
			logger.info("Expiration time : {}", exp);
			builder.setExpiration(Date.from(exp));
		}
		
		return builder.compact();
	}
	
	public static VerifyResult verify(String token) {
		VerifyResult result = new VerifyResult();
		try {
			Jws<Claims> claim = Jwts.parserBuilder()
					.setSigningKey(SECRET_KEY).build()
					.parseClaimsJws(token);
			logger.info("{}", claim);

			if (Validators.isNotNull(claim)) {
				result.setIssuer(claim.getBody().getIssuer());
				result.setSubject(claim.getBody().getSubject());
				result.setMessage("Verify token success.");
			}
		} catch (ExpiredJwtException ex) {
			result.expired(ex.getMessage());
		} catch (SignatureException ex) {
			result.fail(ex.getMessage());
		} catch (Exception ex) {
			result.error(ex.getMessage());
		}

		return result;
	}
		
	public static String getToken(HttpServletRequest req) {
		String token = req.getHeader(TOKEN_HEADER);
		if (token != null && token.startsWith("Bearer ")) {
			return token.substring(7, token.length());
		} else {
			token = req.getParameter(TOKEN_PARAM);
		}
		return token;
	}
	
	public static Authentication getAuthentication(HttpServletRequest req) {
		String token = JwtUtils.getToken(req);
		logger.info("Token parameter : {}", token);
		if (Validators.isEmpty(token)) {
			return null;
		}

		VerifyResult result = JwtUtils.verify(token);
		logger.info("Verify result : {}", result);
		if (result.isSuccess()) {
			return new UsernamePasswordAuthenticationToken(result.getIssuer(), null, Collections.emptyList());
		}

		return null;
	}
	
	public static void addAuthentication(HttpServletResponse res, String userName) {
		SignInfo sigInfo = new SignInfo();
		sigInfo.setId(Utils.getUUID());
		sigInfo.setIssuer("Spring-Security");
		sigInfo.setSubject(userName);
		sigInfo.setExpMinute(30);
		
		String token = sign(sigInfo);
		logger.info("Token : {}", token);
		
		res.addHeader(TOKEN_HEADER, TOKEN_PREFIX + " " + token);
	}
	
	public static String getPath(HttpServletRequest httpReq) {
		return httpReq.getRequestURI().substring(httpReq.getContextPath().length());
	}
	
	public static final class SignInfo implements Serializable {
		private static final long serialVersionUID = 1L;
		
		private String id;
		private String issuer;
		private String subject;
		private int expMinute;
		
		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getIssuer() {
			return issuer;
		}

		public void setIssuer(String issuer) {
			this.issuer = issuer;
		}

		public String getSubject() {
			return subject;
		}

		public void setSubject(String subject) {
			this.subject = subject;
		}

		public int getExpMinute() {
			return expMinute;
		}

		public void setExpMinute(int expMinute) {
			this.expMinute = expMinute;
		}

		@Override
		public String toString() {
			return "JWTInfo [id=" + id + ", issuer=" + issuer + ", subject=" + subject + ", expMinute=" + expMinute
					+ "]";
		}
		
	}
	
	public static final class VerifyResult implements Serializable {
		private static final long serialVersionUID = 1L;
		private static final String SUCCESS = "00";
		private static final String EXPIRED = "01";
		private static final String FAIL = "02";
		private static final String ERROR = "99";
		
		private String code;
		private String message;
		private String desc;
		private String issuer;
		private String subject ;
		
		public VerifyResult() {
			super();
			this.code = SUCCESS;
		}
		
		public VerifyResult(String code) {
			super();
			this.code = code;
		}
		
		public VerifyResult(String code, String message) {
			super();
			this.code = code;
			this.message = message;
		}
		
		public VerifyResult(String code, String message, String desc) {
			super();
			this.code = code;
			this.message = message;
			this.desc = desc;
		}

		public String getCode() {
			return code;
		}

		public void setCode(String code) {
			this.code = code;
		}

		public String getMessage() {
			return message;
		}

		public void setMessage(String message) {
			this.message = message;
		}

		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}
		
		public String getIssuer() {
			return issuer;
		}

		public void setIssuer(String issuer) {
			this.issuer = issuer;
		}

		public String getSubject() {
			return subject;
		}

		public void setSubject(String subject) {
			this.subject = subject;
		}

		public VerifyResult expired(String msg) {
			this.code = EXPIRED;
			this.message = "Verify token was expired.";
			this.desc = msg;
			return this;
		}
		
		public VerifyResult fail(String msg) {
			this.code = FAIL;
			this.message = "Verify token was fail.";
			this.desc = msg;
			return this;
		}
		
		public VerifyResult error(String msg) {
			this.code = ERROR;
			this.message = "Verify token was error.";
			this.desc = msg;
			return this;
		}
		
		public boolean isSuccess() {
			return SUCCESS.equals(code);
		}

		public boolean isExpired() {
			return EXPIRED.equals(code);
		}
		
		public boolean isError() {
			return (ERROR.equals(code) || FAIL.equals(code));
		}
		
		@Override
		public String toString() {
			return "VerifyResult [code=" + code + ", message=" + message + ", desc=" + desc + ", success="
					+ isSuccess() + "]";
		}
		
	}
}
