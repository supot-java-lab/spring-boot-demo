/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.jwt.utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import th.co.cana.jwt.model.CustomerModel;
import th.co.cana.utils.Convertors;
import th.co.cana.utils.Validators;

/**
 * @author supot
 * @version 1.0
 */
public final class ServiceUtils {
	private ServiceUtils() {

	}

	private static List<CustomerModel> customers;

	public static List<CustomerModel> getCustomers() {
		initialCustomer();
		return customers;
	}

	public static CustomerModel getCustomer(String id) {
		if (Validators.isEmpty(id)) {
			return null;
		}

		initialCustomer();
		for (CustomerModel cust : customers) {
			if (id.equalsIgnoreCase(cust.getId())) {
				return cust;
			}
		}

		return null;
	}

	private static void initialCustomer() {
		if (Validators.isNotEmpty(customers)) {
			return;
		}

		customers = new ArrayList<>();
		for (int i = 1; i <= 10; i++) {
			CustomerModel cust = new CustomerModel();
			cust.setId(Convertors.toString(i));
			cust.setFirstName("Firt Name " + i);
			cust.setLastName("Last Name " + i);
			cust.setOraderAmt(BigDecimal.valueOf(i * 2500.45D));

			customers.add(cust);
		}
	}
}
