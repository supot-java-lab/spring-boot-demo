/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.jwt.filter;

import java.io.IOException;
import java.util.Collections;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.fasterxml.jackson.databind.ObjectMapper;

import th.co.cana.jwt.model.UserAuthen;
import th.co.cana.jwt.utils.JwtUtils;

/**
 * @author supot
 * @version 1.0
 */
public class JwtAuthenticationProcessingFilter extends AbstractAuthenticationProcessingFilter {
	
	public JwtAuthenticationProcessingFilter(String url, AuthenticationManager manager) {
		super(new AntPathRequestMatcher(url));
		setAuthenticationManager(manager);
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		UserAuthen user = new ObjectMapper().readValue(request.getInputStream(), UserAuthen.class);
		UsernamePasswordAuthenticationToken authenToken = new UsernamePasswordAuthenticationToken(
				user.getUsername(), user.getPassword(), Collections.emptyList());
		
		return getAuthenticationManager().authenticate(authenToken);
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, 
			FilterChain chain, Authentication authResult) throws IOException, ServletException {
		JwtUtils.addAuthentication(response, authResult.getName());
	}

}
