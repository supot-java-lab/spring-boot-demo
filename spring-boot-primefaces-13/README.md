# Job Monitoring
Demo JSF 13 project for Spring Boot

# Getting Started
### Development Tools
* [Install IntelliJ IDEA](https://www.jetbrains.com/)
* [JavaServer Faces](https://www.oracle.com/java/technologies/javaserverfaces.html)
* [Primefaces](https://primefaces.org/)
* [Spring Boot](https://spring.io/projects/spring-boot)

### Docker build
* docker build --platform=linux/amd64 -t canadev/spring-boot-app:spring-boot-primeface-13-2023.12-alpine .
* docker login -u canadev
* docker push canadev/spring-boot-app:spring-boot-primeface-13-2023.12-alpine

### K8 Command
*  kubectl apply -f deployment/deployment.yaml
* kubectl get pods
* kubectl get services
* kubectl get deployment
* kubectl get replicaset
* kubectl describe pod job-portal
* kubectl port-forward deployment/job-portal 8080:8080
* kubectl port-forward service/job-portal 8080:8080
* kubectl delete service
* kubectl logs <pod_name>