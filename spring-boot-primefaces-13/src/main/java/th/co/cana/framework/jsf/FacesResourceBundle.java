package th.co.cana.framework.jsf;

import io.github.jdevlibs.faces.resource.ClassPathResourceBundle;

import java.util.ArrayList;
import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class FacesResourceBundle extends ClassPathResourceBundle {
    private static final List<String> BUNDLE_NAMES;

    static {
        String packageName = FacesResourceBundle.class.getPackageName();
        packageName = packageName.replace('.', '/') + "/message/";
        BUNDLE_NAMES = new ArrayList<>(10);
        BUNDLE_NAMES.add(packageName + "JSFMessages");
        BUNDLE_NAMES.add(packageName + "CommonMessage");
        BUNDLE_NAMES.add(packageName + "ErrorMessage");
    }

    public FacesResourceBundle() {
        super(BUNDLE_NAMES);
    }
}