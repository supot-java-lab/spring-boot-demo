package th.co.cana.framework.controller;

import jakarta.annotation.PostConstruct;
import jakarta.faces.view.ViewScoped;
import jakarta.inject.Named;
import lombok.Data;
import lombok.EqualsAndHashCode;
import th.co.cana.framework.models.DemoBean;

/**
 * @author supot.jdev
 * @version 1.0
 */


@EqualsAndHashCode(callSuper = true)
@Data
@Named
@ViewScoped
public class DemoCtrl extends WebController {

    private DemoBean form;

    @PostConstruct
    public void init() {
        logger.info("Starting controller: {}", getClass().getName());
        form = new DemoBean();
    }

    public void clickAction() {
        logger.info("Starting click : {}", form);
    }
}
