package th.co.cana.framework.models;

import lombok.Data;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
public class DataTableFrozen implements Serializable {

    private int frozenColumnWidth;
    private int totalColumnWidth;
    private int frozenColumn;
    private Map<String, Integer> columnWidths;

    public void addColumnWidth(String column, int width) {
        if (column == null || column.isEmpty()) {
            return;
        }
        if (columnWidths == null) {
            columnWidths = new LinkedHashMap<>();
        }
        columnWidths.put(column.toLowerCase(), width);
    }

    public int getColumnWidth(String column) {
        if (columnWidths == null || column == null || column.isEmpty()) {
            return 0;
        }

        Integer width = columnWidths.get(column.toLowerCase());
        return width == null ? 0 : width;
    }

    public void calculateAllColumnWidth() {
        if (columnWidths == null || columnWidths.isEmpty()) {
            totalColumnWidth = 0;
            return;
        }
        totalColumnWidth = columnWidths.values()
                .stream()
                .mapToInt(Integer::valueOf)
                .sum();
    }

    public void calculateFrozenColumnWidth(String ... cols) {
        if (columnWidths == null || columnWidths.isEmpty() || cols == null || cols.length == 0) {
            frozenColumnWidth = 0;
            return;
        }

        frozenColumnWidth = 0;
        for (String col : cols) {
            frozenColumnWidth += getColumnWidth(col);
        }
    }
}
