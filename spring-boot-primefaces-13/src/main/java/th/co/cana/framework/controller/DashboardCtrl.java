package th.co.cana.framework.controller;

import jakarta.annotation.PostConstruct;
import jakarta.faces.view.ViewScoped;
import jakarta.inject.Named;

/**
 * @author supot.jdev
 * @version 1.0
 */

@Named
@ViewScoped
public class DashboardCtrl extends WebController {
    @PostConstruct
    public void init() {
        logger.info("Starting controller: {}", getClass().getName());
    }
}
