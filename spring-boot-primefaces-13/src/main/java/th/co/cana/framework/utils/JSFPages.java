package th.co.cana.framework.utils;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class JSFPages {
    private JSFPages() {
    }

    public static final String URL_LOGIN        = "/login";
    public static final String URL_ERROR_401	= "/error/401";
    public static final String URL_ERROR_404	= "/error/404";
    public static final String URL_ERROR_500	= "/error/500";

    public static final String PAGE_LOGIN 		= "/login.xhtml";
    public static final String PAGE_DASHBOARD   = "/dashboard.xhtml";

    public static final String PAGE_ERROR_401 		= "/views/error/401.xhtml";
    public static final String PAGE_ERROR_404 		= "/views/error/404.xhtml";
    public static final String PAGE_ERROR_500		= "/views/error/500.xhtml";


    private static final Map<String, String> MENU_MAPPINGS;
    static {
        MENU_MAPPINGS = new LinkedHashMap<>();
        MENU_MAPPINGS.put("/", PAGE_DASHBOARD);
        MENU_MAPPINGS.put("/login", PAGE_LOGIN);
        MENU_MAPPINGS.put("/dashboard", PAGE_DASHBOARD);

        //Popup

        //Error
        MENU_MAPPINGS.put(URL_ERROR_401, PAGE_ERROR_401);
        MENU_MAPPINGS.put(URL_ERROR_404, PAGE_ERROR_404);
        MENU_MAPPINGS.put(URL_ERROR_500, PAGE_ERROR_500);
    }

    public static final class Popups {
        private Popups() {
        }
    }

    public static Map<String, String> getMenuMappings() {
        return Collections.unmodifiableMap(MENU_MAPPINGS);
    }

    public static String getMenuPath(String url) {
        return MENU_MAPPINGS.get(url);
    }
}
