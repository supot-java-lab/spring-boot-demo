package th.co.cana.framework.exception;

import th.co.cana.framework.enums.ErrorCodes;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class ServiceException extends RuntimeException {
    private final ErrorCodes errorCode;

    public ServiceException(Throwable cause) {
        super(cause);
        this.errorCode = ErrorCodes.CODE_DB_OTHER;
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
        this.errorCode = ErrorCodes.CODE_DB_OTHER;
    }

    public ServiceException(ErrorCodes messageCode, String message) {
        super(message);
        this.errorCode = messageCode;
    }

    public ServiceException(ErrorCodes errorCode, Throwable cause) {
        super(cause);
        this.errorCode = errorCode;
    }

    public ServiceException(ErrorCodes errorCode, String message, Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    public ErrorCodes getErrorCode() {
        return errorCode;
    }

    public String getMessageCode() {
        if (errorCode == null) {
            return null;
        }

        return errorCode.getMessage();
    }
}