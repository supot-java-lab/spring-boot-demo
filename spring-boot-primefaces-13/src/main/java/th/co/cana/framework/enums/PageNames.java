package th.co.cana.framework.enums;

import th.co.cana.framework.utils.JSFPages;

/**
 * @author supot.jdev
 * @version 1.0
 */
public enum PageNames {
    HOME("1000", JSFPages.PAGE_DASHBOARD, "home.view.name");

    private final String code;
    private final String path;
    private final String title;

    PageNames(String code, String path, String title) {
        this.code = code;
        this.path = path;
        this.title = title;
    }

    public String getPath() {
        return path;
    }

    public String getCode() {
        return code;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return this.path;
    }
}
