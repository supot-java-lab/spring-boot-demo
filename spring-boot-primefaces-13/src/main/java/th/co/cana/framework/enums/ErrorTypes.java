package th.co.cana.framework.enums;

/**
 * @author supot.jdev
 * @version 1.0
 */
public enum ErrorTypes {
    ERROR("font-error", "msg.popup.title.error", "fa fa-times-circle"),
    WARNING("font-warning", "msg.popup.title.warning", "fa fa-exclamation-triangle"),
    INFO("font-blue", "msg.popup.title.info", "fa fa-info-circle");

    private final String styleClass;
    private final String header;
    private final String iconCss;

    ErrorTypes(String styleClass, String header, String iconCss) {
        this.styleClass = styleClass;
        this.header = header;
        this.iconCss = iconCss;
    }

    public String getStyleClass() {
        return styleClass;
    }

    public String getHeader() {
        return header;
    }

    public String getIconCss() {
        return iconCss;
    }

}