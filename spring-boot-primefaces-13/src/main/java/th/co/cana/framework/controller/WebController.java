package th.co.cana.framework.controller;

import io.github.jdevlibs.faces.FacesUtils;
import io.github.jdevlibs.primefaces.PFUtils;
import io.github.jdevlibs.utils.Validators;
import jakarta.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StopWatch;
import th.co.cana.framework.enums.ErrorTypes;
import th.co.cana.framework.exception.ServiceException;
import th.co.cana.framework.jsf.bean.UserInfo;
import th.co.cana.framework.jsf.bean.WebPreferences;
import th.co.cana.framework.models.DataTableFrozen;
import th.co.cana.framework.utils.MessageUtils;
import th.co.cana.framework.utils.ResourceBundles;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * @author supot.jdev
 * @version 1.0
 */
public abstract class WebController implements Serializable {
    private static final String ERROR_DIALOG = "messageDialog";
    private static final String PAGING_TEMPLATE = "{RowsPerPageDropdown} {FirstPageLink} {PreviousPageLink} {CurrentPageReport} {NextPageLink} {LastPageLink}";
    private static final String ROW_PER_PAGE = "10, 20, 30, 50, 100, 150, 200";

    protected transient Logger logger = LoggerFactory.getLogger(getClass());

    //Inject service & bean
    @Inject
    private WebPreferences webPreferences;
    @Inject
    private UserInfo userInfo;

    //Variable
    private DataTableFrozen tableFrozen;
    private String scrollWidth;
    private String scrollHeight;
    private Map<String, Boolean> statusLoadings;

    public WebPreferences getWebPreferences() {
        return webPreferences;
    }

    public void setWebPreferences(WebPreferences webPreferences) {
        this.webPreferences = webPreferences;
    }

    public UserInfo getUserInfo() {
        return userInfo;
    }

    public void setUserInfo(UserInfo userInfo) {
        this.userInfo = userInfo;
    }

    public String getPagingTemplate() {
        return PAGING_TEMPLATE;
    }

    public String getRowPerPage() {
        return ROW_PER_PAGE;
    }

    public LocalDate getCurrentDate() {
        return LocalDate.now();
    }

    public Locale getLocale() {
        if (Validators.isNotNull(webPreferences.getLocale())) {
            return webPreferences.getLocale();
        }

        return FacesUtils.getLocale();
    }

    public void setStatusLoadings(Map<String, Boolean> statusLoadings) {
        this.statusLoadings = statusLoadings;
    }

    public Map<String, Boolean> getStatusLoadings() {
        if (Validators.isNull(statusLoadings)) {
            statusLoadings = new HashMap<>();
        }
        return statusLoadings;
    }

    public void addStatusLoading(String name, boolean status) {
        logger.info("Set status of : {} -> {}", name, status);
        getStatusLoadings().put(name, status);
    }

    public boolean isLoadedSuccess(String name) {
        Boolean value = getStatusLoadings().get(name);
        return (value != null && value);
    }

    /* ++++++++++++++++++++++++ Utility Method +++++++++++++++++++++++ */
    public String redirectToOutcome(String outcome) {
        return FacesUtils.redirectToOutcome(outcome);
    }

    public void redirectToPage(String page) {
        FacesUtils.redirectToPage(page);
    }

    public void redirectToUrl(String url) {
        try {
            FacesUtils.redirectToUrl(url);
        } catch (Exception ex) {
            logger.error("redirectToUrl[Error]", ex);
        }
    }

    public String getUsername() {
        if (Validators.isNull(userInfo)) {
            return null;
        }

        return userInfo.getUsername();
    }

    public void setActiveScreenProperty(int screenWidth, int screenHeight) {
        try {
            logger.info("screenWidth: {}, screenHeight: {}", screenWidth, screenHeight);
            getWebPreferences().setScreenWidth(screenWidth);
            getWebPreferences().setScreenHeight(screenHeight);
        } catch (Exception ex) {
            //Skip
        }
    }

    public int getScreenWidth() {
        return getWebPreferences().getScreenWidth();
    }

    public int getScreenHeight() {
        return getWebPreferences().getScreenHeight();
    }

    public String getUserFullName() {
        return getUserInfo().getFullName();
    }

    public void addColumnWidth(String column, int width) {
        if (tableFrozen == null) {
            tableFrozen = new DataTableFrozen();
        }
        tableFrozen.addColumnWidth(column, width);
    }

    public int getColumnWidth(String column) {
        return getColumnWidth(column, 0);
    }

    public int getColumnWidth(String column, int defValue) {
        try {
            if (Validators.isNull(tableFrozen)) {
                return defValue;
            }
            int width = tableFrozen.getColumnWidth(column);

            return width > 0 ? width : defValue;
        } catch (Exception e) {
            return defValue;
        }
    }

    public void calculateAllColumnWidth(){

    }

    /* ++++++++++++++++++++++++ Dialog Method +++++++++++++++++++++++ */
    public void showDialogError(String message, String... jsCommand) {
        getWebPreferences().setType(ErrorTypes.ERROR);
        showDialog(message, jsCommand);
    }

    public void showDialogError(List<String> messages, String... jsCommand) {
        getWebPreferences().setType(ErrorTypes.ERROR);
        showDialog(messages, jsCommand);
    }

    public void showDialogError(Exception ex) {
        showDialogError(ex, "");
    }

    public void showDialogError(Exception ex, String... jsCommand) {
        getWebPreferences().setType(ErrorTypes.ERROR);
        showDialog(getMessage(ex), jsCommand);
    }

    public void showDialogWarning(String message, String... jsCommand) {
        getWebPreferences().setType(ErrorTypes.WARNING);
        showDialog(message, jsCommand);
    }

    public void showDialogInfo(String message, String... jsCommand) {
        getWebPreferences().setType(ErrorTypes.INFO);
        showDialog(message, jsCommand);
    }

    private void showDialog(List<String> messages, String... jsCommand) {
        try {
            getWebPreferences().setErrors(messages);
            getWebPreferences().addJsCommand(jsCommand);
            if (getWebPreferences().isErrorType()) {
                FacesUtils.validationFailed();
            }
            PFUtils.showDialog(ERROR_DIALOG, ERROR_DIALOG);
        } catch (Exception ex) {
            logger.error("showDialog", ex);
        }
    }

    private void showDialog(String message, String... jsCommand) {
        try {
            getWebPreferences().add(message);
            getWebPreferences().addJsCommand(jsCommand);
            if (getWebPreferences().isErrorType()) {
                FacesUtils.validationFailed();
            }

            PFUtils.showDialog(ERROR_DIALOG, ERROR_DIALOG);
        } catch (Exception ex) {
            logger.error("showDialog", ex);
        }
    }

    public String getMessage(Throwable ex) {
        if (ex instanceof ServiceException exception) {
            return getServiceError(exception);
        } else {
            return MessageUtils.getValue("error.exception.other");
        }
    }

    public String getMessage(String key, Object... value) {
        return ResourceBundles.getMessage(key, value);
    }

    public String getDatePattern() {
        return "dd/MM/yyyy";
    }

    public String getDateTimePattern() {
        return "dd/MM/yyyy HH:mm:ss";
    }

    public DataTableFrozen getTableFrozen() {
        return tableFrozen;
    }

    public void setTableFrozen(DataTableFrozen tableFrozen) {
        this.tableFrozen = tableFrozen;
    }

    public String getScrollWidth() {
        return scrollWidth;
    }

    public void setScrollWidth(String scrollWidth) {
        this.scrollWidth = scrollWidth;
    }

    public String getScrollHeight() {
        return scrollHeight;
    }

    public void setScrollHeight(String scrollHeight) {
        this.scrollHeight = scrollHeight;
    }

    public int getFrozenColumns() {
        return getTableFrozen().getFrozenColumn();
    }

    public void logPerformance(StopWatch stopWatch) {
        try {
            logger.info("Performance execution time : {} ms",
                    stopWatch.getTotalTimeMillis());
        } catch (Exception ex) {
            //Ignore
        }
    }

    private String getServiceError(ServiceException ex) {
        String error = MessageUtils.getValue("error.exception.other");
        if (Validators.isNotEmpty(ex.getMessageCode())) {
            error = MessageUtils.getValue(ex.getMessageCode());
        }
        return error;
    }
}
