package th.co.cana.framework.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author supot.jdev
 * @version 1.0
 */

@Data
@Configuration
@ConfigurationProperties(prefix = "system.conf")
public class SystemConfigProperties {

    private boolean performanceAuditEnabled;
    private boolean disabledFilter;
    private String logLevel;
}