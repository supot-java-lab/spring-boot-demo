package th.co.cana.framework.utils;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class StartupUtils {
    public static void initial() {
        setDefaultProperties();
    }

    public static void setDefaultProperties() {
        System.setProperty("file.encoding", AppConstants.UTF8);
        System.setProperty("sun.jnu.encoding", AppConstants.UTF8);
        System.setProperty("sun.stdout.encoding", AppConstants.UTF8);
        System.setProperty("sun.stderr.encoding", AppConstants.UTF8);
    }
}
