package th.co.cana.framework.controller;

import io.github.jdevlibs.utils.IOUtils;
import jakarta.annotation.PostConstruct;
import jakarta.faces.view.ViewScoped;
import jakarta.inject.Named;
import lombok.Data;
import lombok.EqualsAndHashCode;
import th.co.cana.framework.utils.AppConstants;

import java.io.FileInputStream;

/**
 * @author supot.jdev
 * @version 1.0
 */

@EqualsAndHashCode(callSuper = false, of = {"id"})
@Data
@Named
@ViewScoped
public class SvgCtrl extends WebController {
    private String id = "SvgCtrl";

    private String svgData;

    @PostConstruct
    public void init() {
        logger.info("Starting controller: {}", getClass().getName());
        this.initialSvg();
    }

    private void initialSvg() {
        try {
            String svgFile = AppConstants.getSvgDir() + "/infographic.svg";
            svgData = IOUtils.toString(new FileInputStream(svgFile), AppConstants.UTF8);
        } catch (Exception ex) {
            logger.error("initialSvg", ex);
        }
    }
}
