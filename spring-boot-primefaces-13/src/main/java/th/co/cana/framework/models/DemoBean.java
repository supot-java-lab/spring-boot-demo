package th.co.cana.framework.models;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
public class DemoBean implements Serializable {

    private LocalDate date;
    private String name;
    private BigDecimal amount;
}
