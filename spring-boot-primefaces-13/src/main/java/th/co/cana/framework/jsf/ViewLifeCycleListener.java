package th.co.cana.framework.jsf;

import io.github.jdevlibs.utils.Systems;
import jakarta.faces.event.PhaseEvent;
import jakarta.faces.event.PhaseId;
import jakarta.faces.event.PhaseListener;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StopWatch;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Slf4j
public class ViewLifeCycleListener implements PhaseListener {
    private final transient StopWatch stopWatch = new StopWatch();

    @Override
    public void beforePhase(PhaseEvent event) {
        try {
            if (event.getPhaseId() == PhaseId.RESTORE_VIEW) {
                if (stopWatch.isRunning()) {
                    stopWatch.stop();
                }
                stopWatch.start();
            }
        } catch (Exception ex) {
            //Skip error
        }
    }

    @Override
    public void afterPhase(PhaseEvent event) {
        try {
            log.info("Executed Phase {}", event.getPhaseId());
            if (event.getPhaseId() == PhaseId.RENDER_RESPONSE) {
                if (stopWatch.isRunning()) {
                    stopWatch.stop();
                }

                String viewId = event.getFacesContext().getViewRoot().getViewId();

                Object[] params = new Object[] { viewId, stopWatch.toString(), Systems.getUsedMemory() };
                log.info("View ID : {} Execution Time : {}. Memory used : {} MB", params);
            }
        } catch (Exception ex) {
            //Skip error
        }
    }

    @Override
    public PhaseId getPhaseId() {
        return PhaseId.ANY_PHASE;
    }
}
