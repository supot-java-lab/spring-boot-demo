package th.co.cana.framework.enums;

/**
 * @author supot.jdev
 * @version 1.0
 */
public enum ErrorCodes {
    //Database
    CODE_DB_ERROR("000", "error.db.other"),
    CODE_DB_CONNECTION("100", "error.db.connect.fail"),
    CODE_DB_INSERT("101", "error.db.insert"),
    CODE_DB_UPDATE("102", "error.db.update"),
    CODE_DB_DELETE("103", "error.db.delete"),
    CODE_DB_QUERY("104", "error.db.query"),
    CODE_DB_UNIQUE("105", "error.db.unique.constraint"),
    CODE_DB_EMPTY("106", "error.db.empty.result"),
    CODE_DB_LARGER("107", "error.db.value.larger.column"),
    CODE_DB_NULL("108", "error.db.value.null.column"),
    CODE_DB_OTHER("109", "error.db.other"),

    //API Response
    CODE_200("200", "resp.error.200"),
    CODE_204("204", "resp.error.204"),
    CODE_400("400", "resp.error.400"),
    CODE_401("401", "resp.error.401"),
    CODE_404("404", "resp.error.404"),
    CODE_403("405", "resp.error.405"),
    CODE_406("406", "resp.error.406"),
    CODE_415("415", "resp.error.415"),
    CODE_500("500", "resp.error.500"),

    //External API
    CODE_API_ERROR("600", "resp.error.api.external"),
    CODE_API_UNKNOWN_HOST("601", "resp.error.api.unknown.host"),
    CODE_API_TIMEOUT_CONNECTION("602", "resp.error.api.timeout.connection"),
    CODE_API_TIMEOUT_READ("603", "resp.error.api.timeout.read"),
    CODE_API_TIMEOUT_WRITE("604", "resp.error.api.timeout.write"),

    //Ldap
    CODE_LDAP_CONNECTION("510", "ldap.error.connection"),
    CODE_LDAP_NOT_FOUND("511", "ldap.error.result.empty"),
    CODE_LDAP_ERROR("512", "ldap.error"),
    CODE_LDAP_ERROR_525("525", "ldap.error.525"),
    CODE_LDAP_ERROR_52E("52e", "ldap.error.52e"),
    CODE_LDAP_ERROR_530("530", "ldap.error.530"),
    CODE_LDAP_ERROR_532("532", "ldap.error.532"),
    CODE_LDAP_ERROR_533("533", "ldap.error.533"),
    CODE_LDAP_ERROR_534("534", "ldap.error.534"),
    CODE_LDAP_ERROR_701("701", "ldap.error.701"),
    CODE_LDAP_ERROR_773("773", "ldap.error.773"),
    CODE_LDAP_ERROR_775("775", "ldap.error.775");

    final String code;
    final String message;
    ErrorCodes(String code, String message){
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
