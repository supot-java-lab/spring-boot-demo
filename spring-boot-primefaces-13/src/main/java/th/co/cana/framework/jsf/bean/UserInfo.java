package th.co.cana.framework.jsf.bean;

import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Named;
import lombok.Data;

import java.io.Serializable;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
@Named
@SessionScoped
public class UserInfo implements Serializable {

    private String id;
    private String username;
    private String email;
    private String fullName;
    private String firstName;
    private String lastName;
    private String lastAccess;

    public boolean isUserNotLogin() {
        return (username == null || username.isEmpty());
    }
}