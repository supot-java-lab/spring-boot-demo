package th.co.cana.framework.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author supot.jdev
 * @version 1.0
 */

@Data
@Configuration
@ConfigurationProperties(prefix = "application")
public class ApplicationProperties {
    private String name;
    private String version;
    private String buildTime;
    private String javaBuildVersion;
    private String logLevel;
}
