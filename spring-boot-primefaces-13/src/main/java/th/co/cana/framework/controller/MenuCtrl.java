package th.co.cana.framework.controller;

import io.github.jdevlibs.faces.FacesUtils;
import io.github.jdevlibs.utils.Validators;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Named;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.jboss.weld.contexts.SerializableContextualInstanceImpl;
import th.co.cana.framework.enums.PageNames;
import th.co.cana.framework.utils.JSFPages;
import th.co.cana.framework.utils.ResourceBundles;

import java.io.Serializable;
import java.util.Map;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Slf4j
@Data
@Named
@SessionScoped
public class MenuCtrl implements Serializable {

    private String activeMenu;

    @PostConstruct
    public void initial() {
        log.info("Starting initial controller : {}", getClass());
    }

    public String changePageAction(PageNames page) {
        if (page == null) {
            return null;
        }

        clearSession();
        return redirectPage(page.getPath());
    }

    public String gotoPageAction(String page) {
        if (Validators.isEmpty(page)) {
            return null;
        }

        clearSession();
        return redirectPage(page);
    }

    public String redirectPage(String page) {
        if (Validators.isEmpty(page)) {
            log.warn("Empty page for redirect...");
            return null;
        }

        log.info("Send redirect to view : {}", page);
        return FacesUtils.redirectToOutcome(page);
    }

    public void grantRoleAccess(PageNames page) {
        try {
            if (FacesUtils.isPostback()) {
                return;
            }
            activeMenu = ResourceBundles.getMessage(page.getTitle());
            boolean access = isRendered(page);
            if (!access) {
                log.error("Cannot access this page : {}", page.getPath());
                FacesUtils.redirectToPage(JSFPages.URL_ERROR_401);
            }
        } catch (Exception ex) {
            log.error("grantRoleAccess", ex);
        }
    }

    public boolean isRendered(PageNames page) {
        return page != null;
    }

    public String loginAction() {
        return redirectPage(JSFPages.PAGE_LOGIN);
    }

    public String homeAction() {
        return redirectPage(JSFPages.PAGE_DASHBOARD);
    }

    private void clearSession() {
        try {
            Map<String, Object> sessionMap = FacesUtils.getSessionMap();
            if (Validators.isEmpty(sessionMap)) {
                return;
            }

            log.debug("Total session object : {}", sessionMap.size());
            for (Map.Entry<String, Object> map : sessionMap.entrySet()) {
                log.info("Session Key[{}] -> {}", map.getKey(), map.getValue().getClass());
                if (map.getValue() instanceof SerializableContextualInstanceImpl<?, ?> manageBean) {
                    if (manageBean.getInstance() instanceof WebController) {
                        log.info("\t Session type is Controller[SerializableContextual] remove : {}",
                                manageBean.getInstance().getClass().getName());
                        sessionMap.remove(map.getKey());
                    }
                }
                if (map.getValue() instanceof WebController) {
                    log.info("\t Session type is Controller[Controller] remove : {}", map.getValue().getClass().getName());
                    sessionMap.remove(map.getKey());
                }
            }
        } catch (Exception ex) {
            log.error("clearSession", ex);
        }
    }
}
