package th.co.cana.framework.jsf;


import jakarta.faces.view.ViewDeclarationLanguage;
import jakarta.faces.view.ViewDeclarationLanguageFactory;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class ViewLoggerFactory extends ViewDeclarationLanguageFactory {
    private final ViewDeclarationLanguageFactory wrapped;

    @Deprecated
    public ViewLoggerFactory(ViewDeclarationLanguageFactory wrapped) {
        this.wrapped = wrapped;
    }

    @Override
    public ViewDeclarationLanguage getViewDeclarationLanguage(String viewId) {
        return new ViewLogger(wrapped.getViewDeclarationLanguage(viewId));
    }

    @Override
    public ViewDeclarationLanguageFactory getWrapped() {
        return wrapped;
    }
}
