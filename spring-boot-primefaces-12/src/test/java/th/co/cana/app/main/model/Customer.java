package th.co.cana.app.main.model;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
public class Customer implements Serializable {
    private String customerId;
    private String firstName;
    private String lastName;
    private LocalDate birthDate;
    private BigDecimal creditAmt;
    private LocalDateTime accessDate;
    private Date createDate;
}
