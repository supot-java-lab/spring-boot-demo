package th.co.cana.app.main.main;

import th.co.cana.app.main.model.Customer;
import th.co.cana.app.utils.JsonUtils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class JsonUtilsTest {
    public static void main(String[] args) {
        Customer customer = new Customer();
        customer.setCustomerId("1001");
        customer.setFirstName("Supot");
        customer.setLastName("Saelao");
        customer.setBirthDate(LocalDate.now());
        customer.setCreateDate(new Date());
        customer.setAccessDate(LocalDateTime.now());
        customer.setCreditAmt(BigDecimal.valueOf(150000D));
        String json = JsonUtils.json(customer, true);
        System.out.println(json);
    }
}
