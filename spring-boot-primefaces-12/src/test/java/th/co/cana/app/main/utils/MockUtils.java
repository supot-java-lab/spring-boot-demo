package th.co.cana.app.main.utils;

import io.github.jdevlibs.utils.IOUtils;
import th.co.cana.app.utils.AppConstants;

import java.io.FileInputStream;
import java.io.IOException;

/**
 * @author supot.jdev
 * @version 1.0
 */
public final class MockUtils {
    private MockUtils() {

    }

    public static String readFile(String path) {
        try {
            return IOUtils.toString(new FileInputStream(path), AppConstants.UTF8);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return null;
    }
}
