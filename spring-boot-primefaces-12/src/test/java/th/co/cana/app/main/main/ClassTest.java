package th.co.cana.app.main.main;

import th.co.cana.app.resource.FacesResourceBundle;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class ClassTest {
    public static void main(String[] args) {
        String packageName = FacesResourceBundle.class.getPackageName();
        System.out.println(packageName);
        packageName = packageName.replace('.', '/');
        System.out.println(packageName);
    }
}
