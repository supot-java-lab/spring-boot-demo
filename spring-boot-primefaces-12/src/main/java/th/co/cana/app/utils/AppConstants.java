package th.co.cana.app.utils;

import java.util.Locale;

/**
 * @author supot.jdev
 * @version 1.0
 */
public final class AppConstants {
    private AppConstants() {}

    public static final String Y = "Y";
    public static final String N = "N";

    public static final String UTF8 = "UTF-8";
    public static final Locale US = Locale.US;
    public static final Locale TH = new Locale("th", "TH");
    public static final String LANG_TH = "TH";
    public static final String LANG_US = "US";
    public static final String HTML_NEW_LINE = "<br/>";
    public static final String HTML_SPACE 	= "&nbsp;";
    public static final String HTML_SPACE_2 = "&ensp;";
    public static final String HTML_SPACE_4 = "&emsp;";

    public static String getWorkDir() {
        String workDir = System.getProperty("user.dir");
        if ("/".equals(workDir)) {
            workDir = "";
            return workDir;
        } else {
            return convertPathToUnix(workDir);
        }
    }

    public static String getJasperDir() {
        return getWorkDir() + "/reports/jasper";
    }

    public static String getJsonDir() {
        return getWorkDir() + "/reports/json";
    }

    public static String getTemplateDir() {
        return getWorkDir() + "/public/resources/template";
    }

    public static String getFileUploadDir() {
        return getWorkDir() + "/public/resources/file-upload";
    }

    public static String getLogDir() {
        return getWorkDir() + "/logs";
    }

    private static String convertPathToUnix(final String path) {
        if (path == null || path.indexOf('\\') == -1) {
            return path;
        }
        return path.replace('\\', '/');
    }
}
