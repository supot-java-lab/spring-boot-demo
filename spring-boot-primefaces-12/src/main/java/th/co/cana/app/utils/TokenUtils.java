package th.co.cana.app.utils;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

/**
 * @author supot.jdev
 * @version 1.0
 */
public final class TokenUtils {
    private static final String AUTHORIZATION_HEADER = "Authorization";

    private TokenUtils() {

    }

    public static String getBearerToken() {
        ServletRequestAttributes requestAttributes = getServletRequestAttributes();
        if (requestAttributes == null) {
            return null;
        }
        return requestAttributes.getRequest().getHeader(AUTHORIZATION_HEADER);
    }

    private static ServletRequestAttributes getServletRequestAttributes() {
        try {
            return (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        } catch (IllegalStateException ex) {
            return null;
        }
    }
}
