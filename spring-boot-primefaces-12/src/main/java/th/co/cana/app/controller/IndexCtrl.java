package th.co.cana.app.controller;

import jakarta.annotation.PostConstruct;
import jakarta.faces.view.ViewScoped;
import jakarta.inject.Named;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

@Named
@ViewScoped
@Slf4j
public class IndexCtrl implements Serializable {
	@PostConstruct
	public void init() {
		log.info("Starting {}", getClass().getName());
	}
	
	public void clickAction() {
		log.info("Starting click");
	}
}
