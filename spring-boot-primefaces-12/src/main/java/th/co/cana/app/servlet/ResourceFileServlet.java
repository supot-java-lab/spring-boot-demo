package th.co.cana.app.servlet;

import io.github.jdevlibs.utils.Validators;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServletRequest;
import org.omnifaces.servlet.FileServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import th.co.cana.app.utils.AppConstants;

import java.io.File;
import java.io.Serial;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Component
@WebServlet(name = "ResourceFileServlet", urlPatterns = { "/resource/*" })
public class ResourceFileServlet extends FileServlet {
    @Serial
    private static final long serialVersionUID = 1L;
    private static final Logger logger = LoggerFactory.getLogger(ResourceFileServlet.class);

    private File folder;

    @Override
    public void init() throws ServletException {
        String uploadPath = AppConstants.getFileUploadDir();
        logger.info("Load folder form : {}", uploadPath);
        if (Validators.isNotEmpty(uploadPath)) {
            folder = new File(uploadPath);
        }

        logger.info("Lookup file on : {}", folder);
    }

    @Override
    protected File getFile(HttpServletRequest request) {
        String pathInfo = request.getPathInfo();
        if (pathInfo == null || pathInfo.isEmpty() || "/".equals(pathInfo)) {
            throw new IllegalArgumentException("Invalid request path information");
        }

        initialPath();

        if (folder == null) {
            throw new IllegalArgumentException("Folder is empty.");
        }

        return new File(folder, pathInfo);
    }

    private void initialPath() {
        if (folder == null) {
            try {
                init();
            } catch (ServletException ex) {
                logger.error("initialPath", ex);
            }
        }
    }
}
