package th.co.cana.app.servlet;

import jakarta.servlet.ServletContext;
import org.ocpsoft.rewrite.annotation.RewriteConfiguration;
import org.ocpsoft.rewrite.config.Configuration;
import org.ocpsoft.rewrite.config.ConfigurationBuilder;
import org.ocpsoft.rewrite.servlet.config.HttpConfigurationProvider;
import org.ocpsoft.rewrite.servlet.config.rule.Join;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import th.co.cana.app.utils.Pages;

import java.util.Map;

/**
 * @author supot.jdev
 * @version 1.0
 */

@RewriteConfiguration
public class RewriteConfigProvider extends HttpConfigurationProvider {

    private static Logger logger = LoggerFactory.getLogger(RewriteConfigProvider.class);

    @Override
    public Configuration getConfiguration(ServletContext context) {
        logger.info("*** Starting mapping .xhtml path to rewrite url ***");

        ConfigurationBuilder builder = ConfigurationBuilder.begin();
        Map<String, String> mappings = Pages.getMenuMappings();
        for (Map.Entry<String, String> map : mappings.entrySet()) {
            logger.info("Mapping path : {} => : {}", map.getValue(), map.getKey());
            builder.addRule(Join.path(map.getKey()).to(map.getValue()));
        }

        logger.info("*** Finished mapping .xhtml path to rewrite url ***");

        return builder;
    }

    @Override
    public int priority() {
        return 10;
    }
}
