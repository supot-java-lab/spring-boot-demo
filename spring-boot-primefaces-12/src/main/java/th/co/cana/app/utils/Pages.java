package th.co.cana.app.utils;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author supot.jdev
 * @version 1.0
 */
public final class Pages {
    private Pages() {
    }

    public static final String PAGE_INDEX 		= "/index.xhtml";
    public static final String PAGE_LOGIN 		= "/login.xhtml";

    private static final Map<String, String> MENU_MAPPINGS;
    static {
        MENU_MAPPINGS = new LinkedHashMap<>();
        MENU_MAPPINGS.put("/", PAGE_INDEX);
        MENU_MAPPINGS.put("/index", PAGE_INDEX);
    }

    public static Map<String, String> getMenuMappings() {
        return Collections.unmodifiableMap(MENU_MAPPINGS);
    }

    public static String getMenuPath(String url) {
        return MENU_MAPPINGS.get(url);
    }
}
