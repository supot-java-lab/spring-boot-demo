package th.co.cana.framework.security.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import th.co.cana.framework.security.models.Customer;
import th.co.cana.framework.security.service.CustomerService;

import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */

@RequiredArgsConstructor
@Slf4j
@RestController
@RequestMapping("/v1/customers")
public class CustomerController {

    private final CustomerService customerService;

    @GetMapping
    public ResponseEntity<List<Customer>> getCustomers() {
        log.info("Staring call get customer API ");
        return ResponseEntity.ok(customerService.createCustomers());
    }

    @PostMapping
    public ResponseEntity<Customer> save(@Valid @RequestBody Customer model) {
        log.info("Staring call save customer API ");
        return ResponseEntity.ok(model);
    }
}
