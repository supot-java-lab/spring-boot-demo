package th.co.cana.framework.security.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import th.co.cana.framework.security.models.Customer;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */
@RequiredArgsConstructor
@Service
public class CustomerService {
    private static final String[] NAMES = {"นางพิชาพัทธ์|สิริเจริญพานิช"
            , "นางสาวจารุณี|บุญสุข"
            , "นาง|สุนทรี|บุญพร้อม"
            , "นางสาวณัฐกฤตา|เงินเย็น"
            , "นางสาวภาวีนี|บุญนัก"};

    public List<Customer> createCustomers() {
        List<Customer> customers = new ArrayList<>();
        for (int i = 0; i < NAMES.length; i++) {
            Customer customer = new Customer();
            customer.setCustomerId((long) i);
            String[] names = NAMES[i].split("[|]");
            customer.setFirstName(names[0]);
            customer.setLastName(names[1]);
            customer.setBirthDate(LocalDate.now().plusYears(i * -2));
            customer.setCreditAmt(BigDecimal.valueOf(i).multiply(BigDecimal.valueOf(100 * i)));
            customers.add(customer);
        }

        return customers;
    }
}