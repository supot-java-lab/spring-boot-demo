package th.co.cana.framework.security.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author supot.jdev
 * @version 1.0
 */

@RestController
@RequestMapping("/public")
public class IndexController {

    @RequestMapping("/index")
    public ResponseEntity<Map<String, Object>> index() {
        Map<String, Object> resp = new HashMap<>();
        resp.put("id", UUID.randomUUID().toString());
        resp.put("timestamp", LocalDateTime.now());
        resp.put("message", "Hello World");
        return ResponseEntity.ok(resp);
    }
}
