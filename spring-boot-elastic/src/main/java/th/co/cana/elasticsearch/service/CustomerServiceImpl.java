/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.elasticsearch.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import th.co.cana.elasticsearch.data.Customer;
import th.co.cana.elasticsearch.repository.CustomerRepository;

/**
* @author supot
* @version 1.0
*/

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerRepository repository;
	
	@Override
	public List<Customer> getCustomers() {
		return repository.findAll().getContent();
	}

	@Override
	public Optional<Customer> getCustomer(String id) {
		return repository.findById(id);
	}

	@Override
	public List<Customer> getCustomerByName(String name) {
		return repository.findByFirstName(name);
	}

	@Override
	public List<Customer> getCustomerByMobile(String mobile) {
		return repository.findByMobileNo(mobile);
	}

	@Override
	public Customer save(Customer entity) {
		return repository.save(entity);
	}

	@Override
	public Customer update(Customer entity) {
		Optional<Customer> cust = repository.findById(entity.getId());
		if (!cust.isPresent()) {
			return null;
		}
		return repository.save(entity);
	}

	@Override
	public void delete(String id) {
		repository.deleteById(id);
	}

}
