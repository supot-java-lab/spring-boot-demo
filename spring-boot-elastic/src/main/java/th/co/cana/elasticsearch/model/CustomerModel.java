/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.elasticsearch.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.ToString;

/**
* @author supot
* @version 1.0
*/

@Data
@ToString
public class CustomerModel implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@NotNull
	private String firstName;

	@NotNull
	private String lastName;

	@NotNull
	private String mobileNo;
	
	private String email;
	
	private BigDecimal creditAmt;
	
}
