/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.elasticsearch.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

import th.co.cana.elasticsearch.data.Customer;

/**
 * @author supot
 * @version 1.0
 */

@EnableElasticsearchRepositories
public interface CustomerRepository extends ElasticsearchRepository<Customer, String> {
	Page<Customer> findAll();

	List<Customer> findByFirstName(String firstName);

	List<Customer> findByMobileNo(String mobileNo);
}
