/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.elasticsearch.data;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import lombok.Data;
import lombok.ToString;

/**
 * @author supot
 * @version 1.0
 */

@Data
@ToString
@Document(indexName = "customer")
public class Customer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String id;

	@NotNull
	private String firstName;

	@NotNull
	private String lastName;

	@NotNull
	private String mobileNo;
	
	private String email;
	
	private BigDecimal creditAmt;
	
}
