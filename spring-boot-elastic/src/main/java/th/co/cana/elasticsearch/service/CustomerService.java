/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.elasticsearch.service;

import java.util.List;
import java.util.Optional;

import th.co.cana.elasticsearch.data.Customer;

/**
* @author supot
* @version 1.0
*/
public interface CustomerService {

	List<Customer> getCustomers();
	
	Optional<Customer> getCustomer(String id);
	
	List<Customer> getCustomerByName(String name);
	
	List<Customer> getCustomerByMobile(String mobile);
	
	Customer save(Customer entity);
	
	Customer update(Customer entity);
	
	void delete(String id);
}
