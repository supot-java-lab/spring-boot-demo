/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.elasticsearch.controller;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import th.co.cana.elasticsearch.data.Customer;
import th.co.cana.elasticsearch.model.CustomerModel;
import th.co.cana.elasticsearch.service.CustomerService;
import th.co.cana.utils.Validators;

/**
 * @author supot
 * @version 1.0
 */

@RestController
@RequestMapping("/v1/customers")
public class CustomerCtrl extends Controller {

	@Autowired
	private CustomerService service;
	
	@GetMapping()
    public List<Customer> getCustomers() {
		return service.getCustomers();
    }
	
	@GetMapping(params = "name")
	public List<Customer> getCustomerByName(@RequestParam String name) {
		if (Validators.isEmpty(name)) {
			return Collections.emptyList();
		}
		return service.getCustomerByName(name);
	}

	@GetMapping(value = "/mobile", params = "mobile")
	public List<Customer> getCustomerByMobile(@RequestParam String mobile) {
		if (Validators.isEmpty(mobile)) {
			return Collections.emptyList();
		}
		return service.getCustomerByMobile(mobile);
	}
	
    @GetMapping("/{id}")
    public ResponseEntity<Customer> getCustomer(@PathVariable String id) {
		Optional<Customer> cust = service.getCustomer(id);
		if (!cust.isPresent()) {
			return ResponseEntity.badRequest().build();
		}
        return ResponseEntity.ok(cust.get());
    }
    
    @PostMapping()
    public ResponseEntity<Customer> create(@RequestBody CustomerModel body) {
    	logger.info("Dto : {}", body);
    	Customer cust = copyProperties(body, Customer.class);
    	logger.info("Entity : {}", cust);
    	
    	cust = service.save(cust);
    	
        return ResponseEntity.status(HttpStatus.CREATED).body(cust);
    }
    
    @PutMapping("/{id}")
    public ResponseEntity<Customer> update(@PathVariable String id, @RequestBody CustomerModel body) {
		logger.info("Dto : {}", body);
		Customer cust = copyProperties(body, Customer.class);
		if (Validators.isNotNull(cust)) {
			cust.setId(id);
		}
		logger.info("Entity : {}", cust);

		cust = service.update(cust);
        return ResponseEntity.ok(cust);
    }
    
    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteCustomers(@PathVariable String id) {
    	service.delete(id);
        return ResponseEntity.ok().build();
    }
}
