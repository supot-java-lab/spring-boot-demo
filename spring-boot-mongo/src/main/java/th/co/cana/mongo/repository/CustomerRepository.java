/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.mongo.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import th.co.cana.mongo.data.Customer;

/**
 * @author supot
 * @version 1.0
 */

public interface CustomerRepository extends MongoRepository<Customer, String>, CustomerRepositoryExtend {
	List<Customer> findByFirstName(String firstName);
}
