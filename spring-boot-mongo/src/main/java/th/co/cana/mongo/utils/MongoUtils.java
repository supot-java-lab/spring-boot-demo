/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.mongo.utils;

import java.util.List;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.MongoRegexCreator;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.MongoRegexCreator.MatchMode;

/**
* @author supot
* @version 1.0
*/
public final class MongoUtils {

	private MongoUtils() {}
	
	public static void like(List<Criteria> criterias, String key, String value) {
		Criteria cr = likeCriteria(key, value);
		if (cr != null) {
			criterias.add(cr);
		}
	}
	
	public static void equals(List<Criteria> criterias, String key, Object value) {
		if (value != null) {
			criterias.add(Criteria.where(key).is(value));
		}
	}
	
	public static Criteria likeCriteria(String key, String value) {
		String regex = likeRegex(value);
		if (regex == null) {
			return null;
		}
		return Criteria.where(key).regex(regex);
	}

	public static String likeRegex(String value) {
		if (value == null || value.isEmpty()) {
			return null;
		}
		return MongoRegexCreator.INSTANCE.toRegularExpression(value, MatchMode.LIKE);
	}
	
	public static Query createQuery(List<Criteria> criterias) {
		final Query query = new Query();
		if (!criterias.isEmpty()) {
			Criteria cr = new Criteria()
					.andOperator(criterias.toArray(new Criteria[criterias.size()]));
			query.addCriteria(cr);
		}
		
		return query;
	}
}
