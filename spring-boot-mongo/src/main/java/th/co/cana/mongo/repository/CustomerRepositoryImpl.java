/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.mongo.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.MongoRegexCreator;
import org.springframework.data.mongodb.core.query.MongoRegexCreator.MatchMode;
import org.springframework.data.mongodb.core.query.Query;

import th.co.cana.mongo.data.Customer;

/**
 * @author supot
 * @version 1.0
 */
public class CustomerRepositoryImpl implements CustomerRepositoryExtend {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<Customer> query(Customer cust) {
		final Query query = new Query();
		final List<Criteria> criteria = new ArrayList<>();
		if (cust.getFirstName() != null) {
			String regex = MongoRegexCreator.INSTANCE.toRegularExpression(cust.getFirstName(), MatchMode.LIKE);
			Criteria cr = Criteria.where("firstName").regex(regex);
			criteria.add(cr);
		}
		if (cust.getLastName() != null) {
			String regex = MongoRegexCreator.INSTANCE.toRegularExpression(cust.getLastName(), MatchMode.LIKE);
			Criteria cr = Criteria.where("firstName").regex(regex);
			criteria.add(cr);
		}
		if (cust.getEmail() != null) {
			criteria.add(Criteria.where("email").is(cust.getEmail()));
		}
		if (!criteria.isEmpty()) {
			query.addCriteria(new Criteria().andOperator(criteria.toArray(new Criteria[criteria.size()])));
		}
		return mongoTemplate.find(query, Customer.class);
	}

}
