/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.mongo.data;

import java.io.Serializable;

import org.springframework.data.annotation.Id;

import lombok.Data;

/**
 * @author supot
 * @version 1.0
 */

@Data
public class Customer implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String id;
	private String firstName;
	private String lastName;
	private String email;
}
