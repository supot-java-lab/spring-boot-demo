/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.mongo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import th.co.cana.mongo.data.Customer;
import th.co.cana.mongo.repository.CustomerRepository;

/**
 * @author supot
 * @version 1.0
 */

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerRepository customerRepository;

	@Override
	public Customer save(Customer cust) {
		return customerRepository.save(cust);
	}

	@Override
	public Optional<Customer> update(String id, Customer customer) {
		Optional<Customer> customerOpt = customerRepository.findById(id);
		if (!customerOpt.isPresent()) {
			return customerOpt;
		}
		customer.setId(id);
		return Optional.of(customerRepository.save(customer));
	}

	@Override
	public void delete(String id) {
		customerRepository.deleteById(id);
	}

	@Override
	public List<Customer> findCustomers() {
		return customerRepository.findAll();
	}

	@Override
	public List<Customer> findByFirstName(String firstName) {
		return customerRepository.findByFirstName(firstName);
	}

	@Override
	public List<Customer> findDynamic(Customer cust) {
		return customerRepository.query(cust);
	}
	
	@Override
	public Optional<Customer> findCustomer(String id) {
		return customerRepository.findById(id);
	}

}
