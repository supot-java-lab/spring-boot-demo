/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.mongo.service;

import java.util.List;
import java.util.Optional;

import th.co.cana.mongo.data.Customer;

/**
 * @author supot
 * @version 1.0
 */
public interface CustomerService {

	Customer save(Customer cust);

	Optional<Customer> update(String id, Customer customer);

	void delete(String id);
	
	List<Customer> findCustomers();
	
	List<Customer> findByFirstName(String firstName);
	
	List<Customer> findDynamic(Customer cust);
	
	Optional<Customer> findCustomer(String id);
}
