package th.co.cana.framework.models;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author supot.jdev
 * @version 1.0
 */
@EqualsAndHashCode(of = {"code"})
@Data
public class ParameterModel implements Serializable {
    private String code;
    private String name;
    private String value;
    private String desc;
}
