package th.co.cana.framework.jsf.bean;

import io.github.jdevlibs.faces.FacesMessages;
import io.github.jdevlibs.faces.FacesUtils;
import io.github.jdevlibs.primefaces.PFUtils;
import io.github.jdevlibs.primefaces.theme.*;
import io.github.jdevlibs.utils.Calculators;
import io.github.jdevlibs.utils.Validators;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Named;
import lombok.Data;
import th.co.cana.framework.config.enums.ErrorTypes;
import th.co.cana.framework.utils.AppConstants;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Named
@SessionScoped
@Data
public class WebPreferences implements Serializable {
    private static final String NONE_COLOR = "transparent";

    //Themes
    private String menuMode = "layout-horizontal";
    private String menuColor = "colored";
    private String menuTheme = "teallight";
    private String topbarTheme = "light";
    private String inputStyle = "outlined";
    private String componentTheme = "teallight";
    private String layoutPrimaryColor = "teallight";
    private boolean groupedMenu = true;
    private boolean darkLogo;
    private List<ThemeComponent> componentThemes;
    private List<ThemeTopbar> topbarThemes;
    private Map<String, List<ThemeMenu>> menuColors;
    private List<ThemePalette> palettes;
    private ThemePalette selectedPalette;

    //Others
    private ErrorTypes type;
    private List<String> errors;
    private List<String> jsCommands;
    private Locale locale = AppConstants.TH;
    private boolean showDialog;
    private int screenWidth;
    private int screenHeight;

    @PostConstruct
    public void init() {
        componentThemes = ThemeUtils.pandoraComponents();
        topbarThemes = ThemeUtils.pandoraTopbars();
        menuColors = ThemeUtils.pandoraMenuColors();
        palettes = ThemeUtils.pandoraPalettes();
        selectedPalette = palettes.get(2);
    }

    public String getLayoutConfig() {
        StringBuilder sb = new StringBuilder();
        String menuModeClass = ThemeUtils.getMenuModeClass(menuMode);

        sb.append("layout-topbar-").append(this.topbarTheme);
        sb.append(" ").append(getMenuThemeClass());
        sb.append(" ").append(menuModeClass);

        return sb.toString();
    }

    public String getLayoutAdminConfig() {
        StringBuilder sb = new StringBuilder();
        String menuModeClass = ThemeUtils.getMenuModeClass("layout-static");
        sb.append("layout-topbar-").append(this.topbarTheme);
        sb.append(" ").append(getMenuThemeClass());
        sb.append(" ").append(menuModeClass);

        return sb.toString();
    }

    public void changePalette(ThemePalette palette) {
        this.setMenuColor(palette.getMenuColor().getName());
        this.setMenuTheme(palette.getMenuTheme().getName());
        this.setTopbarTheme(palette.getTopbarTheme().getName());
        this.setComponentTheme(palette.getComponentTheme().getName());
        this.setSelectedPalette(palette);
    }

    public void executeCommand() {
        if (Validators.isEmpty(jsCommands)) {
            resetError();
            return;
        }

        for (String command : jsCommands) {
            if (Validators.isNotEmpty(command)) {
                PFUtils.execute(command);
            }
        }
        resetError();
    }

    public void add(String... error) {
        if (Validators.isEmpty(error)) {
            return;
        }

        if (Validators.isNull(errors)) {
            errors = new ArrayList<>();
        }
        errors.addAll(Arrays.asList(error));
    }

    public void addJsCommand(String ... jsCommand) {
        if (Validators.isEmpty(jsCommand)) {
            return;
        }

        if (Validators.isNull(jsCommands)) {
            jsCommands = new ArrayList<>();
        }
        jsCommands.addAll(Arrays.asList(jsCommand));
    }

    public void resetError() {
        type = null;
        errors = null;
        jsCommands = null;
    }

    public String getScreen70Width() {
        return getScreenPercentWidth(70);
    }

    public String getScreen80Width() {
        return getScreenPercentWidth(80);
    }

    public String getScreenPercentWidth(int percent) {
        if (screenWidth <= 0 || percent <= 0) {
            return null;
        }
        BigDecimal value = Calculators.amountOfPercent(BigDecimal.valueOf(screenWidth), BigDecimal.valueOf(percent));
        return value.intValue() + "px";
    }

    public String getScreen70Height() {
        return getScreenPercentHeight(70);
    }

    public String getScreen80Height() {
        return getScreenPercentHeight(80);
    }

    public String getScreen85Height() {
        return getScreenPercentHeight(85);
    }

    public String getScreenPercentHeight(int percent) {
        if (screenHeight <= 0 || percent <= 0) {
            return null;
        }

        BigDecimal value = Calculators.amountOfPercent(BigDecimal.valueOf(screenHeight), BigDecimal.valueOf(percent));
        return value.intValue() + "px";
    }

    /* +++++++++++++++++++++++++++++++++ Get/Set +++++++++++++++++++++++++++++++++ */
    public String getThemeName() {
        return "pandora-" + this.componentTheme;
    }

    public String getLayout() {
        return "layout-" + this.layoutPrimaryColor;
    }

    public String getInputStyleClass() {
        return ThemeUtils.getInputStyleClass(inputStyle);
    }

    public List<ThemeMenu> getThemeMenuItems() {
        if (Validators.isEmpty(menuColors)) {
            return Collections.emptyList();
        }
        return menuColors.get(menuColor);
    }

    public ErrorTypes getType() {
        if (type == null) {
            type = ErrorTypes.INFO;
        }
        return type;
    }

    public Locale getLocale() {
        if (locale == null) {
            locale = FacesUtils.getLocale();
        }
        return locale;
    }

    public String getMenuBgColor() {
        return (selectedPalette != null ? selectedPalette.getMenuColor().getCode() : NONE_COLOR);
    }

    public String getMenuTitle() {
        return (selectedPalette != null ? selectedPalette.getMenuColor().getName() : "");
    }

    public String getThemeBgColor() {
        return (selectedPalette != null ? selectedPalette.getMenuTheme().getCode() : NONE_COLOR);
    }

    public String getThemeTitle() {
        return (selectedPalette != null ? selectedPalette.getMenuTheme().getName() : "");
    }

    public String getTopbarBgColor() {
        return (selectedPalette != null ? selectedPalette.getTopbarTheme().getCode() : NONE_COLOR);
    }

    public String getTopbarTitle() {
        return (selectedPalette != null ? selectedPalette.getTopbarTheme().getName() : "");
    }

    public String getComponentBgColor() {
        return (selectedPalette != null ? selectedPalette.getComponentTheme().getCode() : NONE_COLOR);
    }

    public String getComponentTitle() {
        return (selectedPalette != null ? selectedPalette.getComponentTheme().getName() : "");
    }

    public boolean isEmptyError() {
        return (errors == null || errors.isEmpty());
    }

    public String getHeader() {
        return FacesMessages.getMessage(getType().getHeader());
    }

    public String getStyleClass() {
        return getType().getIconCss();
    }

    public String getIconCss() {
        return getType().getIconCss();
    }

    public boolean isErrorType() {
        return (ErrorTypes.ERROR == type);
    }

    public boolean isOneItem() {
        return (errors != null && errors.size() == 1);
    }

    public boolean isLangThai() {
        return FacesUtils.isLangThai();
    }

    public boolean isValidationFailed() {
        return (FacesUtils.getMaximumSeverity() != null || FacesUtils.isValidationFailed())
                && !FacesUtils.isEmptyError() && isEmptyError();
    }

    private String getMenuThemeClass() {
        return "layout-menu-" + ("colored".equals(menuColor) ? this.menuTheme : this.menuColor);
    }
}

