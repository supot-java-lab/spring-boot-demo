package th.co.cana.framework.service;

import org.springframework.stereotype.Service;
import th.co.cana.framework.utils.CacheUtils;

import java.util.HashMap;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Service
public class CacheService extends AbstractService {

    public void initialCache() {
        initialParameter();
        initialDropdown();
    }

    public void refreshCache() {
        initialParameter();
        initialDropdown();
    }

    private void initialParameter() {
        //TODO: wait for implement parameter
        CacheUtils.setParameterMap(new HashMap<>());
    }

    private void initialDropdown() {
        //TODO: wait for implement dropdown
        CacheUtils.setDropdownMap(new HashMap<>());
    }
}
