package th.co.cana.framework.config.enums;

import lombok.Getter;
import th.co.cana.framework.utils.PageMapping;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Getter
public enum PageNames {
    DASHBOARD("1000", PageMapping.PAGE_DASHBOARD),
    SYSTEM_INFO("9005", PageMapping.PAGE_SYSTEM_INFO),
    DEMO_FORM("9999", PageMapping.PAGE_DEMO_FORM),
    DEMO_FORM_LAYOUT("9999", PageMapping.PAGE_DEMO_FORM_LAYOUT),
    DEMO_FORM_TABLE("9999", PageMapping.PAGE_DEMO_TABLE);

    private final String code;
    private final String path;

    PageNames(String code, String path) {
        this.code = code;
        this.path = path;
    }
}
