package th.co.cana.framework.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Random;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Customer implements Serializable {
    private int id;
    private String name;
    private Country country;
    private LocalDate date;
    private CustomerStatus status;
    private int activity;
    private Representative representative;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Representative implements Serializable {
        private String name;
        private String image;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Country implements Serializable {
        private String name;
        private String code;
    }

    public enum CustomerStatus {
        QUALIFIED,
        UNQUALIFIED,
        NEGOTIATION,
        NEW,
        RENEWAL,
        PROPOSAL;

        public static CustomerStatus random() {
            Random random = new Random();
            return values()[random.nextInt(values().length)];
        }
    }
}
