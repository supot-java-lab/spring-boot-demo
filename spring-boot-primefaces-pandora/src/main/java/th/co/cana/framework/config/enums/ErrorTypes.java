package th.co.cana.framework.config.enums;

import lombok.Getter;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Getter
public enum ErrorTypes {
    ERROR("msg.popup.title.error", "fa-solid fa-circle-xmark text-red-500"),
    WARNING("msg.popup.title.warning", "fa-solid fa-triangle-exclamation text-orange-300"),
    INFO("msg.popup.title.info", "fa-solid fa-circle-info text-blue-100");

    private final String header;
    private final String iconCss;

    ErrorTypes(String header, String iconCss) {
        this.header = header;
        this.iconCss = iconCss;
    }
}