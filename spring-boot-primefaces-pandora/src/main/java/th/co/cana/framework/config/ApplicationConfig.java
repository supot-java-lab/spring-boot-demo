package th.co.cana.framework.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import th.co.cana.framework.config.properties.ApplicationProperties;
import th.co.cana.framework.utils.AppConstants;
import th.co.cana.framework.utils.MessageUtils;

import java.util.concurrent.Executor;

/**
 * @author supot.jdev
 * @version 1.0
 */

@Configuration
@EnableAsync
public class ApplicationConfig {

    @Autowired
    public ApplicationConfig(MessageSource messageSource, ApplicationProperties properties) {
        MessageUtils.setMessageSource(messageSource);
        AppConstants.AppInfo.setProperties(properties);
    }

    @Bean
    public Executor taskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(2);
        executor.setMaxPoolSize(2);
        executor.setQueueCapacity(500);
        executor.setThreadNamePrefix("application-task-");
        executor.initialize();

        return executor;
    }

}
