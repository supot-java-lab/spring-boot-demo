package th.co.cana.framework.controller;

import io.github.jdevlibs.faces.FacesMessages;
import io.github.jdevlibs.faces.FacesUtils;
import io.github.jdevlibs.primefaces.PFUtils;
import io.github.jdevlibs.utils.FileUtils;
import io.github.jdevlibs.utils.Formats;
import io.github.jdevlibs.utils.Validators;
import jakarta.inject.Inject;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import th.co.cana.framework.config.enums.ErrorTypes;
import th.co.cana.framework.config.enums.PageActions;
import th.co.cana.framework.exception.ServiceException;
import th.co.cana.framework.jsf.bean.UserInfo;
import th.co.cana.framework.jsf.bean.WebPreferences;
import th.co.cana.framework.utils.AppConstants;
import th.co.cana.framework.utils.MessageUtils;

import java.io.File;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Getter
@Setter
public abstract class AbstractCtrl implements Serializable {
    private static final String ERROR_DIALOG = "messageDialog";
    private static final String PAGING_TEMPLATE = "{RowsPerPageDropdown} {FirstPageLink} {PreviousPageLink} {CurrentPageReport} {NextPageLink} {LastPageLink}";
    private static final String ROW_PER_PAGE = "10, 20, 30, 50, 100, 150, 200";

    protected transient Logger logger = LoggerFactory.getLogger(getClass());

    //Inject service & bean
    @Inject
    private WebPreferences webPreferences;
    @Inject
    private UserInfo userInfo;

    //Variable
    private int pageSize = 10;
    private PageActions pageAction;
    private Map<String, Boolean> statusLoadings;

    public String getPagingTemplate() {
        return PAGING_TEMPLATE;
    }

    public String getRowPerPage() {
        return ROW_PER_PAGE;
    }

    /* ++++++++++++++++++++++++ Page loading event +++++++++++++++++++++++ */
    public Map<String, Boolean> getStatusLoadings() {
        if (Validators.isNull(statusLoadings)) {
            statusLoadings = new HashMap<>();
        }
        return statusLoadings;
    }

    public void addStatusLoading(String name, boolean status) {
        logger.info("Set status of : {} -> {}", name, status);
        getStatusLoadings().put(name, status);
    }

    public boolean isLoadedSuccess(String name) {
        Boolean value = getStatusLoadings().get(name);
        return (value != null && value);
    }

    /* ++++++++++++++++++++++++ Page Action +++++++++++++++++++++++ */
    public boolean isPageActionAdd() {
        return (PageActions.CREATE == pageAction);
    }

    public boolean isPageActionEdit() {
        return (PageActions.EDIT == pageAction);
    }

    public boolean isPageActionView() {
        return (PageActions.VIEW == pageAction);
    }

    public boolean isPageActionInitial() {
        return (PageActions.INITIAL == pageAction);
    }

    public boolean isPageActionPrint() {
        return (PageActions.PRINT == pageAction);
    }

    public String getPageActionName() {
        if (Validators.isNull(pageAction)) {
            return "";
        }
        return FacesMessages.getMessage(pageAction.getKey());
    }

    /* ++++++++++++++++++++++++ User info +++++++++++++++++++++++ */
    public String getUserId() {
        return userInfo.getUserId();
    }

    public String getUsername() {
        return userInfo.getUsername();
    }

    public String getUserIdentity() {
        return getUserInfo().getUserId();
    }

    /* ++++++++++++++++++++++++ Dialog Method +++++++++++++++++++++++ */
    public void showDialogError(String message, String... jsCommand) {
        getWebPreferences().setType(ErrorTypes.ERROR);
        showDialog(message, jsCommand);
    }

    public void showDialogError(List<String> messages, String... jsCommand) {
        getWebPreferences().setType(ErrorTypes.ERROR);
        showDialog(messages, jsCommand);
    }

    public void showDialogError(Exception ex) {
        showDialogError(ex, "");
    }

    public void showDialogError(Exception ex, String... jsCommand) {
        getWebPreferences().setType(ErrorTypes.ERROR);
        showDialog(getErrorMessage(ex), jsCommand);
    }

    public void showDialogWarning(String message, String... jsCommand) {
        getWebPreferences().setType(ErrorTypes.WARNING);
        showDialog(message, jsCommand);
    }

    public void showDialogInfo(String message, String... jsCommand) {
        getWebPreferences().setType(ErrorTypes.INFO);
        showDialog(message, jsCommand);
    }

    private void showDialog(List<String> messages, String... jsCommand) {
        try {
            getWebPreferences().setErrors(messages);
            getWebPreferences().addJsCommand(jsCommand);
            if (getWebPreferences().isErrorType()) {
                FacesUtils.validationFailed();
            }
            if (getWebPreferences().isShowDialog()) {
                PFUtils.showDialog(ERROR_DIALOG, ERROR_DIALOG);
            }
        } catch (Exception ex) {
            logger.error("showDialog", ex);
        }
    }

    private void showDialog(String message, String... jsCommand) {
        try {
            getWebPreferences().add(message);
            getWebPreferences().addJsCommand(jsCommand);
            if (getWebPreferences().isErrorType()) {
                FacesUtils.validationFailed();
            }
            if (getWebPreferences().isShowDialog()) {
                PFUtils.showDialog(ERROR_DIALOG, ERROR_DIALOG);
            }
        } catch (Exception ex) {
            logger.error("showDialog", ex);
        }
    }

    public String getErrorMessage(Throwable ex) {
        if (ex instanceof ServiceException exception) {
            return getServiceError(exception);
        } else {
            return MessageUtils.getValue("error.exception.other");
        }
    }

    public String getMessage(String key, Object... value) {
        return FacesMessages.getMessage(key, value);
    }

    private String getServiceError(ServiceException ex) {
        String error = MessageUtils.getValue("error.exception.other");
        if (Validators.isNotEmpty(ex.getErrorMessage())) {
            error = MessageUtils.getValue(ex.getErrorMessage());
        }
        return error;
    }

    /* ++++++++++++++++++++++++ Utility Method +++++++++++++++++++++++ */
    public String formatCurrency(Number amount) {
        return Formats.decimal(amount);
    }

    public String formatNumber(Number amount) {
        return Formats.number(amount);
    }

    public String formatNumberWithDash(Number amount) {
        if (Validators.isNull(amount) || amount.longValue() == 0) {
            return "-";
        }
        return Formats.number(amount);
    }

    public String getDatePattern() {
        return AppConstants.DATE_FORMAT;
    }

    public String getDateTimePattern() {
        return AppConstants.DATE_TIME_FORMAT;
    }

    public LocalDate getCurrentDate() {
        return LocalDate.now();
    }

    public Locale getLocale() {
        if (Validators.isNotNull(webPreferences.getLocale())) {
            return webPreferences.getLocale();
        }
        return FacesUtils.getLocale();
    }

    public String redirectToOutcome(String outcome) {
        return FacesUtils.redirectToOutcome(outcome);
    }

    public void redirectToPage(String page) {
        FacesUtils.redirectToPage(page);
    }

    public void redirectToUrl(String url) {
        try {
            FacesUtils.redirectToUrl(url);
        } catch (Exception ex) {
            logger.error("redirectToUrl[Error]", ex);
        }
    }

    public void cleanUploadFile(List<String> fileItems) {
        if (Validators.isEmpty(fileItems)) {
            return;
        }

        for (String fileName : fileItems) {
            File file = new File(fileName);
            if (file.exists()) {
                try {
                    FileUtils.forceDelete(file);
                } catch (Exception ex) {
                    logger.error("cleanUploadFile", ex);
                }
                logger.debug("Delete file : {} success", fileName);
            }
        }
    }
}
