package th.co.cana.framework.jsf.bean;

import io.github.jdevlibs.faces.models.SelectItem;
import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Named;
import lombok.extern.slf4j.Slf4j;
import th.co.cana.framework.config.enums.DropDownTypes;
import th.co.cana.framework.utils.LovUtils;


import java.io.Serializable;
import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Slf4j
@Named
@SessionScoped
public class LovService implements Serializable {

    public List<SelectItem> getMonths() {
        return LovUtils.createMonths();
    }

    public List<SelectItem> getMonthNoSelects() {
        return LovUtils.createMonths(false);
    }

    public String getMonthName(int month) {
        return LovUtils.getMonthName(month);
    }

    public List<SelectItem> getMonthShorts() {
        return LovUtils.createMonthShorts(true);
    }

    public List<SelectItem> getMonthShortNoSelects() {
        return LovUtils.createMonthShorts(false);
    }

    public String getMonthShortName(int month) {
        return LovUtils.getMonthShortName(month);
    }

    public List<SelectItem> getProcessStatusList() {
        return LovUtils.createItems(DropDownTypes.PROCESS_STATUS);
    }

    public List<SelectItem> getRecordStatusList() {
        return LovUtils.createItems(DropDownTypes.RECORD_STATUS);
    }

    public List<SelectItem> getRecordStatusNoSelects() {
        return LovUtils.createItems(DropDownTypes.RECORD_STATUS, false);
    }

    public String getRecordStatus(String code) {
        return LovUtils.getName(DropDownTypes.RECORD_STATUS, code);
    }
}
