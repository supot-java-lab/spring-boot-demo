package th.co.cana.framework.utils;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author supot.jdev
 * @version 1.0
 */
public final class PageMapping {

    private PageMapping() {}

    public static final String URL_LOGIN        = "/login";
    public static final String URL_HOME         = "/home";
    public static final String URL_ERROR	    = "/error";
    public static final String URL_ERROR_401	= "/error/401";
    public static final String URL_ERROR_404	= "/error/404";
    public static final String URL_ERROR_500	= "/error/500";

    public static final String PAGE_LOGIN 		    = "/login.xhtml";
    public static final String PAGE_DASHBOARD 		= "/dashboard.xhtml";

    public static final String PAGE_ERROR_401 		= "/views/error/401.xhtml";
    public static final String PAGE_ERROR_404 		= "/views/error/404.xhtml";
    public static final String PAGE_ERROR_500		= "/views/error/500.xhtml";

    public static final String PAGE_SYSTEM_INFO     = "/views/system/system-info.xhtml";

    //Demo
    public static final String PAGE_DEMO_FORM 		    = "/views/demo/form-input.xhtml";
    public static final String PAGE_DEMO_FORM_LAYOUT 	= "/views/demo/form-layout.xhtml";
    public static final String PAGE_DEMO_TABLE 	        = "/views/demo/form-table.xhtml";

    private static final Map<String, String> MENU_MAPPINGS;
    static {
        MENU_MAPPINGS = new LinkedHashMap<>();
        MENU_MAPPINGS.put("/", PAGE_LOGIN);
        MENU_MAPPINGS.put(URL_LOGIN, PAGE_LOGIN);

        //Error
        MENU_MAPPINGS.put(URL_ERROR, PAGE_ERROR_404);
        MENU_MAPPINGS.put(URL_ERROR_401, PAGE_ERROR_401);
        MENU_MAPPINGS.put(URL_ERROR_404, PAGE_ERROR_404);
        MENU_MAPPINGS.put(URL_ERROR_500, PAGE_ERROR_500);

        //Demo
        MENU_MAPPINGS.put("/demo/form-input", PAGE_DEMO_FORM);
        MENU_MAPPINGS.put("/demo/form-layout", PAGE_DEMO_FORM_LAYOUT);
        MENU_MAPPINGS.put("/demo/form-table", PAGE_DEMO_TABLE);

        MENU_MAPPINGS.put("/system/info", PAGE_SYSTEM_INFO);
    }

    public static final class Popups {
        private Popups() {
        }
        public static final String POPUP_LOG_FILE 	    = "/views/popup/popup-view-log.xhtml";
    }

    public static Map<String, String> getMenuMappings() {
        return Collections.unmodifiableMap(MENU_MAPPINGS);
    }

    public static String getMenuPath(String url) {
        return MENU_MAPPINGS.get(url);
    }
}
