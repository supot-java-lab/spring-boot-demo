package th.co.cana.framework.utils;

import io.github.jdevlibs.faces.FacesMessages;
import io.github.jdevlibs.faces.FacesUtils;
import io.github.jdevlibs.faces.models.SelectItem;
import io.github.jdevlibs.utils.DateUtils;
import io.github.jdevlibs.utils.Utils;
import io.github.jdevlibs.utils.Validators;
import io.github.jdevlibs.utils.Values;
import th.co.cana.framework.config.enums.DropDownTypes;
import th.co.cana.framework.config.enums.MonthNameShorts;
import th.co.cana.framework.config.enums.MonthNames;
import th.co.cana.framework.models.DropdownModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */
public final class LovUtils {
    private LovUtils() {
    }

    public static void fillItems(List<SelectItem> items, List<SelectItem> results) {
        if (Validators.isNotEmpty(results)) {
            items.addAll(results);
        }
    }

    public static String getValue(SelectItem item) {
        return (Validators.isNotNull(item) ? item.getValue() : null);
    }

    public static String getLabel(SelectItem item) {
        return (Validators.isNotNull(item) ? item.getLabel() : null);
    }

    public static List<SelectItem> createEmpty() {
        List<SelectItem> results = new ArrayList<>(1);
        String label = FacesMessages.getMessage("msg.select.item.empty");
        results.add(new SelectItem(Values.EMPTY, label));

        return results;
    }

    public static List<SelectItem> createYears(int total) {
        return createYears(total, true);
    }

    public static List<SelectItem> createYears(int total, boolean showSelect) {
        List<SelectItem> results = new ArrayList<>(total);
        if (showSelect) {
            results.addAll(createEmpty());
        }

        int year = DateUtils.getYearThai();
        if (FacesUtils.isLangEng()) {
            year = DateUtils.getYear();
        }

        year -= (total / 2);

        for (int i = year, maxValue = (year + total); i <= maxValue; i++) {
            results.add(new SelectItem(String.valueOf(i), String.valueOf(i)));
        }
        return results;
    }

    public static List<SelectItem> createSinceYears(int sinceYear, boolean showSelect) {

        int year = DateUtils.getYearThai();
        if (FacesUtils.isLangEng()) {
            year = DateUtils.getYear();
        }

        int total = year - sinceYear;
        List<SelectItem> results = new ArrayList<>(total);
        if (showSelect) {
            results.addAll(createEmpty());
        }
        for (int i = sinceYear; i <= year; i++) {
            results.add(new SelectItem(String.valueOf(i), String.valueOf(i)));
        }
        return results;
    }

    public static List<SelectItem> createPastYears(int total) {
        return createPastYears(total, true);
    }

    public static List<SelectItem> createPastYears(int total, boolean showSelect) {
        return createPastYears(total, 0, showSelect);
    }

    public static List<SelectItem> createPastYears(int total, int nextYear, boolean showSelect) {
        List<SelectItem> results = new ArrayList<>(total);
        if (showSelect) {
            results.addAll(createEmpty());
        }

        int year = DateUtils.getYearThai();
        if (FacesUtils.isLangEng()) {
            year = DateUtils.getYear();
        }
        year += nextYear;

        for (int i = year, max = (year - total); i >= max; i--) {
            results.add(new SelectItem(String.valueOf(i), String.valueOf(i)));
        }
        return results;
    }

    public static List<SelectItem> createMonths() {
        return createMonths(true);
    }

    public static List<SelectItem> createMonths(boolean showSelect) {
        List<SelectItem> list = new ArrayList<>(12);
        if (showSelect) {
            list.addAll(createEmpty());
        }

        for (MonthNames month : MonthNames.values()) {
            list.add(new SelectItem(month.getValue(), FacesMessages.getMessage(month.getMsgCode())));
        }

        return list;
    }

    public static String getMonthName(Integer month) {
        if (Validators.isNull(month) || month <= 0) {
            return Values.EMPTY;
        }
        String pad = Utils.padLeft(month, 2, "0");
        return FacesMessages.getMessage("msg.month." + pad);
    }

    public static List<SelectItem> createMonthShorts(boolean showSelect) {
        List<SelectItem> list = new ArrayList<>(12);
        if (showSelect) {
            list.addAll(createEmpty());
        }

        for (MonthNameShorts month : MonthNameShorts.values()) {
            list.add(new SelectItem(month.getValue(), FacesMessages.getMessage(month.getMsgCode())));
        }

        return list;
    }

    public static String getMonthShortName(Integer month) {
        if (Validators.isNull(month) || month <= 0) {
            return Values.EMPTY;
        }

        String pad = Utils.padLeft(month, 2, "0");
        return FacesMessages.getMessage("msg.month.short." + pad);
    }

    public static List<SelectItem> createItems(DropDownTypes type) {
        return createItems(type, FacesUtils.getLanguage(), true);
    }

    public static List<SelectItem> createItem(DropDownTypes type, String lang) {
        return createItems(type, lang, true);
    }

    public static List<SelectItem> createItems(DropDownTypes type, boolean showSelect) {
        return createItems(type, FacesUtils.getLanguage(), showSelect);
    }

    public static List<SelectItem> createItems(DropDownTypes type, String lang, boolean showSelect) {
        List<DropdownModel> results = CacheUtils.getDropdowns(type);
        if (Validators.isEmpty(results)) {
            return Collections.emptyList();
        }

        List<SelectItem> list = new ArrayList<>(results.size());
        if (showSelect) {
            list.addAll(createEmpty());
        }

        for (DropdownModel model : results) {
            SelectItem item = new SelectItem(model.getValue(), model.getNameTh());
            if (isLangEng(lang)) {
                item.setLabel(model.getNameEn());
            }
            list.add(item);
        }
        return list;
    }

    public static String getName(DropDownTypes type, String key) {
        return getName(type, key, FacesUtils.getLanguage());
    }

    public static String getName(DropDownTypes type, String key, String lang) {
        if (isLangEng(lang)) {
            return CacheUtils.getNameEn(type, key);
        } else {
            return CacheUtils.getNameTh(type, key);
        }
    }

    public static String getValue(DropDownTypes type) {
        if (Validators.isNotNull(type)) {
            return CacheUtils.getFirstValue(type);
        }
        return null;
    }

    private static boolean isLangEng(String lang) {
        return "US".equalsIgnoreCase(lang)
                || "en-US".equalsIgnoreCase(lang)
                || "en_US".equalsIgnoreCase(lang);
    }
}
