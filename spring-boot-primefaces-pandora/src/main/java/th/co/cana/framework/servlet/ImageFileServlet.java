package th.co.cana.framework.servlet;

import java.io.File;

import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.omnifaces.servlet.FileServlet;
import org.springframework.stereotype.Component;
import th.co.cana.framework.utils.AppConstants;

/**
 * @author Supot Saelao
 * @version 1.0
 */

@Slf4j
@Component
@WebServlet(name = "ImageFileServlet", urlPatterns = { "/images/*" })
public class ImageFileServlet extends FileServlet {
    private File folder;

    @Override
    public void init() {
        String uploadPath = AppConstants.workDir();
        log.info("[CONFIG] Load folder form : {}", uploadPath);
        folder = new File(uploadPath);
    }

    @Override
    protected File getFile(HttpServletRequest request) {
        String pathInfo = request.getPathInfo();
        if (pathInfo == null || pathInfo.isEmpty() || "/".equals(pathInfo)) {
            throw new IllegalArgumentException();
        }
        return new File(folder, pathInfo);
    }
}