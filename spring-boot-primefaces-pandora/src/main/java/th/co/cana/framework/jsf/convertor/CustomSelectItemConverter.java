package th.co.cana.framework.jsf.convertor;

import io.github.jdevlibs.faces.FacesMessages;
import io.github.jdevlibs.faces.models.SelectItem;
import io.github.jdevlibs.spring.utils.JsonUtils;
import io.github.jdevlibs.utils.Validators;
import jakarta.faces.application.FacesMessage;
import jakarta.faces.component.UIComponent;
import jakarta.faces.context.FacesContext;
import jakarta.faces.convert.Converter;
import jakarta.faces.convert.ConverterException;
import jakarta.faces.convert.FacesConverter;
import th.co.cana.framework.utils.Base64Utils;

/**
 * @author supot.jdev
 * @version 1.0
 */
@FacesConverter("customSelectItemConverter")
public class CustomSelectItemConverter implements Converter<SelectItem> {

    @Override
    public SelectItem getAsObject(FacesContext fc, UIComponent ui, String value) {
        if (Validators.isEmpty(value)) {
            return null;
        }

        String label = FacesMessages.getMessage("msg.select.item.empty");
        if (value.equals(label)) {
            return null;
        }

        try {
            String encode = Base64Utils.decode(Base64Utils.unescapeHtml4(value));
            return JsonUtils.model(encode, SelectItem.class);
        } catch (Exception ex) {
            throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Conversion Error", "Cannot convert SelectItem to Object"));
        }
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent ui, SelectItem obj) {
        if (Validators.isNull(obj) || Validators.isEmpty(obj.getValue())) {
            return null;
        }
        try {
            return Base64Utils.encode(JsonUtils.json(obj));
        } catch (Exception ex) {
            throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Conversion Error", "Cannot convert SelectItemValue to String"));
        }
    }

}