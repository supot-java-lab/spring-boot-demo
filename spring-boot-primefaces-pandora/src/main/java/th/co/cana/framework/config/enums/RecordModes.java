package th.co.cana.framework.config.enums;

/**
 * @author supot.jdev
 * @version 1.0
 */
public enum RecordModes {
    NONE, NEW, EDIT, VIEW, DELETE, LOCK
}
