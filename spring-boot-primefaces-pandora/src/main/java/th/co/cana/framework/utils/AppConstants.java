package th.co.cana.framework.utils;

import th.co.cana.framework.config.properties.ApplicationProperties;

import java.time.LocalDateTime;
import java.util.Locale;

/**
 * @author supot.jdev
 * @version 1.0
 */
public final class AppConstants {
    private AppConstants() {
    }

    public static final String BASE_PACKAGE_NAME = AppConstants.class.getPackageName().replaceFirst(".utils", "");

    public static final String Y = "Y";
    public static final String N = "N";

    public static final String UTF8 = "UTF-8";
    public static final Locale US = Locale.US;
    public static final Locale TH = new Locale("th", "TH");
    public static final String LANG_TH = "TH";
    public static final String LANG_US = "US";
    public static final String HTML_NEW_LINE = "<br/>";
    public static final String HTML_SPACE = "&nbsp;";
    public static final String HTML_SPACE_2 = "&ensp;";
    public static final String HTML_SPACE_4 = "&emsp;";
    public static final String SESSION_USER_ATT = "userInfo";
    public static final int SCREEN_WIDTH_PADDING = 47;
    public static final int SCREEN_HEIGHT_PADDING = 280;
    public static final int SCREEN_SB_WIDTH = 28;
    public static final int SCREEN_NO_SB_WIDTH = 14;
    public static final int DEFAULT_CHAR_PER_PIXEL = 12;
    public static final int ICON_WITH_PADDING_WIDTH = 48;

    public static final String DATE_FORMAT_FULL     = "dd MMMM yyyy";
    public static final String DATE_FORMAT          = "dd/MM/yyyy";
    public static final String DATE_TIME_FORMAT     = "dd/MM/yyyy HH:mm:ss";

    /**
     * AOP: Aspect Oriented Programming package
     */
    public static final class Aspects {
        private Aspects() {
        }
        public static final String SERVICE      = "execution(public * " + BASE_PACKAGE_NAME + ".service..*.*(..))";
        public static final String REPOSITORY   = "execution(public * " + BASE_PACKAGE_NAME + ".repository..*.*(..))";
        public static final String DAO          = "execution(public * " + BASE_PACKAGE_NAME + ".dao..*.*(..))";
    }

    public static class UISystems {
        private UISystems() {
        }

        public static final String SEARCH_FORM      = "PAGE_SEARCH_FORM";
        public static final String SEARCH_RESULT    = "PAGE_SEARCH_RESULT";
        public static final String PAGE_SYS_FORM    = "PAGE_SYS_FORM";
        public static final String PAGE_SYS_POPUP   = "PAGE_SYS_POPUP";
    }

    public static final class AppInfo {
        private static String name;
        private static String version;
        private static String buildTime;
        private static String time;

        private AppInfo() {
        }

        public static void setProperties(final ApplicationProperties properties) {
            time = System.nanoTime() + "";
            if (properties != null) {
                name = properties.getName();
                version = properties.getVersion();
                buildTime = properties.getBuildTime();
                if ("^timestamp^".equalsIgnoreCase(buildTime)) {
                    buildTime = LocalDateTime.now().toString();
                }
            } else {
                version = "local-v1";
                buildTime = System.nanoTime() + "";
            }
        }

        public static String getVersion() {
            return version;
        }

        public static String getName() {
            return name;
        }

        public static String getBuildTime() {
            return buildTime;
        }

        public static String getTime() {
            return time;
        }
    }

    public static String workDir() {
        String workDir = System.getProperty("user.dir");
        if ("/".equals(workDir)) {
            workDir = "";
            return workDir;
        } else {
            return pathToUnix(workDir);
        }
    }

    public static String resourcesDir() {
        return workDir() + "/resources";
    }

    public static String getConfigDir() {
        return resourcesDir() + "/config";
    }

    public static String logDir() {
        return workDir() + "/logs";
    }

    private static String pathToUnix(final String path) {
        if (path == null || path.indexOf('\\') == -1) {
            return path;
        }
        return path.replace('\\', '/');
    }
}