package th.co.cana.framework.jsf.component;

import jakarta.faces.component.UIComponent;
import jakarta.faces.context.FacesContext;
import org.primefaces.component.api.AjaxSource;
import org.primefaces.expression.SearchExpressionUtils;
import org.primefaces.pandora.component.PandoraMenuRenderer;
import org.primefaces.util.AjaxRequestBuilder;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class CustomPandoraMenuRenderer extends PandoraMenuRenderer {

    @Override
    protected String createAjaxRequest(FacesContext context, AjaxSource source, UIComponent form) {
        UIComponent component = (UIComponent) source;
        String clientId = component.getClientId(context);

        AjaxRequestBuilder builder = getAjaxRequestBuilder();

        builder.init()
                .source(clientId)
                .form(SearchExpressionUtils.resolveClientId(context, component, source.getForm()))
                .process(component, source.getProcess())
                .update(component, source.getUpdate())
                .async(source.isAsync())
                .global(source.isGlobal())
                .delay(source.getDelay())
                .timeout(source.getTimeout())
                .partialSubmit(source.isPartialSubmit(), source.isPartialSubmitSet(), source.getPartialSubmitFilter())
                .resetValues(source.isResetValues(), source.isResetValuesSet())
                .ignoreAutoUpdate(source.isIgnoreAutoUpdate())
                .onstart(source.getOnstart())
                .onerror(source.getOnerror())
                .onsuccess(source.getOnsuccess())
                .oncomplete(source.getOncomplete())
                .params(component);

        if (form != null) {
            builder.form(form.getClientId(context));
        }

        builder.preventDefault();

        return builder.build();
    }
}
