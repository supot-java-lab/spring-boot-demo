package th.co.cana.reactive.config.properties;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.io.Serializable;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "service.api")
public class ApiConfigProperties implements Serializable {
    private ApiMail mail = new ApiMail();
    private ApiSms sms = new ApiSms();

    @Data
    public static class ApiConfig implements Serializable {
        private String baseUrl;
        private boolean mackService;
        private String username;
        private String password;
        private String apiKey;
        private String apiSecretKey;
        private String resourceOwnerID;

        public String getApiUrl(String apiName) {
            return getBaseUrl() + apiName;
        }
    }

    @Data
    @EqualsAndHashCode(callSuper=false)
    public static class ApiMail extends  ApiConfig {
        private String notificationApi;

        public String notificationApiUrl() {
            return getApiUrl(notificationApi);
        }
    }

    @Data
    @EqualsAndHashCode(callSuper=false)
    public static class ApiSms extends  ApiConfig {
        private String smsApi;

        public String smsApiUrl() {
            return getApiUrl(smsApi);
        }
    }
}
