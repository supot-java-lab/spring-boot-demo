/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.redis.main;

import th.co.cana.redis.exception.JsonException;
import th.co.cana.redis.utils.JsonUtils;

/**
* @author supot
* @version 1.0
*/
public class TestJson {

	public static void main(String[] args) throws JsonException {
		String data = "This is supot";
		System.out.println(data);
		String json = JsonUtils.toJson(data);
		System.out.println(json);
	}

}
