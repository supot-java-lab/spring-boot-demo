package th.co.cana.redis;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import th.co.cana.redis.data.Customer;
import th.co.cana.redis.data.Customer.Address;
import th.co.cana.redis.service.CustomerService;

@SpringBootTest
class TestCustomerService {

	@Autowired
	@Qualifier("customerServiceImpl")
	private CustomerService service;
	
	@Test
	@Order(1)
	void save() {
		Customer cust = new Customer();
		cust.setFirstName("Panitra");
		cust.setLastName("Saelao");
		cust.setEmail("Somkid@gmail.com");
		
		Address addRr = new Address();
		addRr.setNo("501");
		addRr.setAdd("Moo 10");
		addRr.setTumbon("Sunsainoi");
		addRr.setAmphur("Sunsai");
		addRr.setProvince("Chiang Mai");
		addRr.setPosCode("50210");
		cust.addAddress(addRr );
		
		System.out.println("1. Save data");
		service.save(cust);		
		System.out.println(cust.getKeyId() + " -> " + cust);		
		assertNotEquals(null, cust.getKeyId());
		
		System.out.println("2. Get data");
		Optional<Customer> custOp = service.findCustomer(cust.getKeyId());
		System.out.println(custOp.get());
		
		System.out.println("3. Get all data");
		List<Customer> custs = service.findCustomers();
		for(Customer bean : custs) {
			System.out.println(bean.getKeyId() + " : " + bean);
		}
	}
}
