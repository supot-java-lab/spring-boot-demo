/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.redis;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.List;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;

import th.co.cana.redis.data.Customer;
import th.co.cana.redis.data.User;
import th.co.cana.redis.service.RedisService;
import th.co.cana.utils.DateUtils;
import th.co.cana.utils.Utils;

/**
* @author supot
* @version 1.0
*/

@SpringBootTest
class TestRedisService {

	@Autowired
	@Qualifier("redisService")
	private RedisService redisService;
	
	@Test
	@Order(1)
	void testByKey() {
		System.out.println("Step 1 : testByKey [Create Normat type]");
		boolean success = redisService.setValue("KEY_INT", 10);
		int intVal = redisService.getValue("KEY_INT", int.class);
		System.out.println("intVal : " + intVal);
		
		success = redisService.setValue("KEY_STRING", "String value");
		String strVal = redisService.getValue("KEY_STRING");
		System.out.println("strVal : " + strVal);
		
		success = redisService.setValue("KEY_BIG_DECIMAL", BigDecimal.valueOf(25600.45D));
		BigDecimal decVal = redisService.getValue("KEY_BIG_DECIMAL", BigDecimal.class);
		System.out.println("decVal : " + decVal);
		
		success = redisService.setValue("KEY_LONG", 1577888L);
		Long longVal = redisService.getValue("KEY_LONG", Long.class);
		System.out.println("longVal : " + longVal);
		
		System.out.println("Step 2 : testByKey [Create Object]");
		String key = "KEY_OBJ_CUST";
		Customer cust = new Customer();
		cust.setKeyId(Utils.getUUID());
		cust.setFirstName("Supot");
		cust.setLastName("Saelao");
		cust.setEmail("supot.jsoft@gmail.com");
		cust.setCreatedDt(DateUtils.getDate());

		success = redisService.setValue(key, cust);
		System.out.println("Store customer : " + success);
		assertTrue(success);
		
		cust = redisService.getValue(key, Customer.class);
		System.out.println("Customer : " + cust);
		
		List<String> keys = redisService.getKeys(key);
		System.out.println(keys);
	}
	
	@Test
	@Order(2)
	void testByKeyAndField() {
		System.out.println("Step 2 : testByKeyAndField [Create data]");
		String key = "KEY_OBJ_CUST_LIST";
		for (int i = 1; i <= 10; i++) {
			User user = new User();
			user.setKeyId(Utils.getUUID());
			user.setUserId(String.valueOf(i));
			user.setUsername("supot-" + i);
			user.setPassword("password-" + i);
			user.setFirstName("Supot-" + i);
			user.setLastName("Saelao-" + i);
			user.setEmail("supot.jsoft@gmail.com");
			user.setRegister(DateUtils.getDate());

			boolean success = redisService.setValue(key, user.getKeyId(), user);
			System.out.println("Store user : " + success);
			assertTrue(success);
		}
		
		List<User> items = redisService.getValues(key, User.class);
		for (User bean : items) {
			System.out.println(bean);
		}
	}
}
