/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.redis.service;

import io.lettuce.core.api.sync.RedisCommands;

/**
* @author supot
* @version 1.0
*/
public abstract class RedisCommand {

	private RedisCommands<String, String> redisCommand;

	public abstract void setRedisCommand(RedisCommands<String, String> redisCommand);
	
	public RedisCommands<String, String> redisCommand() {
		return redisCommand;
	}
	
}
