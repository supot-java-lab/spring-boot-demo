/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.redis.service;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import th.co.cana.redis.data.User;

/**
* @author supot
* @version 1.0
*/
@Service
public class UserServiceImpl extends BaseService implements UserService {

	@Override
	public User save(User user) {
		return null;
	}

	@Override
	public Optional<User> update(String id, User user) {
		return Optional.empty();
	}

	@Override
	public void delete(String id) {
		
	}

	@Override
	public void deleteAll() {
		
	}

	@Override
	public List<User> findUsers() {
		return null;
	}

	@Override
	public Optional<User> find(String id) {
		return Optional.empty();
	}

}
