/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.redis.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;

import io.lettuce.core.RedisClient;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;
import th.co.cana.redis.config.properties.RedisCustomProperties;

/**
* @author supot
* @version 1.0
*/

@Configuration
@EnableRedisRepositories
public class RedisConfig {
	
	@Autowired
	private RedisCustomProperties redisCustomProperties;
	
	@Bean(name = "redisCommand")
	public RedisCommands<String, String> redisCommand() {
		StatefulRedisConnection<String, String> conn = redisClient().connect();		
		return conn.sync();
	}
	
	@Bean
	public RedisClient redisClient() {
		String url = redisCustomProperties.getRedisUrl();
		return RedisClient.create(url);
	}
}
