/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.redis.data;

import java.util.Date;

import org.springframework.data.redis.core.RedisHash;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
* @author supot
* @version 1.0
*/

@Data
@EqualsAndHashCode(callSuper = true)
@ToString
@RedisHash("user")
public class User extends RedisEntity {
	private static final long serialVersionUID = 1L;

	private String userId;
	private String username;
	private String password;
	private String firstName;
	private String lastName;
	private String email;
	private Date register;
}
