/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.redis.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import io.lettuce.core.RedisCommandExecutionException;
import io.lettuce.core.api.sync.RedisCommands;
import th.co.cana.redis.exception.JsonException;
import th.co.cana.redis.utils.JsonUtils;
import th.co.cana.utils.Validators;


/**
* @author supot
* @version 1.0
*/
@Component
public class RedisService extends BaseService {

	@Autowired
	@Qualifier("redisCommand")
	private RedisCommands<String, String> redisCommand;
	
	/**
	 * Set the Object value as Json format of a hash field.
	 * @param key The key
	 * @param field The field
	 * @param value The value of an object
	 * @return true if field is a new field in the hash and value was set. 
	 *  false if field already exists in the hash and the value was updated. Or error when set value
	 */
	public boolean setValue(String key, String field, Object value) {
		if (Validators.isEmptyOne(key, field)) {
			return false;
		}
		try {
			String json = JsonUtils.toJson(value);
			
			logger.info("Store key : {} \n Field : {} \n Value : {}", key, field, json);
			
			Boolean success = redisCommand.hset(key, field, json);
			return (success != null && success.booleanValue());
		} catch (JsonException ex) {
			logger.error("setValue", ex);
		}
		
		return false;
	}
	
	/**
	 * Set the Object value as Json format of a key.
	 * @param key The key of value
	 * @param value The value of an object
	 * @return true if SET was executed correctly. false if other's
	 */
	public boolean setValue(String key, Object value) {
		if (Validators.isEmpty(key)) {
			return false;
		}
		try {
			String json = JsonUtils.toJson(value);
			
			logger.info("Store key : {} \n Value : {}", key, json);
			
			String result = redisCommand.set(key, json);			
			return "OK".equalsIgnoreCase(result);
		} catch (JsonException ex) {
			logger.error("setValue", ex);
		}
		
		return false;
	}
	
	/**
	 * Append one or multiple values to a list.
	 * @param key
	 * @param values
	 * @return
	 */
	public long push(String key, Object ... values) {
		Long result = redisCommand.rpush(key, toJsonArrays(values));
		return result != null ? result.longValue() : 0L;
	}
	
	/**
	 * Find all keys matching the given pattern.
	 * @param pattern
	 * @return
	 */
	public List<String> getKeys(String pattern) {
		List<String> results = redisCommand.keys(pattern);		
		return (results == null ? Collections.emptyList() : results);
	}
	
	/**
	 * Get all the fields in a hash.
	 * @param key The key of HASH
	 * @return All list of fields in the hash, or an empty list when key does not exist.
	 */
	public List<String> getFields(String key) {
		List<String> results = redisCommand.hkeys(key);
		return (results == null ? Collections.emptyList() : results);
	}
	
	/**
	 * Get the value of a key.
	 * @param key The key for get value
	 * @return The value of key, or null when key does not exist.
	 */
	public String getValue(String key) {
		return getValue(key, String.class);
	}
	
	/**
	 * Get the value of a key. and auto convert to Java object when value is json data represent
	 * @param <T> The type of class to convert
	 * @param key The key for get value
	 * @param clazz The class to be convert
	 * @return The value of key, or null when key does not exist.
	 */
	public <T> T getValue(String key, Class<T> clazz) {
		if (Validators.isNullOne(key, clazz)) {
			logger.warn("Empty key or convertor class. Return null");
			return null;
		}
		
		String value = redisCommand.get(key);
		if (Validators.isNull(value)) {
			return null;
		}
		
		try {
			logger.info("Get value of key : {} -> convert to : {}", key, clazz.getName());			
			return JsonUtils.fromJson(value, clazz);
		} catch (JsonException ex) {
			logger.error("getValue", ex);
			return null;
		}
	}
	
	/**
	 * Get the value of a key.
	 * @param key The key for get value
	 * @param field The field type
	 * @return The value of key, or null when key does not exist.
	 */
	public String getValue(String key, String field) {
		return getValue(key, field, String.class);
	}
	
	/**
	 * Get the value of a hash field. And auto convert to Java object when value is json data represent
	 * @param <T> The type of class to convert
	 * @param key The key for get value
	 * @param field The field type
	 * @param clazz The class to be convert
	 * @return The value associated with field, or null when field is not presenting the hash or key does not exist.
	 */
	public <T> T getValue(String key, String field, Class<T> clazz) {
		if (Validators.isEmptyOne(key, field, clazz)) {
			logger.warn("Empty key, field or convertor class. Return null");
			return null;
		}
		
		String value = redisCommand.hget(key, field);
		if (Validators.isNull(value)) {
			return null;
		}
		
		try {
			logger.info("Get value of key/field : {}:{} -> convert to : {}"
					, key, field, clazz.getName());
			
			return JsonUtils.fromJson(value, clazz);
		} catch (JsonException ex) {
			logger.error("getValue", ex);
		}
		
		return null;
	}
	
	/**
	 * Get all the fields and values as String in a hash.
	 * @param key The key for get value
	 * @return All values stored in the hash, or an empty list when key does not exist.
	 */
	public List<String> getValues(String key) {
		return getValues(key, String.class);
	}
	
	/**
	 * Get all the fields and values in a hash. And auto convert to Java object when value is json data represent
	 * @param <T> The type of class to convert
	 * @param key The key for get value
	 * @param clazz The class to be convert
	 * @return All values stored in the hash, or an empty list when key does not exist.
	 */
	public <T> List<T> getValues(String key, Class<T> clazz) {
		if (Validators.isEmptyOne(key, clazz)) {
			logger.warn("Empty key or convertor class. Return null");
			return Collections.emptyList();
		}
				
		try {
			Map<String, String> maps = redisCommand.hgetall(key);
			if (Validators.isEmpty(maps)) {
				return Collections.emptyList();
			}
			
			logger.info("Found total rows : {} value of : {}", maps.size(), key);
			
			List<T> items = new ArrayList<>();
			for (Map.Entry<String, String> map : maps.entrySet()) {
				T obj = JsonUtils.fromJson(map.getValue(), clazz);
				if (obj != null) {
					items.add(obj);
				}
			}
			
			return items;
		} catch (RedisCommandExecutionException ex) {
			logger.error("getValue[RedisCommandExecutionException]", ex);
		} catch (JsonException ex) {
			logger.error("getValue[JsonException]", ex);
		}
		
		return Collections.emptyList();
	}
	
	/**
	 * Remove and get the first element in a list.
	 * @param key
	 * @return
	 */
	public String listPop(String key) {
		return listPop(key, String.class);
	}
	
	/**
	 * Remove and get the first element in a list.
	 * @param <T>
	 * @param key
	 * @return
	 */
	public <T> T listPop(String key, Class<T> clazz) {
		if (Validators.isEmptyOne(key, clazz)) {
			logger.warn("Empty key or convertor class. Return null");
			return null;
		}
		
		try {
			String value = redisCommand.lpop(key);
			if (Validators.isEmpty(value)) {
				return null;
			}
			
			logger.info("list pop value of key : {} -> convert to : {}"
					, key, clazz.getName());			
			return JsonUtils.fromJson(value, clazz);
		} catch (JsonException ex) {
			logger.error("getValue", ex);
		}
		
		return null;
	}
	
	/**
	 * Delete one or more keys.
	 * @param keys 
	 * @return The number of keys that were removed.
	 */
	public long delete(String... keys) {
		Long result = redisCommand.del(keys);
		return result != null ? result.longValue() : 0L;
	}
	
	/**
	 * Delete one or more hash fields.
	 * @param key The key
	 * @param fields The field in HASH key
	 * @return The number of fields that were removed from the hash.
	 */
	public long delete(String key, String... fields) {
		Long result = redisCommand.hdel(key, fields);
		return result != null ? result.longValue() : 0L;
	}
	
	/**
	 * Determine input keys exist.
	 * @param keys
	 * @return true if existing keys.
	 */
	public boolean exists(String... keys) {
		long total = getExistsTotal(keys);
		return total > 0;
	}
	
	/**
	 * Determine how many keys exist.
	 * @param keys
	 * @return total number of existing keys.
	 */
	public long getExistsTotal(String... keys) {
		if (Validators.isEmpty(keys)) {
			return 0L;
		}
		Long total = redisCommand.exists(keys);
		return (total == null ? 0L : total.longValue());
	}
	
    public boolean setExpire(String key, long seconds) {
    	return false;
    }

    public boolean setExpireAt(String key, Date date) {
    	return false;
    }
    
    public boolean setExpireAt(String key, long timestamp) {
    	return false;
    }
    
	private String[] toJsonArrays(Object... values) {
		if (Validators.isEmpty(values)) {
			return new String[] {};
		}

		List<String> jsonItems = new ArrayList<>();	
		for (Object obj : values) {
			try {
				String json = JsonUtils.toJson(obj);
				jsonItems.add(json);
			} catch (JsonException ex) {
				logger.error("toJsonArrays[{}] : {}", obj, ex.getMessage());
			}
		}
		
		return jsonItems.toArray(new String[jsonItems.size()]);
	}
}
