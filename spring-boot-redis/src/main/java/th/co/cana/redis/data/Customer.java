/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.redis.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.data.redis.core.RedisHash;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author supot
 * @version 1.0
 */

@Data
@EqualsAndHashCode(callSuper = true)
@ToString
@RedisHash("customer")
public class Customer extends RedisEntity {
	private static final long serialVersionUID = 1L;

	private String firstName;
	private String lastName;
	private String email;
	private Date createdDt;
	private List<Address> addreess;
	
	public void addAddress(Address bean) {
		if (bean == null) {
			return;
		}
		if (addreess == null) {
			addreess = new ArrayList<>();
		}
		addreess.add(bean);
	}
	
	@Data
	public static class Address implements Serializable {
		private static final long serialVersionUID = 1L;
		
		private String no;
		private String add;
		private String tumbon;
		private String amphur;
		private String province;
		private String posCode;
	}
}
