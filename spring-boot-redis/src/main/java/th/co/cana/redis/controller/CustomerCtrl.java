/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.redis.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import th.co.cana.redis.data.Customer;
import th.co.cana.redis.service.CustomerService;

/**
 * @author supot
 * @version 1.0
 */

@RestController
@RequestMapping("/v1/customers")
public class CustomerCtrl extends Controller {
	@Autowired
	private CustomerService customerService;

	@GetMapping()
	public ResponseEntity<List<Customer>> getCustomers() {
		List<Customer> customers = customerService.findCustomers();
		return ResponseEntity.ok(customers);
	}

	@GetMapping(params = "name")
	public ResponseEntity<List<Customer>> getCustomers(@RequestParam String name) {
		List<Customer> customers = customerService.findByFirstName(name);
		return ResponseEntity.ok(customers);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Customer> getCustomer(@PathVariable String id) {
		Optional<Customer> customer = customerService.findCustomer(id);
		if (!customer.isPresent()) {
			return ResponseEntity.badRequest().build();
		}
		return ResponseEntity.ok().body(customer.get());
	}

	@PostMapping()
	public ResponseEntity<Customer> create(@RequestBody Customer body) {
		Customer customer = customerService.save(body);
		return ResponseEntity.status(HttpStatus.CREATED).body(customer);
	}

	@PutMapping("/{id}")
	public ResponseEntity<Customer> update(@PathVariable String id, @RequestBody Customer body) {
		Optional<Customer> customer = customerService.update(id, body);
		if (customer.isPresent()) {
			return ResponseEntity.ok().body(customer.get());
		}
		return ResponseEntity.ok().body(null);
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<String> delete(@PathVariable String id) {
		customerService.delete(id);
		return ResponseEntity.ok().build();
	}

}
