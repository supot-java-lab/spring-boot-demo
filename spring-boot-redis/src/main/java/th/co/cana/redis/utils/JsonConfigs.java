/*
 * ----------------------------------------------------------------------------
 * Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved
 * ----------------------------------------------------------------------------
 */
package th.co.cana.redis.utils;

/**
* @author Supot Saelao
* @version 1.0
*/
public final class JsonConfigs {
	private JsonConfigs() {}
	
	public static final String DATE_FM = "yyyy-MM-dd'T'HH:mm:ss'Z'"; //2012-04-23T18:25:43Z
}
