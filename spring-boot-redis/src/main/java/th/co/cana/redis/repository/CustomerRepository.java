/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.redis.repository;

import java.util.List;

import th.co.cana.redis.data.Customer;

/**
 * @author supot
 * @version 1.0
 */

public interface CustomerRepository extends RedisRepository<Customer> {
	List<Customer> findByFirstName(String firstName);
}
