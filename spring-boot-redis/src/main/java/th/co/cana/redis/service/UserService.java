/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.redis.service;

import java.util.List;
import java.util.Optional;

import th.co.cana.redis.data.User;

/**
* @author supot
* @version 1.0
*/
public interface UserService {
	User save(User user);

	Optional<User> update(String id, User user);

	void delete(String id);
	
	void deleteAll();
	
	List<User> findUsers();

	Optional<User> find(String id);
}
