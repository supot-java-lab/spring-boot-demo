/*
 * ----------------------------------------------------------------------------
 * Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved
 * ----------------------------------------------------------------------------
 */
package th.co.cana.redis.utils;

import java.io.IOException;
import java.text.SimpleDateFormat;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;

import th.co.cana.redis.exception.JsonException;

/**
 *
 * @author Supot Saelao
 * @version 1.0
 */
public final class JsonUtils {
	private static final ObjectMapper mapper;
	private static final ObjectMapper mapperSmile;
	
	private JsonUtils() {}
	
	static {
		mapper = new ObjectMapper();
		setJsonMapperConfig(mapper);
		
		mapperSmile = new ObjectMapper(new JsonFactory());
		setJsonMapperConfig(mapperSmile);
	}

	public static String toJson(Object obj) throws JsonException {
		return toJson(obj, false);
	}
	
	public static String toJson(Object obj, boolean prettyOutput) throws JsonException {
		String jsonData = null;
		try {
			if (prettyOutput) {
				jsonData = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
			} else {
				jsonData = mapper.writeValueAsString(obj);
			}
		} catch (JsonProcessingException ex) {
			throw new JsonException(ex);
		}

		return jsonData;
	}

	public static <T> T fromJson(String jsonData, Class<T> clazzz) throws JsonException {
		try {
			if (jsonData == null || jsonData.trim().isEmpty() || clazzz == null) {
				return null;
			}

			return mapper.readValue(jsonData.getBytes(), clazzz);
		} catch (IOException ex) {
			throw new JsonException(ex);
		}
	}

	public static <T> T fromJson(String jsonData, JavaType type) throws JsonException {
		try {
			if (jsonData == null || jsonData.trim().isEmpty()) {
				return null;
			}

			return mapper.readValue(jsonData.getBytes(), type);
		} catch (IOException ex) {
			throw new JsonException(ex);
		}
	}

	public static byte[] toJsonBytes(Object obj) throws JsonException {
		return toJsonBytes(obj, false);
	}
	
	public static byte[] toJsonBytes(Object obj, boolean prettyOutput) throws JsonException {
		byte[] jsonDatas = null;
		try {
			if (prettyOutput) {
				jsonDatas = mapperSmile.writerWithDefaultPrettyPrinter().writeValueAsBytes(obj);
			} else {
				jsonDatas = mapperSmile.writeValueAsBytes(obj);
			}
		} catch (JsonProcessingException ex) {
			throw new JsonException(ex);
		}

		return jsonDatas;
	}

	public static <T> T fromJson(byte[] jsonDatas, Class<T> clazzz) throws JsonException {
		try {
			if (jsonDatas == null || jsonDatas.length == 0 || clazzz == null) {
				return null;
			}

			return mapperSmile.readValue(jsonDatas, clazzz);
		} catch (IOException ex) {
			throw new JsonException(ex);
		}
	}
	
	public static <T> T fromJson(byte[] jsonDatas, JavaType type) throws JsonException {
		try {
			if (jsonDatas == null || jsonDatas.length == 0) {
				return null;
			}

			return mapper.readValue(jsonDatas, type);
		} catch (IOException ex) {
			throw new JsonException(ex);
		}
	}
	
	public static void setJsonMapperConfig(ObjectMapper mapper) {
		setJsonMapperConfig(mapper, true);
	}
	
	public static void setJsonMapperConfig(ObjectMapper mapper, boolean skipNullProperty) {
		setJsonMapperConfig(mapper, skipNullProperty, true);
	}
	
    public static void setJsonMapperConfig(ObjectMapper mapper, boolean skipNullProperty, boolean enableDefaultTyping) {
		mapper.setSerializationInclusion(Include.NON_EMPTY);
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.setDateFormat(new SimpleDateFormat(JsonConfigs.DATE_FM));	
		
		if (skipNullProperty) {
			mapper.setVisibility(mapper.getVisibilityChecker()
					.withFieldVisibility(Visibility.ANY)
					.withGetterVisibility(Visibility.NONE)
					.withSetterVisibility(Visibility.NONE)
					.withCreatorVisibility(Visibility.NONE)
					.withIsGetterVisibility(Visibility.NONE));
		}
	}
}
