/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.redis.repository;

import org.springframework.data.repository.CrudRepository;

import th.co.cana.redis.data.RedisEntity;

/**
* @author supot
* @version 1.0
*/

public interface RedisRepository<T extends RedisEntity> extends CrudRepository<T, String> {

}
