package th.co.myapp.vault.listener;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationEnvironmentPreparedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.core.env.PropertiesPropertySource;
import org.springframework.core.env.PropertySource;
import org.springframework.lang.NonNull;
import org.springframework.web.client.RestTemplate;

import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Slf4j
public class ConfigPropertyListener implements ApplicationListener<ApplicationEnvironmentPreparedEvent> {
    private static final String VAULT_CF_TP_KEY     = "spring.cloud.token";
    private static final String VAULT_API_U_KEY     = "api.vault-api.username";
    private static final String VAULT_API_P_KEY     = "api.vault-api.password";
    private static final String VAULT_API_URL_KEY   = "api.vault-api.base-url";
    private static final String VAULT_API_LOGIN_KEY = "api.vault-api.login-api";

    @Override
    public void onApplicationEvent(@NonNull ApplicationEnvironmentPreparedEvent event) {
        log.info("++++++++++++ ConfigPropertyListener initialized ++++++++++++");
        VaultProperty vaultProperty = getVaultProperty(event.getEnvironment().getPropertySources().iterator());

        String token = getAccessToken(vaultProperty);
        Properties properties = new Properties();
        properties.put(VAULT_CF_TP_KEY, token);
        event.getEnvironment().getPropertySources().addFirst(new PropertiesPropertySource("customVaultProperties", properties));
    }

    private String getAccessToken(final VaultProperty vaultProperty) {
        try {
            RestTemplate restTemplate = new RestTemplate();

            Map<String, String> req = new HashMap<>();
            req.put("password", vaultProperty.getPassword());
            log.info("vault login url: {}", vaultProperty.getLoginApiUrl());

            VaultLoginResp resp = restTemplate.postForObject(vaultProperty.getLoginApiUrl(), req, VaultLoginResp.class);
            log.info("vault login response: {}", resp);

            if (resp == null || resp.getAuth() == null) {
                return null;
            }

            return resp.getAuth().getClientToken();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
        return null;
    }

    private VaultProperty getVaultProperty(final Iterator<PropertySource<?>> iterators) {
        VaultProperty vaultProperty = new VaultProperty();
        if (iterators == null) {
            return vaultProperty;
        }

        while (iterators.hasNext()) {
            PropertySource<?> source = iterators.next();
            log.info("Starting reading vault login properties with name : {}", source.getName());

            if (vaultProperty.getUsername() == null || vaultProperty.getUsername().isEmpty()) {
                Object username = getValue(source, VAULT_API_U_KEY);
                if (username != null) {
                    vaultProperty.setUsername(username.toString());
                }
            }

            if (vaultProperty.getPassword() == null || vaultProperty.getPassword().isEmpty()) {
                Object password = getValue(source, VAULT_API_P_KEY);
                if (password != null) {
                    vaultProperty.setPassword(password.toString());
                }
            }

            if (vaultProperty.getLoginApiUrl() == null || vaultProperty.getLoginApiUrl().isEmpty()) {
                Object baseUrl = getValue(source, VAULT_API_URL_KEY);
                if (baseUrl != null) {
                    Object loginAPi = getValue(source, VAULT_API_LOGIN_KEY);
                    if (loginAPi != null) {
                        String url = baseUrl + MessageFormat.format(loginAPi.toString(), vaultProperty.getUsername());
                        vaultProperty.setLoginApiUrl(url);
                    }
                }
            }

            if (vaultProperty.isValidValue()) {
                log.info("Finished reading vault login properties with name : {}", source.getName());
                break;
            }
        }

        return vaultProperty;
    }

    private Object getValue(PropertySource<?> source, String key) {
        if (source.containsProperty(key)) {
            return source.getProperty(key);
        }
        return null;
    }

    @Getter
    @Setter
    public static final class VaultProperty {
        private String username;
        private String password;
        private String loginApiUrl;

        public boolean isValidValue() {
            return username != null && !username.isEmpty()
                    && password != null && !password.isEmpty()
                    && loginApiUrl != null && !loginApiUrl.isEmpty();
        }
    }

    @Getter
    @Setter
    @ToString
    public static final class VaultLoginResp {

        @JsonProperty("request_id")
        private String requestId;
        private Authentication auth = new Authentication();

        @Getter
        @Setter
        @ToString
        public static final class Authentication {
            @JsonProperty("client_token")
            private String clientToken;
        }
    }

}
