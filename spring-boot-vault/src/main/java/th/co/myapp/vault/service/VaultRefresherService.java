package th.co.myapp.vault.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Slf4j
@Component
public class VaultRefresherService {

    @Scheduled(initialDelayString="180000", fixedDelayString = "180000")
    public void refresher() {
        log.info("refresh key-value secret");
    }
}
