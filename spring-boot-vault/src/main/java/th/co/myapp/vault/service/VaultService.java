package th.co.myapp.vault.service;

import io.github.jdevlibs.utils.Validators;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.vault.core.VaultKeyValueOperationsSupport;
import org.springframework.vault.core.VaultTemplate;
import org.springframework.vault.support.VaultResponse;

import java.util.HashMap;
import java.util.Map;

/**
 * @author supot.jdev
 * @version 1.0
 */
@RequiredArgsConstructor
@Slf4j
@Service
public class VaultService {
    private final VaultTemplate vaultTemplate;

    @Value("${spring.config.import}")
    private String vaultPath;

    public void initialVaultCache() {
        try {
            if (Validators.isNotEmpty(vaultPath)) {
                vaultPath = vaultPath.replace("vault://secret/", "");
                log.info("Vault path: {}", vaultPath);
            }

            Map<String, Object> configs = getSecretConfig(vaultPath);
            if (Validators.isNotEmpty(configs)) {
                for (Map.Entry<String, Object> entry : configs.entrySet()) {
                    log.info("Key[{}] -> {}", entry.getKey(), entry.getValue());
                }
            }
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
        }
    }

    public Map<String, Object> getSecretConfig(String path) {
        try {
            VaultResponse response = getVaultResponse(path);
            return response == null ? new HashMap<>() : response.getData();
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            return new HashMap<>();
        }
    }

    private VaultResponse getVaultResponse(String path) {
        return vaultTemplate.opsForKeyValue("secret", VaultKeyValueOperationsSupport.KeyValueBackend.KV_2).get(path);
    }
}