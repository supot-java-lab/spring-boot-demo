package th.co.myapp.vault.config.properties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Getter
@Setter
@ToString
@Configuration
@ConfigurationProperties(prefix = "srs.eapi")
public class VaultApiEapiProperties {
    private String apiKey;
    private String apiSecretKey;
}
