package th.co.myapp.vault.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Configuration
@EnableScheduling
public class ApplicationConfig {

}
