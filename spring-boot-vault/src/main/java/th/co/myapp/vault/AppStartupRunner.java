package th.co.myapp.vault;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import th.co.myapp.vault.config.properties.VaultApiEapiProperties;
import th.co.myapp.vault.config.properties.VaultApplicationProperties;
import th.co.myapp.vault.config.properties.VaultDatabaseProperties;
import th.co.myapp.vault.service.VaultService;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class AppStartupRunner implements CommandLineRunner {
    private final VaultApplicationProperties vaultApplication;
    private final VaultDatabaseProperties vaultDatabase;
    private final VaultApiEapiProperties vaultEnterpriseApi;
    private final VaultService vaultService;

    @Override
    public void run(String... args) {
        log.info("vaultApplication : {}", vaultApplication);
        log.info("vaultDatabase : {}", vaultDatabase);
        log.info("vaultEnterpriseApi : {}", vaultEnterpriseApi);

        vaultService.initialVaultCache();
    }
}
