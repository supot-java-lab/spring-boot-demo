package th.co.myapp.vault.config.properties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "api")
public class ApiConfigProperties {

    private VaultApi vaultApi = new VaultApi();

    @Getter
    @Setter
    @ToString
    public static class DefaultApi {
        private String baseUrl;
        private String apiKey;
        private String apiSecretKey;

        public String apiUrl(String api) {
            return baseUrl + api;
        }
    }

    @Getter
    @Setter
    @ToString
    public static class VaultApi extends DefaultApi {
        private String username;
        private String password;
        private String loginApi;
        private String vaultDataApi;

        public String loginApiUrl() {
            return apiUrl(loginApi);
        }

        public String vaultDataApiUrl() {
            return apiUrl(vaultDataApi);
        }
    }
}
