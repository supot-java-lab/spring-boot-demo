package th.co.myapp.vault.config.properties;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.io.Serializable;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "application")
public class ApplicationConfigProperties implements Serializable {
    private String name;
    private String version;
    private String buildTime;
    private String javaBuildVersion;

    private String secretKey;
    private boolean enabledCache;
    private boolean enabledPerformanceAudit;
}
