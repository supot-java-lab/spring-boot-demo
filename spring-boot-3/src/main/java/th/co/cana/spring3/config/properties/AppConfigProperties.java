package th.co.cana.spring3.config.properties;

import lombok.Data;

import java.io.Serializable;

/**
 * @author supot.jdev
 * @version 1.0
 */

@Data
public class AppConfigProperties implements Serializable {
    private boolean enabledApiLog;
    private boolean enabledPerformanceAudit;
    private boolean enabledToken;
}
