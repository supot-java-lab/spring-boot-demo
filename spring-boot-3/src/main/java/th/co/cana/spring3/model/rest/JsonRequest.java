package th.co.cana.spring3.model.rest;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class JsonRequest<T> extends Request {
    private T model;
}
