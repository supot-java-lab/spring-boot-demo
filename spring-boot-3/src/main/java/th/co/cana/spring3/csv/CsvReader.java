package th.co.cana.spring3.csv;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.bean.CsvToBeanBuilder;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CsvReader {
    private boolean includeHeader;
    private Character separator;
    private String encode;

    public List<String[]> readAll(String csvFile) throws th.co.cana.spring3.csv.CsvException {
        return readAll(csvFile, 0);
    }

    public List<String[]> readAll(String csvFile, int skipLine) throws CsvException {
        try (InputStream ins = Files.newInputStream(Paths.get(csvFile))) {
            return readAll(ins, skipLine);
        } catch (IOException ex) {
            throw new CsvException(ex);
        }
    }

    public List<String[]> readAll(InputStream ins) throws CsvException {
        return readAll(ins, 0);
    }

    public List<String[]> readAll(InputStream ins, int skipLine) throws CsvException {
        try (Reader reader = createReader(ins)) {
            final CSVParser parser = new CSVParserBuilder().withSeparator(csvSeparator()).build();
            try (CSVReader csvReader = new CSVReaderBuilder(reader).withCSVParser(parser).withSkipLines(skipLine).build()) {
                return csvReader.readAll();
            }
        } catch (IOException | com.opencsv.exceptions.CsvException ex) {
            throw new CsvException(ex);
        }
    }

    public <T> List<T> readToBean(String csvFile, Class<T> clazz) throws CsvException {
        return readToBean(csvFile, 0, clazz);
    }

    public <T> List<T> readToBean(String csvFile, int skipLine, Class<T> clazz) throws CsvException {
        try (InputStream ins = Files.newInputStream(Paths.get(csvFile))) {
            return readToBean(ins, skipLine, clazz);
        } catch (IOException ex) {
            throw new CsvException(ex);
        }
    }

    public <T> List<T> readToBean(InputStream ins, Class<T> clazz) throws CsvException {
        return readToBean(ins, 0, clazz);
    }

    public <T> List<T> readToBean(InputStream ins, int skipLine, Class<T> clazz) throws CsvException {
        try (Reader reader = createReader(ins)) {
            CsvToBeanBuilder<T> builder = new CsvToBeanBuilder<T>(reader)
                    .withSeparator(csvSeparator())
                    .withSkipLines(skipLine)
                    .withType(clazz);
            return builder.build().parse();
        } catch (IOException ex) {
            throw new CsvException(ex);
        }
    }

    private Reader createReader(InputStream ins) throws IOException {
        return isEmptyEncode() ? new InputStreamReader(ins) : new InputStreamReader(ins, encode);
    }

    private char csvSeparator() {
        return (separator == null ? ',' : separator);
    }
    private boolean isEmptyEncode() {
        return (encode == null || encode.isEmpty());
    }
}
