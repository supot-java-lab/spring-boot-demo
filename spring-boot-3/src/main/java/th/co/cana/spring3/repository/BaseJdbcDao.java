package th.co.cana.spring3.repository;

import io.github.jdevlibs.spring.jdbc.JdbcDao;
import io.github.jdevlibs.spring.jdbc.criteria.Criteria;
import io.github.jdevlibs.spring.jdbc.criteria.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Service
public class BaseJdbcDao extends JdbcDao {

    @Autowired
    @Override
    protected void autowiredJdbcTemplate(JdbcTemplate jdbcTemplate) {
        setJdbcTemplate(jdbcTemplate);
    }

    @Override
    protected void setPagingOption(StringBuilder sql, Parameter params, Criteria paging) {
        //TODO : wait for implement paging
    }
}
