package th.co.cana.spring3.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import th.co.cana.spring3.utils.MessageUtils;

import java.util.concurrent.Executor;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Configuration
@EnableAsync
public class ApplicationConfig {
    @Autowired
    public ApplicationConfig(MessageSource messageSource) {
        MessageUtils.setMessageSource(messageSource);
    }

    @Bean
    public Executor taskExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(2);
        executor.setMaxPoolSize(2);
        executor.setQueueCapacity(500);
        executor.setThreadNamePrefix("spring3-task-");
        executor.initialize();

        return executor;
    }

}