package th.co.cana.spring3.repository;

import io.github.jdevlibs.spring.jdbc.criteria.NameParameter;
import io.github.jdevlibs.utils.Validators;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.stereotype.Repository;
import th.co.cana.spring3.criteria.CustomerCriteria;
import th.co.cana.spring3.csv.CsvException;
import th.co.cana.spring3.csv.CsvWriter;
import th.co.cana.spring3.entity.Customer;
import th.co.cana.spring3.model.CustomerCsv;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.ResultSet;
import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Repository
public class CustomerDaoImpl extends BaseJdbcDao implements CustomerDao {
    @Override
    public List<Customer> search(CustomerCriteria criteria) {
        NameParameter params = new NameParameter(5);
        StringBuilder sql = new StringBuilder();
        sql.append("SELECT CUSTOMER_ID")
                .append(", FIRST_NAME")
                .append(", LAST_NAME")
                .append(", BIRTH_DATE")
                .append(", CREDIT_AMT")
                .append(", CREATED_BY")
                .append(", CREATED_DATE")
                .append(", UPDATED_BY")
                .append(", UPDATED_DATE");
        sql.append(" FROM CUSTOMER");
        sql.append(" WHERE 1=1");

        if (Validators.isNotNull(criteria.getCustomerId())) {
            sql.append(" AND CUSTOMER_ID = :P_CUSTOMER_ID");
            params.add("P_CUSTOMER_ID", criteria.getCustomerId());
        }
        if (Validators.isNotEmpty(criteria.getFirstName())) {
            sql.append(" AND FIRST_NAME LIKE :P_FIRST_NAME");
            params.add("P_FIRST_NAME", sqlLikeContain(criteria.getFirstName()));
        }
        if (Validators.isNotEmpty(criteria.getLastName())) {
            sql.append(" AND LAST_NAME LIKE :P_LAST_NAME");
            params.add("P_LAST_NAME", sqlLikeContain(criteria.getLastName()));
        }
        if (Validators.isNotNull(criteria.getBirthDate())) {
            sql.append(" AND BIRTH_DATE = :P_BIRTH_DATE");
            params.add("P_BIRTH_DATE", criteria.getBirthDate());
        }

        return queryToList(sql.toString(), params, Customer.class);
    }

    @Override
    public List<CustomerCsv> getCustomerCsv() {
        NameParameter params = new NameParameter(5);
        String sql = "SELECT CUSTOMER_ID" +
                "	, FIRST_NAME" +
                "	, LAST_NAME" +
                "	, BIRTH_DATE" +
                "	, CREDIT_AMT" +
                " FROM CUSTOMER C" +
                " WHERE 1 = 1";
        return queryToList(sql, params, CustomerCsv.class);
    }

    @Override
    public byte[] exportCsv() {
        String sql = "SELECT CUSTOMER_ID" +
                "	, FIRST_NAME" +
                "	, LAST_NAME" +
                "	, BIRTH_DATE" +
                "	, CREDIT_AMT" +
                " FROM CUSTOMER C" +
                " WHERE 1 = ?";

       return getJdbcTemplate().execute(sql, (PreparedStatementCallback<byte[]>) ps -> {
            ps.setInt(1, 1);
            ResultSet rs = ps.executeQuery();

            CsvWriter writer = CsvWriter.builder().build();
            try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
                writer.write(rs, out);
                return out.toByteArray();
            } catch (CsvException | IOException e) {
                throw new RuntimeException(e);
            }
       });
    }
}
