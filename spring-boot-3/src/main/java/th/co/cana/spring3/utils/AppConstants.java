package th.co.cana.spring3.utils;

import java.util.Locale;

/**
 * @author supot.jdev
 * @version 1.0
 */
public final class AppConstants {

    private AppConstants() {
    }

    public static final String Y = "Y";
    public static final String N = "N";
    public static final String C = "C";
    public static final String T = "T";

    public static final String UTF8 = "UTF-8";
    public static final Locale US = Locale.US;
    public static final Locale TH = new Locale("th", "TH");

    /**
     * AOP : Aspect Oriented Programming package
     */
    public static final class Aspects {
        private Aspects() {}

        public static final String SERVICE 	= "execution(public * th.co.cana.spring3.service..*.*(..))";
        public static final String REPOSITORY = "execution(public * th.co.cana.spring3.repository..*.*(..))";
        public static final String REST_LOGGER = "execution(@th.co.cana.spring3.spring.RestApiLogger * *(..))";
    }

    /**
     * API base path for the application
     */
    public static final class Apis {
        private Apis() {}
        public static final String ERROR 	= "/error";
        public static final String DEMO = "/demo";
        public static final String CUSTOMER = "/customer";
    }

    public static final class ApiDocs {
        private ApiDocs() {}

        public static final String SECURITY_KEY = "bearer-key";
        public static final String AUTH 	= "/auth/**";
        public static final String ERROR 	= "/error/**";
    }

    /**
     * API request/response type for logging
     */
    public static final class ApiTypes {
        private ApiTypes() {}

        public static final String REQUEST 	= "REQ";
        public static final String RESPONSE = "RES";
    }

    /**
     * API operation method for logging
     */
    public static final class ApiMethods {
        private ApiMethods() {}

        public static final String GET 		= "GET";
        public static final String POST 	= "POST";
        public static final String PUT 		= "PUT";
        public static final String DELETE 	= "DELETE";
        public static final String OPTION 	= "OPTION";
    }

    public static final class ApiResponseError {
        public static final String ERR_CODE = """
                {
                  "timestamp": "2023-02-05T04:09:36.029Z",
                  "status": XXX,
                  "message": "string",
                  "path": "string",
                  "errors": [
                    "string"
                  ],
                  "success": false
                }""";
    }
}