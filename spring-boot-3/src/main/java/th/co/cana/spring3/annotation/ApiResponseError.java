package th.co.cana.spring3.annotation;

import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import th.co.cana.spring3.model.rest.Response;
import th.co.cana.spring3.utils.AppConstants;

import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE, ElementType.METHOD})
@Documented
@ApiResponses(value = {
        @ApiResponse(responseCode = "[400, 401, 403, 404, 500]", description = "If operation not success.", content = {
                @Content(mediaType = "application/json", schema = @Schema(implementation = Response.class)
                        , examples = {@ExampleObject(value = AppConstants.ApiResponseError.ERR_CODE)})}
        )
})
public @interface ApiResponseError {
}
