package th.co.cana.spring3.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class CustomerPaging extends PagingCriteria {
    private String firstName;
    private String lastName;
}
