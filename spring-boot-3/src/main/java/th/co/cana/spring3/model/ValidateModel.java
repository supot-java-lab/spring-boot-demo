package th.co.cana.spring3.model;

import jakarta.validation.constraints.*;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
public class ValidateModel implements Serializable {
    @Min(value = 1)
    private Long id;

    @NotNull
    private String firstName;

    @NotBlank
    private String lastName;

    @Email
    private String email;

    @AssertTrue
    private boolean working;

    @Size(min = 10, max = 200)
    private String aboutMe;

    @Min(value = 18)
    @Max(value = 150)
    private int age;

    @PositiveOrZero
    private BigDecimal creditAmt;

    @Past
    private LocalDate birtDate;

    @FutureOrPresent
    private LocalDateTime createDate;
}
