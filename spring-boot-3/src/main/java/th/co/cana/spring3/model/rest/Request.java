package th.co.cana.spring3.model.rest;

import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
public class Request implements Serializable {

    private Map<String, String> headers;

    public void addHeader(String name, String value) {
        if (name == null || name.isEmpty()) {
            return;
        }
        if (headers == null) {
            headers = new HashMap<>();
        }
        headers.put(name, value);
    }

    public void authorizationBearer(String token) {
        addHeader("Authorization", "Bearer " + token);
    }

    public void authorizationBasic(String token) {
        addHeader("Authorization", "Basic " + token);
    }

    public void clearHeader() {
        headers = null;
    }
}