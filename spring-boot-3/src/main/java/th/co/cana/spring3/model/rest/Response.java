package th.co.cana.spring3.model.rest;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
public class Response implements Serializable {

    private LocalDateTime timestamp = LocalDateTime.now();
    private int status;
    private String message;
    private String path;
    private List<String> errors = new ArrayList<>();
    private boolean success = false;

    public Response() {
        super();
    }

    public Response(int status) {
        super();
        this.status = status;
    }

    public Response(int status, String message) {
        super();
        this.status = status;
        this.message = message;
    }

    public Response(int status, String message, List<String> errors) {
        super();
        this.status = status;
        this.message = message;
        this.errors = errors;
    }

    public void addError(Object... errs) {
        if (errs == null || errs.length == 0) {
            return;
        }

        if (errors == null) {
            errors = new ArrayList<>();
        }

        for (Object err : errs) {
            if (err != null && !err.toString().isEmpty()) {
                errors.add(String.valueOf(err));
            }
        }
    }
}
