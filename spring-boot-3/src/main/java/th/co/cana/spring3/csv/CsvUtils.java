package th.co.cana.spring3.csv;

import io.github.jdevlibs.utils.Convertors;
import io.github.jdevlibs.utils.Validators;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * @author supot.jdev
 * @version 1.0
 */
public final class CsvUtils {
    private static final String DEF_STR_VAL = "";

    private CsvUtils() {
    }

    public static String getValue(final String[] values, int inx) {
        return getValue(values, inx, 0, DEF_STR_VAL);
    }

    public static String getValue(final String[] values, int inx, int maxLen) {
        return getValue(values, inx, maxLen, DEF_STR_VAL);
    }

    public static String getValue(final String[] values, int inx, int maxLen, String defVal) {
        if (isEmpty(values) || inx < 0) {
            return defVal;
        }

        if (values.length <= inx) {
            return defVal;
        }

        String val = values[inx];
        if (isEmpty(val)) {
            return defVal;
        }

        if (maxLen <= 0 || val.length() < maxLen) {
            return val.trim();
        } else {
            return val.trim().substring(0, maxLen);
        }
    }

    public static String getValue(final Object[] values, int inx) {
        return getValue(values, inx, 0, DEF_STR_VAL);
    }

    public static String getValue(final Object[] values, int inx, int maxLen) {
        return getValue(values, inx, maxLen, DEF_STR_VAL);
    }

    public static String getValue(final Object[] values, int inx, int maxLen, String defVal) {
        if (isEmpty(values) || inx < 0) {
            return defVal;
        }

        if (values.length <= inx) {
            return defVal;
        }

        String val = Convertors.toString(values[inx], defVal);
        if (Validators.isEmpty(val)) {
            return defVal;
        }

        if (maxLen <= 0 || val.length() < maxLen) {
            return val.trim();
        } else {
            return val.trim().substring(0, maxLen);
        }
    }

    public static int getInt(final String[] values, int inx) {
        String val = getValue(values, inx, 0, DEF_STR_VAL);
        if (isEmpty(val)) {
            return 0;
        }

        try {
            return Integer.parseInt(val);
        } catch (Exception ex) {
            return 0;
        }
    }

    public static Integer getInteger(final String[] values, int inx) {
        return getInteger(values, inx, null);
    }

    public static Integer getInteger(final String[] values, int inx, Integer defVal) {
        String val = getValue(values, inx, 0, DEF_STR_VAL);
        if (isEmpty(val)) {
            return defVal;
        }

        try {
            return Integer.valueOf(val);
        } catch (Exception ex) {
            return defVal;
        }
    }

    public static BigDecimal getBigDecimal(final String[] values, int inx) {
        return getBigDecimal(values, inx, null);
    }

    public static BigDecimal getBigDecimal(final String[] values, int inx, BigDecimal defVal) {
        String val = getValue(values, inx, 0, DEF_STR_VAL);
        if (isEmpty(val)) {
            return defVal;
        }

        try {
            return new BigDecimal(val);
        } catch (Exception ex) {
            return defVal;
        }
    }

    public static BigInteger getBigInteger(final String[] values, int inx) {
        return getBigInteger(values, inx, null);
    }

    public static BigInteger getBigInteger(final String[] values, int inx, BigInteger defVal) {
        String val = getValue(values, inx, 0, DEF_STR_VAL);
        if (isEmpty(val)) {
            return defVal;
        }

        try {
            return new BigInteger(val);
        } catch (Exception ex) {
            return defVal;
        }
    }

    public static Double getDouble(final String[] values, int inx) {
        return getDouble(values, inx, null);
    }

    public static Double getDouble(final String[] values, int inx, Double defVal) {
        String val = getValue(values, inx, 0, DEF_STR_VAL);
        if (isEmpty(val)) {
            return defVal;
        }

        try {
            return Double.valueOf(val);
        } catch (Exception ex) {
            return defVal;
        }
    }

    private static boolean isEmpty(Object value) {
        return (value == null);
    }

    private static boolean isEmpty(final String[] values) {
        return (values == null || values.length == 0);
    }
}
