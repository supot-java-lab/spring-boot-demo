package th.co.cana.spring3.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import th.co.cana.spring3.criteria.CustomerCriteria;
import th.co.cana.spring3.entity.Customer;
import th.co.cana.spring3.model.CustomerPaging;
import th.co.cana.spring3.model.Paging;
import th.co.cana.spring3.model.rest.Response;
import th.co.cana.spring3.service.CustomerService;
import th.co.cana.spring3.utils.AppConstants;
import th.co.cana.spring3.utils.ResponseUtils;

import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */
@RequiredArgsConstructor
@RestController
@RequestMapping(AppConstants.Apis.CUSTOMER)
public class CustomerCtrl extends ApiController {

    private final CustomerService customerService;

    @Operation(summary = "Search customer by keyword")
    @GetMapping("/")
    public ResponseEntity<List<Customer>> search(String keyword) {
        CustomerCriteria criteria = new CustomerCriteria();
        criteria.setFirstName(keyword);
        List<Customer> customers = customerService.search(criteria);
        return ok(customers);
    }

    @Operation(summary = "Filter customer by keyword ans sort")
    @GetMapping("/filter")
    public ResponseEntity<Paging<Customer>> search(@ParameterObject CustomerPaging pageable) {
        Paging<Customer> paging = customerService.search(pageable.toPageable());
        paging.setPageable(pageable);

        return ok(paging);
    }

    @Operation(summary = "Search customer by criteria")
    @ApiResponse(responseCode = "200", content = {
            @Content(mediaType = MediaType.APPLICATION_JSON_VALUE, array = @ArraySchema(schema = @Schema(implementation = Customer.class)))
    })
    @PostMapping("/criteria")
    public ResponseEntity<Object> search(@RequestBody CustomerCriteria criteria) {
        List<Customer> customers = customerService.search(criteria);
        return ok(customers);
    }

    @Operation(summary = "Get customer by id")
    @GetMapping("/{customerId}")
    public ResponseEntity<Customer> getCustomer(@PathVariable Long customerId) {
        Customer customer = customerService.findById(customerId);
        return ok(customer);
    }

    @Operation(summary = "Create new customer")
    @PostMapping("/")
    public ResponseEntity<Customer> save(@Valid @RequestBody Customer customer) {
        customer = customerService.save(customer);
        return ok(customer);
    }

    @Operation(summary = "Update customer")
    @ApiResponse(responseCode = "200", content = { @Content(mediaType = "application/json", schema = @Schema(implementation = Customer.class)) })
    @PutMapping("/{customerId}")
    public ResponseEntity<Object> update(@PathVariable Long customerId, @Valid @RequestBody Customer customer) {
        customer.setCustomerId(customerId);
        customer = customerService.update(customer);
        return ok(customer);
    }

    @Operation(summary = "Delete customer")
    @DeleteMapping("/{customerId}")
    public ResponseEntity<Response> delete(@PathVariable Long customerId) {
        Response resp = ResponseUtils.ok();
        resp.addError("customerId is never used " + customerId);
        return ok(resp);
    }
}
