package th.co.cana.spring3.excption;

/**
 * @author supot.jdev
 * @version 1.0
 */
public enum ErrorCodes {
    //Database
    CODE_DB_ERROR("000", "db.error"),
    CODE_DB_CONNECTION("100", "db.error.100"),
    CODE_DB_INSERT("101", "db.error.101"),
    CODE_DB_UPDATE("102", "db.error.102"),
    CODE_DB_DELETE("103", "db.error.103"),
    CODE_DB_QUERY("104", "db.error.104"),
    CODE_DB_UNIQUE("105", "db.error.105"),
    CODE_DB_EMPTY("106", "db.error.106"),
    CODE_DB_LARGER("107", "db.error.107"),
    CODE_DB_NULL("108", "db.error.108"),
    CODE_DB_OTHER("109", "db.error.109"),

    //API Response
    CODE_204("204", "resp.error.204"),
    CODE_400("400", "resp.error.400"),
    CODE_401("401", "resp.error.401"),
    CODE_402("404", "resp.error.404"),
    CODE_403("405", "resp.error.405"),
    CODE_406("406", "resp.error.406"),
    CODE_415("415", "resp.error.415"),
    CODE_500("500", "resp.error.500"),

    //External API
    CODE_API_ERROR("600", "resp.error.api.external"),
    CODE_API_MAIL("601", "resp.error.mail.notification"),
    CODE_API_SMS("602", "resp.error.sms.notification");

    final String code;
    final String message;
    ErrorCodes(String code, String message){
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
