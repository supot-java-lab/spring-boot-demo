/*
 * -----------------------------------------------------------------------------------
 * Copyright (c) 2023.  The Siam Commercial Bank Public Company Limited. All rights reserved.
 * -----------------------------------------------------------------------------------
 */
package th.co.cana.spring3.csv;

import com.opencsv.CSVWriter;
import io.github.jdevlibs.utils.FileUtils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.charset.UnsupportedCharsetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CsvWriter {
    private Character separator;
    private String encode;

    /**
     * Direct write JDBC ResultSet to csv file.
     * @param rs JDBC ResultSet
     * @param filename Output file name
     * @throws CsvException When exception
     */
    public void write(ResultSet rs, String filename) throws CsvException {
        write(rs, filename, true);
    }

    /**
     * Direct write JDBC ResultSet to csv file.
     * @param rs JDBC ResultSet
     * @param filename Output file name
     * @param includeColumnName Include result column.
     * @throws CsvException When exception
     */
    public void write(ResultSet rs, String filename, boolean includeColumnName) throws CsvException {
        try (Writer writer = craeteWriter(filename);
             CSVWriter csvWriter = createCSVWriter(writer)
        ) {
            csvWriter.writeAll(rs, includeColumnName, true);
        } catch (SQLException | IOException ex) {
            throw new CsvException(ex);
        }
    }

    /**
     * Direct write JDBC ResultSet to csv file.
     * @param rs JDBC ResultSet
     * @param out OutputStream
     * @throws CsvException When exception
     */
    public void write(ResultSet rs, OutputStream out) throws CsvException {
        write(rs, out, true);
    }

    /**
     * Direct write JDBC ResultSet to csv file.
     * @param rs JDBC ResultSet
     * @param out OutputStream
     * @param includeColumnName Include result column.
     * @throws CsvException When exception
     */
    public void write(ResultSet rs, OutputStream out, boolean includeColumnName) throws CsvException {
        try (OutputStreamWriter writer = createOutputWriter(out);
             CSVWriter csvWriter = createCSVWriter(writer)
        ) {
            csvWriter.writeAll(rs, includeColumnName, true);
        } catch (SQLException | IOException ex) {
            throw new CsvException(ex);
        }
    }

    /**
     * Write values csv file.
     * @param values The values
     * @param filename csv file name
     * @throws CsvException When exception
     */
    public void write(List<String[]> values, String filename) throws CsvException {
        try (Writer writer = craeteWriter(filename);
             CSVWriter csvWriter = createCSVWriter(writer)
        ) {
            csvWriter.writeAll(values);
        } catch (UnsupportedCharsetException | IOException ex) {
            throw new CsvException(ex);
        }
    }

    /**
     * Write values csv file.
     * @param values The values
     * @param out OutputStream
     * @throws CsvException When exception
     */
    public void write(List<String[]> values, OutputStream out) throws CsvException {
        try (OutputStreamWriter writer = createOutputWriter(out);
             CSVWriter csvWriter = createCSVWriter(writer)
        ) {
            csvWriter.writeAll(values);
        } catch (UnsupportedCharsetException | IOException ex) {
            throw new CsvException(ex);
        }
    }

    private Writer craeteWriter(String filename) throws IOException {
        FileUtils.mkdirByFile(filename);
        Path path = Paths.get(filename);
        if (encode == null || encode.isEmpty()) {
            return Files.newBufferedWriter(path);
        }
        return Files.newBufferedWriter(path, getCharset());
    }

    private OutputStreamWriter createOutputWriter(OutputStream out) throws IOException {
        if (isEmptyEncode()) {
            return new OutputStreamWriter(out);
        }
        return new OutputStreamWriter(out, encode);
    }

    private CSVWriter createCSVWriter(Writer writer) {
        return new CSVWriter(writer, separatorChar(),
                CSVWriter.NO_QUOTE_CHARACTER,
                CSVWriter.DEFAULT_ESCAPE_CHARACTER,
                CSVWriter.DEFAULT_LINE_END);
    }

    private Charset getCharset() {
        return Charset.forName(getEncode());
    }

    private char separatorChar() {
        return (separator == null ? ',' : separator);
    }

    private boolean isEmptyEncode() {
        return (encode == null || encode.isEmpty());
    }
}