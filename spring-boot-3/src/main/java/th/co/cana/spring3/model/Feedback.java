package th.co.cana.spring3.model;

import lombok.Data;

import java.io.Serializable;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
public class Feedback implements Serializable {
    private String name;
    private String email;
    private String comment;
}
