package th.co.cana.spring3.model;

import com.opencsv.bean.CsvBindByPosition;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
public class CustomerCsv {
    @CsvBindByPosition(position = 0)
    private Long customerId;

    @CsvBindByPosition(position = 1)
    private String firstName;

    @CsvBindByPosition(position = 2)
    private String lastName;

    @CsvBindByPosition(position = 3)
    private LocalDate birthDate;

    @CsvBindByPosition(position = 4)
    private BigDecimal creditAmt;
}
