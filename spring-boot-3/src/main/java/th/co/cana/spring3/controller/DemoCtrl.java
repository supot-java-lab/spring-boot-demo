package th.co.cana.spring3.controller;

import jakarta.validation.Valid;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import th.co.cana.spring3.excption.ErrorCodes;
import th.co.cana.spring3.excption.ServiceException;
import th.co.cana.spring3.excption.ValidationException;
import th.co.cana.spring3.model.Feedback;
import th.co.cana.spring3.model.FileInfo;
import th.co.cana.spring3.model.ValidateModel;
import th.co.cana.spring3.model.rest.Response;
import th.co.cana.spring3.model.rest.TaskResponse;
import th.co.cana.spring3.utils.AppConstants;
import th.co.cana.spring3.utils.HttpUtils;
import th.co.cana.spring3.utils.ResponseUtils;

import java.util.Arrays;
import java.util.Locale;

/**
 * @author supot.jdev
 * @version 1.0
 */
@RestController
@RequestMapping(AppConstants.Apis.DEMO)
public class DemoCtrl extends ApiController {
    @GetMapping
    public ResponseEntity<Object> index(@RequestParam(required = false) String name) {
        String resp = "Hello, : " + name;
        return ResponseEntity.ok(resp);
    }

    @PostMapping(path = "/feedback", consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE, MediaType.APPLICATION_JSON_VALUE, MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<Object> formSubmit(@ModelAttribute Feedback feedback) {
        logger.info("feedback Request : {}", feedback);
        Response resp = ResponseUtils.ok();
        return ok(resp);
    }

    @PostMapping(path = "/upload", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<Object> upload(@RequestParam(value = "file") MultipartFile file) {
        FileInfo fileInfo = new FileInfo();
        fileInfo.setName(file.getOriginalFilename());
        fileInfo.setContentType(file.getContentType());
        fileInfo.setSize(file.getSize());

        return ok(fileInfo);
    }

    @PostMapping(path = "/upload-json", produces = {MediaType.APPLICATION_JSON_VALUE}
            , consumes = {MediaType.MULTIPART_FORM_DATA_VALUE, MediaType.APPLICATION_OCTET_STREAM_VALUE})
    public ResponseEntity<Object> uploadJson(@RequestParam(value = "file") MultipartFile file, @RequestPart Feedback feedback) {
        logger.info("{}", feedback);
        FileInfo fileInfo = new FileInfo();
        fileInfo.setName(file.getOriginalFilename());
        fileInfo.setContentType(file.getContentType());
        fileInfo.setSize(file.getSize());

        return ok(fileInfo);
    }

    @GetMapping("/error/ServerException")
    public ResponseEntity<TaskResponse> errorServerException() {
        throw new ServiceException(ErrorCodes.CODE_API_ERROR, "Test ServiceException");
    }

    @GetMapping("/error/RuntimeException")
    public ResponseEntity<TaskResponse> errorRuntimeException() {
        throw new RuntimeException("Test RuntimeException");
    }

    @GetMapping("/error/Exception")
    public ResponseEntity<TaskResponse> errorException() throws Exception {
        throw new Exception("Test service Exception");
    }

    @GetMapping("/error/Throwable")
    public ResponseEntity<TaskResponse> errorThrowable() throws Throwable {
        throw new Throwable("Test service Throwable");
    }

    @GetMapping("/error/ValidationException01")
    public ResponseEntity<TaskResponse> errorValidationException01(){
        throw new ValidationException("Test ValidationException", Arrays.asList("Error 1", "Error 2"));
    }

    @GetMapping("/error/ValidationException02")
    public ResponseEntity<TaskResponse> errorValidationException02(){
        throw new ValidationException(Arrays.asList("Error 1", "Error 2"));
    }

    @PostMapping("/error/validate")
    public ResponseEntity<ValidateModel> validateBean(@Valid @RequestBody ValidateModel model) {
        logger.info("Request data : {}", model);
        String lang = HttpUtils.getAcceptLanguage();
        logger.info("lang : {}", lang);
        Locale locale = LocaleContextHolder.getLocale();
        logger.info("locale : {}", locale);
        return ok(model);
    }
}
