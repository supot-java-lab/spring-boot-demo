package th.co.cana.spring3.model.rest;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.HashMap;
import java.util.Map;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class FormRequest extends Request {
    private Map<String, String> fields;
    public void addField(String name, String value) {
        if (name == null || name.isEmpty()) {
            return;
        }
        if (fields == null) {
            fields = new HashMap<>();
        }
        fields.put(name, value);
    }

    public void clearForm() {
        fields = null;
        clearHeader();
    }
}
