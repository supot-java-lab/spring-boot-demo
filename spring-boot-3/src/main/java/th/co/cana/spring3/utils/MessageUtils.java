package th.co.cana.spring3.utils;

import java.util.Locale;
import java.util.MissingResourceException;

import io.github.jdevlibs.utils.Validators;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

/**
 * @author supot.jdev
 * @version 1.0
 */
public final class MessageUtils {
    private static final Locale LOCALE_TH = new Locale("th", "TH");

    private static MessageSource messageSource;

    private MessageUtils() {}

    public static void setMessageSource(MessageSource messageSource) {
        MessageUtils.messageSource = messageSource;
    }

    public static String getValue(String key) {
        return getValue(key, LocaleContextHolder.getLocale(), "");
    }

    public static String getValue(String key, Object... params) {
        return getValue(key, LocaleContextHolder.getLocale(), params);
    }

    public static String getValueEng(String key) {
        return getValue(key, Locale.US, "");
    }

    public static String getValueEng(String key, Object... params) {
        return getValue(key, Locale.US, params);
    }

    private static String getValue(String key, Locale locale, Object... params) {
        try {
            if (Validators.isEmpty(key) || Validators.isNull(messageSource)) {
                return null;
            }
            if (locale == null) {
                locale = LOCALE_TH;
            }
            String value = messageSource.getMessage(key, toString(params), locale);
            if (Validators.isEmpty(value)) {
                return key;
            }
            return value;
        } catch (MissingResourceException ex) {
            return key;
        }
    }

    private static Object[] toString(Object ... params) {
        if (Validators.isEmpty(params)) {
            return params;
        }

        for (int i = 0; i < params.length; i++) {
            if (!(params[i] instanceof String)) {
                params[i] = String.valueOf(params[i]);
            } else if(params[i] == null) {
                params[i] = "";
            }
        }
        return params;
    }
}
