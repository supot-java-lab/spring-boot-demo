package th.co.cana.spring3.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import th.co.cana.spring3.config.properties.ApiConfigProperties;
import th.co.cana.spring3.config.properties.AppConfigProperties;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Configuration
public class PropertiesConfig {

    @Bean
    @ConfigurationProperties(prefix = "app.conf")
    public AppConfigProperties appConfigProperties() {
        return new AppConfigProperties();
    }

    @Bean
    @ConfigurationProperties(prefix = "service.api")
    public ApiConfigProperties apiConfigProperties() {
        return new ApiConfigProperties();
    }
}
