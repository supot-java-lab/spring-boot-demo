package th.co.cana.spring3.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import th.co.cana.spring3.entity.Customer;

/**
 * @author supot.jdev
 * @version 1.0
 */

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long>, CustomerDao {
}
