package th.co.cana.spring3.utils;

import io.github.jdevlibs.utils.Validators;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.UncategorizedSQLException;
import th.co.cana.spring3.excption.ErrorCodes;
import th.co.cana.spring3.excption.ServiceException;

/**
 * @author supot.jdev
 * @version 1.0
 */
public final class Exceptions {
    private static final String NULL 	    = ".customer. SQL ERROR -407";
    private static final String LARGER 	    = ".customer. SQL ERROR -433";
    private static final String LARGER_302  = ".customer. SQL ERROR: SQLCODE=-302";
    private static final String UNIQUE 	    = ".customer. SQL ERROR -537";

    private Exceptions() {}

    public static RuntimeException toRuntimeException(Throwable throwable) {
        return throwable instanceof RuntimeException ? (RuntimeException) throwable
                : new RuntimeException(throwable);
    }

    public static RuntimeException toRuntimeException(String message, Throwable throwable) {
        return message == null ? toRuntimeException(throwable)
                : new RuntimeException(message, throwable);
    }

    public static RuntimeException toRuntimeException(String message) {
        return new RuntimeException(message);
    }

    public static ServiceException invokeException(Exception ex) {
        return invokeException(ex, ErrorCodes.CODE_500);
    }

    public static ServiceException invokeException(DataAccessException ex) {
        return invokeException(ex, ErrorCodes.CODE_DB_OTHER);
    }

    public static ServiceException invokeException(Exception ex, ErrorCodes errorCode) {
        if (ex instanceof DuplicateKeyException || ex.getCause() instanceof DuplicateKeyException) {
            return new ServiceException(ErrorCodes.CODE_DB_UNIQUE, ex);
        } else if (ex instanceof DataIntegrityViolationException || ex.getCause() instanceof DataIntegrityViolationException) {
            return checkException(ex, errorCode);
        } else if (ex instanceof UncategorizedSQLException || ex.getCause() instanceof UncategorizedSQLException) {
            return checkException(ex, errorCode);
        } else {
            return Validators.isNull(errorCode)
                    ? new ServiceException(ErrorCodes.CODE_DB_OTHER, ex)
                    : new ServiceException(errorCode, ex);
        }
    }

    public static String getExceptionMessage(Exception ex) {
        String msg = getMessage(ex);
        if (msg.contains(LARGER) || ex.getMessage().contains(LARGER)) {
            return ErrorCodes.CODE_DB_LARGER.getMessage();
        } else if (msg.contains(LARGER_302) || ex.getMessage().contains(LARGER_302)) {
            return ErrorCodes.CODE_DB_LARGER.getMessage();
        } else if (msg.contains(NULL) || ex.getMessage().contains(NULL)) {
            return ErrorCodes.CODE_DB_NULL.getMessage();
        } else if (msg.contains(UNIQUE) || ex.getMessage().contains(UNIQUE)) {
            return ErrorCodes.CODE_DB_UNIQUE.getMessage();
        }

        return ErrorCodes.CODE_DB_OTHER.getMessage();
    }

    private static ServiceException checkException(Exception ex, ErrorCodes errorCode) {
        String msg = getMessage(ex);
        if (Validators.isEmpty(msg)) {
            return Validators.isNull(errorCode)
                    ? new ServiceException(ErrorCodes.CODE_DB_OTHER, ex)
                    : new ServiceException(errorCode, ex);
        }

        if (msg.contains(LARGER)) {
            return new ServiceException(ErrorCodes.CODE_DB_LARGER, ex);
        } else if (msg.contains(NULL)) {
            return new ServiceException(ErrorCodes.CODE_DB_NULL, ex);
        } else if (msg.contains(UNIQUE)) {
            return new ServiceException(ErrorCodes.CODE_DB_UNIQUE, ex);
        } else {
            return Validators.isNull(errorCode)
                    ? new ServiceException(ErrorCodes.CODE_DB_OTHER, ex)
                    : new ServiceException(errorCode, ex);
        }
    }

    private static String getMessage(Exception ex) {
        String msg = ex.getMessage();
        if (Validators.isNotNull(ex.getCause())) {
            msg = (ex.getCause().getCause() != null
                    ? ex.getCause().getCause().getMessage()
                    : ex.getCause().getMessage());
        }

        return msg != null ? msg.toUpperCase() : "";
    }
}