package th.co.cana.spring3.config;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.support.RequestContextUtils;
import th.co.cana.spring3.utils.HttpUtils;

import java.util.Locale;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Slf4j
public class ApiLocaleChangeInterceptor extends LocaleChangeInterceptor {
    @Override
    public boolean preHandle(@NotNull HttpServletRequest request
            , @NotNull HttpServletResponse response, @NotNull Object handler) {

        String newLocale = HttpUtils.getAcceptLanguage(request, "th_TH");
        LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);
        if (localeResolver != null) {
            try {
                Locale locale = this.parseLocaleValue(newLocale);
                localeResolver.setLocale(request, response, locale);
            } catch (IllegalArgumentException ex) {
                log.error("Ignoring invalid locale value [{}]: {}", newLocale, ex.getMessage());
            }
        }
        return true;
    }
}
