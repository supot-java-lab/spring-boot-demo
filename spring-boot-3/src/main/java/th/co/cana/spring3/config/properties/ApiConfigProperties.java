package th.co.cana.spring3.config.properties;

import lombok.Data;

import java.io.Serializable;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
public class ApiConfigProperties implements Serializable {

    @Data
    public static class ApiConfig implements Serializable {
        private String baseUrl;
        private String username;
        private String password;
        private String token;
        public String getApiUrl(String apiName) {
            return getBaseUrl() + apiName;
        }
    }
}
