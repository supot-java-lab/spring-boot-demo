package th.co.cana.spring3.criteria;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
public class CustomerCriteria implements Serializable {

    private Long customerId;
    private String firstName;
    private String lastName;
    private LocalDate birthDate;
}
