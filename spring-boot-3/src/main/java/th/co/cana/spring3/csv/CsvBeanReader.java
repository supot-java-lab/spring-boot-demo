package th.co.cana.spring3.csv;

import com.opencsv.CSVParser;
import com.opencsv.CSVParserBuilder;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */
public abstract class CsvBeanReader<T> {
    private static final String ENCODE = "UTF-8";
    private static final char SEPARATOR = ',';

    public List<T> read(String csvFile) throws CsvException {
        return read(csvFile, 0, SEPARATOR, ENCODE);
    }

    public List<T> read(String csvFile, int skipLine) throws CsvException {
        return read(csvFile, skipLine, SEPARATOR, ENCODE);
    }

    public List<T> read(String csvFile, char separator) throws CsvException {
        return read(csvFile, 0, separator, ENCODE);
    }

    public List<T> read(String csvFile, String encode) throws CsvException {
        return read(csvFile, 0, SEPARATOR, encode);
    }

    public List<T> read(String csvFile, int skipLine, char separator) throws CsvException {
        return read(csvFile, skipLine, separator, ENCODE);
    }

    public List<T> read(String csvFile, int skipLine, char separator, String encode) throws CsvException {
        try (InputStream ins = Files.newInputStream(Paths.get(csvFile))) {
            return read(ins, skipLine, separator, encode);
        } catch (IOException ex) {
            throw new CsvException(ex);
        }
    }

    public List<T> read(InputStream ins) throws CsvException {
        return read(ins, 0, SEPARATOR, ENCODE);
    }

    public List<T> read(InputStream ins, int skipLine) throws CsvException {
        return read(ins, skipLine, SEPARATOR, ENCODE);
    }

    public List<T> read(InputStream ins, char separator) throws CsvException {
        return read(ins, 0, separator, ENCODE);
    }

    public List<T> read(InputStream ins, String encode) throws CsvException {
        return read(ins, 0, SEPARATOR, encode);
    }

    public List<T> read(InputStream ins, int skipLine, char separator) throws CsvException {
        return read(ins, skipLine, separator, ENCODE);
    }

    public List<T> read(InputStream ins, int skipLine, char separator, String encode) throws CsvException {
        try (Reader reader = (encode == null || encode.isEmpty()) ? new InputStreamReader(ins) : new InputStreamReader(ins, encode)) {
            final CSVParser parser = new CSVParserBuilder().withSeparator(separator).build();
            try (CSVReader csvReader = new CSVReaderBuilder(reader).withCSVParser(parser).withSkipLines(skipLine).build()) {
                return parse(csvReader);
            }
        } catch (IOException ex) {
            throw new CsvException(ex);
        }
    }

    public List<T> parse(final CSVReader csv) throws CsvException {
        try {
            List<T> results = new ArrayList<>();
            String[] lines;
            while (null != (lines = csv.readNext())) {
                T bean = mapping(lines);
                if (bean != null) {
                    results.add(bean);
                }
            }

            return results;
        } catch (Exception ex) {
            throw new CsvException("Error parsing CSV!", ex);
        }
    }

    protected abstract T mapping(String[] lines);

}
