package th.co.cana.spring3.utils;

import io.github.jdevlibs.utils.Validators;
import jakarta.persistence.NoResultException;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import th.co.cana.spring3.excption.ServiceException;
import th.co.cana.spring3.excption.ValidationException;
import th.co.cana.spring3.model.rest.Response;

import java.util.Collections;
import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */
public final class ResponseUtils {

    private ResponseUtils() {

    }

    public static Response ok() {
        return ok(Response.class);
    }

    public static Response unauthorized() {
        return unauthorized(Response.class);
    }

    public static Response unauthorized(Object... errors) {
        return unauthorized(Response.class, errors);
    }

    public static Response response() {
        return response(HttpStatus.OK, Response.class);
    }

    public static Response response(HttpStatusCode status) {
        return response(status, Response.class);
    }

    public static Response required(List<String> params) {
        return required(Response.class, params);
    }

    public static Response required(String... params) {
        return required(Response.class, params);
    }

    public static Response validateError(String... msg) {
        return validateError(Response.class, null, msg);
    }

    public static Response validateFail(String message) {
        return validateError(Response.class, message, Collections.emptyList());
    }

    public static Response validateError(String message, List<String> msg) {
        return validateError(Response.class, message, msg);
    }

    public static Response validateError(List<String> msg) {
        return validateError(Response.class, null, msg);
    }

    public static Response notContent(String msg) {
        return notContent(Response.class, msg);
    }

    public static <T extends Response> T ok(Class<T> clazz) {
        return response(HttpStatus.OK, clazz);
    }

    public static <T extends Response> T unauthorized(Class<T> clazz) {
        return response(HttpStatus.UNAUTHORIZED, clazz);
    }

    public static <T extends Response> T unauthorized(Class<T> clazz, Object... error) {
        return response(HttpStatus.UNAUTHORIZED, clazz, error);
    }

    public static <T extends Response> T response(HttpStatusCode status, Class<T> clazz, Object... errors) {
        T resp = BeanUtils.instantiateClass(clazz);
        if (Validators.isNotNull(status)) {
            if (status.is2xxSuccessful()) {
                resp.setMessage(MessageUtils.getValue("resp.success"));
                resp.setSuccess(true);
            } else {
                String key = "resp.error." + status.value();
                String msg = MessageUtils.getValue(key);
                resp.setMessage(msg);
            }
            resp.setStatus(status.value());
        } else {
            resp.setStatus(HttpStatus.OK.value());
            resp.setMessage(MessageUtils.getValue("resp.success"));
            resp.setSuccess(true);
        }

        resp.addError(errors);

        setRequestPath(resp);

        return resp;
    }

    public static <T extends Response> T required(Class<T> clazz, List<String> params) {
        if (Validators.isNotEmpty(params)) {
            return required(clazz, params.toArray(new String[0]));
        } else {
            return required(clazz);
        }
    }

    public static <T extends Response> T required(Class<T> clazz, String... params) {
        T resp = BeanUtils.instantiateClass(clazz);
        resp.setStatus(HttpStatus.BAD_REQUEST.value());
        resp.setMessage(MessageUtils.getValue("resp.error.required"));
        if (params != null) {
            for (String param : params) {
                resp.addError(MessageUtils.getValue("resp.error.required.value", param));
            }
        }
        setRequestPath(resp);

        return resp;
    }

    public static <T extends Response> T validateError(Class<T> clazz, String message, List<String> errors) {
        if (Validators.isNotEmpty(errors)) {
            return validateError(clazz, message, errors.toArray(new String[0]));
        } else {
            return validateError(clazz, message, "");
        }
    }

    public static <T extends Response> T validateFail(Class<T> clazz, String message) {
        return validateError(clazz, message, Collections.emptyList());
    }

    public static <T extends Response> T validateError(Class<T> clazz, String message, String... errors) {
        T resp = BeanUtils.instantiateClass(clazz);
        resp.setStatus(HttpStatus.BAD_REQUEST.value());
        if (Validators.isNotEmpty(message)) {
            resp.setMessage(message);
        } else {
            resp.setMessage(MessageUtils.getValue("resp.error.400"));
        }
        if (errors != null) {
            for (String err : errors) {
                resp.addError(err);
            }
        }
        setRequestPath(resp);

        return resp;
    }

    public static Response processError(ServiceException ex) {
        return processError(ex, Response.class);
    }

    public static Response processError(ValidationException ex) {
        return processError(ex, Response.class);
    }

    public static Response processError(Throwable ex) {
        return processError(ex, Response.class);
    }

    public static <T extends Response> T processError(Throwable ex, Class<T> clazz) {
        if (ex instanceof ServiceException) {
            return processError((ServiceException) ex, clazz);
        } else if (ex instanceof ValidationException) {
            return processError((ValidationException) ex, clazz);
        } else if (ex instanceof NoResultException) {
            return processNotfound((NoResultException) ex, clazz);
        }

        T resp = BeanUtils.instantiateClass(clazz);
        resp.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        resp.setMessage(MessageUtils.getValue("resp.error.500"));
        if (ex.getCause() != null) {
            resp.addError(ex.getCause().getMessage());
        } else {
            resp.addError(ex.getMessage());
        }
        setRequestPath(resp);

        return resp;
    }

    public static <T extends Response> T processError(ServiceException ex, Class<T> clazz) {
        T resp = BeanUtils.instantiateClass(clazz);
        resp.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        if (Validators.isNotNull(ex.getErrorCode())) {
            resp.setMessage(MessageUtils.getValue(ex.getErrorMessageCode()));
        } else {
            resp.setMessage(MessageUtils.getValue("resp.error.500"));
        }
        if (Validators.isNotEmpty(ex.getErrors())) {
            resp.setErrors(ex.getErrors());
        } else {
            resp.addError(ex.getMessage());
        }

        setRequestPath(resp);

        return resp;
    }

    public static <T extends Response> T processError(ValidationException ex, Class<T> clazz) {
        T resp = BeanUtils.instantiateClass(clazz);
        resp.setStatus(HttpStatus.BAD_REQUEST.value());
        if (Validators.isNotEmpty(ex.getMessage())) {
            resp.setMessage(ex.getMessage());
        } else {
            resp.setMessage(MessageUtils.getValue("resp.error.400"));
        }
        if (Validators.isNotEmpty(ex.getErrors())) {
            resp.setErrors(ex.getErrors());
        }
        setRequestPath(resp);
        return resp;
    }

    public static <T extends Response> T notContent(Class<T> clazz, String msg) {
        T resp = BeanUtils.instantiateClass(clazz);
        resp.setStatus(HttpStatus.NO_CONTENT.value());
        resp.setMessage(MessageUtils.getValue("resp.error.204"));
        if (Validators.isNotEmpty(msg)) {
            resp.addError(msg);
        }

        setRequestPath(resp);

        return resp;
    }

    private static <T extends Response> T processNotfound(NoResultException ex, Class<T> clazz) {
        T resp = BeanUtils.instantiateClass(clazz);
        resp.setStatus(HttpStatus.NOT_FOUND.value());
        resp.setMessage(MessageUtils.getValue("resp.error.204"));
        resp.addError(ex.getMessage());
        setRequestPath(resp);
        return resp;
    }

    private static void setRequestPath(Response resp) {
        resp.setPath(HttpUtils.getRequestPath());
    }
}