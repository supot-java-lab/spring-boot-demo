package th.co.cana.spring3.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import th.co.cana.spring3.criteria.CustomerCriteria;
import th.co.cana.spring3.entity.Customer;
import th.co.cana.spring3.excption.ErrorCodes;
import th.co.cana.spring3.model.CustomerCsv;
import th.co.cana.spring3.model.Paging;
import th.co.cana.spring3.repository.CustomerRepository;
import th.co.cana.spring3.utils.Exceptions;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class CustomerService {

    private final CustomerRepository repository;

    public Customer save(Customer entity) {
        try {
            return repository.save(entity);
        } catch (Exception ex) {
            throw Exceptions.invokeException(ex, ErrorCodes.CODE_DB_INSERT);
        }
    }

    public Customer update(Customer entity) {
        try {
            Objects.requireNonNull(entity.getCustomerId(), "Customer id cannot be null.");
            return repository.save(entity);
        } catch (Exception ex) {
            throw Exceptions.invokeException(ex, ErrorCodes.CODE_DB_UPDATE);
        }
    }

    public List<Customer> search(final CustomerCriteria criteria) {
        try {
            return repository.search(criteria);
        } catch (Exception ex) {
            throw Exceptions.invokeException(ex, ErrorCodes.CODE_DB_QUERY);
        }
    }

    public Paging<Customer> search(Pageable pageable) {
        try {
            Page<Customer> page = repository.findAll(pageable);
            Paging<Customer> paging = new Paging<>();
            paging.computePagingValue(page);

            return paging;
        } catch (Exception ex) {
            throw Exceptions.invokeException(ex, ErrorCodes.CODE_DB_QUERY);
        }
    }

    public Customer findById(Long id) {
        try {
            Optional<Customer> optional = repository.findById(id);
            return optional.orElse(null);
        } catch (Exception ex) {
            throw Exceptions.invokeException(ex, ErrorCodes.CODE_DB_QUERY);
        }
    }

    public List<CustomerCsv> getCustomerCsv() {
        try {
            return repository.getCustomerCsv();
        } catch (Exception ex) {
            throw Exceptions.invokeException(ex, ErrorCodes.CODE_DB_QUERY);
        }
    }

    public byte[] exportCsv() {
        try {
            return repository.exportCsv();
        } catch (Exception ex) {
            throw Exceptions.invokeException(ex, ErrorCodes.CODE_DB_QUERY);
        }
    }
}
