package th.co.cana.spring3.model;

import lombok.Data;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
public class PagingCriteria implements Serializable {
    private int page = 0;
    private int size = 10;
    private String[] sort;

    public Pageable toPageable() {
        if (sort == null || sort.length == 0) {
            return PageRequest.of(page, size);
        }

        List<Sort.Order> orders = toOrders();
        return PageRequest.of(page, size, Sort.by(orders));
    }

    public List<Sort.Order> toOrders() {
        List<Sort.Order> orders = new ArrayList<>();
        if (sort != null) {
            for (String sortOrder : sort) {
                String[] _sort = sortOrder.split(",");
                this.addSortOrder(orders, _sort);
            }
        }
        return orders;
    }

    private void addSortOrder(List<Sort.Order> orders, String[] sortValue) {
        String property = getValue(sortValue, 0);
        if (property != null && !property.isBlank()) {
            String direction = getValue(sortValue, 1);
            orders.add(new Sort.Order(toDirection(direction), property));
        }
    }

    private Sort.Direction toDirection(String direction) {
        if (direction == null || direction.isBlank()) {
            return Sort.Direction.ASC;
        }

        if ("desc".equalsIgnoreCase(direction)) {
            return Sort.Direction.DESC;
        } else {
            return Sort.Direction.ASC;
        }
    }

    private String getValue(final String[] values, int inx) {
        if (sort == null || sort.length == 0 || inx < 0) {
            return null;
        }

        if (values.length <= inx) {
            return null;
        }
        return values[inx];
    }
}
