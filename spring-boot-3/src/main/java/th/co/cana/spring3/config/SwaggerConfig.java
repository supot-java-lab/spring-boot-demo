package th.co.cana.spring3.config;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import th.co.cana.spring3.utils.AppConstants;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Configuration
public class SwaggerConfig {

    @Bean
    public OpenAPI marketing() {
        return new OpenAPI()
                .info(new Info().title("Spring boot 3 API")
                        .description("Spring boot 3 with Java 17 API")
                        .version("V1.0")
                        .license(new License().name("Apache 2.0").url("https://springdoc.org")))
                .components(new Components()
                        .addSecuritySchemes(AppConstants.ApiDocs.SECURITY_KEY,
                                new SecurityScheme().type(SecurityScheme.Type.HTTP).scheme("bearer").bearerFormat("JWT")));
    }
}