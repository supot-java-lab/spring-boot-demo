package th.co.cana.spring3.model;

import lombok.Data;
import org.springframework.data.domain.Page;

import java.io.Serializable;
import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */

@Data
public class Paging<T> implements Serializable {
    private List<T> content;
    private long totalElements;
    private int totalPages;
    private boolean last;
    private boolean first;
    private PagingCriteria pageable;

    public Paging() {

    }

    public void computePagingValue(Page<T> page ) {
        setTotalPages(page.getTotalPages());
        setTotalElements(page.getTotalElements());
        setContent(page.getContent());
        setFirst(page.isFirst());
        setLast(page.isLast());
    }
}
