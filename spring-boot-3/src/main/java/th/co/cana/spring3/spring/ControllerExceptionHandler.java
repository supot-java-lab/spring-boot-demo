package th.co.cana.spring3.spring;

import io.github.jdevlibs.utils.Validators;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.ConstraintViolationException;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import th.co.cana.spring3.excption.ServiceException;
import th.co.cana.spring3.model.rest.Response;
import th.co.cana.spring3.utils.MessageUtils;
import th.co.cana.spring3.utils.ResponseUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */
@RestControllerAdvice
@Slf4j
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {
    private static final String ERROR_DESC = "Exception Description : ";

    @ExceptionHandler(ConversionFailedException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<Response> handleConversion(ConversionFailedException ex) {
        log.error("Request parameter error [ConversionFailedException]");
        log.error(ERROR_DESC, ex);

        final Response resp = ResponseUtils.validateError(ex.getMessage());
        return new ResponseEntity<>(resp, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ ConstraintViolationException.class })
    public ResponseEntity<Response> handleConstraintViolation(ConstraintViolationException ex) {
        log.error("Request parameter error [ConstraintViolationException]");
        log.error(ERROR_DESC, ex);

        final List<String> errors = new ArrayList<>();
        for (ConstraintViolation<?> validate : ex.getConstraintViolations()) {
            if (Validators.isEmpty(validate.getMessage())) {
                errors.add(MessageUtils.getValue("resp.error.required.value", validate.getPropertyPath()));
            } else {
                errors.add(validate.getPropertyPath() + ": " + validate.getMessage());
            }
        }

        Response resp = ResponseUtils.validateError(errors);
        return new ResponseEntity<>(resp, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ MethodArgumentTypeMismatchException.class })
    public ResponseEntity<Object> handleMethodArgumentTypeMismatch(MethodArgumentTypeMismatchException ex) {
        log.error("Method argument type error [MethodArgumentTypeMismatchException]");
        log.error(ERROR_DESC, ex);

        String error = ex.getName() + " should be of type ";
        Class<?> type = ex.getRequiredType();
        if(type != null) {
            error += type.getName();
        } else {
            error += " null";
        }
        Response resp = ResponseUtils.validateError(error);
        return new ResponseEntity<>(resp, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {ServiceException.class, RuntimeException.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<Response> handleServerError(RuntimeException ex) {
        log.error("Service or process error [ServiceException]");
        log.error(ERROR_DESC, ex);

        Response resp = ResponseUtils.processError(ex);
        return new ResponseEntity<>(resp, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleExceptions(Exception ex) {
        log.error("Service or process error [Exception]");
        log.error(ERROR_DESC, ex);

        Response resp = ResponseUtils.processError(ex);
        return new ResponseEntity<>(resp, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    /** ++++++++++++++++++++++++++++++ Override ++++++++++++++++++++++++++++++ **/
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(@NotNull MethodArgumentNotValidException ex
            , @NotNull HttpHeaders headers, @NotNull HttpStatusCode status, @NotNull WebRequest request) {

        log.error("Method argument not valid [MethodArgumentNotValidException] HttpStatusCode: {}", status);
        log.error(ERROR_DESC, ex);

        Response resp = this.createResponse(ex, status);
        resp.setMessage(MessageUtils.getValue("resp.error.400"));
        resp.setErrors(null);

        for (FieldError error : ex.getBindingResult().getFieldErrors()) {
            resp.addError(error.getField() + ": " + error.getDefaultMessage());
        }
        for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
            resp.addError(error.getObjectName() + ": " + error.getDefaultMessage());
        }

        return new ResponseEntity<>(resp, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(@NotNull MissingServletRequestParameterException ex
            , @NotNull HttpHeaders headers, @NotNull HttpStatusCode status, @NotNull WebRequest request) {

        log.error("Request missing parameter [MissingServletRequestParameterException] HttpStatusCode: {}", status);
        log.error(ERROR_DESC, ex);

        Response resp = ResponseUtils.validateError(ex.getMessage());
        return new ResponseEntity<>(resp, HttpStatus.BAD_REQUEST);
    }

    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(@NotNull NoHandlerFoundException ex
            , @NotNull HttpHeaders headers, @NotNull HttpStatusCode status, @NotNull WebRequest request) {

        log.error("Request service not found [NoHandlerFoundException] HttpStatusCode: {}", status);
        log.error(ERROR_DESC, ex);

        Response resp = this.createResponse(ex, status);
        resp.setMessage(MessageUtils.getValue("resp.error.404"));
        return new ResponseEntity<>(resp, HttpStatus.NOT_FOUND);
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(@NotNull Exception ex
            , @Nullable Object body, @NotNull HttpHeaders headers, @NotNull HttpStatusCode status, @NotNull WebRequest request) {

        log.error("General exception [Exception] HttpStatusCode : {}", status);
        log.error(ERROR_DESC, ex);

        Response resp = this.createResponse(ex, status);
        resp.setMessage(MessageUtils.getValue("resp.error.500"));
        return new ResponseEntity<>(resp, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private Response createResponse(Exception ex, HttpStatusCode status) {
        Response resp = ResponseUtils.response(status);
        if (status.is5xxServerError()) {
            resp = ResponseUtils.processError(ex);
        } else {
            if (Validators.isEmpty(resp.getErrors())) {
                resp.addError(ex.getMessage());
            }
        }

        return resp;
    }
}
