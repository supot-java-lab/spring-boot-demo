package th.co.cana.spring3.model;

import lombok.Data;

import java.io.Serializable;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
public class FileInfo implements Serializable {
    private String name;
    private String contentType;
    private Long size;
}
