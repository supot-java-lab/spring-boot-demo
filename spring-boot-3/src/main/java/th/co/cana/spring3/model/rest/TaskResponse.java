package th.co.cana.spring3.model.rest;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
public class TaskResponse implements Serializable {
    private String taskName;
    private String remark;
    private LocalDateTime processDate = LocalDateTime.now();
}
