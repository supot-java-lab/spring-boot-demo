package th.co.cana.spring3.repository;

import th.co.cana.spring3.criteria.CustomerCriteria;
import th.co.cana.spring3.entity.Customer;
import th.co.cana.spring3.model.CustomerCsv;

import java.util.List;

public interface CustomerDao {
    List<Customer> search(CustomerCriteria criteria);

    List<CustomerCsv> getCustomerCsv();

    byte[] exportCsv();
}
