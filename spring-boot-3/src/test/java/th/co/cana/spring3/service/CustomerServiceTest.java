package th.co.cana.spring3.service;

import io.github.jdevlibs.utils.IOUtils;
import io.github.jdevlibs.utils.Validators;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import th.co.cana.spring3.BaseUnitTest;
import th.co.cana.spring3.criteria.CustomerCriteria;
import th.co.cana.spring3.entity.Customer;
import th.co.cana.spring3.utils.MockUtils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author supot.jdev
 * @version 1.0
 */

@Slf4j
public class CustomerServiceTest extends BaseUnitTest {

    @Autowired
    private CustomerService service;

    @DisplayName("[Customer]: Save data test")
    @Test
    @Disabled
    @Order(1)
    void save() {
        Customer entity = MockUtils.createCustomer();
        entity.setBirthDate(LocalDate.of(1982, 2, 17));
        entity = service.save(entity);
        assertNotNull(entity.getCustomerId(), "Id from Customer should be not null.");
    }

    @DisplayName("[Customer]: Search data all")
    @Test
    @Disabled
    @Order(2)
    void search() {
        CustomerCriteria criteria = new CustomerCriteria();
        List<Customer> customers = service.search(criteria);
        assertTrue(Validators.isNotEmpty(customers));
    }

    @DisplayName("[Customer]: exportCsv")
    @Disabled
    @Test
    @Order(3)
    void exportCsv() throws IOException {
       byte[] contents = service.exportCsv();
        assertNotNull(contents);
        IOUtils.write(contents, new FileOutputStream("src/test/resources/csv/customer.csv"));
    }
}
