package th.co.cana.spring3;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import th.co.cana.spring3.utils.StartupUtils;

/**
 * @author supot.jdev
 * @version 1.0
 */

@ExtendWith(SpringExtension.class)
@TestMethodOrder(OrderAnnotation.class)
public abstract class SpringUnitTest {

    @BeforeAll
    static void initialConfiguration() {
        StartupUtils.initial();
    }
}
