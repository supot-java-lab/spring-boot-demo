package th.co.cana.spring3.utils;

import th.co.cana.spring3.entity.Customer;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author supot.jdev
 * @version 1.0
 */
public final class MockUtils {

    private MockUtils() {

    }

    public static Customer createCustomer() {
        Customer customer = new Customer();
        customer.setFirstName("Supot");
        customer.setLastName("Saelao");
        customer.setBirthDate(LocalDate.now());
        customer.setCreditAmt(BigDecimal.valueOf(150000D));
        customer.setCreatedBy("Supot");
        customer.setCreatedDate(LocalDateTime.now());

        return customer;
    }
}
