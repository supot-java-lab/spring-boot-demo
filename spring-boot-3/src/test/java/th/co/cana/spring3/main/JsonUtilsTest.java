package th.co.cana.spring3.main;

import io.github.jdevlibs.spring.utils.JsonUtils;
import th.co.cana.spring3.entity.Customer;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class JsonUtilsTest {
    public static void main(String[] args) {
        Customer customer = new Customer();
        customer.setCustomerId(100L);
        customer.setFirstName("Supot");
        customer.setLastName("Saelao");
        customer.setBirthDate(LocalDate.now());
        customer.setCreditAmt(BigDecimal.valueOf(150000D));
        customer.setCreatedBy("Supot");
        customer.setCreatedDate(LocalDateTime.now());

        String json = JsonUtils.json(customer, true);
        System.out.println(json);

        customer = JsonUtils.model(json, Customer.class);
        System.out.println(customer);
    }
}
