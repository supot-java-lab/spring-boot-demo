CREATE DATABASE spring_demo;

-- Table
CREATE TABLE `CUSTOMER` (
                            `CUSTOMER_ID` bigint NOT NULL AUTO_INCREMENT,
                            `FIRST_NAME` varchar(100) NOT NULL,
                            `LAST_NAME` varchar(100) DEFAULT NULL,
                            `BIRTH_DATE` date DEFAULT NULL,
                            `CREDIT_AMT` decimal(20,2) DEFAULT NULL,
                            `CREATED_BY` varchar(50) DEFAULT NULL,
                            `CREATED_DATE` timestamp NULL DEFAULT NULL,
                            `UPDATED_BY` varchar(50) DEFAULT NULL,
                            `UPDATED_DATE` timestamp NULL DEFAULT NULL,
                            PRIMARY KEY (`CUST_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;