/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.swagger.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author supot
 * @version 1.0
 */

@Configuration
//@EnableWebMvc
public class WebMvcConfig implements WebMvcConfigurer {

	/*
	@Override
	public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
		configurer.defaultContentType(MediaType.APPLICATION_JSON)
				.mediaType("xml", MediaType.APPLICATION_XML)
				.mediaType("json", MediaType.APPLICATION_JSON);
		WebMvcConfigurer.super.configureContentNegotiation(configurer);

	}
	*/
}
