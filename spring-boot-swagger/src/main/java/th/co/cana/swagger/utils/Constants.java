/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.swagger.utils;

import java.util.Locale;

/**
* @author supot
* @version 1.0
*/
public class Constants {
	private Constants() {
	}
	
	public static final Locale US = Locale.US;
	public static final Locale TH = new Locale("th", "TH");
	
	public static final String Y = "Y";
	public static final String N = "N";
	public static final String UTF8 = "UTF-8";
	public static final String TEXT_EXT = ".txt";
}
