/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.swagger.model;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
* @author supot
* @version 1.0
*/

@Data
public class UserInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	private String id;
	private String firstName;
	private String lastName;
	private Date birthDate;
	
}
