/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.swagger.utils;

/**
* @author supot
* @version 1.0
*/
public final class RespCodes {
	private RespCodes() {}
	
	public static final String C200 		= "200";
	public static final String C200_DESC 	= "OK";
	
	public static final String C201 		= "201";
	public static final String C201_DESC 	= "Resource Created";
	
	public static final String C400 		= "400";
	public static final String C400_DESC 	= "Bad Request";
	
	public static final String C401 		= "401";
	public static final String C401_DESC 	= "Unauthorized";
	
	public static final String C403 		= "403";
	public static final String C403_DESC 	= "Forbidden";
	
	public static final String C404 		= "404";
	public static final String C404_DESC 	= "Resource Not Found";
	
	public static final String C405 		= "405";
	public static final String C405_DESC 	= "Method Not Allowed";
	
	public static final String C500 		= "500";
	public static final String C500_DESC 	= "Internal Server Error";
	
	public static final String C502 		= "502";
	public static final String C502_DESC 	= "Bad Gateway";
	
	public static final String C503 		= "503";
	public static final String C503_DESC 	= "Service Unavailable";
	
	public static final String C504 		= "504";
	public static final String C504_DESC 	= "Gateway Timeout";
}
