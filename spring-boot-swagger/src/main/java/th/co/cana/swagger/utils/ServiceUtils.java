/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.swagger.utils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import th.co.cana.swagger.model.UserInfo;

/**
* @author supot
* @version 1.0
*/
public final class ServiceUtils {
	private ServiceUtils() {}
	
	public static List<UserInfo> createUsers(int total) {
		List<UserInfo> items = new ArrayList<>();
		
		for (int i = 1; i <= total; i++) {
			UserInfo user = new UserInfo();
			user.setId(String.valueOf(i));
			user.setFirstName("Mr. First Name " + i);
			user.setLastName("Last Name " + i);
			user.setBirthDate(new Date());
			
			items.add(user);
		}

		return items;
	}
	
	public static UserInfo createUser(String id) {
		UserInfo user = new UserInfo();
		user.setId(id);
		user.setFirstName("Mr. First Name");
		user.setLastName("Last Name");
		user.setBirthDate(new Date());
		
		return user;
	}
	
	public static String getError() {
		return "";
	}
}
