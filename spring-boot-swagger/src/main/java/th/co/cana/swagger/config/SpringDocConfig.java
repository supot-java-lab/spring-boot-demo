/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.swagger.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;

/**
* @author supot
* @version 1.0
*/

@Configuration
public class SpringDocConfig {
	
	@Bean
    public OpenAPI springDemoAPI() {
        return new OpenAPI()
                .info(new Info().title("Spring boot and OpenAPI 3")
                .description("The sample OpenAPI 3 application")
                .version("v0.0.1")
                .license(new License().name("Apache 2.0").url("http://springdoc.org")))
                .externalDocs(new ExternalDocumentation()
                .description("Springdoc-openapi")
                .url("https://springdoc.org/"));
    }
}
