/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.swagger.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

/**
* @author supot
* @version 1.0
*/

@Tag(name = "File upload API")
@RestController
@RequestMapping("/v1/file")
public class UploadFileCtrl extends Controller {

	@Operation(summary = "upload file")
	@PostMapping()
	public ResponseEntity<String> uploadFile(@RequestParam("file") MultipartFile file) {
		try {
			logger.info("Name : {}", file.getOriginalFilename());
			logger.info("ContentType : {}", file.getContentType());
			logger.info("Size : {}", file.getSize());
			
			return new ResponseEntity<>("Upload file : " + file.getOriginalFilename(), HttpStatus.OK);
		} catch (Exception ex) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

	}
}
