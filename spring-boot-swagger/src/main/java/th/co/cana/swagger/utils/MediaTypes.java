/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.swagger.utils;

/**
 * @author supot
 * @version 1.0
 */
public final class MediaTypes {
	private MediaTypes() {
	}

	public static final String JSON 	= "application/json";
	public static final String XML 		= "application/xml";
}
