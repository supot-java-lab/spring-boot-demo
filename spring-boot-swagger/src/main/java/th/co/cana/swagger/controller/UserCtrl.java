/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.swagger.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import th.co.cana.swagger.model.UserInfo;
import th.co.cana.swagger.utils.MediaTypes;
import th.co.cana.swagger.utils.RespCodes;
import th.co.cana.swagger.utils.ServiceUtils;

/**
 * @author supot
 * @version 1.0
 */

@Tag(name = "User Information API")
@RestController
@RequestMapping("/v1/users")
public class UserCtrl extends Controller {

	@Operation(summary = "List all user")
	@GetMapping()
	public List<UserInfo> getUsers() {
		return ServiceUtils.createUsers(10);
	}

	@Operation(summary = "Get user by id")
	@GetMapping(value = "/{id}")
	public UserInfo getUser(@PathVariable("id") String id) {
		return ServiceUtils.createUser(id);
	}

	@Operation(summary = "Create user", responses = {
			@ApiResponse(responseCode = RespCodes.C201, description = RespCodes.C201_DESC)
	})
	@PostMapping(produces = { MediaTypes.JSON, MediaTypes.XML }, consumes = { MediaTypes.JSON, MediaTypes.XML })
	public ResponseEntity<UserInfo> createUser(@RequestBody UserInfo user) {
		return new ResponseEntity<>(user, HttpStatus.CREATED);
	}

	@Operation(summary = "Update user all information")
	@PutMapping(value = "/{id}", produces = { MediaTypes.JSON, MediaTypes.XML }
		, consumes = { MediaTypes.JSON, MediaTypes.XML })
	public UserInfo updateUser(@PathVariable("id") String id, @RequestBody UserInfo user) {
		return user;
	}

	@Operation(summary = "Update user only give inforamtion")
	@PatchMapping(value = "/{id}", produces = { MediaTypes.JSON, MediaTypes.XML }
		, consumes = { MediaTypes.JSON, MediaTypes.XML })
	public ResponseEntity<UserInfo> updateUserByData(@PathVariable("id") String id, @RequestBody UserInfo user) {
		return new ResponseEntity<>(user, HttpStatus.OK);
	}
}
