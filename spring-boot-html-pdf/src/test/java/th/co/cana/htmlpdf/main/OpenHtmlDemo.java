package th.co.cana.htmlpdf.main;

import com.openhtmltopdf.pdfboxout.PdfRendererBuilder;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class OpenHtmlDemo {
    public static void main(String[] args) throws Exception {
        try (OutputStream os = new FileOutputStream("src/test/resources/pdf/out.pdf")) {
            PdfRendererBuilder builder = new PdfRendererBuilder();
            builder.useFastMode();
            builder.withFile(new File("src/test/resources/html/demo.html"));
            builder.toStream(os);
            builder.run();
        }
    }
}
