package th.co.cana.htmlpdf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootHtmlPdfApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootHtmlPdfApplication.class, args);
	}

}
