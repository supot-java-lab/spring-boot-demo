/*
* -----------------------------------------------------------------------------------
* Copyright © 2021 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.primefaces11.model;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.SOURCE;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import lombok.Getter;
import lombok.Setter;
import lombok.AccessLevel;
/**
* @author supot
* @version 1.0
*/
@Retention(SOURCE)
@Target({ TYPE, FIELD })
@Getter(AccessLevel.NONE)
@Setter(AccessLevel.NONE)
public @interface NoneGetAndSet {

}
