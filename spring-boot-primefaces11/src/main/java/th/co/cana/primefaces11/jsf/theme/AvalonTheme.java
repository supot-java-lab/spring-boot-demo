/*
* -----------------------------------------------------------------------------------
* Copyright © 2021 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.primefaces11.jsf.theme;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.primefaces.PrimeFaces;

import lombok.Data;

/**
* @author supot
* @version 1.0
*/
@Data
public class AvalonTheme implements PrimefacesTheme<AvalonTheme> {
	private static final long serialVersionUID = 1L;
	private static final String OVERLAY = "overlay";
	private static final String HORIZONTAL = "horizontal";
	
	//Themes
	private Map<String, String> themeColors;
    private String theme = "green";
    private String layout = "green";
    private String menuClass 	= null;
    private String profileMode 	= "inline";
    private String menuLayout 	= HORIZONTAL;
    private String inputStyle = "outlined";
    private List<ThemeComponent> componentThemes;
    private List<ThemeLayout> layoutThemes;
    private List<ThemeLayoutSpecial> layoutSpecialThemes;
    
    @Override
	public void initThemeInfo() {
        themeColors = new HashMap<>();
        themeColors.put("green", "#1F8E38");
        componentThemes = ThemeUtils.avalonComponents();
        layoutThemes = ThemeUtils.avalonLayouts();
        layoutSpecialThemes = ThemeUtils.avalonLayoutSpecials();
	}
	
    public String getInputStyleClass() {
    	return ThemeUtils.getInputStyleClass(inputStyle);
    }

	@Override
	public String getLayoutConfig() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getThemeName() {
		return "avalon-" + this.theme;
	}

	@Override
	public AvalonTheme getTheme() {
		return this;
	}
	
    public void onLayoutChange() {
        PrimeFaces.current().executeScript("PrimeFaces.AvalonConfigurator.changeMenuLayout('" + menuLayout + "')");
    }

    public void onMenuThemeChange() {
        if ("layout-menu-dark".equals(menuClass)) {
            PrimeFaces.current().executeScript("PrimeFaces.AvalonConfigurator.changeMenuToDark()");
        } else {
            PrimeFaces.current().executeScript("PrimeFaces.AvalonConfigurator.changeMenuToLight()");
        }
    }
    
    public String getMenu() {
        switch (this.menuLayout) {
            case OVERLAY:
                return "menu-layout-overlay";
            case HORIZONTAL:
                this.profileMode = OVERLAY;
                return "menu-layout-static menu-layout-horizontal";
            case "slim":
                return "menu-layout-static menu-layout-slim";
            default:
                return "menu-layout-static";
        }
    }

    public void setLayout(String layout, boolean special) {
        this.layout = layout;
        if (special) {
            this.menuClass = "layout-menu-dark";
        }
    }
    
    public void setMenuLayout(String menuLayout) {
        if (HORIZONTAL.equalsIgnoreCase(menuLayout)) {
            this.profileMode = OVERLAY;
        }
        
        this.menuLayout = menuLayout;
    }
    
}
