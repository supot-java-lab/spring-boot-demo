/*
* -----------------------------------------------------------------------------------
* Copyright © 2021 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.primefaces11.jsf.theme;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import lombok.Data;
import th.co.cana.utils.Validators;

/**
* @author supot
* @version 1.0
*/
@Data
public class PandoraTheme implements PrimefacesTheme<PandoraTheme> {
	private static final long serialVersionUID = 1L;
	private static final String NONE_COLOR = "transparent";
	
	//Themes
	private String menuMode = "layout-horizontal";
	private String menuColor = "colored";
	private String menuTheme = "teallight";
	private String topbarTheme = "light";
	private String inputStyle = "outlined";
    private String componentTheme = "teallight";     
	private String layoutPrimaryColor = "teallight";
    private boolean groupedMenu = true;
    private boolean darkLogo;
    private List<ThemeComponent> componentThemes;
    private List<ThemeTopbar> topbarThemes;
    private Map<String, List<ThemeMenu>> menuColors;
    private List<ThemePalette> palettes;
    private ThemePalette selectedPalette;
    
    @Override
    public void initThemeInfo() {
        componentThemes = ThemeUtils.pandoraComponents();
        topbarThemes = ThemeUtils.pandoraTopbars();
        menuColors = ThemeUtils.pandoraMenuColors();
        palettes = ThemeUtils.pandoraPalettes();
        
        //Menu: Light | Active: Teal-Light | Topbar: Teal-Light | Components: Teal-Light
        selectedPalette = palettes.get(2);
    }
    
    public void changePalette(ThemePalette palette) {
        this.setMenuColor(palette.getMenuColor().getName());
        this.setMenuTheme(palette.getMenuTheme().getName());
        this.setTopbarTheme(palette.getTopbarTheme().getName());
        this.setComponentTheme(palette.getComponentTheme().getName());
        this.setSelectedPalette(palette);
    }
    
    @Override
    public String getLayoutConfig() {
        StringBuilder sb = new StringBuilder();
        String menuModeClass = ThemeUtils.getMenuModeClass(menuMode);
        String menuThemeClass = "layout-menu-" + ("colored".equals(menuColor) ? this.menuTheme : this.menuColor);

        sb.append("layout-topbar-").append(this.topbarTheme);
        sb.append(" ").append(menuThemeClass);
        sb.append(" ").append(menuModeClass);

        return sb.toString();
    }

	@Override
	public String getInputStyleClass() {
		return ThemeUtils.getInputStyleClass(inputStyle);
	}

	@Override
	public String getLayout() {
		return "layout-" + this.layoutPrimaryColor;
	}

	@Override
	public String getThemeName() {
		return "pandora-" + this.componentTheme;
	}
	
	@Override
	public PandoraTheme getTheme() {
		return this;
	}
	
	public List<ThemeMenu> getThemeMenuItems() {
		if (Validators.isEmpty(menuColors)) {
			return Collections.emptyList();
		}
		return menuColors.get(menuColor);
	}
	
	public String getMenuBgColor() {
		return (selectedPalette != null ? selectedPalette.getMenuColor().getCode() : NONE_COLOR);
	}
	
	public String getMenuTitle() {
		return (selectedPalette != null ? selectedPalette.getMenuColor().getName() : "");
	}
	
	public String getThemeBgColor() {
		return (selectedPalette != null ? selectedPalette.getMenuTheme().getCode() : NONE_COLOR);
	}
	
	public String getThemeTitle() {
		return (selectedPalette != null ? selectedPalette.getMenuTheme().getName() : "");
	}
	
	public String getTopbarBgColor() {
		return (selectedPalette != null ? selectedPalette.getTopbarTheme().getCode() : NONE_COLOR);
	}
	
	public String getTopbarTitle() {
		return (selectedPalette != null ? selectedPalette.getTopbarTheme().getName() : "");
	}
	
	public String getComponentBgColor() {
		return (selectedPalette != null ? selectedPalette.getComponentTheme().getCode() : NONE_COLOR);
	}
	
	public String getComponentTitle() {
		return (selectedPalette != null ? selectedPalette.getComponentTheme().getName() : "");
	}
}
