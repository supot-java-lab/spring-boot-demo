/*
* -----------------------------------------------------------------------------------
* Copyright © 2021 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.primefaces11.jsf.theme;

import java.io.Serializable;

/**
* @author supot
* @version 1.0
*/
public interface PrimefacesTheme<T> extends Serializable {
	
	public void initThemeInfo();
	
	public String getInputStyleClass();
	
	public String getLayoutConfig();
	
	public String getLayout();
	
	public String getThemeName();
	
	public T getTheme();
}
