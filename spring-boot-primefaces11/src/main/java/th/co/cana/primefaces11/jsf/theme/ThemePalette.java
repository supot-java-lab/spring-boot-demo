/*
* -----------------------------------------------------------------------------------
* Copyright © 2021 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.primefaces11.jsf.theme;

import java.io.Serializable;

import lombok.Data;

/**
* @author supot
* @version 1.0
*/
@Data
public class ThemePalette implements Serializable {
	private static final long serialVersionUID = 1L;

	private ThemeColor menuColor;
	private ThemeColor menuTheme;
	private ThemeColor topbarTheme;
	private ThemeColor componentTheme;

    public ThemePalette(ThemeColor menuColor, ThemeColor menuTheme, ThemeColor topbarTheme, ThemeColor componentTheme) {
        this.menuColor = menuColor;
        this.menuTheme = menuTheme;
        this.topbarTheme = topbarTheme;
        this.componentTheme = componentTheme;
    }
}
