/*
* -----------------------------------------------------------------------------------
* Copyright © 2021 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.primefaces11.jsf.controller;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import th.co.cana.primefaces11.model.CustomerModel;
import th.co.cana.utils.Validators;

/**
* @author supot
* @version 1.0
*/
@Named
@RequestScoped
public class FormInputCtrl extends Controller {
	private static final long serialVersionUID = 1L;

	private CustomerModel customer;

	@PostConstruct
	public void initial() {
		info("Starting initial : {}", getClass().getName());
	}
	
	public void saveAction() {
		info("Starting on saveAction");
		info("Customer Data : {}", customer);
	}
	
	public void cancelAction() {
		info("Starting on cancelAction");
		customer = null;
	}

	public CustomerModel getCustomer() {
		if (Validators.isNull(customer)) {
			customer = new CustomerModel();
		}
		return customer;
	}

	public void setCustomer(CustomerModel customer) {
		this.customer = customer;
	}
	
}
