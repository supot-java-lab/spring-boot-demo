/*
* -----------------------------------------------------------------------------------
* Copyright © 2021 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.primefaces11.jsf.theme;

import java.io.Serializable;

import lombok.Data;

/**
* @author supot
* @version 1.0
*/
@Data
public class ThemeComponent implements Serializable {
	private static final long serialVersionUID = 1L;

    private String name;
    private String file;
    private String color;

    public ThemeComponent(String name, String file, String color) {
        this.name = name;
        this.file = file;
        this.color = color;
    }
}
