/*
* -----------------------------------------------------------------------------------
* Copyright © 2021 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.primefaces11.resource;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Supot Saelao
 * @version 1.0
 */
public class JSFResourceBundle extends ClassPathResourceBundle {
	private static final String PACKAGE = "th/co/cana/primefaces11/resource/message/";
	private static final List<String> BUNDLE_NAMES;
	
	static {
		BUNDLE_NAMES = new ArrayList<>(5);
		BUNDLE_NAMES.add(PACKAGE + "JSFMessages");
	}
	
	public JSFResourceBundle() {
		super(BUNDLE_NAMES);
	}
}