/*
* -----------------------------------------------------------------------------------
* Copyright © 2021 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.primefaces11.jsf.utils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
* Add class information here.
* @author Supot Saelao
* @version 1.0
*/
public class PFDialogParam implements Serializable {
	private static final long serialVersionUID = 1L;

	private Map<String, Object> params = null;
	
	public void add(String key, Object value) {
		getParams().put(key, value);
	}
	
	@SuppressWarnings("unchecked")
	public <T> T get(String key) {
		Object value = getParams().get(key);
		if (value == null) {
			return null;
		}
		
		return (T) value;
	}
	
	public Set<String> getKeySet() {
		return getParams().keySet();
	}
	
	public boolean isEmpty() {
		return (params == null || params.isEmpty());
	}
	
	private Map<String, Object> getParams() {
		if (params == null) {
			params = new HashMap<>();
		}
		
		return params;
	}
}
