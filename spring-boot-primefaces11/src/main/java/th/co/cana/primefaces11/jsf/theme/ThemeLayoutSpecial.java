/*
* -----------------------------------------------------------------------------------
* Copyright © 2021 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.primefaces11.jsf.theme;

import java.io.Serializable;

import lombok.Data;

/**
* @author supot
* @version 1.0
*/
@Data
public class ThemeLayoutSpecial implements Serializable {
	private static final long serialVersionUID = 1L;

    private String name;
    private String file;
    private boolean special = true;
    private String color1;
    private String color2;

    public ThemeLayoutSpecial(String name, String file, String color1, String color2) {
        this.name = name;
        this.file = file;
        this.color1 = color1;
        this.color2 = color2;
    }
}
