/*
* -----------------------------------------------------------------------------------
* Copyright © 2021 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.primefaces11.jsf.controller;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import th.co.cana.primefaces11.model.CustomerModel;
import th.co.cana.utils.Validators;

/**
* @author supot
* @version 1.0
*/
@Named
@ViewScoped
public class OutputPanelCtrl extends Controller {
	private static final long serialVersionUID = 1L;

	private CustomerModel customer;
	private boolean loadCustomerSucces;
	private boolean loadOrderSucces;
	
	@PostConstruct
	public void initial() {
		info("Starting initial : {}", getClass().getName());
	}
	
	public void loadCustomAction() {
		try {
			info("Starting on loadCustomAction");
			Thread.sleep(4000);
			customer = new CustomerModel();
			customer.setFirstName("Supot");
			customer.setLastName("Saelao");
			this.loadCustomerSucces = true;
		} catch (Exception ex) {
			error("loadCustomAction", ex);
		} finally {
			info("Finished on loadCustomAction");
		}
	}
	
	public void loadOrderAction() {
		try {
			info("Starting on loadOrderAction");
			Thread.sleep(4000);
			this.loadOrderSucces = true;
		} catch (Exception ex) {
			error("loadOrderAction", ex);
		} finally {
			info("Finished on loadOrderAction");
		}
	}
	
	public void saveAction() {
		info("Starting on saveAction");
		info("Customer Data : {}", customer);
	}
	
	public void cancelAction() {
		info("Starting on cancelAction");
		customer = null;
	}

	public CustomerModel getCustomer() {
		if (Validators.isNull(customer)) {
			customer = new CustomerModel();
		}
		return customer;
	}

	public void setCustomer(CustomerModel customer) {
		this.customer = customer;
	}

	public boolean isLoadCustomerSucces() {
		return loadCustomerSucces;
	}

	public boolean isLoadOrderSucces() {
		return loadOrderSucces;
	}
	
}
