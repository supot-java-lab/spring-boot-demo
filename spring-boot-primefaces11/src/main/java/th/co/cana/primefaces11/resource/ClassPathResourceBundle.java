/*
* -----------------------------------------------------------------------------------
* Copyright © 2021 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.primefaces11.resource;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * @author Supot Saelao
 * @version 1.0
 */
public abstract class ClassPathResourceBundle extends ResourceBundle {
	
	private List<String> bundleNames;
	private Map<String, Object> resourceMap;

	protected ClassPathResourceBundle(List<String> bundleNames) {
		this.bundleNames = bundleNames;
	}

	@Override
	protected Object handleGetObject(String key) {
		if (key == null) {
			throw new NullPointerException();
		}

		this.loadAndMergeBundles();

		return resourceMap.get(key);
	}

	@Override
	public Enumeration<String> getKeys() {
		this.loadAndMergeBundles();

		ResourceBundle parent = this.parent;
		Enumeration<String> keys = (parent != null) ? parent.getKeys() : null;

		return new ResourceBundleEnumeration(resourceMap.keySet(), keys);
	}

	private void loadAndMergeBundles() {
		if (resourceMap != null) {
			return;
		}

		resourceMap = new HashMap<>(512);
		for (String bundleName : bundleNames) {
			ResourceBundle bundle = ResourceBundle.getBundle(bundleName, getLocale());
			Enumeration<String> keys = bundle.getKeys();
			while (keys.hasMoreElements()) {
				String key = keys.nextElement();
				resourceMap.put(key, bundle.getObject(key));
			}
		}
	}

}
