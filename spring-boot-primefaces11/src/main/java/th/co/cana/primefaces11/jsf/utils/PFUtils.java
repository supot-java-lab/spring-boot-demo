/*
* -----------------------------------------------------------------------------------
* Copyright © 2021 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.primefaces11.jsf.utils;

import java.util.Map;

import javax.faces.application.FacesMessage;

import org.primefaces.PrimeFaces;
import org.primefaces.event.SelectEvent;

import th.co.cana.utils.Validators;

/**
 * Utilities class for manage Primefaces UI and component
 * 
 * @author Supot Saelao
 * @version 1.0
 */
public final class PFUtils {
	private static final String CMD_DIALOG = "PF('%s').show();";
	private static final String PARAM_KEY = "PFD_KEY_";

	private PFUtils() {
	}

	public static void openDialog(String pageUrl) {
		openDialog(pageUrl, null, null);
	}

	public static void openDialog(String pageUrl, PFDialogParam params) {
		openDialog(pageUrl, null, params);
	}

	public static void openDialog(String pageUrl, PFDialogOption options) {
		openDialog(pageUrl, options, null);
	}

	public static void openDialog(String pageUrl, PFDialogOption options, PFDialogParam params) {
		if (pageUrl == null || pageUrl.isEmpty()) {
			return;
		}

		setDialogParameter(params);

		if (Validators.isNull(options)) {
			PrimeFaces.current().dialog().openDynamic(pageUrl);
		} else {
			PrimeFaces.current().dialog().openDynamic(pageUrl, options.getOptions(), null);
		}
	}

	public static void closeDialog() {
		closeDialog(null);
	}

	public static void closeDialog(Object obj) {
		clearDialogParameter();
		PrimeFaces.current().dialog().closeDynamic(obj);
	}

	/**
	 * Return Dialog value
	 * 
	 * @param event
	 * @return null if cannot cast to object
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getDialogReturn(SelectEvent<?> event) {
		if (Validators.isNull(event) || Validators.isNull(event.getObject())) {
			return null;
		}

		try {
			return (T) event.getObject();
		} catch (ClassCastException ex) {
			return null;
		}
	}

	public static <T> T getParameter(String key) {
		return FacesUtils.getSessionValue(PARAM_KEY + key);
	}

	public static void showDialog(String widgetVar) {
		if (Validators.isEmpty(widgetVar)) {
			return;
		}
		showDialog(widgetVar, null);
	}

	public static void showDialog(String widgetVar, String clientId) {
		if (Validators.isEmpty(widgetVar)) {
			return;
		}

		if (Validators.isNotEmpty(clientId)) {
			PrimeFaces.current().ajax().update(clientId);
		}

		String command = String.format(CMD_DIALOG, widgetVar);
		PrimeFaces.current().executeScript(command);
	}

	public static void showMessage(FacesMessage message) {
		if (Validators.isNull(message)) {
			return;
		}

		if (message.getSeverity() == FacesMessage.SEVERITY_ERROR
				|| message.getSeverity() == FacesMessage.SEVERITY_FATAL) {
			FacesUtils.validationFailed();
		}
		PrimeFaces.current().dialog().showMessageDynamic(message);
	}

	public static void showMessageInfo(String title, String message) {
		PrimeFaces.current().dialog().showMessageDynamic(FacesUtils.createInfoMessage(title, message));
	}

	public static void showMessageWarning(String title, String message) {
		PrimeFaces.current().dialog().showMessageDynamic(FacesUtils.createWarnMessage(title, message));
	}

	public static void showMessageError(String title, String message) {
		FacesUtils.validationFailed();
		PrimeFaces.current().dialog().showMessageDynamic(FacesUtils.createErrorMessage(title, message));
	}

	public static boolean isAjaxRequest() {
		return PrimeFaces.current().isAjaxRequest();
	}

	public static void execute(String command) {
		PrimeFaces.current().executeScript(command);
	}

	public static void update(String ... clientId) {
		PrimeFaces.current().ajax().update(clientId);
	}

	public static void scrollTo(String clientId) {
		if (Validators.isNotEmpty(clientId)) {
			PrimeFaces.current().scrollTo(clientId);
		}
	}

	public static void scrollToTopPage() {
		PrimeFaces.current().scrollTo("window.scrollTo(0,0);");
	}

	public static void resetInput(String... clientId) {
		if (Validators.isEmpty(clientId)) {
			return;
		}
		PrimeFaces.current().resetInputs(clientId);
	}

	private static void setDialogParameter(PFDialogParam params) {
		if (Validators.isNull(params) || params.isEmpty()) {
			return;
		}

		for (String key : params.getKeySet()) {
			String paramKey = PARAM_KEY + key;
			FacesUtils.addSession(paramKey, params.get(key));
		}
	}

	private static void clearDialogParameter() {
		Map<String, Object> maps = FacesUtils.getSessionMap();
		if (Validators.isEmpty(maps)) {
			return;
		}

		for (Map.Entry<String, Object> map : maps.entrySet()) {
			if (Validators.isEmpty(map.getKey())) {
				continue;
			}

			if (map.getKey().startsWith(PARAM_KEY)) {
				FacesUtils.removeSession(map.getKey());
			}
		}
	}

}
