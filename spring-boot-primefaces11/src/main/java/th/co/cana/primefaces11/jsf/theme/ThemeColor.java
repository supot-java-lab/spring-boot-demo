/*
* -----------------------------------------------------------------------------------
* Copyright © 2021 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.primefaces11.jsf.theme;

import java.io.Serializable;

import lombok.Data;

/**
* @author supot
* @version 1.0
*/
@Data
public class ThemeColor implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String name;
    private String code;

    public ThemeColor() {
    	
    }
    
    public ThemeColor(String name, String code) {
        this.name = name;
        this.code = code;
    }

}
