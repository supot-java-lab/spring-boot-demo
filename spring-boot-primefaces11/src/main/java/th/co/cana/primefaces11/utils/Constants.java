/*
* -----------------------------------------------------------------------------------
* Copyright © 2021 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.primefaces11.utils;

import java.util.Locale;

import th.co.cana.utils.FileUtils;

/**
* @author supot
* @version 1.0
*/
public final class Constants {	
	private Constants() {}
	
	public static final String Y = "Y";
	public static final String N = "N";
	
	public static final String UTF8 = "UTF-8";
	public static final Locale US = Locale.US;
	public static final Locale TH = new Locale("th", "TH");
	public static final String LANG_TH = "TH";
	public static final String LANG_US = "US";
	public static final String HTML_NEW_LINE = "<br/>";
	
    public static final class ASPECT {
    	private ASPECT() {}
    	public static final String SERVUCE 	= "execution(public * th.co.cana.primefacesx.service..*.*(..))";
    	public static final String REPOSITORY = "execution(public * th.co.cana.primefacesx.dao..*.*(..))";
    }
    
	public static String getWorkDir() {
		String workDir = System.getProperty("user.dir");
		if ("/".equals(workDir)) {
			workDir = "/app";
		}
		return FileUtils.convertPathToUnix(workDir);
	}
    
}
