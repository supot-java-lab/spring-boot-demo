/*
* -----------------------------------------------------------------------------------
* Copyright © 2021 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.primefaces11.jsf.controller;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import org.primefaces.PrimeFaces;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.FilesUploadEvent;
import org.primefaces.model.file.UploadedFile;
import org.primefaces.util.EscapeUtils;

/**
* @author supot
* @version 1.0
*/

@Named
@RequestScoped
public class FileUploadCtrl extends Controller {
	private static final long serialVersionUID = 1L;

	@PostConstruct
	public void initial() {
		info("Startin initial : {}", getClass().getName());
	}
	
    public void handleFileUpload(FileUploadEvent event) {
        UploadedFile file = event.getFile();
        this.displayFileInfo(file);
    }
    
	public void handleFileMultipleUpload(FilesUploadEvent event) {
		for (UploadedFile f : event.getFiles().getFiles()) {
			FacesMessage message = new FacesMessage("Successful", f.getFileName() + " is uploaded.");
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
	}
    
    public void handleFileUploadTextarea(FileUploadEvent event) {
    	UploadedFile file = event.getFile();
        String jsVal = "PF('textarea').jq.val";
        String fileName = EscapeUtils.forJavaScript(file.getFileName());
        PrimeFaces.current().executeScript(jsVal + "(" + jsVal + "() + '\\n\\n" + fileName + " uploaded.')");
        this.displayFileInfo(file);
    }
    
    public void clearMultiViewState() {
        FacesContext context = FacesContext.getCurrentInstance();
        String viewId = context.getViewRoot().getViewId();
        PrimeFaces.current().multiViewState().clearAll(viewId, true);
    }
    
    private void displayFileInfo(UploadedFile file) {
        info("File Nmae : {}", file.getFileName());
        info("File ContentType : {}", file.getContentType());
        info("File Size : {}", file.getSize());        
    }
}
