/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.websocket.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import th.co.cana.websocket.model.Message;
import th.co.cana.websocket.model.OutputMessage;

/**
 * @author supot
 * @version 1.0
 */

@Controller
public class ChatCtrl {
	
	@MessageMapping("/chat")
	@SendTo("/topic/messages")
	public OutputMessage send(final Message message) throws Exception {

		final String time = new SimpleDateFormat("HH:mm").format(new Date());
		OutputMessage outmsg = new OutputMessage();
		outmsg.setFrom(message.getFrom());
		outmsg.setText(message.getText());
		outmsg.setTime(time);
		
		return outmsg;
	}
}
