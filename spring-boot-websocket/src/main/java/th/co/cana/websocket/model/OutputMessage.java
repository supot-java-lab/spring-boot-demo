/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.websocket.model;

import java.io.Serializable;

import lombok.Data;

/**
* @author supot
* @version 1.0
*/
@Data
public class OutputMessage implements Serializable {
	private static final long serialVersionUID = 1L;
	
    private String from;
    private String text;
    private String time;
}
