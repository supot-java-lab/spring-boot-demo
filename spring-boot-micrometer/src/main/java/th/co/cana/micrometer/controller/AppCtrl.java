/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.micrometer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import th.co.cana.micrometer.model.AppInfo;

/**
* @author supot
* @version 1.0
*/

@RestController
@RequestMapping("/v1")
public class AppCtrl extends Controller {
	@Autowired
	private BuildProperties buildTag;
	
	@GetMapping("/info")
	public ResponseEntity<AppInfo> appInfo() {
		AppInfo app = AppInfo.builder()
				.id("A1001")
				.name(buildTag.getName())
				.version(buildTag.getVersion())
				.time(buildTag.getTime().toString())
				.javaVersion(buildTag.get("java.version"))
				.build();
		
		return ResponseEntity.ok(app);
	}
}
