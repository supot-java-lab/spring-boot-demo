/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.micrometer.utils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import th.co.cana.micrometer.model.CustomerModel;
import th.co.cana.micrometer.model.UserModel;
import th.co.cana.utils.Convertors;
import th.co.cana.utils.Validators;

/**
 * @author supot
 * @version 1.0
 */
public final class ServiceUtils {
	private ServiceUtils() {

	}

	private static List<UserModel> users;
	private static List<CustomerModel> customers;

	public static List<UserModel> getUsers() {
		initialUser();
		return users;
	}

	public static UserModel getUser(String id) {
		if (Validators.isEmpty(id)) {
			return null;
		}

		initialUser();
		for (UserModel user : users) {
			if (id.equalsIgnoreCase(user.getId())) {
				return user;
			}
		}

		return null;
	}

	public static List<CustomerModel> getCustomers() {
		initialCustomer();
		return customers;
	}

	public static CustomerModel getCustomer(String id) {
		if (Validators.isEmpty(id)) {
			return null;
		}

		initialCustomer();
		for (CustomerModel cust : customers) {
			if (id.equalsIgnoreCase(cust.getId())) {
				return cust;
			}
		}

		return null;
	}

	private static void initialUser() {
		if (Validators.isNotEmpty(users)) {
			return;
		}

		users = new ArrayList<>();
		for (int i = 1; i <= 10; i++) {
			UserModel user = UserModel.builder().id(Convertors.toString(i)).firstName("Firt Name " + i)
					.lastName("Last name " + i).lastAccess(new Date()).build();
			users.add(user);
		}
	}

	private static void initialCustomer() {
		if (Validators.isNotEmpty(customers)) {
			return;
		}

		customers = new ArrayList<>();
		for (int i = 1; i <= 10; i++) {
			CustomerModel cust = new CustomerModel();
			cust.setId(Convertors.toString(i));
			cust.setFirstName("Firt Name " + i);
			cust.setLastName("Last Name " + i);
			cust.setOraderAmt(BigDecimal.valueOf(i * 2500.45D));

			customers.add(cust);
		}
	}
}
