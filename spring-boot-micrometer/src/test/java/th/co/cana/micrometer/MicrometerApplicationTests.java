package th.co.cana.micrometer;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MicrometerApplicationTests {	
	private static Logger logger = LoggerFactory.getLogger(MicrometerApplicationTests.class);
	
	@Test
	void contextLoads() {
		logger.info("Spring application started..");
	}

}
