# Spring boot Actuator and Micrometer 
This is Spring Boot Actuators and Micrometer

### ทำไมถึงใช้ Micrometer
เนื่องจากรองรับระบบ monitoring ต่าง ๆ   ประมาณนี้
- Prometheus
- Netflix Atlas
- CloudWatch
- Datadog
- Graphite
- Ganglia
- JMX
- Influx/Telegraf
- New Relic
- StatsD
- SignalFx
- Wavefront
- Etc.

[Micrometer document](https://micrometer.io/docs "Micrometer document")

### Metric ใน Micrometer
- Dimension/Tag
- Counter
- Gauge
- Timer
- Long task timer
- Distribution summary
- Summary statistic
- Quantile
- Histogram
- Binder

### 1. Package project 
	mvn package
	
### 2. Run Docker Compose 
	docker-compose -f ./docker/docker-compose.yml up -d
	
### 3. Checking 
- [Access Prometheus -> http://localhost:9090/targets](http://localhost:9090/targets)
- [Access Grafana -> http://localhost:3000/metrics](http://localhost:3000)
- [Access Spring Web -> http://localhost:8080/](http://localhost:8080)

### 4. Import Dashboard
 - [Download JVM (Micrometer)](https://grafana.com/grafana/dashboards/4701)
 - Login to Grafana and import