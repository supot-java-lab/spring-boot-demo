# Spring boot and Spring Batch
Demo spring boot application with Spring Batch

# Ref
- [Spring Batch Tutorial: COOL](https://www.toptal.com/spring/spring-batch-tutorial)
- [Large Data Sets With Spring Batch](https://dzone.com/articles/batch-processing-large-data-sets-with-spring-boot)
- [Spring Batch Parallel Processing](https://examples.javacodegeeks.com/enterprise-java/spring/batch/spring-batch-parallel-processing-example/)
- [Spring Batch Document](https://docs.spring.io/spring-batch/docs/current/reference/html/job.html)