package th.co.cana.batch.repository;

import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import th.co.cana.batch.BaseUnitTest;
import th.co.cana.batch.entity.Customer;

import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class CustomerRepositoryTest extends BaseUnitTest {

    @Autowired
    private CustomerRepository repository;

    @DisplayName("[CustomerRepository]: findAll")
    @Test
    @Order(1)
    void findAll() {
        List<Customer> customers = repository.findAll();
        Assertions.assertNotNull(customers);
    }
}
