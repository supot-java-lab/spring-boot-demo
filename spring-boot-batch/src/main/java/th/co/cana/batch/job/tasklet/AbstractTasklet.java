package th.co.cana.batch.job.tasklet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.lang.NonNull;

/**
 * @author supot.jdev
 * @version 1.0
 */
public abstract class AbstractTasklet implements Tasklet, StepExecutionListener {
    protected Logger logger = LoggerFactory.getLogger(getClass());

    public abstract RepeatStatus executeStep(StepContribution stepCon, ChunkContext chunkCtx);

    @Override
    public RepeatStatus execute(@NonNull StepContribution contribution, @NonNull ChunkContext chunkContext) {
        return executeStep(contribution, chunkContext);
    }

    @Override
    public ExitStatus afterStep(@NonNull StepExecution stepExecution) {
        return ExitStatus.COMPLETED;
    }
}
