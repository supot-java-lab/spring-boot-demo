package th.co.cana.batch.config.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.io.Serializable;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "application")
public class ApplicationConfigProperties implements Serializable {
    private String name;
    private String version;
    private String buildTime;
    private String javaBuildVersion;

    private String outputDir;
    private String logDir;
}
