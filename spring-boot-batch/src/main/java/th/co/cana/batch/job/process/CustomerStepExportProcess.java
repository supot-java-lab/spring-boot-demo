package th.co.cana.batch.job.process;

import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.StepExecution;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import th.co.cana.batch.exception.BatchJobException;
import th.co.cana.batch.job.StepExportProcess;
import th.co.cana.batch.job.step.properties.StepProperties;
import th.co.cana.batch.service.AbstractService;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Service
public class CustomerStepExportProcess extends AbstractService implements StepExportProcess {
    private final StepProperties stepProperties;

    public CustomerStepExportProcess(@Qualifier("customerStepProperties") StepProperties stepProperties) {
        this.stepProperties = stepProperties;
    }

    @Override
    public void completed(StepExecution stepExecution) throws BatchJobException {
        JobParameters params = stepExecution.getJobParameters();
        logger.info("JobParameters: {}", params);
        logger.info("Step Properties: {}", stepProperties);
        logger.info("Step completed");
    }
}
