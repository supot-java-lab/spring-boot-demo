package th.co.cana.batch;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.*;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import th.co.cana.batch.service.DynamicBeanFactory;

import java.time.LocalDateTime;
import java.util.Arrays;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Slf4j
@RequiredArgsConstructor
@Component
public class AppStartupRunner implements CommandLineRunner {
    private final JobLauncher jobLauncher;
    private final DynamicBeanFactory dynamicBeanFactory;

    @Override
    public void run(String... args) {
        log.info("++++++++++++++++++++++++++++++ Starting StartupRunner ++++++++++++++++++++++++++++++");
        try {
            log.info("Application argument parameter: {}", Arrays.toString(args));
            Job job = dynamicBeanFactory.getBean("exportCustomer", Job.class);
            if (job == null) {
                throw new Exception("Lookup Bean not found, find by");
            }

            JobParameters jobParams = new JobParametersBuilder()
                    .addLocalDateTime("jobRunTime", LocalDateTime.now())
                    .addString("inputFile", "")
                    .toJobParameters();

            JobExecution execution = jobLauncher.run(job, jobParams);
            if (BatchStatus.COMPLETED.equals(execution.getStatus())) {
                log.info("Job execution completed");
            } else {
                log.info("Job execution running....");
            }
        } catch (Exception ex) {
            log.error("AppStartupRunner Error", ex);
        } finally {
            log.info("++++++++++++++++++++++++++++++ Finished StartupRunner ++++++++++++++++++++++++++++++");
        }
    }

}