package th.co.cana.batch.service;

import io.github.jdevlibs.utils.Validators;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author supot.jdev
 * @version 1.0
 */

@Slf4j
@Component
public class DynamicBeanFactory {
    private final BeanFactory beanFactory;

    @Autowired
    public DynamicBeanFactory(BeanFactory beanFactory) {
        this.beanFactory = beanFactory;
    }

    public <T> T getBean(String name, Class<T> type) {
        if (Validators.isEmpty(name) || Validators.isNull(type)) {
            return null;
        }

        T bean = null;
        try {
            bean = beanFactory.getBean(name, type);
        } catch (BeansException ex) {
            String error = String.format("getBean by name %s error", name);
            log.error(error, ex);
        }

        return bean;
    }
}
