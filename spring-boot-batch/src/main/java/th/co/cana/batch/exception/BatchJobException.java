/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.batch.exception;

/**
* @author supot
* @version 1.0
*/
public class BatchJobException extends RuntimeException {
	public BatchJobException(String msg) {
		super(msg);
	}

	public BatchJobException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public BatchJobException(Throwable cause) {
		super(cause);
	}
}
