package th.co.cana.batch.listener;

import io.github.jdevlibs.utils.Validators;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.StepExecutionListener;
import th.co.cana.batch.exception.BatchJobException;
import th.co.cana.batch.job.StepExportProcess;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Slf4j
public class StepExportCompletionListener implements StepExecutionListener {

    private final StepExportProcess exportProcess;
    public StepExportCompletionListener(StepExportProcess stepExportProcess) {
        this.exportProcess = stepExportProcess;
    }

    @Override
    public ExitStatus afterStep(StepExecution stepExecution) {
        if (stepExecution.getStatus() == BatchStatus.COMPLETED) {
            String stepName = stepExecution.getStepName();
            log.info("Step Name : {} Start {}", stepName, stepExecution.getStartTime());
            log.info("Step Summary: {}", stepExecution.getSummary());
            if (Validators.isNotNull(exportProcess)) {
                try {
                    exportProcess.completed(stepExecution);
                    return ExitStatus.COMPLETED;
                } catch (BatchJobException ex) {
                    log.error(ex.getMessage(), ex);
                    return ExitStatus.FAILED;
                }
            }
            return ExitStatus.COMPLETED;
        } else {
            if (Validators.isNotNull(exportProcess)) {
                exportProcess.failed(stepExecution);
            }
            return ExitStatus.FAILED;
        }
    }
}
