package th.co.cana.batch.utils;

import io.github.jdevlibs.spring.Transformers;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.support.SqlPagingQueryProviderFactoryBean;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.batch.item.file.transform.LineAggregator;
import org.springframework.batch.support.DatabaseType;
import org.springframework.core.io.FileSystemResource;
import th.co.cana.batch.exception.BatchJobException;
import th.co.cana.batch.job.step.properties.StepProperties;
import th.co.cana.batch.model.FileItemExtractor;

import javax.sql.DataSource;
import java.io.File;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Slf4j
public final class BatchJobUtils {

    private BatchJobUtils() {}

    public static String getOutDataFullPath(final StepProperties property, String fileName) {
        return property.getOutPath() + File.separator + fileName + property.getOutDataFilenameExtension();
    }

    public static <T> JdbcPagingItemReader<T> jdbcPagingItemReader(final DataSource dataSource, int pagingSize, Class<T> clazz) {
        JdbcPagingItemReader<T> jdbcPagingReader = new JdbcPagingItemReader<>();
        try {
            jdbcPagingReader.setDataSource(dataSource);
            jdbcPagingReader.setPageSize(pagingSize);
            jdbcPagingReader.setRowMapper(Transformers.toBean(clazz));
        } catch (Exception ex) {
            log.error("jdbcPagingItemReader Error: ", ex);
            throw new BatchJobException(ex);
        }
        return jdbcPagingReader;
    }

    public static SqlPagingQueryProviderFactoryBean pagingQueryProvider(final DataSource dataSource) {
        SqlPagingQueryProviderFactoryBean queryProvider = new SqlPagingQueryProviderFactoryBean();
        queryProvider.setDataSource(dataSource);
        queryProvider.setDatabaseType(DatabaseType.MYSQL.getProductName());

        return queryProvider;
    }

    public static <T extends FileItemExtractor> LineAggregator<T> delimitedLineAggregator(String delimiter) {
        DelimitedLineAggregator<T> delimitedLine = new DelimitedLineAggregator<>();
        delimitedLine.setDelimiter(delimiter);
        delimitedLine.setFieldExtractor(T::extractValues);

        return delimitedLine;
    }

    public static <T extends FileItemExtractor> FlatFileItemWriter<T> flatFileItemWriter(String delimiter, String lineSeparator) {
        FlatFileItemWriter<T> writer = new FlatFileItemWriter<>();
        writer.setLineAggregator(delimitedLineAggregator(delimiter));
        writer.setLineSeparator(lineSeparator);
        return writer;
    }

    public static <T extends FileItemExtractor> FlatFileItemWriter<T> flatDataFileItemWriter(final StepProperties property, String fileName) {
        FlatFileItemWriter<T> writer = new FlatFileItemWriter<>();
        writer.setLineAggregator(delimitedLineAggregator(property.getOutDataDelimiter()));
        writer.setLineSeparator(property.getOutDataLineSeparator());
        writer.setAppendAllowed(true);

        String fullName = BatchJobUtils.getOutDataFullPath(property, fileName);
        writer.setResource(new FileSystemResource(fullName));

        return writer;
    }
}
