package th.co.cana.batch.model;

import java.io.Serializable;

public interface FileItemExtractor extends Serializable {
    Object[] extractValues();
}
