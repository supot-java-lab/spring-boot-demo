package th.co.cana.batch.job.step;

import io.github.jdevlibs.utils.DateFormats;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.core.step.tasklet.TaskletStep;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.Order;
import org.springframework.batch.item.database.support.SqlPagingQueryProviderFactoryBean;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.PlatformTransactionManager;
import th.co.cana.batch.exception.BatchJobException;
import th.co.cana.batch.job.StepExportProcess;
import th.co.cana.batch.job.step.properties.StepProperties;
import th.co.cana.batch.listener.StepExportCompletionListener;
import th.co.cana.batch.model.Customer;
import th.co.cana.batch.utils.BatchJobUtils;

import javax.sql.DataSource;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author supot.jdev
 * @version 1.0
 */
@RequiredArgsConstructor
@Slf4j
@Configuration
public class StepCustomer {

    private final JobRepository jobRepository;
    private final PlatformTransactionManager transactionManager;
    private final DataSource dataSource;

    @Bean
    public TaskletStep customerStep(@Qualifier("customerStepExportProcess") final StepExportProcess stepExportProcess) {

        return new StepBuilder("stepCustomer", jobRepository)
                .<Customer, Customer>chunk(customerStepProperties().getChunkSize(), transactionManager)
                .reader(customerJdbcPagingItemReader())
                .writer(customerItemWriter())
                .listener(new StepExportCompletionListener(stepExportProcess))
                .build();
    }

    @Bean
    @StepScope
    public JdbcPagingItemReader<Customer> customerJdbcPagingItemReader() {
        try {
            JdbcPagingItemReader<Customer> jdbcPagingReader = BatchJobUtils.jdbcPagingItemReader(dataSource
                    , customerStepProperties().getJdbcPagingSize(), Customer.class);

            SqlPagingQueryProviderFactoryBean queryProvider = customerQueryProvider(dataSource);
            jdbcPagingReader.setQueryProvider(Objects.requireNonNull(queryProvider.getObject()));

            Map<String, Object> params = new LinkedHashMap<>();
            params.put("P_REGISTER_DATE", LocalDate.now());
            jdbcPagingReader.setParameterValues(params);

            return jdbcPagingReader;
        } catch (Exception ex) {
            log.error("customerJdbcPagingItemReader Error: ", ex);
            throw new BatchJobException(ex);
        }
    }

    @Bean
    @StepScope
    public FlatFileItemWriter<Customer> customerItemWriter() {
        String date = "2024-07-18";
        String fileName = MessageFormat.format(customerStepProperties().getOutDataFilename(), date);
        return BatchJobUtils.flatDataFileItemWriter(customerStepProperties(), fileName);
    }

    @Bean
    @ConfigurationProperties(prefix = "job.customer")
    public StepProperties customerStepProperties() {
        return new StepProperties();
    }

    private SqlPagingQueryProviderFactoryBean customerQueryProvider(DataSource dataSource) {
        SqlPagingQueryProviderFactoryBean queryProvider = BatchJobUtils.pagingQueryProvider(dataSource);
        queryProvider.setSelectClause("*");
        queryProvider.setFromClause("(SELECT * FROM customer WHERE register_date < :P_REGISTER_DATE) T");

        Map<String, Order> sortKeys = new LinkedHashMap<>();
        sortKeys.put("id", Order.ASCENDING);
        queryProvider.setSortKeys(sortKeys);

        return queryProvider;
    }
}
