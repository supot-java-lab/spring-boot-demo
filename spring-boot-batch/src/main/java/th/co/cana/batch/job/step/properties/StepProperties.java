package th.co.cana.batch.job.step.properties;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Getter
@Setter
@ToString
public class StepProperties {

    protected String endOfDayTime;
    protected String outPath;

    protected String outDataFilename;
    protected String outDataFilenameExtension;

    protected String outDataDelimiter;
    protected String outDataLineSeparator;

    protected String outCtrlFilename;
    protected String outCtrlFilenameExtension;

    protected Integer jdbcPagingSize;
    protected Integer chunkSize = 500;
}
