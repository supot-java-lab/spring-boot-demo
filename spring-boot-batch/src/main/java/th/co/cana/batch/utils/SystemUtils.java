package th.co.cana.batch.utils;

import java.util.concurrent.TimeUnit;

/**
 * @author supot.jdev
 * @version 1.0
 */
public final class SystemUtils {

    private SystemUtils() {}

    public static void sleepWithMinute(long timeout) {
        if (timeout > 0) {
            sleep(TimeUnit.MINUTES, timeout);
        }
    }

    public static void sleepWithSecond(long timeout) {
        if (timeout > 0) {
            sleep(TimeUnit.SECONDS, timeout);
        }
    }

    public static void sleepWithMilliSecond(long timeout) {
        if (timeout > 0) {
            sleep(TimeUnit.MILLISECONDS, timeout);
        }
    }

    public static void sleep(TimeUnit timeUnit, long timeout) {
        try {
            if (timeUnit != null && timeout > 0) {
                timeUnit.sleep(timeout);
            }
        } catch (InterruptedException ex) {
            //Skip
        }
    }
}
