package th.co.cana.batch.job.tasklet;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.stereotype.Component;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Component
public class TaskletDemo extends AbstractTasklet {

    @Override
    public RepeatStatus executeStep(StepContribution stepCon, ChunkContext chunkCtx) {
        String stepName = stepCon.getStepExecution().getStepName();
        logger.info("executeStep Name: {}", stepName);

        return RepeatStatus.FINISHED;
    }
}
