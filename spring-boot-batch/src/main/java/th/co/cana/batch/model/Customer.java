/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.batch.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
* @author supot
* @version 1.0
*/

@EqualsAndHashCode(of = {"id"})
@Data
public class Customer implements FileItemExtractor {
	private String id;
	private String firstName;
	private String lastName;
	private String address;
	private BigDecimal creditAmt;
	private LocalDate registerDate;

	@Override
	public Object[] extractValues() {
		return new Object[] {id
				, firstName
				, lastName
				, address
				, creditAmt
				, registerDate
		};
	}
}
