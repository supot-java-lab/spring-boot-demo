package th.co.cana.batch.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import th.co.cana.batch.entity.Customer;

/**
 * @author supot.jdev
 * @version 1.0
 */
public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
