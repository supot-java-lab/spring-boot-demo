package th.co.cana.batch.job;

import org.springframework.batch.core.StepExecution;
import th.co.cana.batch.exception.BatchJobException;

public interface StepExportProcess {
    void completed(StepExecution stepExecution) throws BatchJobException;

    default void failed(StepExecution stepExecution) throws BatchJobException {

    }
}
