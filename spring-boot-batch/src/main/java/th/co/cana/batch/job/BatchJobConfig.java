package th.co.cana.batch.job;

import lombok.RequiredArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @author supot.jdev
 * @version 1.0
 */
@RequiredArgsConstructor
@Configuration
@EnableScheduling
@EnableBatchProcessing
public class BatchJobConfig {

    private final JobRepository jobRepository;

    @Bean
    public Job exportCustomer(final Step customerStep) {
        return new JobBuilder("jobExportCustomer", jobRepository)
                .incrementer(new RunIdIncrementer())
                .start(customerStep)
                .next(customerStep)
                .build();
    }
}
