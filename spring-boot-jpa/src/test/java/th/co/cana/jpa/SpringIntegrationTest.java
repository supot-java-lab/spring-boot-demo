/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.jpa;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.TestMethodOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

/**
* @author supot
* @version 1.0
*/

@SpringBootTest
@Tag("skip")
@TestMethodOrder(OrderAnnotation.class)
public abstract class SpringIntegrationTest extends SpringUnitTest {
	protected Logger logger = LoggerFactory.getLogger(getClass());

}
