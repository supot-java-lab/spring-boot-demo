/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.jpa;

import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
* @author supot
* @version 1.0
*/

@ExtendWith(SpringExtension.class)
@ActiveProfiles(profiles = "dev")
public abstract class SpringUnitTest {
	protected Logger logger = LoggerFactory.getLogger(getClass());

}
