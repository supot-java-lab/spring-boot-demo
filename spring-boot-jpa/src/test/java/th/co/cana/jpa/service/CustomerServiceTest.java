/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.jpa.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import th.co.cana.jpa.SpringIntegrationTest;
import th.co.cana.jpa.entity.Customer;

/**
 * @author supot
 * @version 1.0
 */
class CustomerServiceTest extends SpringIntegrationTest {

	@Autowired
	private CustomerService service;

	@Test
	@Order(1)
	void save() throws Exception {
		Customer entity = new Customer();
		entity.setFirstName("Supot");
		entity.setLastName("Saelao");
		entity.setRegisterDt(LocalDateTime.now());
		entity.setCreditAmt(BigDecimal.valueOf(2500000D));
		
		Customer cust = service.save(entity);
		assertNotNull(cust.getId());
		assertEquals(entity.getFirstName(), cust.getFirstName());
	}
	
	@Test
	@Order(2)
	void getAllCustomer() throws Exception {
		List<Customer> custs = service.getCustomers();
		assertNotNull(custs);
		custs.forEach(cust-> logger.info("{}", cust));
	}
}
