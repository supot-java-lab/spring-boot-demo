/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.jpa.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import th.co.cana.jpa.entity.Customer;
import th.co.cana.jpa.repository.CustomerRepository;

/**
* @author supot
* @version 1.0
*/

@Service
@Transactional
public class CustomerService extends BaseService {

	@Autowired
	private CustomerRepository repository;
	
	public Customer save(Customer entity) {
		return repository.save(entity);
	}
	
	public Customer update(Customer entity) {
		return repository.save(entity);
	}
	
	public Customer getCustomer(Long id) {
		Optional<Customer> optVal = repository.findById(id);
		return optVal.orElse(null);
	}
	
	public List<Customer> getCustomers() {
		return repository.findAll();
	}
}
