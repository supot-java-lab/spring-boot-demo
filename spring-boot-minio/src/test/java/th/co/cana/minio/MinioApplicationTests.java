package th.co.cana.minio;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import th.co.cana.minio.service.MinioService;

@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
class MinioApplicationTests {

	@Autowired
	private MinioService service;

	@Test
	@Order(1)
	@Disabled
	void upload() throws Exception {
		service.upload("D:/AppProject/GITLAB/SPRING_DEMO/spring-boot-minio/pom.xml", "upload");
		assertTrue(Boolean.TRUE);
	}
	
	@Test
	@Order(2)
	void downlaod() throws Exception {
		byte[] contents = service.getFileToByte("upload/pom.xml");
		assertNotNull(contents);
	}
}
