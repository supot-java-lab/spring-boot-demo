/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.minio.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import io.minio.BucketExistsArgs;
import io.minio.GetObjectArgs;
import io.minio.GetObjectResponse;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import th.co.cana.minio.config.MinioProperties;
import th.co.cana.minio.exception.MinioServiceException;
import th.co.cana.utils.FileUtils;
import th.co.cana.utils.IOUtils;
import th.co.cana.utils.Validators;

/**
* @author supot
* @version 1.0
*/

@Component
public class MinioService {
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private MinioClient minioClient;
	@Autowired
	private MinioProperties miniProperty;
	
	public void upload(String filePath, String srcPath) throws MinioServiceException {
		upload(filePath, srcPath, null);
	}
	
	public void upload(String filePath, String srcPath, String objName) throws MinioServiceException {
		upload(new File(filePath), srcPath, objName);
	}
	
	public void upload(File file, String srcPath) throws MinioServiceException {
		upload(file, srcPath, null);
	}
	
	public void upload(File file, String srcPath, String objName) throws MinioServiceException {
		if (!file.exists()) {
			throw new MinioServiceException("Upload file : " + file.getName() + " not exists");
		}
		if (file.isDirectory()) {
			throw new MinioServiceException("Upload file : " + file.getName() + " is directory");
		}
		
		if (Validators.isEmpty(objName)) {
			objName = file.getName();
		}
		
		try {
			Long fileSize = FileUtils.getFileSize(file);
			upload(new FileInputStream(file), srcPath, objName, fileSize);
		} catch (FileNotFoundException ex) {
			throw new MinioServiceException(ex);
		}
	}
	
	public void upload(InputStream ins, String srcPath, String objName, long fileSize) throws MinioServiceException {
		try {
			PutObjectArgs args = PutObjectArgs.builder()
					.bucket(getBucketName())
					.object(srcPath + "/ " + objName)
					.stream(ins, fileSize, -1)
					.build();
			minioClient.putObject(args);
		} catch (Exception ex) {
			throw new MinioServiceException(ex);
		}
	}
	
	public byte[] getFileToByte(String pathName) throws MinioServiceException {
		if (Validators.isEmpty(pathName)) {
			throw new MinioServiceException("Invalid path name");
		}
		
		try {
			GetObjectArgs args = GetObjectArgs.builder()
					.bucket(getBucketName())
					.object(pathName)
					.build();
			InputStream ins = minioClient.getObject(args);	
			return IOUtils.toBytes(ins);
		} catch (Exception ex) {
			throw new MinioServiceException(ex);
		}
	}
	
	public void createBucket(String name) throws MinioServiceException {
		if (Validators.isEmpty(name)) {
			throw new MinioServiceException("Required bucket name");
		}

		try {
			BucketExistsArgs args = BucketExistsArgs.builder().bucket(name).build();
			boolean isExist = minioClient.bucketExists(args);
			if (isExist) {
				logger.warn("Bucket {} already exists.", name);
			} else {
				minioClient.makeBucket(MakeBucketArgs.builder().bucket(name).build());
				logger.info("Create bucket {} success.", name);
			}
		} catch (Exception ex) {
			throw new MinioServiceException(ex);
		}
	}
	
	private String getBucketName() {
		return miniProperty.getBucketName();
	}
}
