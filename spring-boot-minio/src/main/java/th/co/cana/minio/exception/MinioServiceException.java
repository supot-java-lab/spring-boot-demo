/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.minio.exception;

/**
 * 
 * @author Supot Saelao
 * @version 1.0
 */
public class MinioServiceException extends Exception {
	private static final long serialVersionUID = 1L;

	public MinioServiceException() {
		super();
	}

	public MinioServiceException(String message) {
		super(message);
	}

	public MinioServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	public MinioServiceException(Throwable cause) {
		super(cause);
	}
}
