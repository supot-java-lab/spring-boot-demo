/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.minio.config;

import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author supot
 * @version 1.0
 */

@ConfigurationProperties(prefix = "minio")
public class MinioProperties {

	/**
	 * Minio server URL
	 */
	@NotNull
	private String endpoint;
	
	/**
	 * Minio access key
	 */
	@NotNull
	private String accessKey;
	
	/**
	 * Minio secret key
	 */
	@NotNull
	private String secretKey;
	
	/**
	 * Minio bucket name
	 */
	private String bucketName;
	
	/**
	 * Minio bucket path
	 */
	private String bucketPath;

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public String getAccessKey() {
		return accessKey;
	}

	public void setAccessKey(String accessKey) {
		this.accessKey = accessKey;
	}

	public String getSecretKey() {
		return secretKey;
	}

	public void setSecretKey(String secretKey) {
		this.secretKey = secretKey;
	}

	public String getBucketName() {
		return bucketName;
	}

	public void setBucketName(String bucketName) {
		this.bucketName = bucketName;
	}

	public String getBucketPath() {
		return bucketPath;
	}

	public void setBucketPath(String bucketPath) {
		this.bucketPath = bucketPath;
	}

}
