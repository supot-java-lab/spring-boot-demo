/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.minio.controller;

import java.io.IOException;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import th.co.cana.minio.exception.MinioServiceException;
import th.co.cana.minio.service.MinioService;
import th.co.cana.utils.FileUtils;
import th.co.cana.utils.fotmat.FormatDate;

/**
* @author supot
* @version 1.0
*/

@RestController
@RequestMapping("v1/minio")
public class UploadFileCtrl extends Controller {

	@Autowired
	private MinioService minioService;
	
	@PostMapping
	public ResponseEntity<String> uploadFile(@RequestParam MultipartFile file) {
		try {
			logger.info("Name : {}", file.getOriginalFilename());
			logger.info("ContentType : {}", file.getContentType());
			logger.info("Size : {}", file.getSize());
			
			minioService.upload(file.getInputStream(), "", file.getOriginalFilename(), 
					file.getSize());			
			return ResponseEntity.ok("Upload file : " + file.getOriginalFilename());
		} catch (MinioServiceException | IOException ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
		}
	}
	
	@PostMapping("/path")
	public ResponseEntity<String> uploadFileByPath(@RequestParam MultipartFile file) {
		try {
			logger.info("Name : {}", file.getOriginalFilename());
			logger.info("ContentType : {}", file.getContentType());
			logger.info("Size : {}", file.getSize());
			
			String objName = new FormatDate("yyyyMMdd").format(new Date()) +"/";
			objName += file.getOriginalFilename();
			logger.info("Update as file : {}", objName);
			
			minioService.upload(file.getInputStream(), "", objName, file.getSize());
			return ResponseEntity.ok("Upload file : " + objName);
		} catch (MinioServiceException | IOException ex) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
		}
	}
	
	@GetMapping
	public ResponseEntity<Resource> downloadFile(@RequestParam(name = "file") String filePath) {
		try {
			logger.info("Starting download file : {}", filePath);
			
			String fileName = FileUtils.getFileName(filePath);
			byte[] contents = minioService.getFileToByte(filePath);
			HttpHeaders responseHeaders = fileHttpHeader(fileName);
			Resource insResource = new ByteArrayResource(contents);
			return new ResponseEntity<>(insResource, responseHeaders, HttpStatus.OK);
		} catch (MinioServiceException ex) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
		}
	}
}
