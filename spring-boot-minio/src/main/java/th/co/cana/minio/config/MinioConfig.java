/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.minio.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.minio.MinioClient;

/**
 * @author supot
 * @version 1.0
 */

@Configuration
public class MinioConfig {

	@Bean
	public MinioClient minioClient() {
		return MinioClient.builder()
				.endpoint(miniProperty().getEndpoint())
				.credentials(miniProperty().getAccessKey(), miniProperty().getSecretKey())
				.build();
	}

	@Bean
	public MinioProperties miniProperty() {
		return new MinioProperties();
	}
}
