# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Easy Session Sharing in Spring Boot with Spring Session and MySQL](https://developer.okta.com/blog/2020/10/02/spring-session-mysql)
* [Load Balancing, Affinity, Persistence, Sticky Sessions](https://www.haproxy.com/blog/load-balancing-affinity-persistence-sticky-sessions-what-you-need-to-know/)
* [Horizontal Scaling JSF Applications With Spring Session](https://dzone.com/articles/horizontal-scaling-jsf-applications-with-spring-se)
* [Spring Boot + Redis Clusters + Docker - Complete Guide](https://codingfullstack.com/java/spring-boot/spring-boot-redis-cluster/)

### Guides
Load balancer nginx:

* [Sticky Session Load Balancer with Nginx](https://aggarwalarpit.wordpress.com/2015/11/12/sticky-session-load-balancer-with-nginx/)
* [How to realize load balancing and session sharing in nginx](https://developpaper.com/how-to-realize-load-balancing-and-session-sharing-in-nginx/)

# Deploy & Run
### Docker
docker-compose up --scale primefaces-app=2 -d

