/*
* -----------------------------------------------------------------------------------
* Copyright © 2021 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.primefacesx;

import org.junit.jupiter.api.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

/**
* @author supot
* @version 1.0
*/

@SpringBootTest
@Tag("skip")
public abstract class BaseUnitTest extends SpringUnitTest {
	protected Logger logger = LoggerFactory.getLogger(getClass());

}
