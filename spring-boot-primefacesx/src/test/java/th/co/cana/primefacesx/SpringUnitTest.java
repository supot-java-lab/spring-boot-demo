/*
* -----------------------------------------------------------------------------------
* Copyright © 2021 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.primefacesx;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
* @author supot
* @version 1.0
*/

@ExtendWith(SpringExtension.class)
@TestMethodOrder(OrderAnnotation.class)
public abstract class SpringUnitTest {
	protected Logger logger = LoggerFactory.getLogger(getClass());

}
