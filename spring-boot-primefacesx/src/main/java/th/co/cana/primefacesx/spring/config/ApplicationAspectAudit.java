/*
* -----------------------------------------------------------------------------------
* Copyright © 2021 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.primefacesx.spring.config;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;

import th.co.cana.primefacesx.utils.Constants;
/**
* @author Supot Saelao
* @version 1.0
*/

@Aspect
@Component
@ConditionalOnProperty(value = "system.conf.enabled-audit", havingValue = "true")
public class ApplicationAspectAudit {
	private static Logger logger = LoggerFactory.getLogger(ApplicationAspectAudit.class);

	@Around(Constants.ASPECT.REPOSITORY)
	public Object executeRepositoryProcess(ProceedingJoinPoint joinPoint) throws Throwable {
		return execute(joinPoint);
	}
	
	@Around(Constants.ASPECT.SERVUCE)
	public Object executeServiceProcess(ProceedingJoinPoint joinPoint) throws Throwable {
		return execute(joinPoint);
	}
	
	private Object execute(ProceedingJoinPoint joinPoint) throws Throwable {
		StopWatch stopWatch = new StopWatch();
		try {
			stopWatch.start();
			return joinPoint.proceed();
		} finally {
			stopWatch.stop();
			logs(stopWatch, joinPoint);
		}
	}
	
	private void logs(StopWatch stopWatch, ProceedingJoinPoint joinPoint) {
		try {		
			logger.info("Performance Audit[{}.{}] execution time : {} ms", 
					joinPoint.getTarget().getClass().getName(), 
					joinPoint.getSignature().getName(), 
					stopWatch.getTotalTimeMillis());	
		} catch (Exception ex) {
			//Ignore
		}
	}
}
