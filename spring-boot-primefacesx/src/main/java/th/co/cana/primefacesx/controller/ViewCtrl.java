/*
* -----------------------------------------------------------------------------------
* Copyright © 2021 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.primefacesx.controller;

import java.net.InetAddress;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

import th.co.cana.primefacesx.model.UserModel;
import th.co.cana.primefacesx.utils.FacesUtils;

/**
* @author supot
* @version 1.0
*/

@Named
@ViewScoped
public class ViewCtrl extends Controller {
	private static final long serialVersionUID = 1L;

	private UserModel form;
	private String sessionId;
	private String systemName;
	
	@PostConstruct
	public void initial() {
		info("Startin initial : {}", getClass());
	}
	
	public void clickAction() {
		info("Starting clickAction");
		try {
			info("User : {}", form);
			sessionId = FacesUtils.getExternalContext().getSessionId(true);
			systemName = InetAddress.getLocalHost().getHostName();
		} catch (Exception ex) {
			error("clickAction", ex);
		}
	}

	public UserModel getForm() {
		if (form == null) {
			form = new UserModel();
		}
		return form;
	}

	public void setForm(UserModel form) {
		this.form = form;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getSystemName() {
		return systemName;
	}

	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}
	
}
