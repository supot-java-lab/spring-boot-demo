/*
* -----------------------------------------------------------------------------------
* Copyright © 2021 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.primefacesx.utils;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Configuration class for keep all Primefaces dialog option
 * @author Supot Saelao
 * @version 1.0
 */
public class PFDialogOption implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final int DEF_HEIGHT = 600;
	public static final int DEF_WIDTH 	= 800;
	private static final String PERCENT = "100%";
	
	private Map<String, Object> options = null;
	
	public PFDialogOption() {
		options = new HashMap<>();
		setModal(true);
		setDraggable(true);
		setResizable(false);
		setClosable(false);
		setContentWidth(PERCENT);
		setContentHeight(PERCENT);
		setHeight(DEF_HEIGHT);
		setWidth(DEF_WIDTH);
	}
	
	public PFDialogOption setAutoHeight() {
		options.remove("contentHeight");
		options.remove("height");
		
		return this;
	}
	
	public PFDialogOption setClosable(boolean closable) {
		options.put("closable", closable);
		return this;
	}
	
	public PFDialogOption setDraggable(boolean draggable) {
		options.put("draggable", draggable);
		return this;
	}
	
	public PFDialogOption setModal(boolean modal) {
		options.put("modal", modal);
		return this;
	}
	
	public PFDialogOption setResizable(boolean resizable) {
		options.put("resizable", resizable);
		return this;
	}
	
	public PFDialogOption setIncludeViewParams(boolean includeViewParams) {
		options.put("includeViewParams", includeViewParams);
		return this;
	}
	
	public PFDialogOption setHeight(int height) {
		options.put("height", height);
		return this;
	}
	
	public PFDialogOption setWidth(int width) {
		options.put("width", width);
		return this;
	}
	
	public PFDialogOption setContentHeight(String contentHeight) {
		options.put("contentHeight", contentHeight);
		return this;
	}
	
	public PFDialogOption setContentWidth(String contentWidth) {
		options.put("contentWidth", contentWidth);
		return this;
	}
	
	/**
	 * This method return unmodifiable of Map. Cannot change any value 
	 * @return
	 */
	public Map<String, Object> getOptions() {
		return Collections.unmodifiableMap(options);
	}
}
