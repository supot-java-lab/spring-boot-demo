/*
* -----------------------------------------------------------------------------------
* Copyright © 2021 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.primefacesx.spring.properties;

import java.io.Serializable;

import lombok.Data;

/**
 * @author supot
 * @version 1.0
 */
@Data
public class SystemConfigProperties implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private boolean enabledAudit;
}
