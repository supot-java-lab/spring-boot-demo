/*
* -----------------------------------------------------------------------------------
* Copyright © 2021 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.primefacesx.controller;

import java.io.File;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import th.co.cana.primefacesx.jsf.bean.WebPreferences;
import th.co.cana.primefacesx.utils.FacesUtils;
import th.co.cana.utils.DateUtils;
import th.co.cana.utils.Validators;

/**
* @author supot
* @version 1.0
*/
public abstract class Controller implements Serializable {
	private static final long serialVersionUID 	= 1L;
	private static final String PAGING_TEMPLATE = "{RowsPerPageDropdown} {FirstPageLink} {PreviousPageLink} {CurrentPageReport} {NextPageLink} {LastPageLink}";
	private static final String ROW_PER_PAGE 	= "10, 15, 20, 25, 50, 100, 200, 500, 1000";
	
	private transient Logger logger = null;
	
	@Inject
    private WebPreferences webPreferences;
	
	protected boolean selectAll;
	
	protected Controller() {
		logger = LoggerFactory.getLogger(getClass());
	}
	
	public boolean isSelectAll() {
		return selectAll;
	}

	public void setSelectAll(boolean selectAll) {
		this.selectAll = selectAll;
	}
	
	public WebPreferences getWebPreferences() {
		return webPreferences;
	}

	public void setWebPreferences(WebPreferences webPreferences) {
		this.webPreferences = webPreferences;
	}

	public String getPagingTemplate() {
		return PAGING_TEMPLATE;
	}
	
	public String getRowPerPage() {
		return ROW_PER_PAGE;
	}
	
	public LocalDate getCurrentDate() {
		return DateUtils.toLocalDate(DateUtils.format(DateUtils.getDate()));
	}
	
	/* ++++++++++++++++++++++++ Utility Method +++++++++++++++++++++++ */
	public String redirectPage(String page) {
		if (Validators.isEmpty(page)) {
		    warn("Empty page for redirect...");
			return null;
		}
		
		debug("Send redirect to view : {}", page);
		return FacesUtils.redirectToOutcome(page);
	}
	
	public void cleanUploadFile(List<String> fileItems) {
		try {
			if (Validators.isEmpty(fileItems)) {
				return;
			}

			for (String fileName : fileItems) {
				File file = new File(fileName);
				if (file.exists()) {
					org.apache.commons.io.FileUtils.deleteQuietly(file);
					debug("Delete file : {} success", fileName);
				}
			}
		} catch (Exception ex) {
			error("cleanUploadFile", ex);
		}
	}
	
	/* ++++++++++++++++++++++++ Logger Method +++++++++++++++++++++++ */
	protected void debug(String msg) {
		getLogger().debug(msg);
	}

	protected void debug(String format, Object arg) {
		getLogger().debug(format, arg);
	}

	protected void debug(String format, Object arg1, Object arg2) {
		getLogger().debug(format, arg1, arg2);
	}

	protected void debug(String format, Object... arguments) {
		getLogger().debug(format, arguments);
	}

	protected void debug(String msg, Throwable err) {
		getLogger().debug(msg, err);
	}

	protected void info(String msg) {
		getLogger().info(msg);
	}

	protected void info(String format, Object arg) {
		getLogger().info(format, arg);
	}

	protected void info(String format, Object arg1, Object arg2) {
		getLogger().info(format, arg1, arg2);
	}

	protected void info(String format, Object... arguments) {
		getLogger().info(format, arguments);
	}

	protected void info(String msg, Throwable err) {
		getLogger().info(msg, err);
	}
	
	protected void warn(String msg) {
		getLogger().warn(msg);
	}

	protected void warn(String format, Object arg) {
		getLogger().warn(format, arg);
	}

	protected void warn(String format, Object arg1, Object arg2) {
		getLogger().warn(format, arg1, arg2);
	}

	protected void warn(String format, Object... arguments) {
		getLogger().warn(format, arguments);
	}

	protected void warn(String msg, Throwable err) {
		getLogger().warn(msg, err);
	}
	
	protected void error(String msg) {
		getLogger().error(msg);
	}

	protected void error(String format, Object arg) {
		getLogger().error(format, arg);
	}

	protected void error(String format, Object arg1, Object arg2) {
		getLogger().error(format, arg1, arg2);
	}

	protected void error(String format, Object... arguments) {
		getLogger().error(format, arguments);
	}

	protected void error(String msg, Throwable err) {
		getLogger().error(msg, err);
	}
	
	private Logger getLogger() {
		if (logger == null) {
			logger = LoggerFactory.getLogger(getClass());
		}
		return logger;
	}
}