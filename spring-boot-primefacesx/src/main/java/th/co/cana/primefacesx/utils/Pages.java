/*
* -----------------------------------------------------------------------------------
* Copyright © 2021 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.primefacesx.utils;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Supot Saelao
 * @version 1.0
 */
public final class Pages {
	private Pages() {
	}
	
	public static final String PAGE_INDEX 		= "/index.xhtml";	
	public static final String INDEX_URL 		= "/index";
	public static final String PAGE_DASHBOARD 	= "/dashboard.xhtml";	

	public static final String PAGE_SESSION 		= "/WEB-INF/views/session.xhtml";
	public static final String PAGE_VIEW 			= "/WEB-INF/views/view.xhtml";
	
	private static final Map<String, String> MENU_CONFS;
	static {
		MENU_CONFS = new LinkedHashMap<>();
		MENU_CONFS.put("/", PAGE_DASHBOARD);
		MENU_CONFS.put("/dashboard", PAGE_DASHBOARD);
		MENU_CONFS.put(INDEX_URL, PAGE_INDEX);
		
		MENU_CONFS.put("/session", PAGE_SESSION);
		MENU_CONFS.put("/view", PAGE_VIEW);
	}

	public static Map<String, String> getMenuMapings() {
		return Collections.unmodifiableMap(MENU_CONFS);
	}

	public static String getMenuPath(String url) {
		return MENU_CONFS.get(url);
	}
}