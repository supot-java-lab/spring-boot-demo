/*
* -----------------------------------------------------------------------------------
* Copyright © 2021 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.primefacesx.spring.config;

import java.awt.image.BufferedImage;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.BufferedImageHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;

import th.co.cana.primefacesx.utils.MessageUtils;

/**
 * @author supot
 * @version 1.0
 */
@Configuration
public class ApplicationConfig {
	@Autowired
	private MessageSource messageSource;

	@PostConstruct
	public void initStaticContext() {
		MessageUtils.setMessageSource(messageSource);
	}
	
	@Bean
	public HttpMessageConverter<BufferedImage> createImageHttpMessageConverter() {
		return new BufferedImageHttpMessageConverter();
	}
}
