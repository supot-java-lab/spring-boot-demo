/*
* -----------------------------------------------------------------------------------
* Copyright © 2021 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.primefacesx.jsf;

import java.io.Serializable;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.annotation.FacesConfig;

/**
 * Is specially formed Java class that actually activates JSF 2.3
 * @see https://stackoverflow.com/questions/45682309/changing-faces-config-xml-from-2-2-to-2-3-causes-javax-el-propertynotfoundexcept
* @author supot
* @version 1.0
*/
@ApplicationScoped
@FacesConfig(version = FacesConfig.Version.JSF_2_3)
public class JSFActivator implements Serializable {
	private static final long serialVersionUID = 1L;
}
