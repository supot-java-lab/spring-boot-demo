/*
* -----------------------------------------------------------------------------------
* Copyright © 2021 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.primefacesx.spring.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;
import org.springframework.session.data.redis.config.annotation.web.http.EnableRedisHttpSession;
import org.springframework.session.web.context.AbstractHttpSessionApplicationInitializer;

/**
* @author supot
* @version 1.0
*/
@Configuration
@EnableRedisHttpSession
@ConditionalOnProperty(value = "spring.session.enable-redis-session", havingValue = "true", matchIfMissing = false)
public class SessionConfig extends AbstractHttpSessionApplicationInitializer {

}
