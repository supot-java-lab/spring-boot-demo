/*
* -----------------------------------------------------------------------------------
* Copyright © 2021 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.primefacesx.controller;

import java.net.InetAddress;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import th.co.cana.primefacesx.utils.FacesUtils;

/**
* @author supot
* @version 1.0
*/

@Named
@SessionScoped
public class SessionCtrl extends Controller {
	private static final long serialVersionUID = 1L;
	
	private String name;
	private String sessionId;
	private String systemName;
	
	@PostConstruct
	public void initial() {
		info("Startin initial : {}", getClass());
	}
	
	public void clickAction() {
		info("Starting clickAction");
		try {
			sessionId = FacesUtils.getExternalContext().getSessionId(true);
			systemName = InetAddress.getLocalHost().getHostName();
		} catch (Exception ex) {
			error("clickAction", ex);
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getSystemName() {
		return systemName;
	}

	public void setSystemName(String systemName) {
		this.systemName = systemName;
	}
	
}
