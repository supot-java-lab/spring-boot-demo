/*
* -----------------------------------------------------------------------------------
* Copyright © 2021 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.primefacesx.utils;

import java.util.MissingResourceException;

import org.springframework.context.MessageSource;

import th.co.cana.utils.Validators;

/**
* @author supot
* @version 1.0
*/
public final class MessageUtils {
	private static MessageSource messageSource;
	
	private MessageUtils() {}
	
	public static void setMessageSource(MessageSource messageSource) {
		MessageUtils.messageSource = messageSource;
	}

	public static String getValue(String key) {
		return getValue(key, Constants.LANG_TH, "");
	}
	
	public static String getValue(String key, Object... params) {
		return getValue(key, Constants.LANG_TH, params);
	}
	
	public static String getValueEn(String key) {
		return getValue(key, Constants.LANG_US, "");
	}
	
	public static String getValueEn(String key, Object... params) {
		return getValue(key, Constants.LANG_US, params);
	}
	
	private static String getValue(String key, String lang, Object... params) {
		try {
			if (Validators.isEmpty(key) || Validators.isNull(messageSource)) {
				return null;
			}

			String value = "";
			if (isLangEng(lang)) {
				value = messageSource.getMessage(key, toString(params), Constants.US);
			} else {
				value = messageSource.getMessage(key, toString(params), Constants.TH);
			}
			
			if (Validators.isEmpty(value)) {
				return key;
			}
			return value;
		} catch (MissingResourceException ex) {
			return key;
		}
	}
	
	private static Object[] toString(Object ... params) {
		if (Validators.isEmpty(params)) {
			return params;
		}
		
		for (int i = 0; i < params.length; i++) {
			if (!(params[i] instanceof String)) {
				params[i] = String.valueOf(params[i]);
			} else if(params[i] == null) {
				params[i] = "";
			}
		}
		return params;
	}
	
	private static boolean isLangThai(String lang) {
		return "th".equalsIgnoreCase(lang) || "th_TH".equalsIgnoreCase(lang);
	}
	
	private static boolean isLangEng(String lang) {
		return !isLangThai(lang);
	}
}
