/*
* -----------------------------------------------------------------------------------
* Copyright © 2021 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.primefacesx.resource;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Supot Saelao
 * @version 1.0
 */
public class JSFResourceBundle extends ClassPathResourceBundle {
	private static final String PACKAGE = "th/co/cana/primefacesx/resource/message/";
	private static final List<String> BUNDLE_NAMES;
	
	static {
		BUNDLE_NAMES = new ArrayList<>(5);
		BUNDLE_NAMES.add(PACKAGE + "JSFMessages");
		BUNDLE_NAMES.add(PACKAGE + "CommonMessage");
		BUNDLE_NAMES.add(PACKAGE + "AppMessage");
	}
	
	public JSFResourceBundle() {
		super(BUNDLE_NAMES);
	}
}