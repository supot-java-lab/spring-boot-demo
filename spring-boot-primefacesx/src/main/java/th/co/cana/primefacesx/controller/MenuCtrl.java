/*
* -----------------------------------------------------------------------------------
* Copyright © 2021 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.primefacesx.controller;

import java.io.Serializable;
import java.util.Map;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import org.jboss.weld.contexts.SerializableContextualInstanceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import th.co.cana.primefacesx.enums.PageNames;
import th.co.cana.primefacesx.utils.FacesUtils;
import th.co.cana.utils.Validators;

/**
* @author supot
* @version 1.0
*/

@Named
@SessionScoped
public class MenuCtrl implements Serializable {
	private static final long serialVersionUID = 1L;
	private static Logger logger = LoggerFactory.getLogger(MenuCtrl.class);
	
	public String changePageAction(PageNames page) {
		if (page == null) {
			return null;
		}

		//clearSession();
		return redirectPage(page.getPath());
	}
	
	public String redirectPage(String page) {
		if (Validators.isEmpty(page)) {
		    logger.warn("Empty page for redirect...");
			return null;
		}
		
		logger.debug("Send redirect to view : {}", page);
		return FacesUtils.redirectToOutcome(page);
	}
	
    public void clearSession() {
        try {
        	Map<String, Object> sessionMap = FacesUtils.getSessionMap();
			if (Validators.isEmpty(sessionMap)) {
				return;
			}
        	
			logger.debug("Total session object : {}", sessionMap.size());
			for (Map.Entry<String, Object> map : sessionMap.entrySet()) {
				logger.debug("\t Session Key[{}] -> {}", map.getKey(), map.getValue().getClass());
				if (map.getValue() instanceof SerializableContextualInstanceImpl) {
					SerializableContextualInstanceImpl<?, ?> manageBean = (SerializableContextualInstanceImpl<?, ?>) map.getValue();
					if(manageBean.getInstance() instanceof Controller) {
						logger.info("\t Session type is Controller remove");
						sessionMap.remove(map.getKey());
					}
				}
				if (map.getValue() instanceof Controller) {
					logger.info("\t Session type is Controller remove");
					sessionMap.remove(map.getKey());
				}
			}         
        } catch (Exception ex) {
            logger.error("clearSession", ex);
        }
	}
}
