/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Tourism Authority of Thailand (TAT) All rights reserved
* -----------------------------------------------------------------------------------
*/
package th.co.cana.primefacesx.spring.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import th.co.cana.primefacesx.spring.properties.SystemConfigProperties;

/**
* @author supot
* @version 1.0
*/

@Configuration
public class PropertiesConfig {
	
	@Bean
	@ConfigurationProperties(prefix = "system.conf")
	public SystemConfigProperties systemConfigProperties() {
		return new SystemConfigProperties();
	}
	
}
