/*
* -----------------------------------------------------------------------------------
* Copyright © 2021 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.primefacesx.jsf.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import th.co.cana.primefacesx.jsf.theme.ThemeComponent;
import th.co.cana.primefacesx.jsf.theme.ThemeMenu;
import th.co.cana.primefacesx.jsf.theme.ThemePalette;
import th.co.cana.primefacesx.jsf.theme.ThemeTopbar;
import th.co.cana.primefacesx.jsf.theme.ThemeUtils;
import th.co.cana.primefacesx.utils.Constants;
import th.co.cana.primefacesx.utils.FacesUtils;
import th.co.cana.primefacesx.utils.PFUtils;
import th.co.cana.utils.Validators;

/**
* @author supot
* @version 1.0
*/
@Named
@SessionScoped
public class WebPreferences implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final String NONE_COLOR = "transparent";
	private static final Logger logger = LoggerFactory.getLogger(WebPreferences.class);
	
	//Themes
	private String menuMode = "layout-horizontal";
	private String menuColor = "colored";
	private String menuTheme = "teallight";
	private String topbarTheme = "bluedark";
	private String inputStyle = "outlined";
    private String componentTheme = "teallight";     
	private String layoutPrimaryColor = "teallight";
    private boolean groupedMenu = true;
    private boolean darkLogo;
    private List<ThemeComponent> componentThemes;
    private List<ThemeTopbar> topbarThemes;
    private Map<String, List<ThemeMenu>> menuColors;
    private List<ThemePalette> palettes;
    private ThemePalette selectedPalette;

    //Others
	private List<String> errors;
	private List<String> jsCommands;
	private Locale locale = Constants.TH;
	
    @PostConstruct
    public void init() {
    	logger.info("Starting : {}", getClass().getName());
        componentThemes = ThemeUtils.pandoraComponents();
        topbarThemes = ThemeUtils.pandoraTopbars();
        menuColors = ThemeUtils.pandoraMenuColors();
        palettes = ThemeUtils.pandoraPalettes();
        
        //Menu: Light | Active: Teal-Light | Topbar: Teal-Light | Components: Teal-Light
        selectedPalette = palettes.get(2);
    }
    
    public String getLayoutConfig() {
        StringBuilder sb = new StringBuilder();
        String menuModeClass = ThemeUtils.getMenuModeClass(menuMode);
        String menuThemeClass = "layout-menu-" + ("colored".equals(menuColor) ? this.menuTheme : this.menuColor);

        sb.append("layout-topbar-").append(this.topbarTheme);
        sb.append(" ").append(menuThemeClass);
        sb.append(" ").append(menuModeClass);

        return sb.toString();
    }

    public void changePalette(ThemePalette palette) {
        this.setMenuColor(palette.getMenuColor().getName());
        this.setMenuTheme(palette.getMenuTheme().getName());
        this.setTopbarTheme(palette.getTopbarTheme().getName());
        this.setComponentTheme(palette.getComponentTheme().getName());
        this.setSelectedPalette(palette);
    }

	public void excuteCommand() {
		if (Validators.isEmpty(jsCommands)) {
			resetError();
			return;
		}

		for (String command : jsCommands) {
			if (Validators.isNotEmpty(command)) {
				PFUtils.execute(command);
			}
		}
		resetError();
	}
	
	public void add(String... error) {
		if (Validators.isEmpty(error)) {
			return;
		}

		if (Validators.isNull(errors)) {
			errors = new ArrayList<>();
		}
		errors.addAll(Arrays.asList(error));
	}
	
	public void addJsCommand(String ... jsCommand) {
		if (Validators.isEmpty(jsCommand)) {
			return;
		}
		
		if (Validators.isNull(jsCommands)) {
			jsCommands = new ArrayList<>();
		}
		jsCommands.addAll(Arrays.asList(jsCommand));
	}
	
	public void resetError() {
		errors = null;
		jsCommands = null;
	}
	
    /* +++++++++++++++++++++++++++++++++ Get/Set +++++++++++++++++++++++++++++++++ */
	public String getThemeName() {
		return "pandora-" + this.componentTheme;
	}
	
    public List<ThemePalette> getPalettes() {
        return palettes;
    }
    
    public String getLayout() {
        return "layout-" + this.layoutPrimaryColor;
    }

    public String getLayoutPrimaryColor() {
        return layoutPrimaryColor;
    }

    public void setLayoutPrimaryColor(String layoutPrimaryColor) {
        this.layoutPrimaryColor = layoutPrimaryColor;
    }

    public String getTopbarTheme() {
        return topbarTheme;
    }

    public boolean isDarkLogo() {
        return this.darkLogo;
    }
    
    public void setTopbarTheme(String topbarTheme) {
        this.topbarTheme = topbarTheme;
        this.darkLogo = "light".equals(this.topbarTheme);
        this.setSelectedPalette(null);
    }

    public String getComponentTheme() {
        return componentTheme;
    }

    public void setComponentTheme(String componentTheme) {
        this.componentTheme = componentTheme;
        this.setSelectedPalette(null);
    }

    public String getMenuMode() {
        return menuMode;
    }

    public void setMenuMode(String menuMode) {
        this.menuMode = menuMode;
    }

    public String getMenuColor() {
        return menuColor;
    }

	public void setMenuColor(String menuColor) {
		try {
			this.menuColor = menuColor;
			this.menuTheme = this.menuColors.get(menuColor).get(0).getFile();
			this.setSelectedPalette(null);
		} catch (Exception ex) {
			//ignore error
		}
	}

    public String getMenuTheme() {
        return menuTheme;
    }

    public void setMenuTheme(String menuTheme) {
        this.menuTheme = menuTheme;
        this.setSelectedPalette(null);
    }

    public String getInputStyle() {
        return inputStyle;
    }

    public void setInputStyle(String inputStyle) {
        this.inputStyle = inputStyle;
    }

    public String getInputStyleClass() {
        return ThemeUtils.getInputStyleClass(inputStyle);
    }

    public boolean isGroupedMenu() {
        return this.groupedMenu;
    }

    public void setGroupedMenu(boolean value) {
        this.groupedMenu = value;
    }
    
    public List<ThemeComponent> getComponentThemes() {
        return componentThemes;
    }
    
    public List<ThemeTopbar> getTopbarThemes() {
        return topbarThemes;
    }
    
    public Map<String, List<ThemeMenu>> getMenuColors() {
        return menuColors;
    }

	public List<ThemeMenu> getThemeMenuItems() {
		if (Validators.isEmpty(menuColors)) {
			return Collections.emptyList();
		}
		return menuColors.get(menuColor);
	}
    
    public ThemePalette getSelectedPalette() {
        return selectedPalette;
    }

    public void setSelectedPalette(ThemePalette selectedPalette) {
        this.selectedPalette = selectedPalette;
    }
    
	public List<String> getErrors() {
		return errors;
	}

	public void setErrors(List<String> errors) {
		this.errors = errors;
	}
	
	public List<String> getJsCommands() {
		return jsCommands;
	}

	public void setJsCommands(List<String> jsCommands) {
		this.jsCommands = jsCommands;
	}

	public Locale getLocale() {
		if (locale == null) {
			locale = FacesUtils.getLocale();
		}
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}
	
	public String getMenuBgColor() {
		return (selectedPalette != null ? selectedPalette.getMenuColor().getCode() : NONE_COLOR);
	}
	
	public String getMenuTitle() {
		return (selectedPalette != null ? selectedPalette.getMenuColor().getName() : "");
	}
	
	public String getThemeBgColor() {
		return (selectedPalette != null ? selectedPalette.getMenuTheme().getCode() : NONE_COLOR);
	}
	
	public String getThemeTitle() {
		return (selectedPalette != null ? selectedPalette.getMenuTheme().getName() : "");
	}
	
	public String getTopbarBgColor() {
		return (selectedPalette != null ? selectedPalette.getTopbarTheme().getCode() : NONE_COLOR);
	}
	
	public String getTopbarTitle() {
		return (selectedPalette != null ? selectedPalette.getTopbarTheme().getName() : "");
	}
	
	public String getComponentBgColor() {
		return (selectedPalette != null ? selectedPalette.getComponentTheme().getCode() : NONE_COLOR);
	}
	
	public String getComponentTitle() {
		return (selectedPalette != null ? selectedPalette.getComponentTheme().getName() : "");
	}
	
	public boolean isEmptyError() {
		return (errors == null || errors.isEmpty());
	}
	
	public boolean isOneItem() {
		return (errors != null && errors.size() == 1);
	}
	
	public boolean isLangThai() {
		return FacesUtils.isLangThai();
	}
	
	public boolean isValidationFailed() {
		return (FacesUtils.getMaximumSeverity() != null || FacesUtils.isValidationFailed()) 
				&& !FacesUtils.isEmptyError() && isEmptyError();
	}
}
