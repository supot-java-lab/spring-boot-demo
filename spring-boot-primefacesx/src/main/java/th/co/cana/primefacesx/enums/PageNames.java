/*
* -----------------------------------------------------------------------------------
* Copyright © 2021 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.primefacesx.enums;

import th.co.cana.primefacesx.utils.Pages;

/**
* @author supot
* @version 1.0
*/
public enum PageNames {
	DASHBOARD(Pages.PAGE_DASHBOARD),
	HOME(Pages.PAGE_INDEX),
	SESSION(Pages.PAGE_SESSION),
	VIEW(Pages.PAGE_VIEW);
	
	private String path;
	private PageNames(String path) {
		this.path = path;
	}
    
    public String getPath() {
		return path;
	}

	@Override
    public String toString() {
        return this.path;
    } 
}
