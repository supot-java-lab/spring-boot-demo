/*
* -----------------------------------------------------------------------------------
* Copyright © 2021 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.primefacesx.model;

import java.io.Serializable;

import lombok.Data;

/**
* @author supot
* @version 1.0
*/
@Data
public class UserModel implements Serializable {
	private static final long serialVersionUID = 1L;

	private String fisrtName;
	private String lastName;
	private String address;
	private String city;
	private String state;
	private String zip;
}
