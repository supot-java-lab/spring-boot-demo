/*
* -----------------------------------------------------------------------------------
* Copyright © 2021 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.primefacesx.utils;

import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import th.co.cana.utils.Validators;

/**
* @author supot
* @version 1.0
*/
public class ResourceBundles {
	private static final String BUNDLE_CONF_VAR = "msg";

	private ResourceBundles() {
	}
	
	public static String getMessage(String key) {
		return getValue(key, (Object[]) null);
	}

	public static String getMessage(String key, Object... params) {
		return getValue(key, params);
	}

	private static String getValue(String key, Object[] params) {
		String msg = null;
		try {
			msg = getBundle().getString(key);
			if (Validators.isEmpty(msg)) {
				return msg;
			}

			if (Validators.isNotEmpty(params)) {
				msg = MessageFormat.format(msg, toString(params));
			}
		} catch (MissingResourceException | IllegalArgumentException var4) {
			msg = key;
		}

		return msg;
	}

	private static Object[] toString(Object ... params) {
		if (Validators.isEmpty(params)) {
			return params;
		}
		
		for (int i = 0; i < params.length; i++) {
			if (!(params[i] instanceof String)) {
				params[i] = String.valueOf(params[i]);
			} else if(params[i] == null) {
				params[i] = "";
			}
		}
		return params;
	}
	
	private static ResourceBundle getBundle() {
		String varMsg = System.getProperty("bundle.var", BUNDLE_CONF_VAR);
		return FacesUtils.getResourceBundle(varMsg);
	}
}
