if (PrimeFaces.widget.SimpleFileUpload) {
	PrimeFaces.widget.SimpleFileUpload.prototype.init = function(cfg) {
		PrimeFaces.widget.DeferredWidget.prototype.init.call(this, cfg);
		if (this.cfg.disabled) {
			return;
		}

		this.cfg.invalidFileMessage = this.cfg.invalidFileMessage || 'Invalid file type';
		this.cfg.invalidSizeMessage = this.cfg.invalidSizeMessage || 'Invalid file size';
		this.cfg.fileLimitMessage = this.cfg.fileLimitMessage || 'Maximum number of files exceeded';
		this.cfg.messageTemplate = this.cfg.messageTemplate || '{name} {size}';
		this.cfg.global = (this.cfg.global === true || this.cfg.global === undefined) ? true : false;
		this.sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];

		this.maxFileSize = this.cfg.maxFileSize;

		this.form = this.jq.closest('form');
		this.input = $(this.jqId + "_input");

		if (this.cfg.skinSimple) {
			this.button = this.jq.children('.ui-button');
			this.display = this.jq.children('.ui-fileupload-filename');

			if (!this.input.prop('disabled')) {
				this.bindEvents();
			}
		} else {
            if (this.cfg.auto) {
                var b = this;
                this.input.on("change.fileupload", function() {
                    b.upload()
                })
            }
		}
	}
};