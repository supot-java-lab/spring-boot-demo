/* ++++++++++++++++++++++ Google API +++++++++++++++++++ */
function initGoogleLogin() {
	gapi.load('auth2', function() {
		gapi.auth2.init({
			client_id: googleId,
			scope: 'profile email',
			prompt: 'select_account'
		}).then(
			function() {
				console.log('Load auth2 lirary success');
			},
			function(error) {
				console.log('Failed to load libraries : ');
				console.log(error);
			}
		);
	});
}

function googleLogin() {
	var auth2 = gapi.auth2.getAuthInstance();
	if (auth2.isSignedIn.get()) {
		console.info("User already SignedIn...");
		var user = auth2.currentUser.get();
		var profile = user.getBasicProfile();
		var userInfo = createGooleUser(profile);
		applicationLogin(userInfo);
		return;
	}

	console.info("User not SignedIn...");
	auth2.signIn().then(
		function() {
			var user = auth2.currentUser.get();
			var profile = user.getBasicProfile();
			var userInfo = createGooleUser(profile);

			applicationLogin(userInfo);
		},
		function(error) {
			console.log('Failed to login : ');
			console.error(error);
		}
	);
}

function googleLogout() {
	try {
		var auth2 = gapi.auth2.getAuthInstance();
		auth2.signOut().then(function() {
			console.log('User signed out success');
		});
	} catch (err) {
		console.error(err);
	}
}

function createGooleUser(profile) {
	var userParam = [
		{name: 'type', value: 'google'}
		, {name: 'id', value: profile.getId()}
		, {name: 'fullName', value: profile.getName()}
		, {name: 'firstName', value: profile.getGivenName()}
		, {name: 'lastName', value: profile.getFamilyName()}
		, {name: 'email', value: profile.getEmail()}
	];
	return userParam;
}

/* ++++++++++++++++++++++ Facebook API +++++++++++++++++++ */
function facebookLogin() {
	FB.init({
	      appId      : facebookId,
	      cookie     : true,
	      xfbml      : true,
	      version    : 'v10.0' 
	});	
	
	FB.login(function(resp) {
		if (resp.status === 'connected') {
	      getFacebookProfile(resp);
		} else {
			console.log(resp);
		}		
	}, 
	{ scope: 'email,public_profile', return_scopes: true, });
}

function getFacebookProfile(status) {
	console.log(status);
	FB.api("/me", "GET", { "fields": "id,name,email" }, function(resp) {
		console.log(resp);
		var userInfo = createFacebookUser(resp);
		applicationLogin(userInfo);
	});
}

function facebookLogout() {
	FB.logout(function(resp) {
		console.log(resp);
	});
}

function createFacebookUser(profile) {
	var name = profile.name.split(" ");
	var userParam = [
		{name: 'type', value: 'facebook'}
		, {name: 'id', value: profile.id}
		, {name: 'fullName', value: profile.name}
		, {name: 'firstName', value: name[0]}
		, {name: 'lastName', value: name[1]}
		, {name: 'email', value: profile.email}
	];
	return userParam;
}

/* ++++++++++++++++++++++ Application API +++++++++++++++++++ */
function applicationLogin(user) {
	console.log("Login to application...");
	console.log(user);
	loginAction(user);
}