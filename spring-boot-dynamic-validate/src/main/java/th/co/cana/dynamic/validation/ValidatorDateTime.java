/*
 * ---------------------------------------------------------------------------
 * Copyright (c) 2023. Cana Enterprise Co., Ltd. All rights reserved
 * ---------------------------------------------------------------------------
 */

package th.co.cana.dynamic.validation;

import io.github.jdevlibs.utils.DateFormats;
import io.github.jdevlibs.utils.Validators;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class ValidatorDateTime implements Validator {
    private static final String DATE_FM = "yyyy-MM-dd";

    private final String format;
    private final int length;

    public ValidatorDateTime() {
        this.format = DATE_FM;
        this.length = DATE_FM.length();
    }

    public ValidatorDateTime(String format) {
        this.format = format;
        this.length = (format != null ? format.length() : 0);
    }

    @Override
    public boolean isValid(Object value) {
        if (Validators.isEmpty(value)) {
            return false;
        }
        if (length > 0 && value.toString().trim().length() != length) {
            return false;
        }

        return Validators.isNotNull(DateFormats.date(value, format));
    }

    @Override
    public String getMessage() {
        return "invalid data format[" + format + "]";
    }

    @Override
    public String getOptionalValue() {
        return format;
    }
}
