/*
 * ---------------------------------------------------------------------------
 * Copyright (c) 2023. Cana Enterprise Co., Ltd. All rights reserved
 * ---------------------------------------------------------------------------
 */

package th.co.cana.dynamic.validation;

import io.github.jdevlibs.utils.CsvUtils;
import io.github.jdevlibs.utils.Validators;

import java.util.ArrayList;
import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */
public final class CsvValidators {
    private CsvValidators() {

    }

    public static List<String> validationData(List<List<String>> results, List<ValidationRule> rules) {
        List<String> errors = new ArrayList<>();
        if (Validators.isEmptyOne(results, rules)) {
            return errors;
        }

        int rowIndex = 1;
        for (List<String> values : results) {
            for (ValidationRule rule : rules) {
                String value = CsvUtils.getValue(values, rule.getIndex());
                //Step 1. Data size (Length)
                if (rule.isInValidLength(value)) {
                    errors.add(rule.getProperty() + " length must be less than " + rule.getLength() + "[size=" + value.length() + "]");
                    return errors;
                }

                //Step 2. Business
                errors = validationRule(rule, values, rowIndex);
                if (Validators.isNotEmpty(errors)) {
                    return errors;
                }
            }
            rowIndex++;
        }

        return errors;
    }

    private static List<String> validationRule(ValidationRule rule, List<String> values, int rowIndex) {

        //Validate depend other column value
        boolean validCriteria = isValidCriteria(rule.getCriteriaRules(), values);

        //Validate self
        if (validCriteria) {
            String value = CsvUtils.getValue(values, rule.getIndex());
            return validation(rule, value, rowIndex);
        }

        return new ArrayList<>();
    }

    private static List<String> validation(ValidationRule rule, String value, int rowIndex) {
        if (rule.isEmptyValidator()) {
            return new ArrayList<>();
        }

        List<String> errors = new ArrayList<>();
        for (Validator validation : rule.getValidators()) {
            if (validation.isValid(value)) {
                continue;
            }
            if (Validators.isNotEmpty(validation.getMessage())) {
                errors.add("Row[" + rowIndex + "] " + rule.getMessage() + " : " + validation.getMessage());
            } else {
                errors.add("Row[" + rowIndex + "] " + rule.getMessage());
            }
        }

        return errors;
    }

    private static boolean isValidCriteria(List<CriteriaRule> rules, List<String> values) {
        if (Validators.isEmpty(rules)) {
            return true;
        }

        List<Boolean> andRules = new ArrayList<>();
        List<Boolean> orRules = new ArrayList<>();
        for (CriteriaRule rule : rules) {
            String value = CsvUtils.getValue(values, rule.getIndex());
            if (CriteriaRule.JoinType.OR == rule.getJoin()) {
                orRules.add(rule.isValid(value));
            } else {
                andRules.add(rule.isValid(value));
            }
        }

        if (Validators.isEmptyAll(andRules, orRules)) {
            return false;
        }

        return allTrue(andRules) && someTrue(orRules);
    }

    private static boolean allTrue(List<Boolean> andRules) {
        for (Boolean value : andRules) {
            if (!value) {
                return false;
            }
        }
        return true;
    }

    private static boolean someTrue(List<Boolean> orRules) {
        if (Validators.isEmpty(orRules)) {
            return true;
        }

        for (Boolean value : orRules) {
            if (value) {
                return true;
            }
        }
        return false;
    }
}