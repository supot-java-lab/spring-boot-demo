/*
 * ---------------------------------------------------------------------------
 * Copyright (c) 2024. Cana Enterprise Co., Ltd. All rights reserved
 * ---------------------------------------------------------------------------
 */

package th.co.cana.dynamic.utils;

import java.util.Locale;

/**
 * @author supot.jdev
 * @version 1.0
 */
public final class AppConstants {

    private AppConstants() {
    }
    public static final String A = "A";
    public static final String Y = "Y";
    public static final String N = "N";
    public static final String LANG_TH = "TH";
    public static final String LANG_EN = "EN";

    public static final Locale US   = Locale.US;
    public static final Locale TH   = new Locale("th", "TH");
}
