/*
 * ---------------------------------------------------------------------------
 * Copyright (c) 2023. Cana Enterprise Co., Ltd. All rights reserved
 * ---------------------------------------------------------------------------
 */

package th.co.cana.dynamic.validation;

import lombok.Data;

import java.io.Serializable;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
public class CriteriaRule implements Serializable {
    private int index;
    private String property;
    private String message;
    private Validator validator;
    private JoinType join;
    public CriteriaRule() {
        this.join = JoinType.AND;
    }

    public CriteriaRule(int index, Validator validator, JoinType join) {
        this.index = index;
        this.validator = validator;
        this.join = join;
    }

    public CriteriaRule(String property, Validator validator, JoinType join) {
        this.property = property;
        this.validator = validator;
        this.join = join;
    }

    public boolean isValid(Object value) {
        if (validator == null) {
            return false;
        }
        return validator.isValid(value);
    }

    public enum JoinType {
        AND, OR
    }
}
