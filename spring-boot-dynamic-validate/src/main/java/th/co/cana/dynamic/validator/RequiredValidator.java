/*
 * ---------------------------------------------------------------------------
 * Copyright (c) 2024. Cana Enterprise Co., Ltd. All rights reserved
 * ---------------------------------------------------------------------------
 */

package th.co.cana.dynamic.validator;

import io.github.jdevlibs.utils.Validators;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import th.co.cana.dynamic.constraint.Required;
import th.co.cana.dynamic.utils.ConstraintUtils;
import th.co.cana.dynamic.utils.MessageUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class RequiredValidator implements ConstraintValidator<Required, Object> {

    private int min;
    private int max;
    private String message;
    private List<String> allowValues;
    private List<Object> allowObjValues;

    @Override
    public void initialize(Required cont) {
        min = cont.min();
        max = cont.max();
        if (Validators.isEmpty(cont.allowValues())) {
            allowValues = new ArrayList<>();
        } else {
            allowValues = Arrays.asList(cont.allowValues());
        }
        message = cont.message();
    }

    @Override
    public boolean isValid(Object value, ConstraintValidatorContext context) {
        if (Validators.isEmpty(value)) {
            if (ConstraintUtils.isDynamicMessage(message)) {
                message = MessageUtils.getValue(ConstraintUtils.formatMessage(message));
            }
            ConstraintUtils.setConstraintViolationError(context, message);
            return false;
        }

        //Contains allow value
        if (Validators.isNotEmpty(allowValues)) {
            if (Validators.isNull(allowObjValues)) {
                allowObjValues = ConstraintUtils.convert(value.getClass(), allowValues);
            }
            if (!allowObjValues.contains(value)) {
                message = MessageUtils.getValue("validation.custom.allow.value", String.join(",", allowValues));
                ConstraintUtils.setConstraintViolationError(context, message);
                return false;
            }
        }

        int size = ConstraintUtils.getObjectSize(value);
        //Case set min only
        if (min > 0 && max == Integer.MAX_VALUE) {
            if (size != min) {
                message = MessageUtils.getValue("validation.custom.size.greater.equals", min);
                ConstraintUtils.setConstraintViolationError(context, message);
                return false;
            }
        }

        //Case min and max
        if (min > 0 && max > 0 && max < Integer.MAX_VALUE) {
            if (size < min || size > max) {
                message = MessageUtils.getValue("validation.custom.size.message", min, max);
                ConstraintUtils.setConstraintViolationError(context, message);
                return false;
            }
        }

        if (max > 0 && max < Integer.MAX_VALUE) {
            if (size != max) {
                message = MessageUtils.getValue("validation.custom.size.less.equals", max);
                ConstraintUtils.setConstraintViolationError(context, message);
                return false;
            }
        }

        return true;
    }
}
