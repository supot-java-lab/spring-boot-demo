/*
 * ---------------------------------------------------------------------------
 * Copyright (c) 2023. Cana Enterprise Co., Ltd. All rights reserved
 * ---------------------------------------------------------------------------
 */

package th.co.cana.dynamic.validation;

import io.github.jdevlibs.utils.Convertors;
import io.github.jdevlibs.utils.Validators;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class ValidatorDecimal implements Validator {
    @Override
    public boolean isValid(Object value) {
        if (Validators.isEmpty(value)) {
            return false;
        }

        return Validators.isNotNull(Convertors.toBigDecimal(value, null));
    }

    @Override
    public String getMessage() {
        return "Value must be a decimal[0-9].";
    }
    @Override
    public String getOptionalValue() {
        return null;
    }
}
