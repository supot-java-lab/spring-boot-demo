/*
 * ---------------------------------------------------------------------------
 * Copyright (c) 2023. Cana Enterprise Co., Ltd. All rights reserved
 * ---------------------------------------------------------------------------
 */

package th.co.cana.dynamic.validation;

import io.github.jdevlibs.utils.Validators;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class ValidatorNumber implements Validator {
    @Override
    public boolean isValid(Object value) {
        if (Validators.isEmpty(value)) {
            return false;
        }
        return isNumber(value);
    }

    @Override
    public String getMessage() {
        return "Value must be a number[0-9]";
    }

    @Override
    public String getOptionalValue() {
        return null;
    }

    private boolean isNumber(Object value) {
        try {
            Long.valueOf(value.toString().trim());
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }
}
