/*
 * ---------------------------------------------------------------------------
 * Copyright (c) 2024. Cana Enterprise Co., Ltd. All rights reserved
 * ---------------------------------------------------------------------------
 */

package th.co.cana.dynamic.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Configuration;
import th.co.cana.dynamic.utils.MessageUtils;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Configuration
public class ApplicationConfig {

    @Autowired
    public ApplicationConfig(MessageSource messageSource) {
        MessageUtils.setMessageSource(messageSource);
    }
}
