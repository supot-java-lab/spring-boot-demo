/*
 * ---------------------------------------------------------------------------
 * Copyright (c) 2023. Cana Enterprise Co., Ltd. All rights reserved
 * ---------------------------------------------------------------------------
 */

package th.co.cana.dynamic.validation;

import io.github.jdevlibs.utils.Convertors;
import io.github.jdevlibs.utils.Validators;

import java.math.BigDecimal;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class ValidatorDecimalRange implements Validator {

    private final BigDecimal min;
    private final BigDecimal max;

    public ValidatorDecimalRange() {
        this.min = BigDecimal.valueOf(Double.MIN_VALUE);
        this.max = BigDecimal.valueOf(Double.MAX_VALUE);
    }

    public ValidatorDecimalRange(BigDecimal min, BigDecimal max) {
        if (min == null) {
            min = BigDecimal.valueOf(Double.MIN_VALUE);
        }
        if (max == null) {
            max = BigDecimal.valueOf(Double.MAX_VALUE);
        }
        this.min = min;
        this.max = max;
    }

    @Override
    public boolean isValid(Object value) {
        if (Validators.isEmpty(value)) {
            return false;
        }
        BigDecimal amt = Convertors.toBigDecimal(value, null);
        if (amt == null) {
            return false;
        }

        return (amt.compareTo(min) >= 0 && amt.compareTo(max) <= 0);
    }

    @Override
    public String getMessage() {
        return "value must between[" + min + "," + max + "]";
    }

    @Override
    public String getOptionalValue() {
        return "[" + min + "," + max + "]";
    }
}
