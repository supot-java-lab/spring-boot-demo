/*
 * ---------------------------------------------------------------------------
 * Copyright (c) 2023. Cana Enterprise Co., Ltd. All rights reserved
 * ---------------------------------------------------------------------------
 */

package th.co.cana.dynamic.validation;

import io.github.jdevlibs.utils.Convertors;
import io.github.jdevlibs.utils.Validators;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class ValidatorNumberRange implements Validator {

    private final long min;
    private final long max;

    public ValidatorNumberRange() {
        this.min = Long.MIN_VALUE;
        this.max = Long.MAX_VALUE;
    }

    public ValidatorNumberRange(long min, long max) {
        this.min = min;
        this.max = max;
    }

    @Override
    public boolean isValid(Object value) {
        if (Validators.isEmpty(value)) {
            return false;
        }
        Long amt = Convertors.toLong(value, null);
        if (amt == null) {
            return false;
        }

        return (amt >= min && amt <= max);
    }

    @Override
    public String getMessage() {
        return "value must between[" + min + "," + max + "]";
    }

    @Override
    public String getOptionalValue() {
        return "[" + min + "," + max + "]";
    }
}
