/*
 * ---------------------------------------------------------------------------
 * Copyright (c) 2023. Cana Enterprise Co., Ltd. All rights reserved
 * ---------------------------------------------------------------------------
 */

package th.co.cana.dynamic.validation;

import io.github.jdevlibs.utils.Validators;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class ValidatorThaiCitizenId implements Validator {
    @Override
    public boolean isValid(Object value) {
        if (Validators.isNull(value)) {
            return false;
        }
        return Validators.isThaiCitizenId((String) value);
    }

    @Override
    public String getMessage() {
        return "Invalid thai id-card format";
    }

    @Override
    public String getOptionalValue() {
        return null;
    }
}
