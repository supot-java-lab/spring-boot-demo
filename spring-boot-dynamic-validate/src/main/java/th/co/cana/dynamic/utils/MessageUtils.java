/*
 * ---------------------------------------------------------------------------
 * Copyright (c) 2024. Cana Enterprise Co., Ltd. All rights reserved
 * ---------------------------------------------------------------------------
 */

package th.co.cana.dynamic.utils;

import io.github.jdevlibs.utils.Validators;
import org.springframework.context.MessageSource;

import java.util.MissingResourceException;

public final class MessageUtils {
    private static MessageSource messageSource;

    private MessageUtils() {}

    public static void setMessageSource(MessageSource messageSource) {
        MessageUtils.messageSource = messageSource;
    }

    public static String getValue(String key) {
        return getValue(key, "th", "");
    }

    public static String getValue(String key, Object... params) {
        return getValue(key, "th", params);
    }

    public static String getValueEng(String key) {
        return getValue(key, "en_US", "");
    }

    public static String getValueEng(String key, Object... params) {
        return getValue(key, "en_US", params);
    }

    private static String getValue(String key, String lang, Object... params) {
        try {
            if (Validators.isEmpty(key) || Validators.isNull(messageSource)) {
                return null;
            }

            String value = "";
            if (isLangEng(lang)) {
                value = messageSource.getMessage(key, toString(params), AppConstants.US);
            } else {
                value = messageSource.getMessage(key, toString(params), AppConstants.TH);
            }

            if (Validators.isEmpty(value)) {
                return key;
            }
            return value;
        } catch (MissingResourceException ex) {
            return key;
        }
    }

    private static Object[] toString(Object ... params) {
        if (Validators.isEmpty(params)) {
            return params;
        }

        for (int i = 0; i < params.length; i++) {
            if (!(params[i] instanceof String)) {
                params[i] = String.valueOf(params[i]);
            } else if(params[i] == null) {
                params[i] = "";
            }
        }
        return params;
    }

    private static boolean isLangThai(String lang) {
        return "th".equalsIgnoreCase(lang) || "th_TH".equalsIgnoreCase(lang);
    }

    private static boolean isLangEng(String lang) {
        return !isLangThai(lang);
    }
}