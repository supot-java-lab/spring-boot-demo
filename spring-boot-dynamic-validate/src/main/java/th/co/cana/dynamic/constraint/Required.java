/*
 * ---------------------------------------------------------------------------
 * Copyright (c) 2024. Cana Enterprise Co., Ltd. All rights reserved
 * ---------------------------------------------------------------------------
 */

package th.co.cana.dynamic.constraint;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import th.co.cana.dynamic.validator.RequiredValidator;

import java.lang.annotation.*;

/**
 * @author supot.jdev
 * @version 1.0
 */

@Documented
@Constraint(validatedBy = RequiredValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD, ElementType.TYPE, ElementType.PARAMETER, ElementType.CONSTRUCTOR })
@Retention(RetentionPolicy.RUNTIME)
public @interface Required {
    String message() default "{validation.custom.required}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    /**
     * @return Validate allow value
     */
    String[] allowValues() default {};

    /**
     * @return size the element must be equal to
     */
    int size() default 0;

    /**
     * @return size the element must be higher or equal to
     */
    int min() default 0;

    /**
     * @return size the element must be lowed or equal to
     */
    int max() default Integer.MAX_VALUE;

    /**
     * Defines several <code>@Required</code> annotations on the same element
     * @see Required
     */
    @Target( { ElementType.METHOD, ElementType.FIELD, ElementType.TYPE, ElementType.PARAMETER, ElementType.CONSTRUCTOR })
    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    @interface List  {
        Required[] value();
    }
}
