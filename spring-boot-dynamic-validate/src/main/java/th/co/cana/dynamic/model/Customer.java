/*
 * ---------------------------------------------------------------------------
 * Copyright (c) 2024. Cana Enterprise Co., Ltd. All rights reserved
 * ---------------------------------------------------------------------------
 */

package th.co.cana.dynamic.model;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import th.co.cana.dynamic.constraint.Required;

import java.math.BigDecimal;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
public class Customer {

    @Required(min = 2, max = 20)
    private String customerId;

    @Required
    private String firstName;

    @Required
    private String lastName;

    @Required(max = 1, allowValues = {"Y", "N"})
    private String vatType;

    @Email
    private String email;

    @NotNull
    private BigDecimal balanceAmt;
}
