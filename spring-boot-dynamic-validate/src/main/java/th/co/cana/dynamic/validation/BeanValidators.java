/*
 * ---------------------------------------------------------------------------
 * Copyright (c) 2023. Cana Enterprise Co., Ltd. All rights reserved
 * ---------------------------------------------------------------------------
 */

package th.co.cana.dynamic.validation;

import io.github.jdevlibs.utils.BeanUtils;
import io.github.jdevlibs.utils.Validators;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */
public final class BeanValidators {
    private BeanValidators() {

    }

    public static List<String> validation(List<Object> items, List<ValidationRule> rules) {
        List<String> errors = new ArrayList<>();
        if (Validators.isEmptyOne(items, rules)) {
            return errors;
        }

        for (Object bean : items) {
            errors = validation(bean, rules);
            if (Validators.isNotEmpty(errors)) {
                return errors;
            }
        }

        return errors;
    }

    public static List<String> validation(Object bean, List<ValidationRule> rules) {
        List<String> errors = new ArrayList<>();
        if (Validators.isEmptyOne(bean, rules)) {
            return errors;
        }

        for (ValidationRule rule : rules) {
            Object value = BeanUtils.getValue(bean, rule.getProperty());
            //Step 1. Data size (Length)
            if (rule.isInValidLength(value)) {
                errors.add(rule.getProperty() + " length must be less than " + rule.getLength() + "[size=" + value.toString().length() + "]");
                return errors;
            }

            //Step 2. Business
            errors = validationRule(rule, bean);
            if (Validators.isNotEmpty(errors)) {
                return errors;
            }
        }
        return errors;
    }

    private static List<String> validationRule(ValidationRule rule, Object bean) {

        //Validate depend other column value
        boolean validCriteria = isValidCriteria(rule.getCriteriaRules(), bean);

        //Validate self
        if (validCriteria) {
            Object value = BeanUtils.getValue(bean, rule.getProperty());
            return validation(rule, value);
        }

        return Collections.emptyList();
    }

    private static List<String> validation(ValidationRule rule, Object value) {
        if (rule.isEmptyValidator()) {
            return Collections.emptyList();
        }

        List<String> errors = new ArrayList<>();
        for (Validator validation : rule.getValidators()) {
            if (validation.isValid(value)) {
                continue;
            }
            if (Validators.isNotEmpty(validation.getMessage())) {
                errors.add(rule.getMessage() + " : " + validation.getMessage());
            } else {
                errors.add(rule.getMessage());
            }
        }

        return errors;
    }

    private static boolean isValidCriteria(List<CriteriaRule> rules, Object bean) {
        if (Validators.isEmpty(rules)) {
            return true;
        }

        List<Boolean> andRules = new ArrayList<>();
        List<Boolean> orRules = new ArrayList<>();
        for (CriteriaRule rule : rules) {
            Object value = BeanUtils.getValue(bean, rule.getProperty());
            if (CriteriaRule.JoinType.OR == rule.getJoin()) {
                orRules.add(rule.isValid(value));
            } else {
                andRules.add(rule.isValid(value));
            }
        }

        if (Validators.isEmptyAll(andRules, orRules)) {
            return false;
        }

        return allTrue(andRules) && someTrue(orRules);
    }

    private static boolean allTrue(List<Boolean> andRules) {
        for (Boolean value : andRules) {
            if (!value) {
                return false;
            }
        }
        return true;
    }

    private static boolean someTrue(List<Boolean> orRules) {
        if (Validators.isEmpty(orRules)) {
            return true;
        }

        for (Boolean value : orRules) {
            if (value) {
                return true;
            }
        }
        return false;
    }
}
