/*
 * ---------------------------------------------------------------------------
 * Copyright (c) 2023. Cana Enterprise Co., Ltd. All rights reserved
 * ---------------------------------------------------------------------------
 */

package th.co.cana.dynamic.validation;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
public class ValidationRule implements Serializable {
    private int index;
    private int length;
    private boolean lengthMatches;
    private boolean required;
    private String property;
    private String message;
    private List<Validator> validators;
    private List<CriteriaRule> criteriaRules;

    public ValidationRule() {
        validators = new ArrayList<>();
        criteriaRules = new ArrayList<>();
    }

    public ValidationRule(int index, String message) {
        this();
        this.index = index;
        this.message = message;
    }

    public ValidationRule(int index, String message, Validator validator) {
        this();
        this.index = index;
        this.message = message;
        addValidator(validator);
    }

    public ValidationRule(String property, String message) {
        this();
        this.property = property;
        this.message = message;
    }

    public ValidationRule(String property, String message, Validator validator) {
        this();
        this.property = property;
        this.message = message;
        addValidator(validator);
    }

    public boolean isValidLength(Object value) {
        if (length <= 0 || value == null || value.toString().isEmpty()) {
            return true;
        }
        return (lengthMatches ? value.toString().length() == length
                : value.toString().length() <= length);
    }

    public boolean isInValidLength(Object value) {
        return !isValidLength(value);
    }

    public void addValidator(Validator validator){
        if (validator == null) {
            return;
        }
        validators.add(validator);
    }

    public void addCriteriaRule(CriteriaRule criteriaRule){
        if (criteriaRule == null) {
            return;
        }
        criteriaRules.add(criteriaRule);
    }

    public boolean isEmptyValidator() {
        return (validators == null || validators.isEmpty());
    }
}