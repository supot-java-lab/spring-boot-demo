/*
 * ---------------------------------------------------------------------------
 * Copyright (c) 2024. Cana Enterprise Co., Ltd. All rights reserved
 * ---------------------------------------------------------------------------
 */

package th.co.cana.dynamic.utils;

import io.github.jdevlibs.utils.Convertors;
import io.github.jdevlibs.utils.Validators;
import jakarta.validation.ConstraintValidatorContext;

import java.util.ArrayList;
import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */
public final class ConstraintUtils {

    private ConstraintUtils() {}

    public static boolean isDynamicMessage(String message) {
        if (Validators.isEmpty(message)) {
            return false;
        }

        return message.startsWith("{") && message.endsWith("}");
    }

    public static String formatMessage(String message) {
        return message.replace("{", "")
                .replace("}", "");
    }

    public static List<Object> convert(Class<?> type, List<String> allowValues) {
        List<Object> results = new ArrayList<>();
        if (Validators.isEmpty(allowValues)) {
            return results;
        }

        for (String value : allowValues) {
            Object convertValue = Convertors.convertWithType(type, value);
            if (convertValue != null) {
                results.add(convertValue);
            }
        }

        return results;
    }

    public static int getObjectSize(Object value) {

        return 0;
    }

    public static void setConstraintViolationError(final ConstraintValidatorContext context, String message) {
        setConstraintViolationError(context, null, message);
    }

    public static void setConstraintViolationError(final ConstraintValidatorContext context, String property, String message) {
        if (Validators.isEmpty(message)) {
            return;
        }
        context.disableDefaultConstraintViolation();
        if (Validators.isNotEmpty(property)) {
            context.buildConstraintViolationWithTemplate(message)
                    .addPropertyNode(property).addConstraintViolation();
        } else {
            context.buildConstraintViolationWithTemplate(message).addConstraintViolation();
        }
    }
}
