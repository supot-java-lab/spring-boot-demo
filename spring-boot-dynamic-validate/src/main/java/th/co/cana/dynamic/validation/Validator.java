/*
 * ---------------------------------------------------------------------------
 * Copyright (c) 2023. Cana Enterprise Co., Ltd. All rights reserved
 * ---------------------------------------------------------------------------
 */

package th.co.cana.dynamic.validation;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * @author supot.jdev
 * @version 1.0
 */

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        property = "type",
        defaultImpl = ValidatorEmpty.class
)
@JsonSubTypes({
        @JsonSubTypes.Type(value = ValidatorNull.class, name = "NULL"),
        @JsonSubTypes.Type(value = ValidatorEmpty.class, name = "EMPTY"),
        @JsonSubTypes.Type(value = ValidatorDateTime.class, name = "DATE"),
        @JsonSubTypes.Type(value = ValidatorNumber.class, name = "NUMBER"),
        @JsonSubTypes.Type(value = ValidatorDecimal.class, name = "DECIMAL"),
        @JsonSubTypes.Type(value = ValidatorContain.class, name = "CONTAIN"),
        @JsonSubTypes.Type(value = ValidatorDecimalRange.class, name = "RANGE_DECIMAL"),
        @JsonSubTypes.Type(value = ValidatorNumberRange.class, name = "RANGE_NUMBER"),
        @JsonSubTypes.Type(value = ValidatorThaiCitizenId.class, name = "THAI_CITIZEN")
}
)
public interface Validator {
    boolean isValid(Object value);

    String getMessage();

    String getOptionalValue();
}
