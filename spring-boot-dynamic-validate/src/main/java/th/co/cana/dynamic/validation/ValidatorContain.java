/*
 * ---------------------------------------------------------------------------
 * Copyright (c) 2023. Cana Enterprise Co., Ltd. All rights reserved
 * ---------------------------------------------------------------------------
 */

package th.co.cana.dynamic.validation;

import io.github.jdevlibs.utils.Validators;

import java.util.ArrayList;
import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class ValidatorContain implements Validator {
    private final List<Object> values;
    public ValidatorContain() {
        this.values = new ArrayList<>();
    }

    public ValidatorContain(List<Object> values) {
        this.values = values;
    }

    @Override
    public boolean isValid(Object value) {
        if (Validators.isEmptyOne(value, values)) {
            return false;
        }

        return values.contains(value);
    }

    @Override
    public String getMessage() {
        return "Value must be[" + toStringValue() + "]";
    }

    @Override
    public String getOptionalValue() {
        return toStringValue();
    }

    private String toStringValue() {
        if (Validators.isEmpty(values)) {
            return "";
        }

        StringBuilder sb = new StringBuilder();
        boolean first = true;
        for (Object obj : values) {
            if (first) {
                sb.append(obj);
                first = false;
            } else {
                sb.append(",").append(obj);
            }
        }
        return sb.toString();
    }
}
