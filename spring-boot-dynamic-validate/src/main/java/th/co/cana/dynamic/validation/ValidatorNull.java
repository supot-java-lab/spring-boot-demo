/*
 * ---------------------------------------------------------------------------
 * Copyright (c) 2023. Cana Enterprise Co., Ltd. All rights reserved
 * ---------------------------------------------------------------------------
 */

package th.co.cana.dynamic.validation;

import io.github.jdevlibs.utils.Validators;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class ValidatorNull implements Validator {
    @Override
    public boolean isValid(Object value) {
        return Validators.isNotNull(value);
    }

    @Override
    public String getMessage() {
        return "Value cannot be null.";
    }

    @Override
    public String getOptionalValue() {
        return null;
    }
}
