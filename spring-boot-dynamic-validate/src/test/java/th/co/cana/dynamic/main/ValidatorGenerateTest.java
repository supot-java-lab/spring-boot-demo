/*
 * ---------------------------------------------------------------------------
 * Copyright (c) 2023. Cana Enterprise Co., Ltd. All rights reserved
 * ---------------------------------------------------------------------------
 */

package th.co.cana.dynamic.main;

import io.github.jdevlibs.spring.utils.JsonUtils;
import th.co.cana.dynamic.validation.*;

import java.util.Arrays;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class ValidatorGenerateTest {
    public static void main(String[] args) {
        ValidationRule rule = new ValidationRule();
        rule.setIndex(1);
        rule.setProperty("name");
        rule.setLength(100);
        rule.setMessage("Name cannot be null.");
        rule.addValidator(new ValidatorNumber());
        rule.addValidator(new ValidatorNumberRange(10L, 500L));
        rule.addValidator(new ValidatorContain(Arrays.asList("1", "2")));
        rule.addValidator(new ValidatorDateTime());

        String json = JsonUtils.json(rule, true);
        System.out.println(json);
    }
}
