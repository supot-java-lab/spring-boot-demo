/*
 * ---------------------------------------------------------------------------
 * Copyright (c) 2023. Cana Enterprise Co., Ltd. All rights reserved
 * ---------------------------------------------------------------------------
 */

package th.co.cana.dynamic.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author supot.jdev
 * @version 1.0
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class Dog extends Animal{
    private String walkStep;
    private int age;
    private AnimalHome home;
}
