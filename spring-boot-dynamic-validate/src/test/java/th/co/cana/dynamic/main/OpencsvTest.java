/*
 * ---------------------------------------------------------------------------
 * Copyright (c) 2023. Cana Enterprise Co., Ltd. All rights reserved
 * ---------------------------------------------------------------------------
 */

package th.co.cana.dynamic.main;

import com.opencsv.CSVIterator;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.exceptions.CsvException;
import com.opencsv.exceptions.CsvValidationException;

import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class OpencsvTest {
    private static final String PATH = "src/test/resources/csv-data.csv";

    public static void main(String[] args) throws Exception {
        System.out.println("++++++++++++++ Line +++++++++++++++");
        readByLine();
        System.out.println("++++++++++++++ ALL +++++++++++++++");
        readByAll();
        System.out.println("++++++++++++++ Iterator +++++++++++++++");
        readByIterator();
    }

    private static void readByLine() throws IOException, CsvValidationException {
        CSVReader reader = new CSVReaderBuilder(new FileReader(PATH)).build();
        String [] nextLine;
        while ((nextLine = reader.readNext()) != null) {
            System.out.println(Arrays.toString(nextLine));
        }
    }

    private static void readByAll() throws IOException, CsvException {
        CSVReader reader = new CSVReaderBuilder(new FileReader(PATH)).build();
        List<String[]> results = reader.readAll();
        for (String[] data : results) {
            System.out.println(Arrays.toString(data));
        }
    }

    private static void readByIterator() throws IOException, CsvValidationException {
        CSVReader reader = new CSVReaderBuilder(new FileReader(PATH)).build();
        CSVIterator iterator = new CSVIterator(reader);
        while (iterator.hasNext()) {
            String[] nextLine = iterator.next();
            System.out.println(Arrays.toString(nextLine));
        }

    }
}
