/*
 * ---------------------------------------------------------------------------
 * Copyright (c) 2023. Cana Enterprise Co., Ltd. All rights reserved
 * ---------------------------------------------------------------------------
 */

package th.co.cana.dynamic.main;

import io.github.jdevlibs.utils.BeanUtils;
import th.co.cana.dynamic.model.AnimalHome;
import th.co.cana.dynamic.model.Dog;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class BeanUtilsTest {
    public static void main(String[] args) {
        Dog dog = new Dog();
        dog.setAge(10);
        dog.setHome(new AnimalHome());
        dog.getHome().setOwner("Supot");
        dog.setName("Lockok");
        dog.setSpicy("Animal");

        Object value = BeanUtils.getValue(dog, "age");
        System.out.println("Age : " + value);

        value = BeanUtils.getValue(dog, "name");
        System.out.println("Name : " + value);

        value = BeanUtils.getValue(dog, "Spicy");
        System.out.println("Spicy : " + value);

        value = BeanUtils.getValue(dog, "home");
        System.out.println("home : " + value);

        value = BeanUtils.getValue(dog, "home.name");
        System.out.println("home.name : " + value);

        value = BeanUtils.getValue(dog, "home.owner");
        System.out.println("home.owner : " + value);
    }
}
