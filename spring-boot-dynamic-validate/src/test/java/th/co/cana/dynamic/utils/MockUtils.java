/*
 * ---------------------------------------------------------------------------
 * Copyright (c) 2023. Cana Enterprise Co., Ltd. All rights reserved
 * ---------------------------------------------------------------------------
 */

package th.co.cana.dynamic.utils;

import io.github.jdevlibs.utils.IOUtils;

import java.io.FileInputStream;
import java.io.IOException;

/**
 * @author supot.jdev
 * @version 1.0
 */
public final class MockUtils {
    private MockUtils() {

    }

    public static String readFile(String path) {
        try {
            return IOUtils.toString(new FileInputStream(path), "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        return null;
    }
}