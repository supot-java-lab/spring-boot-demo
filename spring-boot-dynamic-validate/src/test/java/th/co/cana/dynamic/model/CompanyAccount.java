/*
 * ---------------------------------------------------------------------------
 * Copyright (c) 2023. Cana Enterprise Co., Ltd. All rights reserved
 * ---------------------------------------------------------------------------
 */

package th.co.cana.dynamic.model;

import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvDate;
import com.opencsv.bean.CsvDates;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
public class CompanyAccount implements Serializable {

    @CsvBindByName
    private String code;

    @CsvBindByName
    private BigDecimal amount;

    @CsvBindByName
    private Integer quantity;

    @CsvBindByName
    private String currency;

    @CsvBindByName
    private String type;

    @CsvBindByName
    private String amountType;

    @CsvBindByName
    private BigDecimal payCredit;

    @CsvBindByName
    private String company;

    @CsvBindByName
    private String description;

    @CsvBindByName
    private String job;

    @CsvBindByName
    private String location;

    @CsvBindByName
    private String adjusted;

    @CsvBindByName
    @CsvDate("yyyy-MM-dd'T'HH:mm:ss")
    private LocalDateTime transactionDate;

    @CsvBindByName(format = "yyyy-MM-dd")
    @CsvDate("yyyy-MM-dd")
    private LocalDate AcceptDate;
}
