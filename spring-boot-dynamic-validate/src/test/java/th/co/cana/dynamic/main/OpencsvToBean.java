/*
 * ---------------------------------------------------------------------------
 * Copyright (c) 2023. Cana Enterprise Co., Ltd. All rights reserved
 * ---------------------------------------------------------------------------
 */

package th.co.cana.dynamic.main;

import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.ColumnPositionMappingStrategyBuilder;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;
import th.co.cana.dynamic.model.CompanyAccount;
import th.co.cana.dynamic.model.CompanyProfit;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.function.Function;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class OpencsvToBean {
    private static final String PATH = "src/test/resources/csv-data.csv";
    private static final Function<String[], Boolean> ROW_MUST_HAVE_THREE_COLUMNS = (x) -> {
        return x.length == 3;
    };

    public static void main(String[] args) throws Exception {
        System.out.println("++++++++++++++ ALL +++++++++++++++");
        readAllToBean();
        System.out.println("++++++++++++++ Iterator +++++++++++++++");
        readByIterator();
        System.out.println("++++++++++++++ Reading without annotations +++++++++++++++");
        readingWithoutAnnotations();
    }

    private static void readAllToBean() throws IOException {
        List<CompanyAccount> beans = new CsvToBeanBuilder<CompanyAccount>(new FileReader(PATH))
                .withType(CompanyAccount.class).build().parse();
        for (CompanyAccount bean : beans) {
            System.out.println(bean);
        }
    }

    private static void readByIterator() throws IOException {
        CsvToBean<CompanyAccount> reader = new CsvToBeanBuilder<CompanyAccount>(new FileReader(PATH))
                .withType(CompanyAccount.class).build();
        for (CompanyAccount bean : reader) {
            System.out.println(bean);
        }

    }

    private static void readingWithoutAnnotations() throws IOException {
        ColumnPositionMappingStrategy<CompanyProfit> strategy = new ColumnPositionMappingStrategyBuilder<CompanyProfit>().build();
        strategy.setType(CompanyProfit.class);
        String[] columns = new String[] {"code", "amount", "quantity", "currency", "type", "amountType", "payCredit"
                , "company", "description", "job", "location", "adjusted", "transactionDate", "acceptDate"}; // the fields to bind to in your bean
        strategy.setColumnMapping(columns);
        List<CompanyProfit> beans = new CsvToBeanBuilder<CompanyProfit>(new FileReader(PATH))
                .withMappingStrategy(strategy)
                .build()
                .parse();
        for (CompanyProfit bean : beans) {
            System.out.println(bean);
        }
    }
}
