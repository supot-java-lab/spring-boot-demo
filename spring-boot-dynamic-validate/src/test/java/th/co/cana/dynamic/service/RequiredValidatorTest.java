/*
 * ---------------------------------------------------------------------------
 * Copyright (c) 2024. Cana Enterprise Co., Ltd. All rights reserved
 * ---------------------------------------------------------------------------
 */

package th.co.cana.dynamic.service;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import th.co.cana.dynamic.BaseUnitTest;
import th.co.cana.dynamic.model.Customer;

import java.util.Set;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class RequiredValidatorTest extends BaseUnitTest {

    @DisplayName("[RequiredValidator]: test 1")
    @Test
    @Order(1)
    void ok_validate() {
        try (ValidatorFactory factory = Validation.buildDefaultValidatorFactory()) {
            Validator validator = factory.getValidator();

            Customer customer = new Customer();
            customer.setVatType("ABC");

            Set<ConstraintViolation<Customer>> violations = validator.validate(customer);
            for (ConstraintViolation<Customer> violation : violations) {
               logger.info("{} -> {}", violation.getPropertyPath().iterator().next().getName(), violation.getMessage());
            }
            Assertions.assertNotNull(violations);
        }
    }
}
