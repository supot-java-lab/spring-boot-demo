/*
 * ---------------------------------------------------------------------------
 * Copyright (c) 2023. Cana Enterprise Co., Ltd. All rights reserved
 * ---------------------------------------------------------------------------
 */

package th.co.cana.dynamic.model;

import lombok.Data;

/**
* @author supot.jdev
* @version 1.0
*/
@Data
public class Animal {
    private String name;
    private String spicy;
}
