/*
 * ---------------------------------------------------------------------------
 * Copyright (c) 2023. Cana Enterprise Co., Ltd. All rights reserved
 * ---------------------------------------------------------------------------
 */

package th.co.cana.dynamic.model;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
public class CompanyProfit implements Serializable {
    private String code;
    private BigDecimal amount;
    private Integer quantity;
    private String currency;
    private String type;
    private String amountType;
    private BigDecimal payCredit;
    private String company;
    private String description;
    private String job;
    private String location;
    private String adjusted;
    //private LocalDateTime transactionDate;
    //private LocalDate AcceptDate;
}
