/*
 * ---------------------------------------------------------------------------
 * Copyright (c) 2023. Cana Enterprise Co., Ltd. All rights reserved
 * ---------------------------------------------------------------------------
 */

package th.co.cana.dynamic.main;

import io.github.jdevlibs.utils.ThaiCitizenUtils;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class ThaiIdCardTest {
    public static void main(String[] args) {
        for (int i = 1; i <= 10; i++) {
            System.out.println(ThaiCitizenUtils.generateThaiCitizenId());
        }
    }
}
