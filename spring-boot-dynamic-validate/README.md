# Getting Started

### Reference Documentation
For further reference, please consider the following sections:

* [Inheritance with Jackson](https://www.baeldung.com/jackson-inheritance)
* [Polymorphism and Inheritance with Jackson](https://octoperf.com/blog/2018/02/01/polymorphism-with-jackson/#implementations)
