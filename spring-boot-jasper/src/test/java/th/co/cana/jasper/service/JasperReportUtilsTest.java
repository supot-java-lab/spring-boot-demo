package th.co.cana.jasper.service;

import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.JasperReport;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import th.co.cana.jasper.BaseUnitTest;
import th.co.cana.jasper.exception.JasperException;
import th.co.cana.jasper.utils.JasperReportUtils;

import java.io.*;
import java.nio.file.Files;
import java.util.Map;

/**
 * @author supot.jdev
 * @version 1.0
 */

@Slf4j
class JasperReportUtilsTest extends BaseUnitTest {

    @DisplayName("[JasperReportUtils]: compileReport_to_JasperReport")
    @Test
    @Order(1)
    void compileReport_to_JasperReport() throws JasperException {
        String jrxml = "src/main/resources/jasper/font-thai.jrxml";
        JasperReport report = JasperReportUtils.compileReport(jrxml);
        Assertions.assertNotNull(report);
        log.info("Name : {}", report.getName());
    }

    @DisplayName("[JasperReportUtils]: exportToPdfStream_byte_array")
    @Test
    @Order(2)
    void exportToPdfStream_byte_array() throws JasperException {
        String jrxml = "src/main/resources/jasper/font-thai.jrxml";
        JasperReport report = JasperReportUtils.compileReport(jrxml);
        Assertions.assertNotNull(report);

        Map<String, Object> params = JasperReportUtils.createReportParameter();
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        JasperReportUtils.exportToPdf(report, params, bos);
        Assertions.assertNotNull(bos.toByteArray());
        log.info("File size : {}", bos.size());
    }

    @DisplayName("[JasperReportUtils]: exportToPdfFile")
    @Test
    @Order(3)
    void exportToPdfFile() throws JasperException, IOException {
        String filePath = "src/main/resources/jasper/font-thai.jasper";
        InputStream jasper = Files.newInputStream(new File(filePath).toPath());
        FileOutputStream out = new FileOutputStream("src/test/resources/font-thai.pdf");
        Map<String, Object> params = JasperReportUtils.createReportParameter();
        JasperReportUtils.exportToPdf(jasper, params, out);
        Assertions.assertNotNull(out);
    }
}
