package th.co.cana.jasper;

import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;

/**
 * @author supot.jdev
 * @version 1.0
 */
@ExtendWith(SpringExtension.class)
@TestMethodOrder(OrderAnnotation.class)
public abstract class SpringUnitTest {

}
