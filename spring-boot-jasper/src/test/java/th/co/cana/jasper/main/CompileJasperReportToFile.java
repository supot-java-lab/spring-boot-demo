package th.co.cana.jasper.main;

import th.co.cana.jasper.exception.JasperException;
import th.co.cana.jasper.utils.JasperReportUtils;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class CompileJasperReportToFile {
    public static void main(String[] args) {
        try {
            String jrxml = "src/main/resources/jasper/font-thai.jrxml";
            String targetFile = "src/main/resources/jasper/font-thai.jasper";
            JasperReportUtils.compileReportToFile(jrxml, targetFile);
        } catch (JasperException ex) {
            ex.printStackTrace();
        }
    }
}
