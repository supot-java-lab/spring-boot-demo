package th.co.cana.jasper.utils;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Slf4j
public final class JsonUtils {
    private static final String FORMAT_DT = "yyyy-MM-dd'T'HH:mm:ss";

    private static final JsonMapper mapper;

    private JsonUtils() {}

    static {
        mapper = JsonMapper.builder().build();
        mapperConfig(mapper);
    }

    /**
     * Serialize object value to JSON data format
     * @param obj The object to serialize.
     * @return Serialize value as string
     */
    public static String json(Object obj) {
        return json(obj, false);
    }

    /**
     * Serialize object value to JSON data format
     * @param obj The object to convert.
     * @param prettyOutput Write out pretty readable.
     * @return Serialize value as string
     */
    public static String json(Object obj, boolean prettyOutput) {
        String jsonData = null;
        try {
            if (prettyOutput) {
                jsonData = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
            } else {
                jsonData = mapper.writeValueAsString(obj);
            }
        } catch (JsonProcessingException ex) {
            log.error("json", ex);
        }

        return jsonData;
    }

    /**
     * deserializing JSON data format to Object class.
     * @param <T> The type of class for deserializing.
     * @param json JSON data format.
     * @param clazz The class for deserializing.
     * @return Object class
     */
    public static <T> T model(String json, Class<T> clazz) {
        if (Validators.isEmpty(json)) {
            return null;
        }
        return model(json.getBytes(), clazz);
    }

    /**
     * <pre>
     * deserializing JSON data format to Object class
     * In case need full generic type information
     * </pre>
     * @param <T> The type of class for deserializing.
     * @param json JSON data format.
     * @param type JavaType of deserializing.
     * @return Object class
     */
    public static <T> T model(String json, JavaType type) {
        if (Validators.isEmptyOne(json, type)) {
            return null;
        }
        return model(json.getBytes(), type);
    }

    /**
     * deserializing JSON data format to List of model
     * @param <T> The type of class for deserializing.
     * @param json JSON data format must be array.
     * @param clazz The class for deserializing.
     * @return Object class
     */
    public static <T> List<T> models(String json, Class<T> clazz) {
        if (Validators.isEmpty(json)) {
            return Collections.emptyList();
        }
        return models(json.getBytes(), clazz);
    }

    /**
     * Serialize object value to JSON data format
     * @param obj The object to serialize.
     * @return Serialize value as byte[] arrays
     */
    public static byte[] jsonAsBytes(Object obj) {
        byte[] jsonData = null;
        try {
            jsonData = mapper.writeValueAsBytes(obj);
        } catch (JsonProcessingException ex) {
            log.error("jsonAsBytes", ex);
        }

        return jsonData;
    }

    /**
     * deserializing JSON data format to Object class.
     * @param <T> The type of class for deserializing.
     * @param jsonData JSON byte[] data format.
     * @param clazz The class for deserializing.
     * @return Object class
     */
    public static <T> T model(byte[] jsonData, Class<T> clazz) {
        try {
            if (Validators.isEmpty(jsonData)) {
                return null;
            }

            return mapper.readValue(jsonData, clazz);
        } catch (IOException ex) {
            log.error("model", ex);
        }

        return null;
    }

    /**
     * <pre>
     * deserializing JSON data format to Object class.
     * In case need full generic type information
     * </pre>
     * @param <T> The type of class for deserializing.
     * @param jsonData JSON byte[] data format.
     * @param type JavaType of deserializing.
     * @return Object class
     */
    public static <T> T model(byte[] jsonData, JavaType type) {
        try {
            if (Validators.isEmpty(jsonData)) {
                return null;
            }

            return mapper.readValue(jsonData, type);
        } catch (IOException ex) {
            log.error("model", ex);
        }

        return null;
    }

    /**
     * deserializing JSON data format to List of model.
     * @param <T> The type of class for deserializing.
     * @param jsonData JSON byte[] data format.
     * @param clazz The class for deserializing.
     * @return Object class
     */
    public static <T> List<T> models(byte[] jsonData, Class<T> clazz) {
        try {
            if (Validators.isEmpty(jsonData)) {
                return Collections.emptyList();
            }

            JavaType javaType = collectionType(List.class, clazz);
            return mapper.readValue(jsonData, javaType);
        } catch (IOException ex) {
            log.error("models", ex);
        }

        return Collections.emptyList();
    }

    /**
     * deserializing JSON data format to Map class.
     * @param json json JSON data format.
     * @return deserializing result of Map.
     */
    public static Map<String, Object> map(String json) {
        if (Validators.isEmpty(json)) {
            return Collections.emptyMap();
        }
        return map(json.getBytes());
    }

    /**
     * deserializing JSON data format to Map class.
     * @param jsonData JSON byte[] data format.
     * @return deserializing result of Map.
     */
    @SuppressWarnings("unchecked")
    public static Map<String, Object> map(byte[] jsonData) {
        Map<String, Object> maps = JsonUtils.model(jsonData, Map.class);
        if (Validators.isNull(maps)) {
            maps = new HashMap<>();
        }

        return maps;
    }

    /**
     * deserializing JSON data format to Map class.
     * @param <K> Map key type
     * @param <V> Map value type
     * @param json JSON data format.
     * @param keyClass Map key class
     * @param valueClass Map value class
     * @return deserializing result of Map.
     */
    public static <K, V> Map<K, V> map(String json, Class<K> keyClass, Class<V> valueClass) {
        if (Validators.isEmpty(json) || Validators.isNullOne(keyClass, valueClass)) {
            return Collections.emptyMap();
        }
        return map(json.getBytes(), keyClass, valueClass);
    }

    /**
     * deserializing JSON data format to Map class.
     * @param <K> Map key type
     * @param <V> Map value type
     * @param jsonData JSON byte[] data format.
     * @param keyClass Map key class
     * @param valueClass Map value class
     * @return deserializing result of Map.
     */
    public static <K, V> Map<K, V> map(byte[] jsonData, Class<K> keyClass, Class<V> valueClass) {
        try {
            if (Validators.isEmpty(jsonData)
                    || Validators.isNullOne(keyClass, valueClass)) {
                return Collections.emptyMap();
            }

            JavaType javaType = mapType(Map.class, keyClass, valueClass);
            return mapper.readValue(jsonData, javaType);
        } catch (IOException ex) {
            log.error("map", ex);
        }

        return Collections.emptyMap();
    }

    /**
     * Create CollectionType instance.
     * @param collClass The Collection class type
     * @param elementClazz The element of collection class type
     * @return CollectionType
     */
    @SuppressWarnings("rawtypes")
    public static JavaType collectionType(Class<? extends Collection> collClass, Class<?> elementClazz) {
        return mapper.getTypeFactory().constructCollectionType(collClass, elementClazz);
    }

    /**
     * Create MapType instance.
     * @param mapClass The class of map type
     * @param keyClass The class of map key
     * @param valueClass The class of map value
     * @return MapType
     */
    @SuppressWarnings("rawtypes")
    public static JavaType mapType(Class<? extends Map> mapClass,
                                   Class<?> keyClass, Class<?> valueClass) {
        return mapper.getTypeFactory().constructMapType(mapClass, keyClass, valueClass);
    }

    private static void mapperConfig(JsonMapper mapper) {
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.setDateFormat(new SimpleDateFormat(FORMAT_DT));
        mapper.registerModule(new JavaTimeModule());

        // Only serialization class attributes or member ignore all get/sets
        mapper.setVisibility(mapper.getVisibilityChecker()
                .withFieldVisibility(JsonAutoDetect.Visibility.ANY)
                .withGetterVisibility(JsonAutoDetect.Visibility.NONE)
                .withSetterVisibility(JsonAutoDetect.Visibility.NONE)
                .withCreatorVisibility(JsonAutoDetect.Visibility.NONE)
                .withIsGetterVisibility(JsonAutoDetect.Visibility.NONE));
    }
}
