package th.co.cana.jasper.exception;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class JasperException extends Exception {
    public JasperException(Throwable cause) {
        super(cause);
    }

    public JasperException(String message) {
        super(message);
    }

    public JasperException(String message, Throwable cause) {
        super(message, cause);
    }
}
