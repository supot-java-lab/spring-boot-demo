package th.co.cana.jasper.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import th.co.cana.jasper.exception.JasperException;
import th.co.cana.jasper.utils.JasperReportUtils;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Map;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Slf4j
@Service
public class JasperReportService {
    public byte[] generateReport(final String reportName, final Map<String, Object> params) throws JasperException {
        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            InputStream inputStream = getResourceAsStream(reportName);
            JasperReportUtils.exportToPdf(inputStream, params, out);
            return out.toByteArray();
        } catch (Exception ex) {
            throw new JasperException("Load template error", ex);
        }
    }

    public InputStream getResourceAsStream(final String fileName) {
        try {
            Class<?> clazz = Class.forName(this.getClass().getName());
            return clazz.getClassLoader().getResourceAsStream("jasper/" + fileName);
        } catch (Exception ex) {
            log.error("getResourceAsStream", ex);
        }
        return null;
    }
}
