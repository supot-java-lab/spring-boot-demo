package th.co.cana.jasper.utils;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.data.JsonDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.query.JsonQueryExecuterFactory;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import th.co.cana.jasper.exception.JasperException;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;

/**
 * @author supot.jdev
 * @version 1.0
 */
public final class JasperReportUtils {
    private JasperReportUtils() {

    }

    public static JasperReport compileReport(String jrxml) throws JasperException {
        return compileReport(new File(jrxml));
    }

    public static JasperReport compileReport(File file) throws JasperException {
        try {
            return JasperCompileManager.compileReport(JRXmlLoader.load(file));
        } catch (JRException ex) {
            throw new JasperException("Cannot load or compile jasper report from : " + file.getAbsolutePath(), ex);
        }
    }

    public static JasperReport compileReport(InputStream ins) throws JasperException {
        try {
            return JasperCompileManager.compileReport(JRXmlLoader.load(ins));
        } catch (JRException ex) {
            throw new JasperException("Cannot load or compile jasper", ex);
        }
    }

    public static void compileReportToFile(String jrxml, String targetFile) throws JasperException {
        try {
            JasperCompileManager.compileReportToFile(jrxml, targetFile);
        } catch (JRException ex) {
            throw new JasperException("Cannot load or compile jasper", ex);
        }
    }

    public static JasperPrint fillReport(JasperReport jasper, Collection<?> beans) throws JasperException {
        return fillReport(jasper, beans, createReportParameter());
    }

    public static JasperPrint fillReport(JasperReport jasper, Map<String, Object> params) throws JasperException {
        return fillReport(jasper, new ArrayList<>(), params);
    }

    public static JasperPrint fillReport(JasperReport jasper, Collection<?> beans, Map<String, Object> params)
            throws JasperException {
        try {
            return JasperFillManager.fillReport(jasper, params, createDataSource(beans));
        } catch (JRException ex) {
            throw new JasperException("Cannot fill data with collection bean to report", ex);
        }
    }

    public static JasperPrint fillReport(InputStream ins, Object jsonBean)
            throws JasperException {
        return fillReport(ins, jsonBean, createReportParameter());
    }

    public static JasperPrint fillReport(InputStream ins, Object jsonBean, Map<String, Object> params)
            throws JasperException {
        try {
            JsonDataSource ds = createDataSource(jsonBean);
            return JasperFillManager.fillReport(ins, params, ds);
        } catch (JRException ex) {
            throw new JasperException("Cannot fill json data to report", ex);
        }
    }

    public static JasperPrint fillReport(InputStream ins, Collection<?> beans) throws JasperException {
        return fillReport(ins, beans, createReportParameter());
    }

    public static JasperPrint fillReport(InputStream ins, Map<String, Object> params) throws JasperException {
        return fillReport(ins, new ArrayList<>(), params);
    }

    public static JasperPrint fillReport(InputStream ins, Collection<?> beans, Map<String, Object> params)
            throws JasperException {
        try {
            return JasperFillManager.fillReport(ins, params, createDataSource(beans));
        } catch (JRException ex) {
            throw new JasperException("Cannot fill data to report", ex);
        }
    }

    public static void fillReport(JasperReport jasper, Collection<?> beans, OutputStream out) throws JasperException {
        fillReport(jasper, beans, createReportParameter(), out);
    }

    public static void fillReport(JasperReport jasper, Map<String, Object> params, OutputStream out) throws JasperException {
        fillReport(jasper, new ArrayList<>(), params, out);
    }

    public static void fillReport(JasperReport jasper, Collection<?> beans, Map<String, Object> params
            , OutputStream out) throws JasperException {
        try {
            JasperFillManager.fillReportToStream(jasper, out, params, createDataSource(beans));
        } catch (JRException ex) {
            throw new JasperException("Cannot fill data to report", ex);
        }
    }

    public static void fillReport(InputStream ins, Collection<?> beans, OutputStream out) throws JasperException {
        fillReport(ins, beans, createReportParameter(), out);
    }

    public static void fillReport(InputStream ins, Map<String, Object> params, OutputStream out) throws JasperException {
        fillReport(ins, new ArrayList<>(), params, out);
    }

    public static void fillReport(InputStream ins, Collection<?> beans, Map<String, Object> params, OutputStream out) throws JasperException {
        try {
            JasperFillManager.fillReportToStream(ins, out, params, createDataSource(beans));
        } catch (JRException ex) {
            throw new JasperException("Cannot fill data to report", ex);
        }
    }

    public static JasperPrint merge(JasperPrint master, JasperPrint ... merge) {
        if (master == null || merge == null) {
            return master;
        }

        for (JasperPrint jr : merge) {
            if (jr == null) {
                continue;
            }

            for (JRPrintPage jrPage : jr.getPages()) {
                master.addPage(jrPage);
            }
        }
        return master;
    }

    public static void exportToPdf(JasperPrint jasperPrint, OutputStream outputStream) throws JasperException {
        try {
            JRPdfExporter exporter = createJRPdfExporter(jasperPrint);
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputStream));

            exporter.exportReport();
        } catch (JRException ex) {
            throw new JasperException("Cannot export jasper to PDF OutputStream", ex);
        }
    }

    public static void exportToPdf(InputStream ins, Collection<?> beans, OutputStream out)
            throws JasperException {
        exportToPdf(ins, beans, out, createReportParameter());
    }

    public static void exportToPdf(InputStream ins, Map<String, Object> params, OutputStream out)
            throws JasperException {
        exportToPdf(ins, new ArrayList<>(), out, params);
    }

    public static void exportToPdf(InputStream ins, Collection<?> beans, OutputStream outputStream, Map<String, Object> params) throws JasperException {
        try {
            JRPdfExporter exporter = createJRPdfExporter(fillReport(ins, beans, params));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputStream));
            exporter.exportReport();
        } catch (JRException ex) {
            throw new JasperException("Cannot export jasper to PDF OutputStream", ex);
        }
    }

    public static void exportToPdf(JasperReport jasperReport, Collection<?> beans, OutputStream out) throws JasperException {
        exportToPdf(jasperReport, beans, out, createReportParameter());
    }

    public static void exportToPdf(JasperReport jasperReport, Map<String, Object> params, OutputStream out) throws JasperException {
        exportToPdf(jasperReport, new ArrayList<>(), out, params);
    }

    public static void exportToPdf(JasperReport jasperReport, Collection<?> beans, OutputStream outputStream
            , Map<String, Object> params) throws JasperException {
        try {
            JRPdfExporter exporter = createJRPdfExporter(fillReport(jasperReport, beans, params));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputStream));

            exporter.exportReport();
        } catch (JRException ex) {
            throw new JasperException("Cannot export jasper to PDF OutputStream", ex);
        }
    }

    public static void exportToPdfWithPassword(JasperReport jasperReport, Collection<?> beans, OutputStream out, String password) throws JasperException {
        exportToPdfWithPassword(jasperReport, beans, out, createReportParameter(), password);
    }

    public static void exportToPdfWithPassword(JasperReport jasperReport, Map<String, Object> params, OutputStream out, String password) throws JasperException {
        exportToPdfWithPassword(jasperReport, new ArrayList<>(), out, params, password);
    }

    public static void exportToPdfWithPassword(JasperReport jasperReport, Collection<?> beans, OutputStream outputStream
            , Map<String, Object> params, String password) throws JasperException {
        try {
            JRPdfExporter exporter = createJRPdfExporter(fillReport(jasperReport, beans, params));
            if (Validators.isNotEmpty(password)) {
                SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
                configuration.setEncrypted(true);
                configuration.set128BitKey(true);
                configuration.setUserPassword(password);
                exporter.setConfiguration(configuration);
            }
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputStream));

            exporter.exportReport();
        } catch (JRException ex) {
            throw new JasperException("Cannot export jasper to PDF OutputStream", ex);
        }
    }

    public static void exportToPdfWithPassword(InputStream ins, Collection<?> beans, OutputStream out, String password)
            throws JasperException {
        exportToPdfWithPassword(ins, beans, out, createReportParameter(), password);
    }

    public static void exportToPdfWithPassword(InputStream ins, Map<String, Object> params, OutputStream out, String password)
            throws JasperException {
        exportToPdfWithPassword(ins, new ArrayList<>(), out, params, password);
    }

    public static void exportToPdfWithPassword(InputStream ins, Collection<?> beans, OutputStream outputStream
            , Map<String, Object> params, String password) throws JasperException {
        try {
            JRPdfExporter exporter = createJRPdfExporter(fillReport(ins, beans, params));
            if (Validators.isNotEmpty(password)) {
                SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
                configuration.setEncrypted(true);
                configuration.set128BitKey(true);
                configuration.setUserPassword(password);
                exporter.setConfiguration(configuration);
            }
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputStream));
            exporter.exportReport();
        } catch (JRException ex) {
            throw new JasperException("Cannot export jasper to PDF OutputStream", ex);
        }
    }

    public static void exportToPdfWithPassword(JasperPrint jasperPrint, OutputStream outputStream, String password) throws JasperException {
        try {
            JRPdfExporter exporter = createJRPdfExporter(jasperPrint);
            if (Validators.isNotEmpty(password)) {
                SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
                configuration.setEncrypted(true);
                configuration.set128BitKey(true);
                configuration.setUserPassword(password);
                exporter.setConfiguration(configuration);
            }
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputStream));
            exporter.exportReport();
        } catch (JRException ex) {
            throw new JasperException("Cannot export jasper to PDF OutputStream", ex);
        }
    }

    public static Map<String, Object> createReportParameter() {
        return createReportParameter(Locale.US);
    }

    public static Map<String, Object> createReportParameter(Locale locale) {
        return createReportParameter(locale, null);
    }

    public static Map<String, Object> createReportParameter(Locale locale, ResourceBundle resource) {
        Map<String, Object> params = new HashMap<>();
        params.put(JRParameter.REPORT_LOCALE, locale);
        if (resource != null) {
            params.put(JRParameter.REPORT_RESOURCE_BUNDLE, resource);
        }
        params.put(JsonQueryExecuterFactory.JSON_DATE_PATTERN, "yyyy-MM-dd");
        params.put(JsonQueryExecuterFactory.JSON_NUMBER_PATTERN, "#,##0.##");
        params.put(JsonQueryExecuterFactory.JSON_LOCALE, Locale.ENGLISH);

        return params;
    }

    private static JRDataSource createDataSource(Collection<?> beans) {
        if (Validators.isEmpty(beans)) {
            return new JREmptyDataSource();
        }
        return new JRBeanCollectionDataSource(beans);
    }

    private static JsonDataSource createDataSource(Object jsonBean) throws JRException {
        String json = JsonUtils.json(jsonBean);
        return createDataSource(json);
    }

    private static JsonDataSource createDataSource(String json) throws JRException {
        if (Validators.isEmpty(json)) {
            return null;
        }
        ByteArrayInputStream jsonDataStream = new ByteArrayInputStream(json.getBytes());
        return new JsonDataSource(jsonDataStream);
    }

    private static JRPdfExporter createJRPdfExporter(JasperPrint jasperPrint) {
        JRPdfExporter exporter = new JRPdfExporter();
        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));

        return exporter;
    }
}
