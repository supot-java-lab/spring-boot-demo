package th.co.cana.jasper.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import th.co.cana.jasper.service.JasperReportService;

import java.util.HashMap;
import java.util.Map;

/**
 * @author supot.jdev
 * @version 1.0
 */

@RestController
@RequestMapping("/report")
@RequiredArgsConstructor
public class ReportCtrl extends ApiController {

    private final JasperReportService jasperReportService;

    @GetMapping(path = "/jasper/pdf")
    public ResponseEntity<Object> exportPdf() {

        logger.info("Starting call export pdf api");
        try {
            Map<String, Object> parameters = new HashMap<>();
            byte[] contents = jasperReportService.generateReport("font-thai.jasper", parameters);
            ByteArrayResource resource = new ByteArrayResource(contents);

            String filename = "font-thai.pdf";
            return download(resource, filename);
        } catch (Exception ex) {
            logger.error("generate report pdf error", ex);
            return internalServerError(ex);
        } finally {
            logger.info("Finished call export pdf api");
        }
    }
}
