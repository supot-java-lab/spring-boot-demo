package th.co.cana.jasper.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import th.co.cana.jasper.utils.MimeTypes;
import th.co.cana.jasper.utils.Validators;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;

/**
 * @author supot.jdev
 * @version 1.0
 */
public abstract class ApiController {
    protected Logger logger = LoggerFactory.getLogger(getClass());

    public ResponseEntity<Object> notFound() {
        return ResponseEntity.notFound().build();
    }

    public ResponseEntity<Object> internalServerError(Exception ex) {
        return internalServerError(ex.getMessage());
    }

    public ResponseEntity<Object> internalServerError(Object body) {
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(body);
    }

    public ResponseEntity<Object> download(Resource resource) {
        return download(resource, resource.getFilename(), true);
    }

    public ResponseEntity<Object> download(Resource resource, boolean attachment) {
        return download(resource, resource.getFilename(), attachment);
    }

    public ResponseEntity<Object> download(Resource resource, String fileName) {
        return download(resource, fileName, true);
    }

    public ResponseEntity<Object> download(Resource resource, String fileName, boolean attachment) {
        if (Validators.isNull(resource)) {
            return notFound();
        }
        if (Validators.isEmpty(fileName)) {
            fileName = resource.getFilename();
        }

        logger.info("Download file with name : {}", fileName);
        ResponseEntity.BodyBuilder builder = ResponseEntity.ok();
        if (attachment) {
            if (Validators.isNotEmpty(fileName)) {
                String filenameEncode = encode(fileName);
                builder.header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename*='UTF-8'" + filenameEncode);
            } else {
                builder.header(HttpHeaders.CONTENT_DISPOSITION, "attachment");
            }
        } else {
            builder.header(HttpHeaders.CONTENT_DISPOSITION, "inline");
        }
        if (Validators.isNotEmpty(fileName)) {
            String mimeType = MimeTypes.getMimeType(fileName);
            builder.header(HttpHeaders.CONTENT_TYPE, mimeType);
        } else {
            builder.header(HttpHeaders.CONTENT_TYPE, "application/octet-stream");
        }
        return builder.body(resource);
    }

    public ResponseEntity<Object> download(String file) {
        return download(new File(file), null, true);
    }

    public ResponseEntity<Object> download(String file, boolean attachment) {
        return download(new File(file), null, attachment);
    }

    public ResponseEntity<Object> download(String file, String fileName) {
        return download(new File(file), fileName, true);
    }

    public ResponseEntity<Object> download(String file, String fileName, boolean attachment) {
        return download(new File(file), fileName, attachment);
    }

    public ResponseEntity<Object> download(File file) {
        return download(file, file.getName(), true);
    }

    public ResponseEntity<Object> download(File file, boolean attachment) {
        return download(file, file.getName(), attachment);
    }

    public ResponseEntity<Object> download(File file, String fileName) {
        return download(file, fileName, true);
    }

    public ResponseEntity<Object> download(File file, String fileName, boolean attachment) {
        if (Validators.isNull(file) || !file.exists() || !file.isFile()) {
            logger.error("Download File not found");
            return notFound();
        }
        if (Validators.isEmpty(fileName)) {
            fileName = file.getName();
        }

        logger.info("Download file {} with name : {}", file.getPath(), fileName);
        Resource resource = loadFileAsResource(file);
        if (Validators.isNull(resource)) {
            return notFound();
        }

        ResponseEntity.BodyBuilder builder = ResponseEntity.ok();
        if (attachment) {
            if (Validators.isNotEmpty(fileName)) {
                String filenameEncode = encode(fileName);
                builder.header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename*='UTF-8'" + filenameEncode);
            } else {
                builder.header(HttpHeaders.CONTENT_DISPOSITION, "attachment");
            }
        } else {
            builder.header(HttpHeaders.CONTENT_DISPOSITION, "inline");
        }
        if (Validators.isNotEmpty(fileName)) {
            String mimeType = MimeTypes.getMimeType(fileName);
            builder.header(HttpHeaders.CONTENT_TYPE, mimeType);
        } else {
            builder.header(HttpHeaders.CONTENT_TYPE, "application/octet-stream");
        }
        return builder.body(resource);
    }

    private Resource loadFileAsResource(File fileName) {
        try {
            Path filePath = fileName.toPath().normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                return resource;
            }

            logger.error("File {} not found", fileName.getPath());
        } catch (MalformedURLException ex) {
            logger.error("loadFileAsResource not found", ex);
        }

        return null;
    }

    private String encode(String value) {
        try {
            return URLEncoder.encode(value, StandardCharsets.UTF_8);
        } catch (Exception ex) {
            return value;
        }
    }
}
