package th.co.cana.performance;

import org.junit.jupiter.api.Tag;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * @author supot.jdev
 * @version 1.0
 */

@SpringBootTest
@Tag("skip")
public class BaseUnitTest extends SpringUnitTest {

}
