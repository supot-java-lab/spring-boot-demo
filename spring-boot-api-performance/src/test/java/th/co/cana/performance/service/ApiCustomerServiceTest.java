package th.co.cana.performance.service;

import io.github.jdevlibs.utils.Utils;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StopWatch;
import th.co.cana.performance.BaseUnitTest;
import th.co.cana.performance.model.Customer;
import th.co.cana.performance.service.client.ApiCustomerService;
import th.co.cana.performance.utils.MockUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class ApiCustomerServiceTest extends BaseUnitTest {

    @Autowired
    private ApiCustomerService apiCustomerService;

    @DisplayName("[ApiCustomerService]: createCustomer")
    @Disabled
    @Test
    @Order(1)
    void createCustomer() {
        Customer customer = new Customer();
        customer.setId(Utils.getUUID());
        customer.setFirstName("Supot");
        customer.setLastName("Saelao");
        customer.setCreditAmt(BigDecimal.valueOf(1000000D));
        customer.setEmail("supot@gmail.com");

        Customer resp = apiCustomerService.createCustomer(customer);
        Assertions.assertNotNull(resp);
        logger.info("{}", resp);
    }

    @DisplayName("[ApiCustomerService]: createCustomerAsync")
    @Disabled
    @Test
    @Order(2)
    void getCustomerAsync() {
        apiCustomerService.getCustomerAsync();
        Assertions.assertTrue(Boolean.TRUE);
    }

    @DisplayName("[CustomerService]: save_CompletableFuture")
    //@Disabled
    @Test
    @Order(3)
    void save_CompletableFuture() throws ExecutionException, InterruptedException {

        int max = 20;
        List<Customer> customers = MockUtils.mockCustomer(max);

        StopWatch stopWatch = new StopWatch();
        stopWatch.start("Execute service");

        //Define completableFuture task
        List<CompletableFuture<Customer>> futureTasks = new ArrayList<>();
        ExecutorService executor = Executors.newFixedThreadPool(max);
        for (Customer customer : customers) {
            CompletableFuture<Customer> future = CompletableFuture.supplyAsync(() -> apiCustomerService.createCustomer(customer), executor);
            futureTasks.add(future);
        }
        CompletableFuture.allOf(futureTasks.toArray(new CompletableFuture[0])).join();

        for (CompletableFuture<Customer> future : futureTasks) {
            Customer customer = future.get();
            logger.info("{}: start: {}  end: {}", customer.getId(), customer.getStart(), customer.getEnd());
        }

        stopWatch.stop();
        logger.info("Process time: {}", stopWatch.prettyPrint(TimeUnit.SECONDS));
    }
}
