package th.co.cana.performance.service;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StopWatch;
import th.co.cana.performance.BaseUnitTest;
import th.co.cana.performance.model.Customer;
import th.co.cana.performance.utils.MockUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.*;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class ExecutorServiceTest extends BaseUnitTest {

    @Autowired
    public CustomerService customerService;

    @DisplayName("[CustomerService]: save_executor")
    //@Disabled
    @Test
    @Order(1)
    void save_executor() throws InterruptedException {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        List<Customer> customers = MockUtils.mockCustomer(50);
        ExecutorService executorService = Executors.newFixedThreadPool(customers.size());
        CountDownLatch latch = new CountDownLatch(customers.size());
        for (Customer customer : customers) {
            executorService.submit(() -> {
                customerService.save(customer);
                latch.countDown();
            });
        }
        latch.await();
        if (stopWatch.isRunning()) {
            stopWatch.stop();
        }
        logger.info("Process time: {}", stopWatch);

        assertTrue(Boolean.TRUE);
    }

    @DisplayName("[CustomerService]: save_parallelStream")
    @Disabled
    @Test
    @Order(2)
    void save_parallelStream() {
        List<Customer> customers = MockUtils.mockCustomer(50);
        ExecutorService executorService = Executors.newFixedThreadPool(customers.size());
        customers.parallelStream().forEach(customer -> executorService.submit(() -> {
            Customer customer1 = customerService.save(customer);
            logger.info("{} -> {}", Thread.currentThread().getName(), customer1);
        }));
        assertTrue(Boolean.TRUE);
    }

    @DisplayName("[CustomerService]: save_CompletableFuture")
    @Disabled
    @Test
    @Order(3)
    void save_CompletableFuture() {

        int max =  50;
        List<Customer> customers = MockUtils.mockCustomer(max);
        //Define completableFuture task
        List<CompletableFuture<Customer>> futureTasks = new ArrayList<>();
        ExecutorService executor = Executors.newFixedThreadPool(max);
        for (Customer customer : customers) {
            CompletableFuture<Customer> future = CompletableFuture.supplyAsync(() -> customerService.save(customer), executor).exceptionally(ex -> {
                logger.info("Error {}", ex.getMessage());
                return new Customer();
            });
            futureTasks.add(future);
        }
        CompletableFuture.allOf(futureTasks.toArray(new CompletableFuture[0])).join();
    }

    @DisplayName("[CustomerService]: save_saveAsync")
    @Disabled
    @Test
    @Order(4)
    void save_saveAsync() {

        List<Customer> customers = MockUtils.mockCustomer(50);

        //Define completableFuture task
        StopWatch stopWatch = new StopWatch();
        stopWatch.start("Execute service");
        ExecutorService executor = Executors.newFixedThreadPool(customers.size());
        for (Customer customer : customers) {
            CompletableFuture.runAsync(() -> customerService.saveAsync(customer), executor);
        }

        //Execute parallel task
        stopWatch.stop();
        logger.info("Process time: {}", stopWatch);
    }
}
