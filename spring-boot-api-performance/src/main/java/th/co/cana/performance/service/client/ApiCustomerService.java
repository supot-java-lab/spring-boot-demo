package th.co.cana.performance.service.client;

import io.github.jdevlibs.spring.client.request.JsonRequest;
import io.github.jdevlibs.utils.Utils;
import lombok.RequiredArgsConstructor;
import okhttp3.*;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import th.co.cana.performance.model.Customer;

import java.io.IOException;

/**
 * @author supot.jdev
 * @version 1.0
 */

@RequiredArgsConstructor
@Service
public class ApiCustomerService extends OkHttpClientService {
    private static final String CONTENT_TYPE_JSON = "application/json; charset=utf-8";
    private static final String URL = "http://localhost:8080/v1/api-performance/customers";

    public Customer createCustomer(final Customer customer) {
        try {
            JsonRequest<Customer> jsonRequest = new JsonRequest<>();
            jsonRequest.addHeader("client-id", Utils.getUUID());
            jsonRequest.setModel(customer);
            return post(URL, jsonRequest, Customer.class);
        } catch (Exception ex) {
            logger.error("Failed to create customer", ex);
            return null;
        }
    }

    public void getCustomerAsync() {
        try {
            Request.Builder builder = jsonRequest();
            Request request = builder.get().build();
            OkHttpClient client = newHttpClient();

            client.newCall(request).enqueue(new Callback() {
                @Override
                public void onResponse(@NotNull Call call, @NotNull Response resp) throws IOException {
                    try (ResponseBody respBody = resp.body()) {
                        if (resp.isSuccessful() && respBody != null) {
                            logger.info("Successfully get customer: {}", respBody.string());
                        }
                    }
                }

                @Override
                public void onFailure(@NotNull Call call, @NotNull IOException ex) {
                    logger.error("Failed to get customer[onFailure]", ex);
                }
            });
        } catch (Exception ex) {
            logger.error("Failed to get Async customer", ex);
        }
    }

    private Request.Builder jsonRequest() {
        Request.Builder builder = new Request.Builder().url(URL);
        builder.addHeader("Content-Type", CONTENT_TYPE_JSON)
                .addHeader("Accept", CONTENT_TYPE_JSON);
        return builder;
    }
}
