package th.co.cana.performance.utils;

import java.util.Locale;

/**
 * @author supot.jdev
 * @version 1.0
 */
public final class AppConstants {

    private AppConstants() {
    }
    public static final String Y = "Y";
    public static final String N = "N";

    public static final String UTF8 = "UTF-8";
    public static final String TIS620 = "TIS-620";

    public static final Locale US   = Locale.US;
    public static final Locale TH   = new Locale("th", "TH");

    public static final class Apis {
        private Apis() {
        }

        public static final String ERROR = "/error";
        public static final String BASE_URL = "/v1/api-performance";
    }
}
