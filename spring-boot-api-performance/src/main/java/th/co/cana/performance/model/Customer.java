package th.co.cana.performance.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author supot.jdev
 * @version 1.0
 */
@EqualsAndHashCode(of = {"id"})
@Data
public class Customer {
    private String id;
    private String firstName;
    private String lastName;
    private String email;
    private BigDecimal creditAmt;
    private LocalDateTime start;
    private LocalDateTime end;
}
