package th.co.cana.performance.utils;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Slf4j
public final class AppUtils {
    private AppUtils() {}

    public static void sleepOnMinute(long minute) {
        try {
            TimeUnit.MINUTES.sleep(minute);
        } catch (InterruptedException ex) {
            log.error("sleepOnMilliseconds", ex);
            Thread.currentThread().interrupt();
        }
    }

    public static void sleepOnSecond(long second) {
        try {
            TimeUnit.SECONDS.sleep(second);
        } catch (InterruptedException ex) {
            log.error("sleepOnMilliseconds", ex);
            Thread.currentThread().interrupt();
        }
    }

    public static void sleepOnMilliseconds(long millisecond) {
        try {
            TimeUnit.MILLISECONDS.sleep(millisecond);
        } catch (InterruptedException ex) {
            log.error("sleepOnMilliseconds", ex);
            Thread.currentThread().interrupt();
        }
    }
}

