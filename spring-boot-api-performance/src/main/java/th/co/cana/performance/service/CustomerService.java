package th.co.cana.performance.service;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import th.co.cana.performance.model.Customer;
import th.co.cana.performance.utils.AppUtils;
import th.co.cana.performance.utils.MockUtils;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Service
public class CustomerService extends AbstractService {

    public Customer save(final Customer customer) {
        try {
            customer.setStart(LocalDateTime.now());
            TimeUnit.SECONDS.sleep(1);

            customer.setEnd(LocalDateTime.now());
            logger.info("Starting save customer: {}", customer.getId());
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
        return customer;
    }

    @Async
    public void saveAsync(final Customer customer) {
        customer.setStart(LocalDateTime.now());
        AppUtils.sleepOnSecond(1);
        customer.setEnd(LocalDateTime.now());
        logger.info("Starting save Async customer: {}", customer.getId());
    }

    public List<Customer> getCustomers() {
        return MockUtils.mockCustomer(10);
    }
}
