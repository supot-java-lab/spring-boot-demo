package th.co.cana.performance.controller;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import th.co.cana.performance.model.Customer;
import th.co.cana.performance.service.CustomerService;
import th.co.cana.performance.utils.AppConstants;

import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */

@RequiredArgsConstructor
@RestController
@RequestMapping(AppConstants.Apis.BASE_URL)
public class CustomerController extends ApiController {

    private final CustomerService customerService;

    @GetMapping("/customers")
    public ResponseEntity<List<Customer>> getCustomers() {
        return ok(customerService.getCustomers());
    }

    @PostMapping("customers")
    public ResponseEntity<Customer> create(@Valid @RequestBody Customer model) {
        return ok(customerService.save(model));
    }
}
