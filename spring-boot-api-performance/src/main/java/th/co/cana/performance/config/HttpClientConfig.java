package th.co.cana.performance.config;

import io.github.jdevlibs.utils.Validators;
import lombok.AllArgsConstructor;
import okhttp3.OkHttpClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import th.co.cana.performance.utils.SSLSocketUtils;

import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * @author supot.jdev
 * @version 1.0
 */

@AllArgsConstructor
@Configuration
public class HttpClientConfig {
    private final Environment environment;

    @Bean
    public OkHttpClient okHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .retryOnConnectionFailure(false)
                .connectionPool(SSLSocketUtils.pool())
                .connectTimeout(10, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .build().newBuilder();
        if (isProfileLocal()) {
            builder.sslSocketFactory(Objects.requireNonNull(SSLSocketUtils.sslSocketFactory()), SSLSocketUtils.x509TrustManager());
            builder.hostnameVerifier(SSLSocketUtils.createHostnameVerifier());
        }
        return builder.build();
    }

    private boolean isProfileLocal() {
        if (Validators.isEmpty(environment.getActiveProfiles())) {
            return false;
        }
        return Arrays.stream(environment.getActiveProfiles()).anyMatch("local"::equalsIgnoreCase);
    }
}
