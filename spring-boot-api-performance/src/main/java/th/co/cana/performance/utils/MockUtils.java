package th.co.cana.performance.utils;

import io.github.jdevlibs.utils.Utils;
import th.co.cana.performance.model.Customer;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */
public final class MockUtils {

    private MockUtils() {}

    public static List<Customer> mockCustomer(int total) {
        List<Customer> customers = new ArrayList<>();
        for (int i = 1; i <= total; i++) {
            Customer customer = new Customer();
            customer.setId(Utils.padLeft(i, 5, "0"));
            customer.setFirstName("First Name " + i);
            customer.setLastName("Last Name " + i);
            customer.setEmail("Email " + i);
            customer.setCreditAmt(BigDecimal.valueOf(i).multiply(BigDecimal.valueOf(10L * i)));
            customers.add(customer);
        }

        return customers;
    }
}
