package th.co.cana.performance.service.client;

import io.github.jdevlibs.spring.client.OkHttpClientAdapter;
import okhttp3.OkHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Component
public class OkHttpClientService extends OkHttpClientAdapter {

    @Override
    @Autowired
    protected void autowiredHttpClient(OkHttpClient httpClient) {
        setHttpClient(httpClient);
    }
}
