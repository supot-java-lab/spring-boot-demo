# Spring boot with JSF (Primefaces)

Sample project for Spring Boot and JSF (Primefaces) using joinfaces

# Site Ref
	1. https://github.com/joinfaces/joinfaces
	2. https://docs.joinfaces.org/master-SNAPSHOT/reference/
	3. https://medium.com/@tsepomaleka/integrating-spring-boot-with-java-server-faces-using-joinfaces-297e64f6a28f