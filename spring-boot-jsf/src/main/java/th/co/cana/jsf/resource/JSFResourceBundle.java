/*
 * ----------------------------------------------------------------------------
 * Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
 * ----------------------------------------------------------------------------
 */
package th.co.cana.jsf.resource;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Supot Saelao
 * @version 1.0
 */
public class JSFResourceBundle extends ClassPathResourceBundle {
	private static final String PACKAGE = "th/co/cana/jsf/resource/message/";
	private static final List<String> BUNDLE_NAMES;
	
	static {
		BUNDLE_NAMES = new ArrayList<>(2);
		BUNDLE_NAMES.add(PACKAGE + "JSFMessages");
		BUNDLE_NAMES.add(PACKAGE + "CommonMessage");
	}
	
	public JSFResourceBundle() {
		super(BUNDLE_NAMES);
	}
}