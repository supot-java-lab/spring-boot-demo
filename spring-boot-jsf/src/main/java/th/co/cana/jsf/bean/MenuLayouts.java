/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.jsf.bean;

/**
* @author Supot Saelao
* @version 1.0
*/
public enum MenuLayouts {
	STATIC("menu-layout-static"),
	OVERLAY("menu-layout-overlay"),
	HORIZONTAL("menu-layout-static menu-layout-horizontal"),
	SLIM("menu-layout-static menu-layout-slim");
	
	private String menuName;

	private MenuLayouts(String menuName) {
		this.menuName = menuName;
	}

	public String getMenuName() {
		return menuName;
	}
	
}