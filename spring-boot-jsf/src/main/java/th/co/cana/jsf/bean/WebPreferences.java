/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.jsf.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

/**
* @author Supot Saelao
* @version 1.0
*/
@Named
@SessionScoped
public class WebPreferences implements Serializable {
	private static final long serialVersionUID = 1L;

	private Map<String, String> themeColors;

    private String theme = "green";

    private String layout = "sunkist";

    private String menuClass = "layout-menu-dark";

    private String profileMode = "inline";

    private String menuLayout = "horizontal";

    private String version = "4";

    private List<ComponentTheme> componentThemes = new ArrayList<>();

    private List<LayoutTheme> layoutThemes = new ArrayList<>();

    private List<LayoutSpecialTheme> layoutSpecialThemes = new ArrayList<>();

    @PostConstruct
    public void init() {
        themeColors = new HashMap<>();
        themeColors.put("blue", "#03A9F4");

        componentThemes.add(new ComponentTheme("Amber", "amber", "amber.png"));
        componentThemes.add(new ComponentTheme("Blue", "blue", "blue.svg"));
        componentThemes.add(new ComponentTheme("Cyan", "cyan", "cyan.svg"));
        componentThemes.add(new ComponentTheme("Indigo", "indigo", "indigo.svg"));
        componentThemes.add(new ComponentTheme("Purple", "purple", "purple.svg"));
        componentThemes.add(new ComponentTheme("Teal", "teal", "teal.svg"));
        componentThemes.add(new ComponentTheme("Orange", "orange", "orange.svg"));
        componentThemes.add(new ComponentTheme("Deep Purple", "deeppurple", "deeppurple.svg"));
        componentThemes.add(new ComponentTheme("Light Blue", "lightblue", "lightblue.svg"));
        componentThemes.add(new ComponentTheme("Green", "green", "green.png"));
        componentThemes.add(new ComponentTheme("Light Green", "lightgreen", "lightgreen.png"));
        componentThemes.add(new ComponentTheme("Brown", "brown", "brown.png"));
        componentThemes.add(new ComponentTheme("Dark Grey", "darkgrey", "darkgrey.svg"));
        componentThemes.add(new ComponentTheme("Pink", "pink", "pink.svg"));
        componentThemes.add(new ComponentTheme("Lime", "lime", "lime.svg"));

        layoutThemes.add(new LayoutTheme("Blue", "blue", "blue.png"));
        layoutThemes.add(new LayoutTheme("Cyan", "cyan", "cyan.png"));
        layoutThemes.add(new LayoutTheme("Indigo", "indigo", "indigo.png"));
        layoutThemes.add(new LayoutTheme("Purple", "purple", "purple.png"));
        layoutThemes.add(new LayoutTheme("Teal", "teal", "teal.png"));
        layoutThemes.add(new LayoutTheme("Pink", "pink", "pink.png"));
        layoutThemes.add(new LayoutTheme("Lime", "lime", "lime.png"));
        layoutThemes.add(new LayoutTheme("Green", "green", "green.png"));
        layoutThemes.add(new LayoutTheme("Amber", "amber", "amber.png"));
        layoutThemes.add(new LayoutTheme("Brown", "brown", "brown.png"));
        layoutThemes.add(new LayoutTheme("Orange", "orange", "orange.png"));
        layoutThemes.add(new LayoutTheme("Deep Purple", "deeppurple", "deeppurple.png"));
        layoutThemes.add(new LayoutTheme("Light Blue", "lightblue", "lightblue.png"));
        layoutThemes.add(new LayoutTheme("Light Green", "lightgreen", "lightgreen.png"));
        layoutThemes.add(new LayoutTheme("Daerk Grey", "darkgrey", "darkgrey.png"));

        layoutSpecialThemes.add(new LayoutSpecialTheme("Influenza", "influenza", "influenza.png"));
        layoutSpecialThemes.add(new LayoutSpecialTheme("Calm", "calm", "calm.png"));
        layoutSpecialThemes.add(new LayoutSpecialTheme("Crimson", "crimson", "crimson.png"));
        layoutSpecialThemes.add(new LayoutSpecialTheme("Night", "night", "night.png"));
        layoutSpecialThemes.add(new LayoutSpecialTheme("Skyline", "skyline", "skyline.png"));
        layoutSpecialThemes.add(new LayoutSpecialTheme("Sunkist", "sunkist", "sunkist.png"));
        layoutSpecialThemes.add(new LayoutSpecialTheme("Little Leaf", "littleleaf", "littleleaf.png"));
        layoutSpecialThemes.add(new LayoutSpecialTheme("Joomla", "joomla", "joomla.png"));
        layoutSpecialThemes.add(new LayoutSpecialTheme("Firewatch", "firewatch", "firewatch.png"));
        layoutSpecialThemes.add(new LayoutSpecialTheme("Suzy", "suzy", "suzy.png"));

        setVersion(version);
    }

    public String getMenuClass() {
        return this.menuClass;
    }

    public String getProfileMode() {
        return this.profileMode;
    }

    public String getTheme() {
        return theme + this.version;
    }

    public String getLayout() {
        return layout + this.version;
    }

    public String getMenuLayout() {
        if (this.menuLayout.equals("static")) {
            return "menu-layout-static";
        } else if (this.menuLayout.equals("overlay")) {
            return "menu-layout-overlay";
        } else if (this.menuLayout.equals("horizontal")) {
            this.profileMode = "overlay";
            return "menu-layout-static menu-layout-horizontal";
        } else if (this.menuLayout.equals("slim")) {
            return "menu-layout-static menu-layout-slim";
        } else {
            return "menu-layout-static";
        }
    }

    public boolean isEqual(String val1, String val2) {
        if ("-v4".equals(this.version)) {
            return val2.equals(val1 + "-v4");
        } else {
            return val2.equals(val1);
        }
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    public void setLayout(String layout, boolean special) {
        this.layout = layout;
        if (special) {
            this.setDarkMenu();
        }
    }

    public void setLightMenu() {
        this.menuClass = null;
    }

    public void setDarkMenu() {
        this.menuClass = "layout-menu-dark";
    }

    public void setProfileMode(String profileMode) {
        this.profileMode = profileMode;
    }

    public void setMenuLayout(String menuLayout) {
        if (menuLayout.equals("horizontal")) {
            this.profileMode = "overlay";
        }
        
        this.menuLayout = menuLayout;
    }

    public Map<String, String> getThemeColors() {
        return this.themeColors;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        if (version.equals("3")) {
            this.version = "";
        } else if (version.equals("4")) {
            this.version = "-v4";
        }
    }

    public List<LayoutTheme> getLayoutThemes() {
        return layoutThemes;
    }

    public List<LayoutSpecialTheme> getLayoutSpecialThemes() {
        return layoutSpecialThemes;
    }

    public List<ComponentTheme> getComponentThemes() {
        return componentThemes;
    }

    public class ComponentTheme {

        String name;
        String file;
        String image;

        public ComponentTheme(String name, String file, String image) {
            this.name = name;
            this.file = file;
            this.image = image;
        }

        public String getName() {
            return this.name;
        }

        public String getFile() {
            return this.file;
        }

        public String getImage() {
            return this.image;
        }
    }

    public class LayoutTheme {

        String name;
        String file;
        boolean special = false;
        String image;

        public LayoutTheme(String name, String file, String image) {
            this.name = name;
            this.file = file;
            this.image = image;
        }

        public String getName() {
            return this.name;
        }

        public String getFile() {
            return this.file;
        }

        public String getImage() {
            return this.image;
        }

        public boolean isSpecial() {
            return this.special;
        }
    }

    public class LayoutSpecialTheme {

        String name;
        String file;
        boolean special = true;
        String image;

        public LayoutSpecialTheme(String name, String file, String image) {
            this.name = name;
            this.file = file;
            this.image = image;
        }

        public String getName() {
            return this.name;
        }

        public String getFile() {
            return this.file;
        }

        public String getImage() {
            return this.image;
        }

        public boolean isSpecial() {
            return this.special;
        }
    }

}
