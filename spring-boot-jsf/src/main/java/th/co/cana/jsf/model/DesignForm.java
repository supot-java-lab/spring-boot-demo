/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.jsf.model;

import java.io.Serializable;

import lombok.Data;
import lombok.ToString;

/**
* @author supot
* @version 1.0
*/

@Data
@ToString
public class DesignForm implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer template;
	private String rule;
	private String fileName;
	private String jsonData;
	private String[] selectedRules;
	private String dataSource;
}
