/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.jsf.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @author supot
 * @version 1.0
 */
public class BathProcessItem implements Serializable {
	private static final long serialVersionUID = 1L;

	private boolean selected;
	private String id;
	private String lineNo;
	private String status;
	private Date processStart;
	private Date processEnd;
	private String data;
	private String remark;
	private String error;

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getLineNo() {
		return lineNo;
	}

	public void setLineNo(String lineNo) {
		this.lineNo = lineNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getProcessStart() {
		return processStart;
	}

	public void setProcessStart(Date processStart) {
		this.processStart = processStart;
	}

	public Date getProcessEnd() {
		return processEnd;
	}

	public void setProcessEnd(Date processEnd) {
		this.processEnd = processEnd;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}
	
	public boolean isRenderedError() {
		return "1001".equals(id);
	}

}
 