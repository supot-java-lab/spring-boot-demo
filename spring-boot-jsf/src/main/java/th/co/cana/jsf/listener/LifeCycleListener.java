/*
 * ----------------------------------------------------------------------------
 * Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
 * ----------------------------------------------------------------------------
 */
package th.co.cana.jsf.listener;

import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StopWatch;

import th.co.cana.utils.Systems;

/**
 * @author Supot Saelao
 * @version 1.0
 */
public class LifeCycleListener implements PhaseListener {
	private static final long serialVersionUID = 1L;

	private static Logger logger = LoggerFactory.getLogger(LifeCycleListener.class);
	private transient StopWatch stopWatch = new StopWatch();

	@Override
	public void beforePhase(PhaseEvent event) {
		try {
			if (event.getPhaseId() == PhaseId.RESTORE_VIEW) {
				if (stopWatch.isRunning()) {
					stopWatch.stop();
				}
				stopWatch.start();
			}
		} catch (Exception ex) {
			logger.error("beforePhase", ex);
		}
	}

	@Override
	public void afterPhase(PhaseEvent event) {
		logger.debug("Executed Phase {}", event.getPhaseId());
		try {
			if (event.getPhaseId() == PhaseId.RENDER_RESPONSE) {
				if (stopWatch.isRunning()) {
					stopWatch.stop();
				}

				String viewId = event.getFacesContext().getViewRoot().getViewId();

				Object[] params = new Object[] { viewId, stopWatch.toString(), Systems.getUsedMemory() };
				logger.debug("View ID : {} Execution Time : {}. Memory used : {} MB", params);
			}
		} catch (Exception ex) {
			logger.error("afterPhase", ex);
		}
	}

	@Override
	public PhaseId getPhaseId() {
		return PhaseId.ANY_PHASE;
	}
}
