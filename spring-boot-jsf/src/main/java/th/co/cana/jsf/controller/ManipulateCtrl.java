/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.jsf.controller;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.file.UploadedFile;

import th.co.cana.utils.Validators;

/**
* @author supot
* @version 1.0
*/

@Named
@SessionScoped
public class ManipulateCtrl extends Controller {
	private static final long serialVersionUID = 1L;

	private List<UploadedFile> fileItems;
	
	@PostConstruct
	public void initial() {
		logger.info("Startin initial : {}", getClass());
		fileItems = null;
	}
	
	public void handleUpload(FileUploadEvent event) {
		try {
			if (Validators.isNotNull(event.getFile())) {
				logger.info("File name : {}", event.getFile().getFileName());
				fileItems.add(event.getFile());
			}
		} catch (Exception ex) {
			logger.error("handleFileUpload", ex);
		}
	}
	
	public void processAction() {
		logger.info("Process raw data");
	}
	
	public void cancelAction() {
		fileItems = null;
	}
}
