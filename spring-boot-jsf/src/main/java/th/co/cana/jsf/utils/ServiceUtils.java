/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana Enterprise Co.,Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.jsf.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import th.co.cana.jsf.model.BathProcess;
import th.co.cana.jsf.model.BathProcessItem;
import th.co.cana.utils.CsvUtils;
import th.co.cana.utils.IOUtils;
import th.co.cana.utils.Utils;
import th.co.cana.utils.fotmat.FormatDate;

/**
* @author Supot Saelao
* @version 1.0
*/
public final class ServiceUtils {
	private static final String PATH = "src/main/resources/";

	private static Logger logger = LoggerFactory.getLogger(ServiceUtils.class);
	
	private ServiceUtils() {}
	
	public static List<BathProcess> createBathProcesss() {
		try {
			List<String> datas = readFile("bath-process.txt");
			List<BathProcess> items = new ArrayList<>(datas.size());
			FormatDate fmt = new FormatDate("yyyy-MM-dd HH:mm:ss.SSS");
			for (String line : datas) {
				String[] lines = line.split("[|]");
				BathProcess bean = new BathProcess();
				bean.setId(Utils.getUUID());
				bean.setStatus(CsvUtils.getValue(lines, 0));
				bean.setProcessStart(fmt.parse(CsvUtils.getValue(lines, 2)));
				bean.setProcessEnd(fmt.parse(CsvUtils.getValue(lines, 3)));
				bean.setTotal(CsvUtils.getInteger(lines, 4));
				bean.setPercent(CsvUtils.getBigDecimal(lines, 5));
				bean.setRemark(CsvUtils.getValue(lines, 6));
				bean.setCycle(CsvUtils.getValue(lines, 7));
				items.add(bean);
			}
			
			return items;
		} catch (IOException ex) {
			logger.error("createOpenProjects", ex);
		}
		
		return Collections.emptyList();
	}
	
	public static List<BathProcessItem> createProcesssDetail() {
		try {
			List<String> datas = readFile("process-detail.txt");
			List<BathProcessItem> items = new ArrayList<>(datas.size());
			FormatDate fmt = new FormatDate("yyyy-MM-dd HH:mm:ss.SSS");
			for (String line : datas) {
				String[] lines = line.split("[|]");
				BathProcessItem bean = new BathProcessItem();
				bean.setId(CsvUtils.getValue(lines, 0));
				bean.setProcessStart(fmt.parse(CsvUtils.getValue(lines, 1)));
				bean.setProcessEnd(fmt.parse(CsvUtils.getValue(lines, 2)));
				bean.setRemark(CsvUtils.getValue(lines, 3));
				
				items.add(bean);
			}
			
			return items;
		} catch (IOException ex) {
			logger.error("createProcesssDetail", ex);
		}
		
		return Collections.emptyList();
	}
	private static List<String> readFile(String file) throws IOException {
		return IOUtils.readLines(new FileInputStream(PATH + file));
	}
	
}
