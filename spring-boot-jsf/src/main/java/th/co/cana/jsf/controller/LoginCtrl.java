/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.jsf.controller;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import th.co.cana.jsf.utils.FacesUtils;
import th.co.cana.jsf.utils.PageNames;

/**
* @author supot
* @version 1.0
*/

@Named
@SessionScoped
public class LoginCtrl extends Controller {
	private static final long serialVersionUID = 1L;
	private static final String OPERATION = "operation";
	private static final String MANIPULATE = "manipulate";
	
	@Inject
	private IndexCtrl indexCtrl;
	@Inject
	private ManipulateCtrl manipulateCtrl;
	
	private String userName;
	private String password;
	
	@PostConstruct
	public void initial() {
		logger.info("Startin initial : {}", getClass());
	}
	
	public String loginAction() {
		try {
			if (isManipulate()) {
				manipulateCtrl.initial();
				return FacesUtils.redirectToOutcome(PageNames.PAGE_MANIPULATE);
			}
			
			indexCtrl.initailData(false);
			return FacesUtils.redirectToOutcome(PageNames.PAGE_DASHBOARD);
		} catch (Exception ex) {
			logger.error("loginAction", ex);
		}

		return null;
	}

	public String logout() {
		FacesUtils.invalidateSession();
		return FacesUtils.redirectToOutcome(PageNames.PAGE_LOGIN);
	}
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getFullName() {
		if (isOpersation()) {
			return "Supatcha Srisome";
		} else if (isManipulate()) {
			return "Raweewon Sukjai";
		}
		
		return "Mongkol Rakjai";
	}
	
	public String getPosition() {
		if (isOpersation()) {
			return "Operation";
		} else if (isManipulate()) {
			return "Manipulate User";
		}
		
		return "Administrator";
	}
	
	public String getAvatar() {
		if (isOpersation()) {
			return "images/avatar.png";
		} else if (isManipulate()) {
			return "images/avatar2.png";
		}
		
		return "images/avatar1.png";
	}
	
	public boolean isOpersation() {
		return (OPERATION.equalsIgnoreCase(userName));
	}
	
	public boolean isManipulate() {
		return (MANIPULATE.equalsIgnoreCase(userName));
	}
	
	public boolean isAdmin() {
		return !isOpersation() && !isManipulate();
	}
}
