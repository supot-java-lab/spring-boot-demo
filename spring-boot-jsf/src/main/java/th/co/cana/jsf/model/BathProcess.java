/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.jsf.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author supot
 * @version 1.0
 */

public class BathProcess implements Serializable {
	private static final long serialVersionUID = 1L;

	private String id;
	private String status;
	private Date processStart;
	private Date processEnd;
	private Integer total;
	private BigDecimal percent;
	private String remark;
	private String cycle;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getProcessStart() {
		return processStart;
	}

	public void setProcessStart(Date processStart) {
		this.processStart = processStart;
	}

	public Date getProcessEnd() {
		return processEnd;
	}

	public void setProcessEnd(Date processEnd) {
		this.processEnd = processEnd;
	}

	public Integer getTotal() {
		return total;
	}

	public void setTotal(Integer total) {
		this.total = total;
	}

	public BigDecimal getPercent() {
		return percent;
	}

	public void setPercent(BigDecimal percent) {
		this.percent = percent;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getCycle() {
		return cycle;
	}

	public void setCycle(String cycle) {
		this.cycle = cycle;
	}

	public String getStatusName() {
		if (isError()) {
			return "Process Fail";
		} else if (isSuccess()) {
			return "Success";
		}
		return "Processing";
	}
	
	public String getCssStatus() {
		if ("02".equals(status)) {
			return "fa fa-times-circle-o fa-2x fa-red";
		} else if ("01".equals(status)) {
			return "fa fa-spinner fa-pulse fa-2x fa-blue";
		}
		return "fa fa-check-circle-o fa-green fa-2x";
	}
	
	public String getCssFont() {
		if ("02".equals(status)) {
			return "font-red";
		} else if ("01".equals(status)) {
			return "font-blue";
		}
		return "";
	}
	
	public boolean isSuccess() {
		return "00".equals(status);
	}
	
	public boolean isError() {
		return "02".equals(status);
	}
}
