/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.jsf.file;

import java.io.InputStream;

import org.primefaces.util.SerializableSupplier;

/**
* @author supot
* @version 1.0
*/
public class PdfFile implements SerializableSupplier<InputStream> {
	private static final long serialVersionUID = 1L;
	
	private InputStream ins;
	public PdfFile(InputStream ins) {
		this.ins = ins;
	}
	
	@Override
	public InputStream get() {
		return ins;
	}

}
