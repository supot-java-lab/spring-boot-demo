/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.jsf.controller;

import java.io.InputStream;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import th.co.cana.jsf.model.BathProcess;
import th.co.cana.jsf.model.BathProcessItem;
import th.co.cana.jsf.utils.Constants;
import th.co.cana.jsf.utils.FacesUtils;
import th.co.cana.jsf.utils.PageNames;
import th.co.cana.jsf.utils.ServiceUtils;
import th.co.cana.utils.IOUtils;
import th.co.cana.utils.Validators;

/**
* @author supot
* @version 1.0
*/

@Named
@SessionScoped
public class IndexCtrl extends Controller {
	private static final long serialVersionUID = 1L;

	private List<BathProcess> batchItems;
	private BathProcess form;
	private List<BathProcessItem> items;
	private boolean selectAll;
	private String jsonData;
	private String error;
	
	@PostConstruct
	public void initial() {
		logger.info("Startin initial : {}", getClass());
	}
	
	public void initailData(boolean admin) {
		items = null;
		error = null;
		jsonData = null;
		batchItems = ServiceUtils.createBathProcesss();
	}
	
	public String viewDeatil(BathProcess bean) {
		this.form = bean;
		if (bean.isError()) {
			items = ServiceUtils.createProcesssDetail();
		} else {
			error = null;
			jsonData = null;
			items = null;
		}
		return FacesUtils.redirectToOutcome(PageNames.PAGE_PROCESS_DETAIL);
	}
	
	public String backAction() {
		items = null;
		return FacesUtils.redirectToOutcome(PageNames.PAGE_DASHBOARD);
	}
	
	public void selectAction() {
		if (Validators.isNotEmpty(items)) {
			for (BathProcessItem bean : items) {
				bean.setSelected(selectAll);
			}
		}
	}
	
	public void viewJsonAction() {
		try {
			if (Validators.isNotEmpty(jsonData)) {
				return;
			}

			InputStream ins = FacesUtils.getResourceAsStream("/static/source-error.json");
			jsonData = IOUtils.toString(ins, Constants.UTF8);
		} catch (Exception ex) {
			logger.error("viewJsonAction", ex);
		}
	}
	
	public void viewErrorAction() {
		try {
			if (Validators.isNotEmpty(error)) {
				return;
			}

			InputStream ins = FacesUtils.getResourceAsStream("/static/error.txt");
			error = IOUtils.toString(ins, Constants.UTF8);
		} catch (Exception ex) {
			logger.error("viewJsonAction", ex);
		}
	}
	
	public List<BathProcess> getBatchItems() {
		return batchItems;
	}

	public List<BathProcessItem> getItems() {
		return items;
	}

	public void setItems(List<BathProcessItem> items) {
		this.items = items;
	}

	public String getJsonData() {
		return jsonData;
	}

	public String getError() {
		return error;
	}

	public BathProcess getForm() {
		return form;
	}

	public void setForm(BathProcess form) {
		this.form = form;
	}

	public boolean isSelectAll() {
		return selectAll;
	}

	public void setSelectAll(boolean selectAll) {
		this.selectAll = selectAll;
	}
	
}
