/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.jsf.servlet;

import java.util.Map;

import javax.servlet.ServletContext;

import org.ocpsoft.rewrite.annotation.RewriteConfiguration;
import org.ocpsoft.rewrite.config.Configuration;
import org.ocpsoft.rewrite.config.ConfigurationBuilder;
import org.ocpsoft.rewrite.servlet.config.HttpConfigurationProvider;
import org.ocpsoft.rewrite.servlet.config.rule.Join;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import th.co.cana.jsf.utils.PageNames;

/**
 * @author Supot Saelao
 * @version 1.0
 */
@RewriteConfiguration
public class RewriteConfigProvider extends HttpConfigurationProvider {
	private static Logger logger = LoggerFactory.getLogger(RewriteConfigProvider.class);

	@Override
	public Configuration getConfiguration(ServletContext context) {
		logger.debug("*** Starting mapping .xhtml path to rewrite url ***");

		ConfigurationBuilder builder = ConfigurationBuilder.begin();
		Map<String, String> mappings = PageNames.getMenuMapings();
		for (Map.Entry<String, String> map : mappings.entrySet()) {
			logger.debug("Mapping path : {} => : {}", map.getValue(), map.getKey());
			builder.addRule(Join.path(map.getKey()).to(map.getValue()));
		}

		logger.debug("*** Finished mapping .xhtml path to rewrite url ***");
		
		return builder;
	}

	@Override
	public int priority() {
		return 10;
	}
}