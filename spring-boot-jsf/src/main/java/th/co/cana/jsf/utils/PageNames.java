/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.jsf.utils;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Supot Saelao
 * @version 1.0
 */
public final class PageNames {
	private PageNames() {
	}

	public static final String LOGIN_URL 	= "/login";
	public static final String PAGE_DESIGN = "/design.xhtml";
	public static final String PAGE_LOGIN 	= "/login.xhtml";
	public static final String PAGE_DASHBOARD 	= "/dashboard.xhtml";
	public static final String PAGE_MANIPULATE 	= "/manipulate.xhtml";
	public static final String PAGE_PROCESS_DETAIL 	= "/process-detail.xhtml";
	
	private static final Map<String, String> MENU_CONFS;
	static {
		MENU_CONFS = new LinkedHashMap<>();
		MENU_CONFS.put("/", PAGE_LOGIN);
		MENU_CONFS.put("/design", PAGE_DESIGN);
		MENU_CONFS.put("/dashboard", PAGE_DASHBOARD);
		MENU_CONFS.put("/manipulate", PAGE_DASHBOARD);
		MENU_CONFS.put("/process-detail", PAGE_PROCESS_DETAIL);
	}

	public static Map<String, String> getMenuMapings() {
		return Collections.unmodifiableMap(MENU_CONFS);
	}

	public static String getMenuPath(String url) {
		return MENU_CONFS.get(url);
	}
}