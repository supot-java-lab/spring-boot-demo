/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.jsf.utils;

import th.co.cana.utils.CsvUtils;

/**
* @author supot
* @version 1.0
*/
public final class Constants {
	
	private Constants() {}
	
	public static final String UTF8 = "UTF-8";
	
	private static final String[] URS =  {
		"https://onedrive.live.com/embed?cid=6CCBD4E7F55A6832&resid=6CCBD4E7F55A6832%214914&authkey=AAfAQwkCKKugb6s&em=2"
		, "https://onedrive.live.com/embed?cid=6CCBD4E7F55A6832&resid=6CCBD4E7F55A6832%214912&authkey=AJeNMBLyjK8fScE&em=2"
	};
	
	public static String getWord(int inx) {
		return CsvUtils.getValue(URS, inx);
	}
}
