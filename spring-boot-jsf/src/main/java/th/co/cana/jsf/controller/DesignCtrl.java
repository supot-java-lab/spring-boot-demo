/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.jsf.controller;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.model.SelectItem;
import javax.faces.model.SelectItemGroup;
import javax.inject.Named;

import org.primefaces.component.tabview.Tab;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.TreeNode;

import th.co.cana.jsf.file.PdfFile;
import th.co.cana.jsf.model.DesignForm;
import th.co.cana.jsf.utils.Constants;
import th.co.cana.jsf.utils.FacesUtils;
import th.co.cana.utils.IOUtils;
import th.co.cana.utils.MimeTypes;
import th.co.cana.utils.Validators;

/**
* @author supot
* @version 1.0
*/

@Named
@SessionScoped
public class DesignCtrl extends Controller {
	private static final long serialVersionUID = 1L;

	private StreamedContent media;
	private TreeNode root;
	private int tabIndex;
	private DesignForm form;
	private List<SelectItem> ruleItems;
	private List<SelectItem> dataSources;
	
	@PostConstruct
	public void initial() {
		logger.info("Startin initial : {}", getClass());
		this.initialData();
	}
	
	private void initialData() {
		getForm().setTemplate(0);
		getForm().setJsonData("");	
		this.initialRulebase();
	}
	
	private void initialPdf() {
		try {
			String file = "/static/corp_truemove.pdf";
			if (1 == form.getTemplate().intValue()) {
				file = "/static/indy_truemove.pdf";
			}
			
			logger.info("Load pdf : {}", file);
			
			InputStream ins = FacesUtils.getResourceAsStream(file);
			PdfFile pdf = new PdfFile(ins);
			media = DefaultStreamedContent.builder()
					.contentType(MimeTypes.getMimeType("xx.pdf"))
					.stream(pdf).build();
		} catch (Exception ex) {
			logger.error("initialData", ex);
		}
	}
	
	private void initialRulebase() {
		getForm().setSelectedRules(null);
		ruleItems = new ArrayList<>();
		SelectItemGroup group = new SelectItemGroup("Address Rule");
		group.setSelectItems(new SelectItem[] { new SelectItem("Address 1 Line", "Address 1 Line"),
				new SelectItem("Address 4 Line", "Address 4 Line"),
				new SelectItem("Name Address", "Name Address") });
		ruleItems.add(group);

		group = new SelectItemGroup("Previous Due Date");
		group.setSelectItems(new SelectItem[] { new SelectItem("Outstanding > 0", "Outstanding > 0"),
				new SelectItem("Outstanding <= 0", "Outstanding <= 0") });
		ruleItems.add(group);
		
		if (getForm().getTemplate() == 0) {
			group = new SelectItemGroup("Usage Rule");
			group.setSelectItems(new SelectItem[] { new SelectItem("EP", "EP"), new SelectItem("INT", "INT"),
					new SelectItem("INR", "INR") });
			ruleItems.add(group);
		}
		
		this.initialDatasource();
	}
	
	private void initialDatasource() {
		getForm().setDataSource(null);
		dataSources = new ArrayList<>();
		if (getForm().getTemplate() == 0) {
			dataSources.add(new SelectItem("01", "Corporate Truemove-H"));
			dataSources.add(new SelectItem("02", "Corporate TOL"));
		} else {
			dataSources.add(new SelectItem("01", "Individual Truemove-H"));
			dataSources.add(new SelectItem("02", "Individual TOL"));
			dataSources.add(new SelectItem("03", "Individual TVS"));
			dataSources.add(new SelectItem("04", "Individual Mobile"));
		}
		
		this.viewJsonAction();
		this.createTreeNode();
	}
	
	public void handleUpload(FileUploadEvent event) {
		try {
			if (Validators.isNotNull(event.getFile())) {
				logger.info("File name : {}", event.getFile().getFileName());
				String jsonData = IOUtils.toString(event.getFile().getInputStream(), Constants.UTF8);
				getForm().setJsonData(jsonData);
				getForm().setFileName(event.getFile().getFileName());
			}
			this.createTreeNode();
		} catch (Exception ex) {
			logger.error("handleFileUpload", ex);
		}
	}
	
	private void createTreeNode() {
		root = new DefaultTreeNode("Root", null);
		root.getChildren().add(new DefaultTreeNode("accountNo"));
		root.getChildren().add(new DefaultTreeNode("cycle"));
		root.getChildren().add(new DefaultTreeNode("customerType"));
		
		TreeNode report = new DefaultTreeNode("reportInfo", root);
		report.getChildren().add(new DefaultTreeNode("companyName"));
		report.getChildren().add(new DefaultTreeNode("companyAddress"));
		report.getChildren().add(new DefaultTreeNode("companyTaxId"));
		report.getChildren().add(new DefaultTreeNode("logo"));
		report.getChildren().add(new DefaultTreeNode("qrCode1"));
		report.getChildren().add(new DefaultTreeNode("qrCode2"));
		
		TreeNode cust = new DefaultTreeNode("customerInfo", root);
		cust.getChildren().add(new DefaultTreeNode("barcode"));
		cust.getChildren().add(new DefaultTreeNode("customerName"));
		cust.getChildren().add(new DefaultTreeNode("customerAddrLine1"));
		cust.getChildren().add(new DefaultTreeNode("customerAddrLine2"));
		cust.getChildren().add(new DefaultTreeNode("customerAddrLine3"));
		cust.getChildren().add(new DefaultTreeNode("customerAddrLine4"));
		cust.getChildren().add(new DefaultTreeNode("welcomeMsg"));
		cust.getChildren().add(new DefaultTreeNode("dateAsOf"));
		cust.getChildren().add(new DefaultTreeNode("invoiceNumber"));
		cust.getChildren().add(new DefaultTreeNode("primarySubscriber"));
		cust.getChildren().add(new DefaultTreeNode("subscriberMsg"));
		
		TreeNode charge = new DefaultTreeNode("chargeItems[]", root);
		charge.getChildren().add(new DefaultTreeNode("item"));
		charge.getChildren().add(new DefaultTreeNode("amount"));
		charge.getChildren().add(new DefaultTreeNode("rowType"));
		
		TreeNode item = new DefaultTreeNode("detailItems[]", root);
		item.getChildren().add(new DefaultTreeNode("item"));
		item.getChildren().add(new DefaultTreeNode("amount"));
		item.getChildren().add(new DefaultTreeNode("rowType"));
		item.getChildren().add(new DefaultTreeNode("padding"));
	}
	
	public void previewAction() {
		try {
			logger.info("Starting previewAction");
			this.initialPdf();
			tabIndex = 1;
		} catch (Exception ex) {
			logger.error("Exception", ex);
		}
	}
	
	public void viewJsonAction() {
		try {
			InputStream ins = FacesUtils.getResourceAsStream("/static/source.json");
			getForm().setJsonData(IOUtils.toString(ins, Constants.UTF8));
		} catch (Exception ex) {
			logger.error("viewJsonAction", ex);
		}
	}
	
	public void viewJsonTransformAction() {
		try {
			String file = "/static/transform.json";
			if (Validators.isEmpty(form.getSelectedRules())) {
				file = "/static/source.json";
			}
			
			InputStream ins = FacesUtils.getResourceAsStream(file);
			getForm().setJsonData(IOUtils.toString(ins, Constants.UTF8));
		} catch (Exception ex) {
			logger.error("viewJsonAction", ex);
		}
	}
	
	public void changeTemplateAction() {
		tabIndex = 0;
		initialRulebase();
	}
	
	public void changeDataSource() {
		tabIndex = 0;
		this.initialDatasource();
	}
	
	public void onTabChange(TabChangeEvent<Object> event) {
		try {
			Tab tab = event.getTab();
			if (Validators.isNull(tab)) {
				return;
			}

			logger.info("Select tab : {} -> {}", tab.getId(), tab.getTitle());
			if ("pdf".equalsIgnoreCase(tab.getId())) {
				logger.info("Click pdf tab : {}",  tabIndex);
				this.initialPdf();
			}
		} catch (Exception ex) {
			logger.error("", ex);
		}
	}
	 
    public StreamedContent getPdfMedia() { 
        return media;
    }

	public TreeNode getRoot() {
		return root;
	}

	public int getTabIndex() {
		return tabIndex;
	}

	public void setTabIndex(int tabIndex) {
		this.tabIndex = tabIndex;
	}

	public String getWordUrl() {
		if (Validators.isNull(form.getTemplate())) {
			return Constants.getWord(0);
		}
		return Constants.getWord(form.getTemplate().intValue());
	}

	public DesignForm getForm() {
		if (form == null) {
			form = new DesignForm();
		}
		return form;
	}

	public void setForm(DesignForm form) {
		this.form = form;
	}

	public List<SelectItem> getRuleItems() {
		return ruleItems;
	}

	public void setRuleItems(List<SelectItem> ruleItems) {
		this.ruleItems = ruleItems;
	}

	public List<SelectItem> getDataSources() {
		return dataSources;
	}

	public void setDataSources(List<SelectItem> dataSources) {
		this.dataSources = dataSources;
	}
	
}
