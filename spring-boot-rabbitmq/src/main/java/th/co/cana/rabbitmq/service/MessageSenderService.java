/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.rabbitmq.service;

import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import th.co.cana.rabbitmq.model.Customer;
import th.co.cana.rabbitmq.utils.Constants;

/**
 * @author supot
 * @version 1.0
 */

@Slf4j
@Component
public class MessageSenderService {
	@Autowired
	private TopicExchange topic;

	@Autowired
	private RabbitTemplate rabbitTemplate;

	public void send(Customer customer) {
		log.info("Sender : {}", customer);
		rabbitTemplate.convertAndSend(topic.getName(), Constants.CUSTOMER_ROUTING_KEY, customer);
	}
}
