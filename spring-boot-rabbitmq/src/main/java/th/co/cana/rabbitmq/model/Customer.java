/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.rabbitmq.model;

import java.io.Serializable;

import lombok.Data;
import lombok.ToString;

/**
 * @author supot
 * @version 1.0
 */

@Data
@ToString
public class Customer implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String id;
	private String firstName;
	private String lastName;
	private String email;
}
