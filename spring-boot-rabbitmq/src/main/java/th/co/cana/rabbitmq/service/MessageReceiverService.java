/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.rabbitmq.service;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;
import th.co.cana.rabbitmq.model.Customer;
import th.co.cana.rabbitmq.utils.Constants;

/**
* @author supot
* @version 1.0
*/

@Slf4j
@Component
public class MessageReceiverService {

	@RabbitListener(queues = Constants.CUSTOMER_QUEUE)
    public void receive(Customer customer) {
		log.info("Receiver : {}", customer);
    }
}
