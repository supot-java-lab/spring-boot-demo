/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.rabbitmq.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import th.co.cana.rabbitmq.model.Customer;
import th.co.cana.rabbitmq.service.MessageSenderService;

/**
* @author supot
* @version 1.0
*/

@RestController
@RequestMapping("/rabbitmq/customers")
public class CustomerCtrl extends Controller {

	@Autowired
	private MessageSenderService senderService;

	@PostMapping
	public ResponseEntity<Customer> sendReportInfo(@RequestBody Customer data) {
		try {
			senderService.send(data);
			return ResponseEntity.ok(data);
		} catch (Exception ex) {
			logger.error("sendReportInfo", ex);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}
}
