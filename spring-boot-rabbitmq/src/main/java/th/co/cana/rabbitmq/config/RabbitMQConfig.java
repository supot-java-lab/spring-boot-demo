/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.rabbitmq.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import th.co.cana.rabbitmq.utils.Constants;

/**
 * @author supot
 * @version 1.0
 */

@Configuration
public class RabbitMQConfig {
	
	//Producing 
	@Bean
	public TopicExchange topic() {
		return new TopicExchange(Constants.CUSTOMER_TOPIC);
	}
	
	//Consuming 
    @Bean
    public Queue queue() {
    	return new Queue(Constants.CUSTOMER_QUEUE, false);
    }

    @Bean
    public Binding binding(TopicExchange topic, Queue queue) {
        return BindingBuilder.bind(queue)
        		.to(topic)
        		.with(Constants.CUSTOMER_ROUTING_KEY);
    }
}
