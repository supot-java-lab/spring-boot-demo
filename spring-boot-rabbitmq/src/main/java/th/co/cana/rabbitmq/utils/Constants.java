/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.rabbitmq.utils;

/**
* @author supot
* @version 1.0
*/
public final class Constants {

	private Constants() {}
	
	public static final String CUSTOMER_TOPIC 			= "customer.topic";
	public static final String CUSTOMER_QUEUE 			= "customer-queue";
	public static final String CUSTOMER_ROUTING_KEY 	= "key.customer";
}
