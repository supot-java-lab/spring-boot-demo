package th.co.cana.kibana.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import th.co.cana.kibana.model.Customer;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */
@RestController
@RequestMapping("/v1/customers")
public class CustomerCtrl {
    private static final Logger logger = LoggerFactory.getLogger(CustomerCtrl.class);

    @GetMapping
    public ResponseEntity<List<Customer>> getCustomers() {
        logger.info("Starting call customer API");
        try {
            List<Customer> customers = new ArrayList<>();
            for (int i = 1; i <= 5; i++) {
                Customer customer = new Customer();
                customer.setId((long) i);
                customer.setFirstName("First name " + i);
                customer.setLastName("Last name " + i);
                customer.setBirthDate(LocalDate.now().minusDays(i * 25));
                customer.setCreditAmt(BigDecimal.valueOf(i * i * 2500));
                customers.add(customer);
            }
            return ResponseEntity.ok(customers);
        } finally {
            logger.info("Finished call customer API");
        }
    }
}
