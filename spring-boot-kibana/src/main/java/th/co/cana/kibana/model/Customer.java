package th.co.cana.kibana.model;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
public class Customer implements Serializable {
    private Long id;
    private String firstName;
    private String lastName;
    private LocalDate birthDate;
    private BigDecimal creditAmt;
}
