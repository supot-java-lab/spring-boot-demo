package th.co.cana.kibana;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootKibanaApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootKibanaApplication.class, args);
	}

}
