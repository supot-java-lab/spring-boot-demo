/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.kafka;

import java.time.Duration;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndTimestamp;
import org.apache.kafka.common.PartitionInfo;
import org.apache.kafka.common.TopicPartition;

import th.co.cana.kafka.utils.Constants;
import th.co.cana.utils.DateUtils;

/**
 * @author supot
 * @version 1.0
 */
public class KafkaTime {
	
	public static void main(String[] args) {
		Properties props = new Properties();
		props.put("bootstrap.servers", "localhost:9092");
		props.put("group.id", "spring_kafka");
		props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		props.put("value.deserializer", "org.springframework.kafka.support.serializer.JsonDeserializer");
		props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, 100);
		
		KafkaConsumer<String, Object> consumer = new KafkaConsumer<>(props);
		consumer.subscribe(Collections.singletonList(Constants.KAFKA.TOPIC));
		
		// Get the list of partitions
		List<PartitionInfo> partitionInfos = consumer.partitionsFor(Constants.KAFKA.TOPIC);
		// Transform PartitionInfo into TopicPartition
		List<TopicPartition> topicPartitionList = partitionInfos.stream()
				.map(info -> new TopicPartition(Constants.KAFKA.TOPIC, info.partition()))
				.collect(Collectors.toList());
		// Assign the consumer to these partitions
		//consumer.assign(topicPartitionList);
		
		// Look for offsets based on timestamp
		Long time = DateUtils.addDay(new Date(), 1).getTime();
		Map<TopicPartition, Long> partitionTimestampMap = topicPartitionList.stream()
				.collect(Collectors.toMap(tp -> tp, tp -> time));
		
		Map<TopicPartition, OffsetAndTimestamp> partitionOffsetMap = consumer.offsetsForTimes(partitionTimestampMap);
		
		// Force the consumer to seek for those offsets
		partitionOffsetMap.forEach((tp, offsetAndTimestamp) -> consumer.seek(tp, offsetAndTimestamp.offset()));
		
		ConsumerRecords<String, Object> records = consumer.poll(Duration.ofMinutes(1));
		records.forEach(row -> {
			System.out.println(row.value());
		});
		
		consumer.close();
	}

}
