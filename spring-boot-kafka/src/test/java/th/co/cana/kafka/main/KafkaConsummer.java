/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.kafka.main;

import java.time.Duration;
import java.util.Collections;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import th.co.cana.kafka.utils.Constants;
import th.co.cana.kafka.utils.KafkaUtils;

/**
 * @author supot
 * @version 1.0
 */
public class KafkaConsummer {

	public static void main(String[] args) {
		KafkaConsumer<String, Object> consumer = KafkaUtils.createConsumer();
		try {
			consumer.subscribe(Collections.singletonList(Constants.KAFKA.TOPIC));
			while (true) {
				ConsumerRecords<String, Object> records = consumer.poll(Duration.ofMinutes(1));
				for (ConsumerRecord<String, Object> record : records) {
					System.out.println(record.key() + " : " + record.value() + " -> " + record.offset());
				}
				consumer.commitSync();
			}
		} finally {
			consumer.close();
		}
	}

}
