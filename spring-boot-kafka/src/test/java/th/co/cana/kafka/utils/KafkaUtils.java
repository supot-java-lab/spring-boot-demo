/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.kafka.utils;

import java.util.Properties;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;

/**
 * @author supot
 * @version 1.0
 */
public final class KafkaUtils {

	private KafkaUtils() {
	}

	public static KafkaConsumer<String, Object> createConsumer() {
		Properties props = new Properties();
		props.put("bootstrap.servers", "localhost:9092");
		props.put("group.id", "spring_kafka");
		props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
		props.put("value.deserializer", "org.springframework.kafka.support.serializer.JsonDeserializer");
		props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, 100);

		KafkaConsumer<String, Object> consumer = new KafkaConsumer<>(props);
		return consumer;
	}
}
