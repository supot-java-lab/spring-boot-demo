/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.kafka.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import th.co.cana.kafka.model.Customer;
import th.co.cana.kafka.service.ProducerService;
import th.co.cana.kafka.utils.Constants;

/**
 * @author supot
 * @version 1.0
 */

@RestController
@RequestMapping("/kafka/customers")
public class KafkaCtrl extends Controller {

	@Autowired
	private ProducerService producer;

	@PostMapping
	public ResponseEntity<Customer> sendReportInfo(@RequestBody Customer data) {
		try {
			producer.sendMessage(Constants.KAFKA.TOPIC, data);
			return ResponseEntity.ok(data);
		} catch (Exception ex) {
			logger.error("sendReportInfo", ex);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		}
	}
}
