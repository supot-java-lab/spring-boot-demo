/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.kafka.service;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import th.co.cana.kafka.utils.Constants;

/**
* @author supot
* @version 1.0
*/

@Service
public class ComsumerService extends KafkaService {
	
    @KafkaListener(topics = Constants.KAFKA.TOPIC)
    public void consume(ConsumerRecord<String, Object> record) {
    	logger.info("Consumed message : {}", record.value());
    }
}
