/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.kafka.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;

/**
* @author supot
* @version 1.0
*/
public abstract class KafkaService {
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
    @Autowired
    protected KafkaTemplate<String, Object> kafkaTemplate;

}
