/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.kafka.service;

import org.springframework.stereotype.Service;

/**
* @author supot
* @version 1.0
*/
@Service
public class ProducerService extends KafkaService {

	public void sendMessage(String topic, String msg) {
		logger.info("Producer send message : {} to topic {}", msg, topic);
		kafkaTemplate.send(topic, msg);
	}

	public void sendMessage(String topic, Object obj) {
		String name = null;
		if (obj != null) {
			name = obj.getClass().getName();
		}

		logger.info("Producer send type : {} to topic {}", name, topic);
		kafkaTemplate.send(topic, obj);
	}
}
