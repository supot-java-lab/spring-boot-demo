# Spring boot and Apache Kafka
Demo spring boot application with apache kafka

### 1. Start docker with docker compose in docker directory
	docker-compose up -d
### 2. Check container 
	docker ps
### 3. List topic
	kafka-topics.bat --list --zookeeper localhost:2181