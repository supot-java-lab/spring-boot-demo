Kafka in Docker
https://github.com/wurstmeister/kafka-docker/wiki/Connectivity

Source System -> Producer -> Kafka -> Consumer - Target System

ส่วนขยาย
Kafka Streams ทำหน้าที่เปลี่ยนแปลงข้อมูลใน Kafka
Kafka Connect ทำหน้าที่แปลงข้อมูลจาก Source systems ไปยัง Target systems โดยที่เราไม่ต้องสร้าง producer และ consumer ขึ้นมาเอง
Confluent Platform ซึ่งเป็นชุดเครื่องมือเสริมเพื่อปรับปรุงประสิทธิภาพของการทำงานของ Apache Kafka ซึ่งมีทั้งแบบ Open source และแบบ Enterprise

1. Start zookeeper (port 2181)
zookeeper-server-start.bat D:\AppServer\kafka_2.11-1.1.0\config\zookeeper.properties

2. Start Kafka (port 9092)
kafka-server-start.bat D:\AppServer\kafka_2.11-1.1.0\config\server.properties

3. Create topic
kafka-topics.bat --create --zookeeper localhost:2181 --replication-factor 1 --partitions 3 --topic test

4. List topic
kafka-topics.bat --list --zookeeper localhost:2181

5. Delete Topic 
kafka-topics.bat --delete --zookeeper localhost:2181 --topic test

    Note: หากอยากลบทันที่
    zookeeper-shell.bat localhost:2181
    get /brokers/topics/bill-presentment

6. describe topic
kafka-topics.bat --describe --zookeeper localhost:2181 --topic test

7. Send message
kafka-console-producer.bat --broker-list localhost:9092 --topic test

8. Read message
kafka-console-consumer.bat --bootstrap-server localhost:9092 --topic spring_kafka_topic --from-beginning
