/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.shutdown.exception;

/**
* @author supot
* @version 1.0
*/
public class JobExecuteException extends Exception {
	private static final long serialVersionUID = 1L;

	public JobExecuteException(String msg) {
		super(msg);
	}

	public JobExecuteException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
