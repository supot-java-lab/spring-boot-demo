/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.shutdown.task;

import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.stereotype.Component;

/**
 * @author supot
 * @version 1.0
 */

@Component
public class DemoTask implements Tasklet {
	private static Logger logger = LoggerFactory.getLogger(DemoTask.class);

	@Override
	public RepeatStatus execute(StepContribution stepCon, ChunkContext ctx) throws Exception {
		logger.info("++++++++++++++++++++++ Starting DemoTask ++++++++++++++++++++++ ");
		try {
			logger.info("Long process on DemoTask : {}", LocalDateTime.now());
			TimeUnit.MINUTES.sleep(1);
		} catch (InterruptedException ex) {
			logger.error("InterruptedException", ex);
			Thread.currentThread().interrupt();
		}
		logger.info("++++++++++++++++++++++ Finished DemoTask ++++++++++++++++++++++ ");

		return RepeatStatus.FINISHED;
	}

}
