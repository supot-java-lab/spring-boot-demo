/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.shutdown.service;

import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

import javax.annotation.PreDestroy;

import org.springframework.stereotype.Component;

/**
 * @author supot
 * @version 1.0
 */

@Component
public class LongProcessService extends BaseService {

	public void starting() {
		logger.info("++++++++++++++++++++++ Starting LongProcessService ++++++++++++++++++++++ ");
		logger.info("Time : {}", LocalDateTime.now());
		try {
			TimeUnit.MINUTES.sleep(2);
		} catch (InterruptedException ex) {
			logger.error("starting", ex);
			Thread.currentThread().interrupt();
		} finally {
			logger.info("++++++++++++++++++++++ Finished LongProcessService ++++++++++++++++++++++ ");
			logger.info("Time : {}", LocalDateTime.now());
		}
	}

	@PreDestroy
	public void onDestroy() {
		logger.info("Spring Container is destroyed! : {}", LocalDateTime.now());
	}
}
