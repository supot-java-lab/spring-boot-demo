/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.shutdown.controller;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpHeaders;

import th.co.cana.utils.MimeTypes;

/**
* @author supot
* @version 1.0
*/
public abstract class Controller {
	private static final String ERR = "Property ''{0}'' cannot null.";
	private static final String ERR_MISS_MATCHED = "Property ''{0}'' is not correct.";
	
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	public String requiredParameter(String parameterName) {
		return MessageFormat.format(ERR, parameterName);
	}
	
	public String missMatchedParameter(String paramName) {
		return MessageFormat.format(ERR_MISS_MATCHED, paramName);
	}

	/**
	 * Copy Entity to DTO 
	 * @param sources The List<T> of source objects
	 * @param clazz Target copy class
	 * @param ignores Ignore properties for copy
	 * @return
	 */
	public <T> List<T> copyProperties(List<?> sources, Class<T> clazz, String... ignores) {
		if (sources == null || sources.isEmpty() || clazz == null) {
			return Collections.emptyList();
		}
		
		List<T> items = new ArrayList<>();
		for (Object source : sources) {
			if (source == null) {
				continue;
			}
			
			T target = copyProperties(source, clazz, ignores);
			if (target != null) {
				items.add(target);
			}
		}
		
		return items;
	}

	/**
	 * Copy Entity to DTO
	 * @param source The source object
	 * @param clazz Target copy class
	 * @param ignores Ignore properties for copy
	 * @return
	 */
	public <T> T copyProperties(Object source, Class<T> clazz, String... ignores) {
		if (source == null || clazz == null) {
			return null;
		}

		T target = BeanUtils.instantiateClass(clazz);
		BeanUtils.copyProperties(source, target, ignores);
		return target;
	}
	
	public HttpHeaders fileHttpHeader(String filename) {
		String mimeType = MimeTypes.getMimeType(filename);
		HttpHeaders headers = new HttpHeaders();
		headers.add("Access-Control-Allow-Headers", "Content-Type");
		headers.add("Content-Type", mimeType);
		headers.add("Content-Disposition", "filename=" + filename);
		headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
		headers.add("Pragma", "no-cache");
		headers.add("Expires", "0");
		
		return headers;
	}
}
