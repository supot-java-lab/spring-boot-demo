/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.shutdown;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import th.co.cana.shutdown.service.LongProcessService;
import th.co.cana.shutdown.service.ShutdownHookService;

/**
 * @author supot
 * @version 1.0
 */

@Component
public class AppStartupRunner implements CommandLineRunner {
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private ApplicationContext appContext;
	@Autowired
	private LongProcessService longProcessService;
	@Autowired
	private ShutdownHookService shutdownHookService;

	@Override
	public void run(String... args) throws Exception {
		this.listBeanDefinition();
		Thread printingHook = new Thread(() -> shutdownHookService.checkProcessingTask());
		Runtime.getRuntime().addShutdownHook(printingHook);
		longProcessService.starting();
	}

	private void listBeanDefinition() {
		logger.info("Total BeanDefinition : {} Object", appContext.getBeanDefinitionCount());
	}
}
