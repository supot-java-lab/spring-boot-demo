/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.shutdown.controller;

import java.util.List;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import th.co.cana.shutdown.exception.JobExecuteException;
import th.co.cana.shutdown.model.JobName;
import th.co.cana.shutdown.service.JobService;

/**
* @author supot
* @version 1.0
*/

@RestController
@RequestMapping("/v1/job")
public class JobCtrl extends Controller {
	
	@Autowired
	private JobService jobService;
	
	@Autowired
	private Job jobDemo;
	
	@GetMapping
	public List<JobName> listJobRunning() {			
		return jobService.getJobRunnings();
	}
	
	@GetMapping("/demo/start")
	public ResponseEntity<String> executeTask() {		
		try {
			if (jobService.isJobRunning(jobDemo.getName())) {
				String msg = "Job name : " + jobDemo.getName() + " already run.";
				logger.warn(msg);
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(msg);
			}
			
			JobParametersBuilder jobBuilder = new JobParametersBuilder();
			jobBuilder.addString("JobId", String.valueOf(System.currentTimeMillis()));
			JobParameters params = jobBuilder.toJobParameters();
			
			jobService.run(jobDemo, params);
		} catch (JobExecuteException ex) {
			logger.error("executeTask", ex);
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ex.getMessage());
		}
		return ResponseEntity.ok("Success");
	}
}
