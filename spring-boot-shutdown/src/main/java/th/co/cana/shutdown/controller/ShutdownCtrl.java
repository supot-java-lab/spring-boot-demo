/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.shutdown.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author supot
 * @version 1.0
 */

@RestController
@RequestMapping("/task")
public class ShutdownCtrl implements ApplicationContextAware {
	private static Logger logger = LoggerFactory.getLogger(ShutdownCtrl.class);
	
	private ApplicationContext context;
	
	@Override
	public void setApplicationContext(ApplicationContext ctx) throws BeansException {
		this.context = ctx;
	}
	
	@GetMapping("/shutdown")
	public void shutdown() {
		logger.info("Statring shutdown ApplicationContext");
		try {
			((ConfigurableApplicationContext) context).close();
		} catch (Exception ex) {
			logger.error("shutdownContext", ex);
		} finally {
			logger.info("Finished shutdown ApplicationContext");
		}
	}
}
