/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.shutdown.service;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import th.co.cana.shutdown.model.JobName;

/**
 * @author supot
 * @version 1.0
 */

@Component
public class ShutdownHookService extends BaseService {

	@Autowired
	private JobService jobService;

	public void cleanupSystem() {
		logger.info("Starting clean-up system resources");
	}

	public void checkProcessingTask() {
		try {
			logger.info("Starting check running process...");
			List<JobName> runningJobs = jobService.getJobRunnings();
			while (!runningJobs.isEmpty()) {
				try {
					logger.info("\tHas some job running wait until process success..");
					TimeUnit.SECONDS.sleep(15);
				} catch (InterruptedException ex) {
					logger.error("sleep error", ex);
					Thread.currentThread().interrupt();
				}
				runningJobs = jobService.getJobRunnings();
			}
		} finally {
			cleanupSystem();
			logger.info("Finished check running process...");
		}
	}
}
