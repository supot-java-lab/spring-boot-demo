/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.shutdown.model;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
* @author supot
* @version 1.0
*/

@Data
public class JobName implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long id;
	private Integer version;
	private String name;
	private Date startTime;
	private Date endTime;
	private String exitCode;
}
