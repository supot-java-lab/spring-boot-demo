/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved..
* -----------------------------------------------------------------------------------
*/

package th.co.cana.shutdown.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.JobOperator;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import th.co.cana.shutdown.exception.JobExecuteException;
import th.co.cana.shutdown.model.JobName;
import th.co.cana.utils.Validators;

/**
* @author supot
* @version 1.0
*/

@Component
public class JobService extends BaseService {
	
	@Autowired
	@Qualifier("asyncJobLauncher")
	private JobLauncher jobLauncher;	
	@Autowired
	private JobExplorer jobExplorer;
    @Autowired
    private JobOperator jobOperator;
    
	public JobExecution run(Job job, JobParameters params) throws JobExecuteException {
		try {
			logger.info("AsyncJobLauncher name : {}", job.getName());
			logger.info("AsyncJobLauncher parameter : {}", params);
			
			return jobLauncher.run(job, params);
		} catch (JobExecutionAlreadyRunningException | JobRestartException | JobInstanceAlreadyCompleteException
				| JobParametersInvalidException ex) {
			throw new JobExecuteException("Run job name : " + job.getName(), ex);
		}
	}
	
	public void stop(String jobName) throws JobExecuteException {
		try {
			Set<JobExecution> jobExes = jobExplorer.findRunningJobExecutions(jobName);
			if (Validators.isEmpty(jobExes)) {
				logger.info("Empty job name : {} on running process.", jobName);
				return;
			}

			for (JobExecution jobExe : jobExes) {
				logger.info("Job name : {} with id : {} status : {}", jobName, jobExe.getJobId(), jobExe.getStatus());
				if (jobExe.getStatus().isRunning()) {
					jobOperator.stop(jobExe.getId());
					logger.info("Stop job name : {} with starting id : {} success.", jobName, jobExe.getId());
				}
			}
		} catch (Exception ex) {
			throw new JobExecuteException("Stop job name : " + jobName, ex);
		}
	}
	
	public boolean isJobRunning(String jobName){
		Set<JobExecution> jobExes = jobExplorer.findRunningJobExecutions(jobName);
		return Validators.isNotEmpty(jobExes);
	}
	
	public boolean isJobRunning(String jobName, Map<String, String> params){
		Set<JobExecution> jobExes = jobExplorer.findRunningJobExecutions(jobName);
		if (Validators.isEmpty(jobExes)) {
			return false;
		}

		if (Validators.isEmpty(params)) {
			return Validators.isNotEmpty(jobExes);
		}
		
		int match = 0;
		for (JobExecution job : jobExes) {
			JobParameters jobParams = job.getJobParameters();
			if (Validators.isNull(jobParams)) {
				continue;
			}

			for (Map.Entry<String, String> map : params.entrySet()) {
				String value = jobParams.getString(map.getKey(), "");
				if (value.equals(map.getValue())) {
					match += 1;
				}
			}
		}
		
		return (match == params.size());
	}
	
	public boolean isJobStop(String jobName) {
		Set<JobExecution> jobExes = jobExplorer.findRunningJobExecutions(jobName);
		if (Validators.isEmpty(jobExes)) {
			return true;
		}
		for (JobExecution jobExe : jobExes) {
			if(jobExe.getStatus() == BatchStatus.STOPPING 
					|| jobExe.getStatus() == BatchStatus.STOPPED
					|| jobExe.getStatus() == BatchStatus.COMPLETED) {
				return true;
			}
		}

		return false;
	}

	public List<String> getJobNames() {
		return jobExplorer.getJobNames();
	}
	
	public List<JobName> getJobRunnings() {
		List<JobName> runningJobs = new ArrayList<>();
		List<String> jobNames = getJobNames();
		for (String name : jobNames) {
			Set<JobExecution> jobExes = jobExplorer.findRunningJobExecutions(name);
			for (JobExecution jobExe : jobExes) {
				JobName job = new JobName();
				job.setId(jobExe.getId());
				job.setVersion(jobExe.getVersion());
				job.setName(jobExe.getJobConfigurationName());
				job.setStartTime(jobExe.getStartTime());
				job.setEndTime(jobExe.getEndTime());
				job.setExitCode(jobExe.getStatus().name());
				
				if (Validators.isEmpty(job.getName())) {
					job.setName(name);
				}

				runningJobs.add(job);
			}
		}
		
		return runningJobs;
	}
}
