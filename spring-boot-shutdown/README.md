[[_TOC_]]

#Build docker Image
## 1. Build
	docker build -t supot/spring-boot-shutdown:1.0.0 .

## 2. Push Image
	docker push supot/spring-boot-shutdown:1.0.0

# Deploy on Docker

	docker run -p 8080:8080 -d --name spring-boot-shutdown supot/spring-boot-shutdown:1.0.1
	
# Deploy On Kube
## 1. Apply

	kubectl apply -f deployment.yaml
	
	or
	
	kubectl apply -f deployment-http.yaml
	
## 2. Verify deploy

	kubectl get deployments
	
and

	kubectl get service

and
	
	kubectl get pod
		
## 2. Create forward port

	kubectl port-forward svc/spring-boot-shutdown 8080:8080
	
## 3. Try access api
	http://localhost:8080/health

# Undeploy & Deleted

	kubectl delete service spring-boot-shutdown

and 

	kubectl delete deployment spring-boot-shutdown