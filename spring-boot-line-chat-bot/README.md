# Spring boot and Line
Demo spring boot application with Line

# Ref
- [Line Notify](https://notify-bot.line.me/)
- [LINE API](https://saixiii.com/chapter5-line-api-send-message/)
- [LINE Messaging API](https://www.unzeen.com/article/3506/)