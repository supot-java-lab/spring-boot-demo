/*
 * -------------------------------------------------------------------------------
 * Copyright (c) 2023. Cana Enterprise Co., Ltd.
 * -------------------------------------------------------------------------------
 */

package th.co.cana.quartz.utils;

import io.github.jdevlibs.utils.Utils;
import io.github.jdevlibs.utils.Validators;
import org.quartz.*;
import org.quartz.impl.triggers.CronTriggerImpl;
import th.co.cana.quartz.model.SchedulerJob;

import java.text.ParseException;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.Map;
import java.util.Objects;

/**
 * @author supot.jdev
 * @version 1.0
 */
public final class JobUtils {
    private static final String JOB_GROUP        = "quartz-jobs";
    private static final String TRIGGER_GROUP    = "quartz-trigger";

    private JobUtils() {}

    public static JobDetail jobDetail(SchedulerJob job) {
        final JobDataMap jobData = new JobDataMap(job.getJobData());
        return jobDetail(job.getJobClass(), jobData, job.getJobName(), job.getJobGroup());
    }

    /**
     * Create JobDetail with job data parameter
     * @param jobClass The job class.
     * @return JobDetail
     */
    public static JobDetail jobDetail(Class <? extends Job> jobClass) {
        return jobDetail(jobClass, new JobDataMap(), null);
    }

    /**
     * Create JobDetail with job data parameter
     * @param jobClass The job class.
     * @param name Job name, If null use default generate by 'UUID'
     * @return JobDetail
     */
    public static JobDetail jobDetail(Class <? extends Job> jobClass, String name) {
        return jobDetail(jobClass, new JobDataMap(), name, JOB_GROUP);
    }

    /**
     * Create JobDetail with job data parameter
     * @param jobClass The job class.
     * @param jobData The JobDataMap parameter
     * @return JobDetail
     */
    public static JobDetail jobDetail(Class <? extends Job> jobClass, JobDataMap jobData) {
        return jobDetail(jobClass, jobData, null, JOB_GROUP);
    }

    /**
     * Create JobDetail with job data parameter
     * @param jobClass The job class.
     * @param jobData The JobDataMap parameter
     * @param name Job name, If null use default generate by 'UUID'
     * @return JobDetail
     */
    public static JobDetail jobDetail(Class <? extends Job> jobClass, JobDataMap jobData, String name) {
        return jobDetail(jobClass, jobData, name, JOB_GROUP);
    }

    /**
     * Create JobDetail with job data parameter
     * @param jobClass The job class.
     * @param jobData The JobDataMap parameter
     * @param name Job name, If null use default generate by 'UUID'
     * @param group Job group, If null use default 'quartz-jobs'
     * @return JobDetail
     */
    public static JobDetail jobDetail(Class <? extends Job> jobClass, JobDataMap jobData, String name, String group) {
        jobData = Validators.isNull(jobData) ? new JobDataMap() : jobData;
        name = Validators.isEmpty(name) ? "job:" + Utils.getUUID() :  name;
        group = Validators.isEmpty(group) ? JOB_GROUP : group;

        return JobBuilder.newJob(jobClass)
                .withIdentity(name, group)
                .withDescription("Job of : " + jobClass.getName())
                .usingJobData(jobData)
                .storeDurably()
                .build();
    }

    /**
     * Create JobDetail with parameter map
     * @param jobClass The job class.
     * @param params The Map<String, Object> parameter
     * @return JobDetail
     */
    public static JobDetail jobDetail(Class<? extends Job> jobClass, Map<String, Object> params) {
        return jobDetail(jobClass, params, null);
    }

    /**
     * Create JobDetail with parameter map
     * @param jobClass The job class.
     * @param params The Map<String, Object> parameter
     * @param name Job name, If null use default generate by 'UUID'
     * @return JobDetail
     */
    public static JobDetail jobDetail(Class<? extends Job> jobClass, Map<String, Object> params, String name) {
        JobDataMap jobData = new JobDataMap(params);
        return jobDetail(jobClass, jobData, name);
    }

    /**
     * Create trigger with JobDetail
     * @param jobDetail The JobDetail class
     * @return Trigger
     */
    public static Trigger trigger(JobDetail jobDetail) {
        return trigger(jobDetail, ZonedDateTime.now(), null, TRIGGER_GROUP);
    }

    /**
     * Create trigger with JobDetail
     * @param jobDetail The JobDetail class
     * @param name The trigger name, If null use default generate by 'UUID'
     * @return Trigger
     */
    public static Trigger trigger(JobDetail jobDetail, String name) {
        return trigger(jobDetail, ZonedDateTime.now(), name, TRIGGER_GROUP);
    }

    /**
     * Create trigger with JobDetail
     * @param jobDetail The JobDetail class
     * @param start DateTime for start trigger, If null current date/time
     * @param name The trigger name, If null use default generate by 'UUID'
     * @return Trigger
     */
    public static Trigger trigger(JobDetail jobDetail, ZonedDateTime start, String name) {
        return trigger(jobDetail, start, name, TRIGGER_GROUP);
    }

    /**
     * Create trigger with JobDetail
     * @param jobDetail The JobDetail class
     * @param start DateTime for start trigger, If null current date/time
     * @param name The trigger name, If null use default generate by 'UUID'
     * @param group The trigger group, If null use default 'quartz-trigger'
     * @return Trigger
     */
    public static Trigger trigger(JobDetail jobDetail, ZonedDateTime start, String name, String group) {
        Objects.requireNonNull(jobDetail, "jobDetail parameter cannot be null.");

        start = Validators.isNull(start) ? ZonedDateTime.now() : start;
        group = Validators.isEmpty(group) ? TRIGGER_GROUP : group;
        name = Validators.isEmpty(name) ? "trigger:" + Utils.getUUID() : name;
        return TriggerBuilder.newTrigger()
                .forJob(jobDetail)
                .withIdentity(name, group)
                .startAt(Date.from(start.toInstant()))
                .withSchedule(SimpleScheduleBuilder.simpleSchedule().withMisfireHandlingInstructionFireNow())
                .build();
    }

    public static CronTrigger cronTrigger(String name, String group, String cronExpression) {

        try {
            CronTriggerImpl trigger = new CronTriggerImpl();
            trigger.setName(name);
            trigger.setGroup(group);
            trigger.setStartTime(new Date());
            trigger.setCronExpression(cronExpression);
            trigger.setMisfireInstruction(SimpleTrigger.MISFIRE_INSTRUCTION_FIRE_NOW);

            return trigger;
        } catch (ParseException ex) {
            //Skip
        }
        return null;
    }
}
