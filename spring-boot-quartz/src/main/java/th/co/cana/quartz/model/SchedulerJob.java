/*
 * -------------------------------------------------------------------------------
 * Copyright (c) 2023. Cana Enterprise Co., Ltd.
 * -------------------------------------------------------------------------------
 */

package th.co.cana.quartz.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.quartz.Job;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SchedulerJob implements Serializable {
    private String jobName;
    private String jobGroup;
    private Class <? extends Job> jobClass;
    private String cronExpression;
    private LocalDateTime jobStart;
    private Map<String, Object> jobData;

    public Map<String, Object> getJobData() {
        if (jobData == null) {
            jobData = new HashMap<>(0);
        }
        return jobData;
    }

    public String getJobClassName() {
        if (jobClass != null) {
            return jobClass.getName();
        }
        return "";
    }

    public void addData(String name, Object value) {
        if (name != null && !name.isEmpty()) {
            if (jobData == null) {
                jobData = new HashMap<>();
            }
            jobData.put(name, value);
        }
    }

    public boolean isCronJob() {
        return (cronExpression != null && !cronExpression.isEmpty());
    }
}
