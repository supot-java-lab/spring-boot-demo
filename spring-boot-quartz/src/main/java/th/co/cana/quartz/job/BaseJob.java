/*
 * -------------------------------------------------------------------------------
 * Copyright (c) 2023. Cana Enterprise Co., Ltd.
 * -------------------------------------------------------------------------------
 */

package th.co.cana.quartz.job;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */
public abstract class BaseJob extends QuartzJobBean {
    protected Logger logger = LoggerFactory.getLogger(getClass());

    public boolean isJobInstanceRunning(JobExecutionContext context, Job job) {
        try {
            List<JobExecutionContext> jobs = context.getScheduler().getCurrentlyExecutingJobs();
            if (jobs != null) {
                for (JobExecutionContext jobCtx : jobs) {
                    if (jobCtx.getTrigger().equals(context.getTrigger())
                            && !jobCtx.getJobInstance().equals(job)) {
                        return true;
                    }
                }
            }
        } catch (SchedulerException ex) {
            logger.error("isJobInstanceRunning : {}", ex.getMessage());
        }
        return false;
    }
}
