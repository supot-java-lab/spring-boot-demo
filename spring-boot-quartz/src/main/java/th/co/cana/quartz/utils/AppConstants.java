/*
 * -------------------------------------------------------------------------------
 * Copyright (c) 2023. Cana Enterprise Co., Ltd.
 * -------------------------------------------------------------------------------
 */

package th.co.cana.quartz.utils;

import java.util.Locale;

/**
 * @author supot.jdev
 * @version 1.0
 */
public final class AppConstants {
    private AppConstants() {
    }

    public static final String Y = "Y";
    public static final String N = "N";
    public static final String C = "C";
    public static final String T = "T";

    public static final String UTF8 = "UTF-8";
    public static final Locale US = Locale.US;
    public static final Locale TH = new Locale("th", "TH");

    /**
     * AOP : Aspect Oriented Programming package
     */
    public static final class Aspects {
        private Aspects() {}

        public static final String SERVICE 	= "execution(public * th.co.cana.quartz.service..*.*(..))";
        public static final String REPOSITORY = "execution(public * th.co.cana.quartz.repository..*.*(..))";
        public static final String REST_LOGGER = "execution(@th.co.cana.quartz.spring.RestApiLogger * *(..))";
    }

    /**
     * API base path for the application
     */
    public static final class Apis {
        private Apis() {}
        public static final String ERROR 	= "/error";
        public static final String JOB 	    = "/v1/job";
    }
}
