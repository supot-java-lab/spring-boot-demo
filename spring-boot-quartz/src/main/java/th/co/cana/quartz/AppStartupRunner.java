/*
 * -------------------------------------------------------------------------------
 * Copyright (c) 2023. Cana Enterprise Co., Ltd.
 * -------------------------------------------------------------------------------
 */

package th.co.cana.quartz;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Component
public class AppStartupRunner implements CommandLineRunner {
    private static final Logger logger = LoggerFactory.getLogger(AppStartupRunner.class);

    @Override
    public void run(String... args) {
        logger.info("++++++++++++++++++++++++++++++ Starting StartupRunner ++++++++++++++++++++++++++++++");
        try {

        } catch (Exception ex) {
            logger.error("executeTask", ex);
        } finally {
            logger.info("+++++++++++++++++++++++++++ Finished StartupRunner ++++++++++++++++++++++++++++++");
        }
    }
}
