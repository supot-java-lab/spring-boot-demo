/*
 * -------------------------------------------------------------------------------
 * Copyright (c) 2023. Cana Enterprise Co., Ltd.
 * -------------------------------------------------------------------------------
 */

package th.co.cana.quartz.job;

import lombok.extern.slf4j.Slf4j;
import org.quartz.JobDataMap;
import org.quartz.JobExecutionContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import th.co.cana.quartz.utils.JobUtils;

import java.util.Date;

/**
 * @author supot.jdev
 * @version 1.0
 */

@Component
@Scope("prototype")
@Slf4j
public class DemoJob extends BaseJob {
    @Override
    protected void executeInternal(JobExecutionContext context) {
        String jobName = "";
        try {
            jobName = context.getTrigger().getJobKey().getName();
            Date fireTime = context.getFireTime();
            log.info("Starting job[{}] at[{}]", jobName, fireTime);

            if (isJobInstanceRunning(context, this)) {
                log.warn("Stop job name {} because there's another instance running", jobName);
                return;
            }

            JobDataMap dataMap = context.getMergedJobDataMap();
            String taskId = (String) dataMap.get("taskId");
            log.info("Starting Process by task id : {}", taskId);
            Thread.sleep(10000L);
        } catch (Exception ex) {
            log.error("executeInternal job DemoJob error", ex);
        } finally {
            log.info("Finished Process by Job : {}", jobName);
        }
    }
}
