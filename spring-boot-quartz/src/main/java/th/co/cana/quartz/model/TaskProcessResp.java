/*
 * -------------------------------------------------------------------------------
 * Copyright (c) 2023. Cana Enterprise Co., Ltd.
 * -------------------------------------------------------------------------------
 */

package th.co.cana.quartz.model;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
public class TaskProcessResp implements Serializable {
    private String id;
    private String name;
    private LocalDateTime dateTime;
}
