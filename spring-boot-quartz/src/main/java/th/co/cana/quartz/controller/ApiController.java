/*
 * -------------------------------------------------------------------------------
 * Copyright (c) 2023. Cana Enterprise Co., Ltd.
 * -------------------------------------------------------------------------------
 */

package th.co.cana.quartz.controller;

import io.github.jdevlibs.utils.Validators;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.File;
import java.net.MalformedURLException;
import java.nio.file.Path;

/**
 * @author supot.jdev
 * @version 1.0
 */
public abstract class ApiController {
    protected Logger logger = LoggerFactory.getLogger(getClass());

    public <T> ResponseEntity<T> ok(T body) {
        return ResponseEntity.ok(body);
    }

    public ResponseEntity<Object> download(Resource resource) {
        return download(resource, resource.getFilename());
    }

    public ResponseEntity<Object> download(Resource resource, String fileName) {
        if (Validators.isNull(resource)) {
            return notFound();
        }
        if (Validators.isEmpty(fileName)) {
            fileName = resource.getFilename();
        }

        logger.info("Download file with name : {}", fileName);

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + "\"")
                .body(resource);
    }

    public ResponseEntity<Object> download(String file) {
        return download(new File(file), null);
    }

    public ResponseEntity<Object> download(String file, String fileName) {
        return download(new File(file), fileName);
    }

    public ResponseEntity<Object> download(File file) {
        return download(file, file.getName());
    }

    public ResponseEntity<Object> download(File file, String fileName) {
        if (Validators.isNull(file) || !file.exists() || !file.isFile()) {
            logger.error("Download File not found");
            return notFound();
        }
        if (Validators.isEmpty(fileName)) {
            fileName = file.getName();
        }

        logger.info("Download file {} with name : {}", file.getPath(), fileName);
        Resource resource = loadFileAsResource(file);
        if (Validators.isNull(resource)) {
            return notFound();
        }

        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + "\"")
                .body(resource);
    }

    public ResponseEntity<Object> notFound() {
        return ResponseEntity.notFound().build();
    }

    public ResponseEntity<Object> notFound(Object body) {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(body);
    }

    private Resource loadFileAsResource(File fileName) {
        try {
            Path filePath = fileName.toPath().normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                return resource;
            }

            logger.error("File {} not found", fileName.getPath());
        } catch (MalformedURLException ex) {
            logger.error("loadFileAsResource not found", ex);
        }

        return null;
    }
}
