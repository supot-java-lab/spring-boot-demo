/*
 * -------------------------------------------------------------------------------
 * Copyright (c) 2023. Cana Enterprise Co., Ltd.
 * -------------------------------------------------------------------------------
 */

package th.co.cana.quartz.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.Trigger;
import org.springframework.stereotype.Service;
import th.co.cana.quartz.model.SchedulerJob;
import th.co.cana.quartz.utils.JobUtils;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Slf4j
@RequiredArgsConstructor
@Service
public class SchedulerService {

    private final Scheduler scheduler;

    public void startJob(SchedulerJob job) {
        try {
            JobDetail jobDetail = JobUtils.jobDetail(job);
            if (scheduler.checkExists(jobDetail.getKey())) {
                log.warn("Job class : {} exists by key : {}", job.getJobClassName(), jobDetail.getKey().getName());
                return;
            }

            if (job.isCronJob()) {
                Trigger trigger = JobUtils.cronTrigger(job.getJobName(), job.getJobGroup(), job.getCronExpression());
                scheduler.scheduleJob(jobDetail, trigger);
            } else {
                Trigger trigger = JobUtils.trigger(jobDetail);
                scheduler.scheduleJob(jobDetail, trigger);
            }
        } catch (Exception ex) {
            log.error("startJob", ex);
        }
    }

    public boolean deleteJob(SchedulerJob job) {
        try {
            JobKey jobKey = new JobKey(job.getJobName(), job.getJobGroup());
            return scheduler.deleteJob(jobKey) && scheduler.interrupt(jobKey);
        } catch (Exception ex) {
            log.error("deleteJob", ex);
        }

        return false;
    }
}
