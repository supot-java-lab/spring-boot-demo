/*
 * -------------------------------------------------------------------------------
 * Copyright (c) 2023. Cana Enterprise Co., Ltd.
 * -------------------------------------------------------------------------------
 */

package th.co.cana.quartz.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import th.co.cana.quartz.model.TaskProcessResp;
import th.co.cana.quartz.service.SchedulerService;
import th.co.cana.quartz.utils.AppConstants;

import java.time.LocalDateTime;

/**
 * @author supot.jdev
 * @version 1.0
 */

@RestController
@RequestMapping(AppConstants.Apis.JOB)
@RequiredArgsConstructor
public class JobController extends ApiController {

    private final SchedulerService schedulerService;

    @GetMapping
    public ResponseEntity<Object> index() {
        logger.info("Starting call index");
        return ok("Hello, I'm demo API");
    }

    @GetMapping("/execute/{jobId}")
    public ResponseEntity<TaskProcessResp> startJob(@PathVariable String jobId) {

        schedulerService.startDemoJob(jobId);

        TaskProcessResp resp = new TaskProcessResp();
        resp.setId(jobId);
        resp.setName("Jobname : " + jobId);
        resp.setDateTime(LocalDateTime.now());

        return ok(resp);
    }
}
