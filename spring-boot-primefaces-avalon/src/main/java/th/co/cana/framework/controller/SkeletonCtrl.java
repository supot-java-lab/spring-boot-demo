package th.co.cana.framework.controller;

import io.github.jdevlibs.utils.Validators;
import jakarta.annotation.PostConstruct;
import jakarta.faces.view.ViewScoped;
import jakarta.inject.Named;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Named
@ViewScoped
@Data
public class SkeletonCtrl implements Serializable {
    private static final String COL_TEMP = "col1,col2,col3,col4,col5";

    private String columnName;
    private List<SkeletonColumn> columns;
    private List<SkeletonData> items;
    private List<SkeletonData> popupItems;

    @PostConstruct
    public void init() {
        createRows();
        createDynamicColumns();
    }

    private void createDynamicColumns() {
        if (Validators.isEmpty(columnName)) {
            columnName = COL_TEMP;
        }

        String[] columnKeys = columnName.split("[,]");
        columns = new ArrayList<>(columnKeys.length);
        for (String columnKey : columnKeys) {
            String key = columnKey.trim();
            SkeletonColumn col = new SkeletonColumn();
            col.setHeader(key);
            columns.add(col);
        }
    }

    private void createRows() {
        items = new ArrayList<>(5);
        for (int i = 1; i <= 5; i++) {
            items.add(new SkeletonData());
        }

        popupItems = new ArrayList<>(15);
        for (int i = 1; i <= 15; i++) {
            popupItems.add(new SkeletonData());
        }
    }

    @Data
    public static class SkeletonData implements Serializable {
        private String value;
    }

    @Data
    public static class SkeletonColumn implements Serializable {
        private String header;
        private String property;
    }
}
