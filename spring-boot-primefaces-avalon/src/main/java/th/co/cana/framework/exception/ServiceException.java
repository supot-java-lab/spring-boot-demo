package th.co.cana.framework.exception;

import lombok.Getter;
import th.co.cana.framework.config.enums.ErrorCodes;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Getter
public class ServiceException extends RuntimeException {
    private final ErrorCodes error;

    public ServiceException(Throwable cause) {
        super(cause);
        this.error = ErrorCodes.CODE_DB_OTHER;
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
        this.error = ErrorCodes.CODE_DB_OTHER;
    }

    public ServiceException(String message) {
        super(message);
        this.error = ErrorCodes.CODE_DB_OTHER;
    }

    public ServiceException(ErrorCodes messageCode, String message) {
        super(message);
        this.error = messageCode;
    }

    public ServiceException(ErrorCodes errorCode, Throwable cause) {
        super(cause);
        this.error = errorCode;
    }

    public ServiceException(ErrorCodes errorCode, String message, Throwable cause) {
        super(message, cause);
        this.error = errorCode;
    }

    public String getErrorCode() {
        if (error == null) {
            return null;
        }
        return error.getCode();
    }

    public String getErrorMessage() {
        if (error == null) {
            return null;
        }
        return error.getMessage();
    }
}