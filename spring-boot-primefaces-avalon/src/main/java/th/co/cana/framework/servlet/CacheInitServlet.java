package th.co.cana.framework.servlet;

import io.github.jdevlibs.utils.Validators;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import th.co.cana.framework.service.CacheService;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Slf4j
@RequiredArgsConstructor
@Component
@WebServlet(name = "CacheInitServlet", loadOnStartup = 10, urlPatterns = { "/CacheInitServlet" })
public class CacheInitServlet extends HttpServlet {

    private final CacheService cacheService;

    @Override
    public void init() throws ServletException {
        super.init();
        initialCache(null);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) {
        String type = request.getParameter("type");
        this.initialCache(type);
    }

    private void initialCache(String type) {
        log.info("[CONFIG] Starting initial cache value : {}", type);
        try {
            if (Validators.isEmpty(type)) {
                cacheService.initialCache();
            }
        } catch (Exception ex) {
            log.error("[CONFIG] initialCache Error:", ex);
        } finally {
            log.info("[CONFIG] Finish initial cache value");
        }
    }
}