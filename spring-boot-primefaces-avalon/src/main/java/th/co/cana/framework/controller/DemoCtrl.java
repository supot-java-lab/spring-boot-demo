package th.co.cana.framework.controller;

import io.github.jdevlibs.utils.Convertors;
import io.github.jdevlibs.utils.Validators;
import jakarta.annotation.PostConstruct;
import jakarta.faces.view.ViewScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import lombok.Getter;
import lombok.Setter;
import th.co.cana.framework.models.Customer;
import th.co.cana.framework.service.CustomerService;

import java.util.List;
import java.util.Locale;

/**
 * @author supot.jdev
 * @version 1.0
 */

@Getter
@Setter
@Named
@ViewScoped
public class DemoCtrl extends AbstractCtrl {
    @Inject
    private CustomerService service;

    private List<Customer> customers1;
    private List<Customer> customers2;
    private Customer selectedCustomer;
    private List<Customer> selectedCustomers;

    private List<Customer> filteredCustomers1;
    private List<Customer> filteredCustomers2;

    @PostConstruct
    public void init() {
        customers1 = service.getCustomers(50);
        customers2 = service.getCustomers(50);
    }

    public long getTotalCount(String name) {
        return customers1.stream().filter(customers -> name.equals(customers.getRepresentative().getName())).count();
    }

    public boolean globalFilterFunction(Object value, Object filter, Locale locale) {
        logger.info("Filter: {}", filter);
        String filterValue = (filter == null) ? null : filter.toString().trim().toLowerCase();
        if (Validators.isEmpty(filterValue)) {
            return true;
        }
        int filterInt = Convertors.toInteger(filterValue, 0);

        Customer customer = (Customer) value;
        return customer.getName().toLowerCase().contains(filterValue)
                || customer.getCountry().getName().toLowerCase().contains(filterValue)
                || customer.getRepresentative().getName().toLowerCase().contains(filterValue)
                || customer.getDate().toString().toLowerCase().contains(filterValue)
                || customer.getStatus().name().toLowerCase().contains(filterValue)
                || customer.getActivity() < filterInt;
    }
}
