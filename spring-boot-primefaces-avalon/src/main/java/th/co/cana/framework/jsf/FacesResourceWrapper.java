package th.co.cana.framework.jsf;

import jakarta.faces.application.Resource;
import jakarta.faces.application.ResourceWrapper;
import th.co.cana.framework.utils.AppConstants;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class FacesResourceWrapper extends ResourceWrapper {
    private final String version;

    public FacesResourceWrapper(final Resource resource) {
        super(resource);
        String path = super.getRequestPath();
        if (path != null && path.contains("?")) {
            version = "&tmv=" + AppConstants.AppInfo.getTime();
        } else {
            version = "?tmv=" + AppConstants.AppInfo.getTime();
        }
    }

    @Override
    public String getRequestPath() {
        return super.getRequestPath() + version;
    }
}
