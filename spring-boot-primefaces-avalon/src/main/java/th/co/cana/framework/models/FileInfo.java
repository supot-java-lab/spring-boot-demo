package th.co.cana.framework.models;

import io.github.jdevlibs.utils.Convertors;
import io.github.jdevlibs.utils.FileMappings;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author supot.jdev
 * @version 1.0
 */
@EqualsAndHashCode(of = {"id"})
@Data
public class FileInfo implements Serializable {

    private String id;
    private String fileName;
    private String fileExt;
    private String filePath;
    private LocalDateTime lastModified;
    private String fileDate;
    private long fileSize;

    public String getFileSizeStr() {
        return Convertors.toFileSize(fileSize);
    }

    public String getFileCssIcon() {
        return FileMappings.getFileCssIcon(fileExt);
    }

    public boolean isDisabled() {
        return (fileName != null && fileName.toLowerCase().lastIndexOf(".log") > 0);
    }
}