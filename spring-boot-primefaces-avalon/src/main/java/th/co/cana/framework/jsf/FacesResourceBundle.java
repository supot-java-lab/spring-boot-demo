package th.co.cana.framework.jsf;

import io.github.jdevlibs.faces.resource.ClassPathResourceBundle;

import java.util.ArrayList;
import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class FacesResourceBundle extends ClassPathResourceBundle {
    private static final String BASE_PACKAGE = "i18n/";
    private static final List<String> BUNDLE_NAMES;

    static {
        BUNDLE_NAMES = new ArrayList<>(2);
        BUNDLE_NAMES.add(BASE_PACKAGE + "JSFMessages");
        BUNDLE_NAMES.add(BASE_PACKAGE + "JSFAppMessage");
    }

    public FacesResourceBundle() {
        super(BUNDLE_NAMES);
    }
}