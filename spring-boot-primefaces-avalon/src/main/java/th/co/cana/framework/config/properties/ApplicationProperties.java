package th.co.cana.framework.config.properties;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author supot.jdev
 * @version 1.0
 */

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "application")
public class ApplicationProperties {
    private String name;
    private String version;
    private String buildTime;
    private String javaBuildVersion;

    private LogConfig logConfig = new LogConfig();

    @Data
    public static final class LogConfig {
        private String level;
    }
}
