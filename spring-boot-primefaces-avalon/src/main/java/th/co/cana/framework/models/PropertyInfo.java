package th.co.cana.framework.models;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author supot.jdev
 * @version 1.0
 */
@EqualsAndHashCode(of = {"id"})
@Data
public class PropertyInfo implements Serializable {
    private String id;
    private int type;
    private String key;
    private String value;

    public String getSortKey() {
        return type + "-" + key;
    }
}
