package th.co.cana.framework.filter;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import lombok.extern.slf4j.Slf4j;
import org.springframework.lang.NonNull;
import org.springframework.web.filter.OncePerRequestFilter;
import th.co.cana.framework.jsf.bean.UserInfo;
import th.co.cana.framework.utils.AppConstants;
import th.co.cana.framework.utils.PageMapping;

import java.io.IOException;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Slf4j
public class AuthenticationFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(@NonNull HttpServletRequest request, @NonNull HttpServletResponse response
            , @NonNull FilterChain filterChain) throws ServletException, IOException {

        //Step 1. Check ajax request
        if (isAjaxRequest(request)) {
            filterChain.doFilter(request, response);
            return;
        }

        String reqPath = getPath(request);
        if ("/".equals(reqPath)) {
            filterChain.doFilter(request, response);
            return;
        }
        log.info("reqPath : {}", reqPath);

        //Step 3. Get UserSession from HttpSession.
        UserInfo userInfo = getUserInfo(request.getSession());
        if (userInfo == null || userInfo.isUserNotLogin()) {
            request.getSession().invalidate();
            String logoutUrl = PageMapping.URL_LOGIN;
            log.info("Case 1: User is no session on application.");
            log.warn("redirect to logout : {}", logoutUrl);
            response.sendRedirect(logoutUrl);
        } else {
            log.info("Case 2: User is session on application.");
            filterChain.doFilter(request, response);
        }
    }

    public UserInfo getUserInfo(HttpSession httpSession) {
        try {
            return (UserInfo) httpSession.getAttribute(AppConstants.SESSION_USER_ATT);
        } catch (Exception ex) {
            log.error("getUserInfo : {}", ex.getMessage());
        }

        return null;
    }

    private String getPath(HttpServletRequest req) {
        return req.getRequestURI().substring(req.getContextPath().length());
    }

    private boolean isAjaxRequest(HttpServletRequest request) {
        String jsfReq = request.getHeader("Faces-Request");
        return (jsfReq != null && jsfReq.equals("partial/ajax"));
    }
}
