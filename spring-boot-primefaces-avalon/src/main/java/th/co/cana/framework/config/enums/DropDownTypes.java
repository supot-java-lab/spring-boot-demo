package th.co.cana.framework.config.enums;

/**
 * @author supot.jdev
 * @version 1.0
 */
public enum DropDownTypes {
    PROCESS_STATUS("PROCESS_STATUS"),
    RECORD_STATUS("RECORD_STATUS");

    private final String type;
    DropDownTypes(String type) {
        this.type = type;
    }

    public String getTypeName() {
        return type;
    }
}
