package th.co.cana.framework.config.enums;

import lombok.Getter;

@Getter
public enum MonthNameShorts {
    JANUARY("1", "msg.month.short.01"),
    FEBRUARY("2", "msg.month.short.02"),
    MARCH("3", "msg.month.short.03"),
    APRIL("4", "msg.month.short.04"),
    MAY("5", "msg.month.short.05"),
    JUNE("6", "msg.month.short.06"),
    JULY("7", "msg.month.short.07"),
    AUGUST("8", "msg.month.short.08"),
    SEPTEMBER("9", "msg.month.short.09"),
    OCTOBER("10", "msg.month.short.10"),
    NOVEMBER("11", "msg.month.short.11"),
    DECEMBER("12", "msg.month.short.12");

    private final String value;
    private final String msgCode;
    MonthNameShorts(String value, String msgCode) {
        this.value = value;
        this.msgCode = msgCode;
    }
}
