package th.co.cana.framework.models;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * @author supot.jdev
 * @version 1.0
 */
@EqualsAndHashCode(of = {"id"})
@Data
public class DropdownModel implements Serializable {
    private String id;
    private String type;
    private String nameTh;
    private String nameEn;
    private String value;
    private String condition01;
    private String condition02;
    private String condition03;
    private String condition04;
    private String condition05;
}
