package th.co.cana.framework.service;

import org.springframework.stereotype.Service;
import th.co.cana.framework.models.Customer;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Service
public class CustomerService extends AbstractService {
    private final static Customer.Country[] countries;
    private final static Customer.Representative[] representatives;
    private final static String[] names;
    List<Customer> customers = new ArrayList<>();

    static {
        countries = new Customer.Country[]{
                new Customer.Country("Argentina", "ar")
                , new Customer.Country("Australia", "au")
                , new Customer.Country("Brazil", "br")
                , new Customer.Country("Canada", "ca")
                , new Customer.Country("Germany", "de")
                , new Customer.Country("France", "fr")
                , new Customer.Country("India", "in")
                , new Customer.Country("Italy", "it")
                , new Customer.Country("Japan", "jp")
                , new Customer.Country("Russia", "ru")
                , new Customer.Country("Spain", "es")
                , new Customer.Country("United Kingdom", "gb")};

        representatives = new Customer.Representative[]{new Customer.Representative("Amy Elsner", "amyelsner.png")
                , new Customer.Representative("Anna Fali", "annafali.png")
                , new Customer.Representative("Asiya Javayant", "asiyajavayant.png")
                , new Customer.Representative("Bernardo Dominic", "bernardodominic.png"),
                new Customer.Representative("Elwin Sharvill", "elwinsharvill.png"), new Customer.Representative("Ioni Bowcher", "ionibowcher.png"),
                new Customer.Representative("Ivan Magalhaes", "ivanmagalhaes.png"), new Customer.Representative("Onyama Limba", "onyamalimba.png"),
                new Customer.Representative("Stephen Shaw", "stephenshaw.png"), new Customer.Representative("Xuxue Feng", "xuxuefeng.png")};

        names = new String[]{"James","David","Jeanfrancois","Ivar","Tony","Adams","Claire","Costa","Juan","Maria","Jennifer","Stacey","Leja","Morrow",
                "Arvin","Darci","Izzy","Lionel","Clifford","Emily","Kadeem","Mujtaba","Aika","Mayumi","Misaki","Silvio","Nicolas","Antonio",
                "Deepesh","Aditya","Aruna","Jones","Julie","Smith","Johnson","Francesco","Salvatore","Kaitlin","Faith","Maisha","Jefferson",
                "Leon","Rodrigues","Alejandro","Munro","Cody","Chavez","Sinclair","Isabel","Octavia","Murillo","Greenwood","Wickens","Ashley"};
    }


    public List<Customer> getCustomers(int number) {
        List<Customer> customers = new ArrayList<>();
        for (int i = 0; i < number; i++) {
            customers.add(new Customer(i + 1000, getCustomerName(), getCountry(), getDate(), Customer.CustomerStatus.random(), getActivity(), getRepresentative()));
        }
        return customers;
    }

    private String getCustomerName() {
        String firstName = this.getName();
        String lastName;
        while((lastName = this.getName()).equals(firstName)) {
            firstName = this.getName();
        }
        return firstName + " " + lastName;
    }

    private String getName() {
        return names[(int) (Math.random() * names.length)];
    }

    private Customer.Country getCountry() {
        return countries[(int) (Math.random() * countries.length)];
    }

    private LocalDate getDate() {
        LocalDate now = LocalDate.now();
        long randomDay = ThreadLocalRandom.current().nextLong(now.minusDays(30).toEpochDay(), now.toEpochDay());
        return LocalDate.ofEpochDay(randomDay);
    }

    private int getActivity() {
        return (int) (Math.random() * 100);
    }

    private Customer.Representative getRepresentative() {
        return representatives[(int) (Math.random() * representatives.length)];
    }
}
