package th.co.cana.framework.jsf.component;

import jakarta.faces.component.UIComponent;
import jakarta.faces.context.FacesContext;
import jakarta.faces.context.ResponseWriter;
import org.primefaces.avalon.component.AvalonMenuRenderer;
import org.primefaces.component.api.AjaxSource;
import org.primefaces.component.menu.AbstractMenu;
import org.primefaces.component.submenu.UISubmenu;
import org.primefaces.expression.SearchExpressionUtils;
import org.primefaces.model.menu.MenuElement;
import org.primefaces.model.menu.MenuItem;
import org.primefaces.model.menu.Separator;
import org.primefaces.model.menu.Submenu;
import org.primefaces.util.AjaxRequestBuilder;

import java.io.IOException;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class CustomAvalonMenuRenderer extends AvalonMenuRenderer {

    @Override
    protected void encodeElement(FacesContext context, AbstractMenu menu, MenuElement element, boolean root) throws IOException {
        ResponseWriter writer = context.getResponseWriter();
        if (element.isRendered()) {
            String submenuClientId;
            String containerStyleClass;
            if (element instanceof MenuItem menuItem) {
                submenuClientId = menuItem instanceof UIComponent ? menuItem.getClientId() : menu.getClientId(context) + "_" + menuItem.getClientId();
                containerStyleClass = menuItem.getContainerStyleClass();
                writer.startElement("li", null);
                writer.writeAttribute("id", submenuClientId, null);
                writer.writeAttribute("role", "menuitem", null);
                if (containerStyleClass != null) {
                    writer.writeAttribute("class", containerStyleClass, null);
                }

                this.encodeMenuItem(context, menu, menuItem);
                writer.endElement("li");
            } else if (element instanceof Submenu submenu) {
                submenuClientId = submenu instanceof UIComponent ? ((UIComponent)submenu).getClientId() : menu.getClientId(context) + "_" + submenu.getId();
                containerStyleClass = submenu.getStyleClass();
                String className = root ? (containerStyleClass != null ? containerStyleClass + " layout-root-menuitem" : "layout-root-menuitem") : containerStyleClass;
                writer.startElement("li", null);
                writer.writeAttribute("id", submenuClientId, null);
                writer.writeAttribute("role", "menuitem", null);

                if (className != null) {
                    writer.writeAttribute("class", className, null);
                }

                this.encodeSubmenu(context, menu, submenu, root);
                writer.endElement("li");
            } else if (element instanceof Separator) {
                this.encodeSeparator(context, (Separator)element);
            }
        }

    }

    @Override
    protected void encodeSubmenu(FacesContext context, AbstractMenu menu, Submenu submenu, boolean root) throws IOException {
        ResponseWriter writer = context.getResponseWriter();
        String icon = submenu.getIcon();
        String label = submenu.getLabel();
        int childrenElementsCount = submenu.getElementsCount();
        if (root && label != null) {
            writer.startElement("div", null);
            writer.writeAttribute("class", "layout-menuitem-root-text", null);
            writer.writeText(label, null);
            writer.endElement("div");
        }

        writer.startElement("a", null);
        writer.writeAttribute("href", "#", null);
        writer.writeAttribute("class", "ripplelink", null);
        this.encodeItemIcon(context, icon);
        if (label != null) {
            writer.startElement("span", null);
            writer.writeText(label, null);
            writer.endElement("span");
            writer.startElement("span", null);
            writer.writeAttribute("class", "ink animate", null);
            writer.endElement("span");
            this.encodeToggleIcon(context, submenu, childrenElementsCount);
            if (submenu instanceof UISubmenu) {
                this.encodeBadge(context, ((UISubmenu)submenu).getAttributes().get("badge"));
            }
        }

        writer.endElement("a");
        if (childrenElementsCount > 0) {
            String style = submenu.getStyle();
            writer.startElement("ul", null);
            writer.writeAttribute("role", "menu", null);
            if (style != null && !style.isEmpty()) {
                writer.writeAttribute("style", style, null);
            }
            this.encodeElements(context, menu, submenu.getElements(), false);
            writer.endElement("ul");
        }
    }

    @Override
    protected String createAjaxRequest(FacesContext context, AjaxSource source, UIComponent form) {
        UIComponent component = (UIComponent) source;
        String clientId = component.getClientId(context);
        AjaxRequestBuilder builder = this.getAjaxRequestBuilder();
        builder.init().source(clientId).form(SearchExpressionUtils.resolveClientId(context, component, source.getForm()))
                .process(component, source.getProcess())
                .update(component, source.getUpdate())
                .async(source.isAsync())
                .global(source.isGlobal())
                .delay(source.getDelay())
                .timeout(source.getTimeout())
                .partialSubmit(source.isPartialSubmit(), source.isPartialSubmitSet(), source.getPartialSubmitFilter())
                .resetValues(source.isResetValues(), source.isResetValuesSet())
                .ignoreAutoUpdate(source.isIgnoreAutoUpdate())
                .onstart(source.getOnstart()).onerror(source.getOnerror())
                .onsuccess(source.getOnsuccess())
                .oncomplete(source.getOncomplete()).params(component);
        if (form != null) {
            builder.form(form.getClientId(context));
        }

        builder.preventDefault();
        return builder.build();
    }
}
