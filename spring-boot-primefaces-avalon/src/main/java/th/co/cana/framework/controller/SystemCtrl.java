package th.co.cana.framework.controller;

import io.github.jdevlibs.faces.FacesUtils;
import io.github.jdevlibs.utils.DateUtils;
import io.github.jdevlibs.utils.FileUtils;
import io.github.jdevlibs.utils.Validators;
import io.github.jdevlibs.utils.fotmat.FormatDate;
import jakarta.annotation.PostConstruct;
import jakarta.faces.view.ViewScoped;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import lombok.Data;
import lombok.EqualsAndHashCode;
import th.co.cana.framework.config.properties.ApplicationProperties;
import th.co.cana.framework.models.AppBuildInfo;
import th.co.cana.framework.models.FileInfo;
import th.co.cana.framework.models.PropertyInfo;
import th.co.cana.framework.utils.AppConstants;

import java.io.File;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Named
@ViewScoped
@Data
@EqualsAndHashCode(callSuper = false)
public class SystemCtrl extends AbstractCtrl {

    @Inject
    private ApplicationProperties properties;

    private AppBuildInfo form;
    private List<FileInfo> logItems;
    private List<FileInfo> filteredLogs;
    private List<PropertyInfo> propertyItems;
    private List<PropertyInfo> filteredProperties;
    private int tabindex;

    @PostConstruct
    public void initial() {
        logger.info("Starting initial controller : {}", getClass());
        addStatusLoading(AppConstants.UISystems.PAGE_SYS_FORM, false);
        this.setBuildInformation();
    }

    public void loadPageAction() {
        logger.info("Starting loading form data");
        try {
            logger.info("Step 1. Set loading status ");
            addStatusLoading(AppConstants.UISystems.PAGE_SYS_FORM, false);

            logger.info("Step 2. Load data ");
            loadSystemPropertyAction();
            loadLogAction();

        } catch (Exception ex) {
            logger.error("loadPageAction", ex);
            showDialogError(ex);
        } finally {
            addStatusLoading(AppConstants.UISystems.PAGE_SYS_FORM, true);
        }
    }

    public void loadLogAction() {
        try {
            logItems = new ArrayList<>();
            List<File> files = FileUtils.listFileToList(AppConstants.logDir(), true);
            FormatDate dateFm = new FormatDate(DateUtils.FM_DATE_TIME);
            if (Validators.isNotEmpty(files)) {
                int index = 1;
                for (File file : files) {
                    FileInfo fileInfo = new FileInfo();
                    fileInfo.setId(String.valueOf(index++));
                    fileInfo.setFileName(file.getName());
                    fileInfo.setFilePath(file.getPath());
                    fileInfo.setLastModified(toLocalDateTime(file.lastModified()));
                    fileInfo.setFileSize(file.length());
                    fileInfo.setFileDate(dateFm.format(fileInfo.getLastModified()));
                    logItems.add(fileInfo);
                }
            }
        } catch (Exception ex) {
            logger.error("loadLogAction", ex);
        }
    }

    public void loadSystemPropertyAction() {
        try {
            propertyItems = new ArrayList<>();
            Properties pro = System.getProperties();
            for(Map.Entry<Object, Object> map : pro.entrySet()) {
                if ("java.class.path".equalsIgnoreCase(map.getKey().toString())) {
                    continue;
                }
                PropertyInfo proValue = new PropertyInfo();
                proValue.setType(0);
                proValue.setKey(map.getKey().toString());
                proValue.setValue(map.getValue().toString());
                propertyItems.add(proValue);
            }

            Map<String, String> maps = System.getenv();
            if (Validators.isNotEmpty(maps)) {
                for (Map.Entry<String, String> map : maps.entrySet()) {
                    if ("java.class.path".equalsIgnoreCase(map.getKey())) {
                        continue;
                    }
                    PropertyInfo proValue = new PropertyInfo();
                    proValue.setType(1);
                    proValue.setKey(map.getKey());
                    proValue.setValue(map.getValue());
                    propertyItems.add(proValue);
                }
            }

            Comparator<PropertyInfo> comp = Comparator.comparing(PropertyInfo::getKey);
            propertyItems.sort(comp);
            AtomicInteger index = new AtomicInteger(1);
            propertyItems.forEach(en -> en.setId(String.valueOf(index.getAndIncrement())));
        } catch (Exception ex) {
            logger.error("loadSystemPropertyAction", ex);
        }
    }

    public void deleteFileAction(FileInfo bean) {
        try {
            logger.info("Delete file : {}", bean.getFilePath());
            FileUtils.delete(bean.getFilePath());

            loadLogAction();
        } catch (Exception ex) {
            logger.error("deleteFileAction", ex);
        }
    }

    public void downloadFileAction(FileInfo bean) {
        try {
            logger.info("Download file : {}", bean.getFilePath());
            File file = new File(bean.getFilePath());
            if (file.exists()) {
                FacesUtils.downloadFile(file, file.getName());
            } else {
                showDialogError(getMessage("msg.download.file.not.found"));
            }
        } catch (Exception ex) {
            logger.error("downloadFileAction", ex);
        }
    }

    public boolean globalFilterPropertyFunction(Object value, Object filter, Locale locale) {
        try {
            String filterText = (filter == null) ? null : filter.toString().trim().toLowerCase();
            if (Validators.isEmpty(filterText)) {
                return true;
            }

            PropertyInfo bean = (PropertyInfo) value;
            return bean.getKey().toLowerCase().contains(filterText)
                    || bean.getValue().toLowerCase().contains(filterText);
        } catch (Exception ex) {
            logger.error("globalFilterPropertyFunction", ex);
        }

        return true;
    }

    public boolean globalFilterLogFunction(Object value, Object filter, Locale locale) {
        try {
            String filterText = (filter == null) ? null : filter.toString().trim().toLowerCase();
            if (Validators.isEmpty(filterText)) {
                return true;
            }

            FileInfo bean = (FileInfo) value;
            return bean.getFileName().toLowerCase().contains(filterText)
                    || bean.getFileDate().toLowerCase().contains(filterText);
        } catch (Exception ex) {
            logger.error("globalFilterLogFunction", ex);
        }

        return true;
    }

    private void setBuildInformation() {
        form = new AppBuildInfo();
        form.setName(properties.getName());
        form.setVersion(properties.getVersion());
        form.setBuildTime(properties.getBuildTime());
        form.setJavaBuildVersion(properties.getJavaBuildVersion());
        form.setSystemDate(LocalDateTime.now().toString());
        form.setSystemLocale(Locale.getDefault().toString());
    }

    private LocalDateTime toLocalDateTime(long timestamp) {
        try {
            if (timestamp <= 0) {
                return null;
            }

            return LocalDateTime.ofInstant(Instant.ofEpochMilli(timestamp),
                    TimeZone.getDefault().toZoneId());
        } catch (Exception ex) {
            return null;
        }
    }
}
