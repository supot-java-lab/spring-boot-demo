package th.co.cana.framework.utils;


import io.github.jdevlibs.utils.Convertors;
import io.github.jdevlibs.utils.Validators;
import th.co.cana.framework.config.enums.DropDownTypes;
import th.co.cana.framework.config.enums.ParamNames;
import th.co.cana.framework.models.DropdownModel;
import th.co.cana.framework.models.ParameterModel;

import java.math.BigDecimal;
import java.util.*;

/**
 * @author supot.jdev
 * @version 1.0
 */
public final class CacheUtils {

    private static final String SPLIT = ",";
    private static Map<String, ParameterModel> parameterMap = null;
    private static Map<String, List<DropdownModel>> dropdownMap = null;
    private CacheUtils() {

    }

    public static Map<String, ParameterModel> getParameterMap() {
        return Collections.unmodifiableMap(parameterMap);
    }

    public static void setParameterMap(Map<String, ParameterModel> parameterMap) {
        CacheUtils.parameterMap = parameterMap;
    }

    public static Map<String, List<DropdownModel>> getDropdownMap() {
        return Collections.unmodifiableMap(dropdownMap);
    }

    public static void setDropdownMap(Map<String, List<DropdownModel>> dropdownMap) {
        CacheUtils.dropdownMap = dropdownMap;
    }

    /** ++++++++++++++++++++++++++++++++++ LOV ++++++++++++++++++++++++++++++++++ */
    public static List<DropdownModel> getDropdowns(DropDownTypes type) {
        if (type == null || Validators.isEmpty(dropdownMap)) {
            return Collections.emptyList();
        }
        return getDropdowns(type.getTypeName());
    }

    public static List<DropdownModel> getDropdowns(String name) {
        if (Validators.isEmpty(name) || Validators.isEmpty(dropdownMap)) {
            return Collections.emptyList();
        }

        List<DropdownModel> results = dropdownMap.get(name);
        if (Validators.isNull(results)) {
            results = Collections.emptyList();
        }

        return results;
    }

    public static String getName(DropDownTypes type, String key) {
        return getNameTh(type, key);
    }

    public static String getNameTh(DropDownTypes type, String key) {
        return getName(type, key, AppConstants.LANG_TH);
    }

    public static String getNameEn(DropDownTypes type, String key) {
        return getName(type, key, AppConstants.LANG_US);
    }

    public static String getFirstValue(DropDownTypes type) {
        List<DropdownModel> results = getDropdowns(type);
        if (Validators.isNotEmpty(results)) {
            return results.get(0).getValue();
        } else {
            return null;
        }
    }

    public static DropdownModel getDropdown(DropDownTypes type, String key) {
        if (Validators.isNull(type) || Validators.isEmpty(key)) {
            return null;
        }

        List<DropdownModel> results = getDropdowns(type);
        if (Validators.isEmpty(results)) {
            return null;
        }

        for (DropdownModel en : results) {
            if (key.equalsIgnoreCase(en.getValue())) {
                return en;
            }
        }

        return null;
    }

    private static String getName(DropDownTypes type, String key, String lan) {
        if (Validators.isNull(type) || Validators.isEmpty(key)) {
            return "";
        }

        DropdownModel value = getDropdown(type, key);
        if (value == null) {
            return "";
        }

        return isLangThai(lan) ? value.getNameTh() : value.getNameEn();
    }

    /** ++++++++++++++++++++++++++++++++++ Parameter ++++++++++++++++++++++++++++++++++ */
    public static String getValue(ParamNames name) {
        return getValue(name, null);
    }

    public static String getValue(ParamNames name, String defValue) {
        if (name == null) {
            return defValue;
        }
        return getParamValue(name.getName());
    }

    public static String[] getValues(ParamNames name) {
        String value = getValue(name);
        if (value == null || value.isEmpty()) {
            return new String[] {};
        }

        String[] values = value.split(SPLIT);
        if (Validators.isNull(values)) {
            return new String[] {};
        }

        for (int i = 0; i < values.length; i++) {
            if (Validators.isNull(values[i])) {
                continue;
            }
            values[i] = values[i].trim();
        }

        return values;
    }

    public static Map<String, String> getParamToMaps(ParamNames name) {
        String[] values = getValues(name);
        Map<String, String> maps = new HashMap<>(values.length);
        for (String str : values) {
            if (Validators.isEmpty(str)) {
                continue;
            }
            maps.put(str, str);
        }

        return maps;
    }

    public static Map<String, String> getParamToMapIgnoreCase(ParamNames name) {
        String[] values = getValues(name);
        Map<String, String> maps = new HashMap<>(values.length);
        for (String str : values) {
            if (Validators.isEmpty(str)) {
                continue;
            }
            maps.put(str.toLowerCase(), null);
        }

        return maps;
    }

    public static List<String> getParamToList(ParamNames name) {
        return name == null ? Collections.emptyList() : getParamToList(name.getName());
    }

    public static List<String> getParamToList(String name) {
        String value = getParamValue(name);
        if (value == null || value.isEmpty()) {
            return Collections.emptyList();
        }

        String[] values = value.split(SPLIT);
        if (Validators.isNull(values)) {
            return Collections.emptyList();
        }

        List<String> items = new ArrayList<>(values.length);
        for (String str : values) {
            if (Validators.isNotEmpty(str)) {
                items.add(str.trim());
            }
        }

        return items;
    }

    public static Integer getIntegerValue(ParamNames name) {
        return getIntegerValue(name, 0);
    }

    public static Integer getIntegerValue(ParamNames name, Integer defVal) {
        return Convertors.toInteger(getValue(name), defVal);
    }

    public static Long getLongValue(ParamNames name) {
        return getLongValue(name, 0L);
    }

    public static Long getLongValue(ParamNames name, Long defVal) {
        return Convertors.toLong(getValue(name), defVal);
    }

    public static Double getDoubleValue(ParamNames name) {
        return getDoubleValue(name, 0D);
    }

    public static Double getDoubleValue(ParamNames name, Double defVal) {
        return Convertors.toDouble(getValue(name), defVal);
    }

    public static BigDecimal getDecimalValue(ParamNames paramName) {
        return getDecimalValue(paramName, BigDecimal.ZERO);
    }

    public static BigDecimal getDecimalValue(ParamNames name, BigDecimal defVal) {
        return Convertors.toBigDecimal(getValue(name), defVal);
    }

    private static String getParamValue(String paramName) {
        if (Validators.isEmpty(paramName)) {
            return null;
        }

        if (Validators.isEmpty(parameterMap)) {
            return null;
        }

        ParameterModel parameter = parameterMap.get(paramName);
        if (parameter == null) {
            return null;
        }

        return parameter.getValue();
    }

    private static boolean isLangThai(String lang) {
        return "th".equalsIgnoreCase(lang) || "th_TH".equalsIgnoreCase(lang);
    }
}
