package th.co.cana.framework.config.enums;

import lombok.Getter;

/**
 * Error message reference to: resources/i18n/messages.properties
 * @author supot.jdev
 * @version 1.0
 */
@Getter
public enum ErrorCodes {
    //Database
    CODE_DB_ERROR("0000", "error.db.other"),
    CODE_DB_CONNECTION("1000", "error.db.connect.fail"),
    CODE_DB_INSERT("1001", "error.db.insert"),
    CODE_DB_UPDATE("1002", "error.db.update"),
    CODE_DB_DELETE("1003", "error.db.delete"),
    CODE_DB_QUERY("1004", "error.db.query"),
    CODE_DB_UNIQUE("1005", "error.db.unique.constraint"),
    CODE_DB_EMPTY("1006", "error.db.empty.result"),
    CODE_DB_LARGER("1007", "error.db.value.larger.column"),
    CODE_DB_NULL("1008", "error.db.value.null.column"),
    CODE_DB_OTHER("1009", "error.db.other"),

    CODE_EXCEPTION("9999", "error.exception.other"),;

    final String code;
    final String message;
    ErrorCodes(String code, String message){
        this.code = code;
        this.message = message;
    }

}
