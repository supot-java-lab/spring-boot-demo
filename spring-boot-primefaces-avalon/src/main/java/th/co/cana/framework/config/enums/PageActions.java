package th.co.cana.framework.config.enums;

import lombok.Getter;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Getter
public enum PageActions {
    INITIAL("msg.page.action.initial"),
    CREATE("msg.page.action.create"),
    EDIT("msg.page.action.edit"),
    VIEW("msg.page.action.view"),
    DELETE("msg.page.action.delete"),
    PRINT("msg.page.action.print");

    private final String key;
    PageActions(String key) {
        this.key = key;
    }
}