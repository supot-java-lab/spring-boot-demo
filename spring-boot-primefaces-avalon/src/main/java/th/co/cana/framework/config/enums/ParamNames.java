package th.co.cana.framework.config.enums;

import lombok.Getter;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Getter
public enum ParamNames {
    DEFAULT_CHAR_PER_PIXEL("DEFAULT_CHAR_PER_PIXEL"),
    DEFAULT_MENU_ICON("DEFAULT_MENU_ICON"),
    DEFAULT_MENU_WIDTH("DEFAULT_MENU_WIDTH"),
    DEFAULT_UPLOAD_FILE_SIZE("DEFAULT_UPLOAD_FILE_SIZE");

    private final String name;
    ParamNames(String name) {
        this.name= name;
    }
}