package th.co.cana.framework.jsf.bean;

import io.github.jdevlibs.faces.FacesMessages;
import io.github.jdevlibs.faces.FacesUtils;
import io.github.jdevlibs.primefaces.PFUtils;
import io.github.jdevlibs.primefaces.theme.ThemeComponent;
import io.github.jdevlibs.primefaces.theme.ThemeLayout;
import io.github.jdevlibs.primefaces.theme.ThemeLayoutSpecial;
import io.github.jdevlibs.primefaces.theme.ThemeUtils;
import io.github.jdevlibs.utils.Validators;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Named;
import lombok.Data;
import th.co.cana.framework.config.enums.ErrorTypes;
import th.co.cana.framework.utils.AppConstants;

import java.io.Serializable;
import java.util.*;

/**
 * @author supot.jdev
 * @version 1.0
 */

@Named
@SessionScoped
@Data
public class WebPreferences implements Serializable {
    private Map<String, String> themeColors;
    private String theme = "blue";
    private String layout = "blue";
    private String menuClass = null;
    private String profileMode = "inline";
    private String menuLayout = "horizontal";
    private String inputStyle = "outlined";

    private List<ThemeComponent> componentThemes;
    private List<ThemeLayout> layoutThemes;
    private List<ThemeLayoutSpecial> layoutSpecialThemes;

    //Other
    private ErrorTypes type;
    private List<String> errors;
    private List<String> jsCommands;
    private boolean showDialog;
    private Locale locale = AppConstants.TH;

    @PostConstruct
    public void init() {
        themeColors = new HashMap<>();
        themeColors.put("blue", "#03A9F4");
        componentThemes = ThemeUtils.avalonComponents();
        layoutThemes = ThemeUtils.avalonLayouts();
        layoutSpecialThemes = ThemeUtils.avalonLayoutSpecials();
        FacesUtils.setLocale(locale);
    }

    public void onLayoutChange() {
        PFUtils.execute("PrimeFaces.AvalonConfigurator.changeMenuLayout('" + menuLayout + "')");
    }

    public void onMenuThemeChange() {
        if ("layout-menu-dark".equals(menuClass)) {
            PFUtils.execute("PrimeFaces.AvalonConfigurator.changeMenuToDark()");
        } else {
            PFUtils.execute("PrimeFaces.AvalonConfigurator.changeMenuToLight()");
        }
    }

    public String getMenu() {
        return switch (this.menuLayout) {
            case "overlay" -> "menu-layout-overlay";
            case "horizontal" -> {
                this.profileMode = "overlay";
                yield "menu-layout-static menu-layout-horizontal";
            }
            case "slim" -> "menu-layout-static menu-layout-slim";
            default -> "menu-layout-static";
        };
    }

    public void setMenuLayout(String menuLayout) {
        if (Validators.isNotEmpty(menuLayout) && menuLayout.equals("horizontal")) {
            this.profileMode = "overlay";
        }
        this.menuLayout = menuLayout;
    }

    public void setLayout(String layout, boolean special) {
        this.layout = layout;
        if (special) {
            this.menuClass = "layout-menu-dark";
        }
    }

    public String getInputStyleClass() {
        return this.inputStyle.equals("filled") ? "ui-input-filled" : "";
    }

    public String getThemeName() {
        return "avalon-" + this.theme;
    }

    public Locale getLocale() {
        if (locale == null) {
            locale = FacesUtils.getLocale();
        }
        return locale;
    }

    public boolean isProfileModeOverlay() {
        return "overlay".equalsIgnoreCase(profileMode)
                || "menu-layout-static menu-layout-horizontal".equalsIgnoreCase(menuLayout);
    }

    public boolean isProfileModeInline() {
        return "inline".equalsIgnoreCase(profileMode)
                && !"menu-layout-static menu-layout-horizontal".equalsIgnoreCase(menuLayout);
    }

    //++++++++++++++++++++++++++ Error & Command +++++++++++++++++++++++++++++++++
    public ErrorTypes getType() {
        if (type == null) {
            type = ErrorTypes.INFO;
        }
        return type;
    }

    public void executeCommand() {
        if (Validators.isEmpty(jsCommands)) {
            resetError();
            return;
        }

        for (String command : jsCommands) {
            if (Validators.isNotEmpty(command)) {
                PFUtils.execute(command);
            }
        }
        resetError();
    }

    public void add(String... error) {
        if (Validators.isEmpty(error)) {
            return;
        }

        if (Validators.isNull(errors)) {
            errors = new ArrayList<>();
        }
        errors.addAll(Arrays.asList(error));
    }

    public void addJsCommand(String ... jsCommand) {
        if (Validators.isEmpty(jsCommand)) {
            return;
        }

        if (Validators.isNull(jsCommands)) {
            jsCommands = new ArrayList<>();
        }
        jsCommands.addAll(Arrays.asList(jsCommand));
    }

    public void resetError() {
        type = null;
        errors = null;
        jsCommands = null;
    }

    public boolean isEmptyError() {
        return (errors == null || errors.isEmpty());
    }

    public String getHeader() {
        return FacesMessages.getMessage(getType().getHeader());
    }

    public String getIconCss() {
        return getType().getIconCss();
    }

    public String getStyleClass() {
        if (getType() == ErrorTypes.ERROR) {
            return "text-red-500";
        } else if (getType() == ErrorTypes.WARNING) {
            return "text-orange-300";
        }

        return "text-blue-100";
    }

    public boolean isErrorType() {
        return (ErrorTypes.ERROR == type);
    }

    public boolean isOneItem() {
        return (errors != null && errors.size() == 1);
    }

    public boolean isLangThai() {
        return FacesUtils.isLangThai();
    }

    public boolean isValidationFailed() {
        return (FacesUtils.getMaximumSeverity() != null || FacesUtils.isValidationFailed())
                && !FacesUtils.isEmptyError() && isEmptyError();
    }
}
