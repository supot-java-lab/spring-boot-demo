package th.co.cana.framework.jsf.bean;

import java.io.Serializable;

import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Named;
import lombok.Data;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
@Named
@SessionScoped
public class UserInfo implements Serializable {

    private String userId;
    private String username;
    private String email;
    private String firstName;
    private String lastName;

    public boolean isUserNotLogin() {
        return (username == null || username.isEmpty()) && (userId == null || userId.isEmpty());
    }
}