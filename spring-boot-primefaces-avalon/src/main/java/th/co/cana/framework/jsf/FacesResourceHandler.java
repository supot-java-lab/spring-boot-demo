package th.co.cana.framework.jsf;

import io.github.jdevlibs.utils.Validators;
import jakarta.faces.application.Resource;
import jakarta.faces.application.ResourceHandler;
import jakarta.faces.application.ResourceHandlerWrapper;
import org.primefaces.util.Constants;

import java.util.Arrays;
import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */
public class FacesResourceHandler extends ResourceHandlerWrapper {
    private static final List<String> CONTENT_TYPES = Arrays.asList("text/css", "image/png", "image/svg+xml"
            , "image/webp", "image/jpeg", "image/jpeg", "image/gif", "application/x-javascript" , "text/javascript");

    public FacesResourceHandler(ResourceHandler wrapped) {
        super(wrapped);
    }

    @Override
    public Resource createResource(String resourceName, String libraryName) {
        Resource resource = super.createResource(resourceName, libraryName);
        return wrapResource(resource, libraryName);
    }

    @Override
    public Resource createResource(String resourceName, String libraryName, String contentType) {
        Resource resource = super.createResource(resourceName, libraryName, contentType);
        return wrapResource(resource, libraryName);
    }

    private Resource wrapResource(Resource resource, String libraryName) {
        if (resource != null) {
            if (Validators.isNotEmpty(libraryName) && libraryName.toLowerCase().startsWith(Constants.LIBRARY)) {
                return new FacesResourceWrapper(resource);
            }

            if (Validators.isNotEmpty(resource.getContentType())
                    && CONTENT_TYPES.contains(resource.getContentType().toLowerCase())) {
                return new FacesResourceWrapper(resource);
            }
            return resource;
        }
        return null;
    }
}
