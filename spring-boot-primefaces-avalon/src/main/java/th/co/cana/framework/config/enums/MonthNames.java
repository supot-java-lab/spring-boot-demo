package th.co.cana.framework.config.enums;

import lombok.Getter;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Getter
public enum MonthNames {
    JANUARY("1", "msg.month.01"),
    FEBRUARY("2", "msg.month.02"),
    MARCH("3", "msg.month.03"),
    APRIL("4", "msg.month.04"),
    MAY("5", "msg.month.05"),
    JUNE("6", "msg.month.06"),
    JULY("7", "msg.month.07"),
    AUGUST("8", "msg.month.08"),
    SEPTEMBER("9", "msg.month.09"),
    OCTOBER("10", "msg.month.10"),
    NOVEMBER("11", "msg.month.11"),
    DECEMBER("12", "msg.month.12");

    private final String value;
    private final String msgCode;
    MonthNames(String value, String msgCode) {
        this.value = value;
        this.msgCode = msgCode;
    }
}
