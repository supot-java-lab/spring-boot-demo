package th.co.cana.framework.utils;

import io.github.jdevlibs.utils.Validators;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.text.StringEscapeUtils;

import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;

/**
 * @author supot.jdev
 * @version 1.0
 */
public final class Base64Utils {
    private static final String IMAGE_BASE64 = "data:image/{0};base64,";
    private static final Base64 BASE64 = new Base64();

    private Base64Utils() {

    }

    public static String encode(String value) {
        try {
            if (Validators.isEmpty(value)) {
                return null;
            }
            return new String(BASE64.encode(value.getBytes(StandardCharsets.UTF_8)), StandardCharsets.UTF_8);
        } catch (Exception ex) {
            return null;
        }
    }

    public static String encode(byte[] values) {
        try {
            if (Validators.isEmpty(values)) {
                return null;
            }
            return new String(BASE64.encode(values), StandardCharsets.UTF_8);
        } catch (Exception ex) {
            return null;
        }
    }

    public static String decode(String value) {
        try {
            if (Validators.isEmpty(value)) {
                return null;
            }
            return new String(BASE64.decode(value.getBytes(StandardCharsets.UTF_8)), StandardCharsets.UTF_8);
        } catch (Exception ex) {
            return null;
        }
    }

    public static String unescapeHtml4(String value) {
        return StringEscapeUtils.unescapeHtml4(value);
    }

    public static String encodeImageBase64(byte[] contents, String ext) {
        if (Validators.isNull(contents) || Validators.isEmpty(ext)) {
            return null;
        }

        return MessageFormat.format(IMAGE_BASE64, ext) + encode(contents);
    }
}
