package th.co.cana.framework.servlet;

import jakarta.servlet.ServletContext;
import lombok.extern.slf4j.Slf4j;
import org.ocpsoft.rewrite.annotation.RewriteConfiguration;
import org.ocpsoft.rewrite.config.Configuration;
import org.ocpsoft.rewrite.config.ConfigurationBuilder;
import org.ocpsoft.rewrite.servlet.config.HttpConfigurationProvider;
import org.ocpsoft.rewrite.servlet.config.rule.Join;
import th.co.cana.framework.utils.PageMapping;

import java.util.Map;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Slf4j
@RewriteConfiguration
public class RewriteConfigProvider extends HttpConfigurationProvider {
    @Override
    public Configuration getConfiguration(ServletContext context) {
        log.info("********** [CONFIG] Starting mapping .xhtml path to rewrite url **********");

        ConfigurationBuilder builder = ConfigurationBuilder.begin();
        Map<String, String> mappings = PageMapping.getMenuMappings();
        for (Map.Entry<String, String> map : mappings.entrySet()) {
            log.info("Mapping path : {} => : {}", map.getValue(), map.getKey());
            builder.addRule(Join.path(map.getKey()).to(map.getValue()));
        }

        log.info("********** [CONFIG] Finished mapping .xhtml path to rewrite url **********");
        return builder;
    }

    @Override
    public int priority() {
        return 10;
    }
}
