package th.co.cana.framework.controller;

import io.github.jdevlibs.faces.FacesUtils;
import io.github.jdevlibs.utils.Validators;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Named;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import th.co.cana.framework.config.enums.PageNames;
import th.co.cana.framework.utils.PageMapping;

import java.io.Serializable;
import java.util.Map;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Slf4j
@Data
@Named
@SessionScoped
public class MenuCtrl implements Serializable {
    //private static final String ACTION_METHOD 	= "#{menuCtrl.menuAction('%s')}";
    //private static final String MENU_ICON       = "fa-solid fa-bars";

    private String activeMenu;
    private String menuIcon;

    @PostConstruct
    public void initial() {
        log.info("Starting initial controller : {}", getClass().getName());
    }

    public String loginAction() {
        FacesUtils.invalidateSession();
        return redirectPage(PageMapping.PAGE_LOGIN);
    }

    public String menuAction(String menuId) {
        String viewId = FacesUtils.getCurrentViewId();
        log.info("Change view {} to menu : {}", viewId, menuId);
        try {
            if (Validators.isEmpty(menuId)) {
                log.error("Empty menu id cannot get view");
                return null;
            }

            //TODO: wait for implement dynamic menu

            this.clearSession();
            return FacesUtils.redirectToOutcome(menuId);
        } catch (Exception ex) {
            log.error("menuAction", ex);
        }

        return null;
    }

    public String changePageAction(PageNames page) {
        if (page == null) {
            return null;
        }

        clearSession();
        return redirectPage(page.getPath());
    }

    public String gotoPageAction(String page) {
        if (Validators.isEmpty(page)) {
            return null;
        }

        clearSession();
        return redirectPage(page);
    }

    public String redirectPage(String page) {
        if (Validators.isEmpty(page)) {
            log.warn("Empty page for redirect...");
            return null;
        }

        log.info("Send redirect to view : {}", page);
        return FacesUtils.redirectToOutcome(page);
    }

    public void grantRoleAccess(PageNames page) {
        try {
            if (FacesUtils.isPostback()) {
                return;
            }

            boolean access = isRendered(page);
            if (!access) {
                log.error("Cannot access this page : {}", page.getPath());
                FacesUtils.redirectToPage(PageMapping.URL_ERROR_401);
            }
        } catch (Exception ex) {
            log.error("grantRoleAccess", ex);
        }
    }

    public boolean isRendered(PageNames page) {
        return page != null;
    }

    private void clearSession() {
        try {
            Map<String, Object> sessionMap = FacesUtils.getSessionMap();
            if (Validators.isEmpty(sessionMap)) {
                return;
            }

            log.debug("Total session object : {}", sessionMap.size());
            for (Map.Entry<String, Object> map : sessionMap.entrySet()) {
                log.info("Session Key[{}] -> {}", map.getKey(), map.getValue().getClass());
                if (map.getValue() instanceof AbstractCtrl) {
                    log.info("\t Session type is Controller[Controller] remove : {}", map.getValue().getClass().getName());
                    sessionMap.remove(map.getKey());
                }
            }
        } catch (Exception ex) {
            log.error("clearSession", ex);
        }
    }
}
