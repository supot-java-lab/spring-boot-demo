package th.co.cana.framework.controller;

import io.github.jdevlibs.faces.FacesUtils;
import io.github.jdevlibs.primefaces.PFUtils;
import jakarta.annotation.PostConstruct;
import jakarta.faces.view.ViewScoped;
import jakarta.inject.Named;
import lombok.Getter;
import lombok.Setter;
import th.co.cana.framework.utils.AppConstants;
import th.co.cana.framework.utils.PageMapping;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Getter
@Setter
@Named
@ViewScoped
public class LoginCtrl extends AbstractCtrl {
    private String username;
    private String password;
    private int screenWidth;
    private int screenHeight;

    @PostConstruct
    public void init() {
        logger.info("Starting controller: {}", getClass().getName());
    }

    public String loginAction() {
        try {
            logger.info("Starting login action");

            getWebPreferences().setLocale(AppConstants.TH);
            FacesUtils.setLocale(AppConstants.TH);
            return redirectToOutcome(PageMapping.PAGE_SYSTEM_INFO);
        } catch (Exception ex) {
            PFUtils.showMessageError(getMessage("msg.popup.title.error"), getMessage("msg.page.500.message"));
            logger.error("loginAction", ex);
        }
        return null;
    }

    public String logoutAction() {
        FacesUtils.invalidateSession();
        return redirectToOutcome(PageMapping.PAGE_LOGIN);
    }
}
