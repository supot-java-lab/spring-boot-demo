/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.docker.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.info.BuildProperties;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import th.co.cana.docker.model.AppInfo;

/**
* @author supot
* @version 1.0
*/

@RestController
public class DockerCtrl extends Controller {

	@Autowired
	private BuildProperties buildTag;
	
	@GetMapping("/")
	public String index() {
		logger.info("Starting docker controller...");
		return "Hello : " + new Date();
	}
	
	@GetMapping("/info")
	public ResponseEntity<AppInfo> appInfo() {
		AppInfo app = AppInfo.builder()
				.id("A1001")
				.name(buildTag.getName())
				.version(buildTag.getVersion())
				.time(buildTag.getTime().toString())
				.javaVersion(buildTag.get("java.version"))
				.build();
		
		return ResponseEntity.ok(app);
	}
}
