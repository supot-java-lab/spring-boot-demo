/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.docker.model;

import java.io.Serializable;

import lombok.Builder;
import lombok.Data;

/**
* @author supot
* @version 1.0
*/

@Data
@Builder
public class AppInfo implements Serializable {
	private static final long serialVersionUID = 1L;

	private String id;
	private String name;
	private String version;
	private String time;
	private String javaVersion;
}
