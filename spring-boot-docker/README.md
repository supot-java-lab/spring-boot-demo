# spring-boot-docker
Demo project for Spring Boot and build docker image

### 1. Package project 
	mvn package
### 2. Build docker image
	docker build -t spring-docker .
### 3. Check docker image
	docker images
### 4. Run docker image 
	docker run -p 8080:8080 spring-docker
### 5. Access web
	http://localhost:8080/info
### 6. Run with Maven Docker Image
	docker run -it --rm --name spring-docker \
	-v "$PWD":/usr/src/app -v "$HOME"/.m2:/root/.m2 \
	-w /usr/src/app -d -p 9000:8080 maven:3.3-jdk-8 mvn clean spring-boot:run
[Maven Docker More info](https://hub.docker.com/_/maven/)