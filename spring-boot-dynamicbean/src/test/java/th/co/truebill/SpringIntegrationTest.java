/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.truebill;

import org.springframework.boot.test.context.SpringBootTest;

/**
* @author supot
* @version 1.0
*/

@SpringBootTest
public abstract class SpringIntegrationTest extends SpringUnitTest {

}
