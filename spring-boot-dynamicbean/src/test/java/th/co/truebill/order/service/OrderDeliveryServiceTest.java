/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.truebill.order.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import th.co.truebill.SpringIntegrationTest;
import th.co.truebill.SpringUnitTest;

/**
* @author supot
* @version 1.0
*/
@SuppressWarnings("unused")
class OrderDeliveryServiceTest extends SpringIntegrationTest {

	@Autowired
	private OrderDeliveryService service;
	
	@Test
	void testOrder() throws Exception {
		service.processOrder();
	}
}
