/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.truebill.main;

import th.co.truebill.core.rule.BaseCondition;
import th.co.truebill.core.rule.ConditionNames;
import th.co.truebill.exception.JsonException;
import th.co.truebill.model.RestResponse;
import th.co.truebill.model.redis.ChangeBillingFormat;
import th.co.truebill.util.JsonUtils;

/**
* @author supot
* @version 1.0
*/
public class TestJsonUtils {

	public static void main(String[] args) throws JsonException {
		ChangeBillingFormat model = new ChangeBillingFormat();
		model.setBan(123456L);
		model.setBcban(23446L);
		model.setBillMedia("S");
		model.setReqBillMedia("PS");
		
		BaseCondition condition = new BaseCondition();
		condition.setName(ConditionNames.ACTIVE_BAN);
		condition.addParameter("days", 40);
		condition.addParameter("name", "Supot");			
		model.addCondition(condition);
		
		condition = new BaseCondition();
		condition.setName(ConditionNames.NO_PAYMENT);
		condition.addParameter("days", 40);		
		model.addCondition(condition);
		
		String json = JsonUtils.json(model, true);
		System.out.println(json);
		
		model = JsonUtils.model(json, ChangeBillingFormat.class);
		System.out.println(model);
		
		RestResponse<ChangeBillingFormat> str = new RestResponse<>();
		str.setData(model);
		json = JsonUtils.json(str, true);
		System.out.println(json);
		str = JsonUtils.model(json, RestResponse.class);
		System.out.println(str);
	}

}
