/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.truebill.main;

import org.mapstruct.factory.Mappers;

import th.co.truebill.mapper.OrderDeliveryMapper;
import th.co.truebill.mapper.OrderDeliveryObjectMapper;
import th.co.truebill.model.redis.ChangeBillingFormat;
import th.co.truebill.model.req.OMXOrderReq;

/**
 * @author supot
 * @version 1.0
 */
public class TestOrderDeliveryMapper {

	public static void main(String[] args) {
		OrderDeliveryMapper mapper = Mappers.getMapper(OrderDeliveryMapper.class);

		ChangeBillingFormat source = new ChangeBillingFormat();
		source.setBan(12345L);
		source.setOrderId("ORDER_ID");
		source.setBillMedia("S");
		source.setReqBillMedia("PS");

		OMXOrderReq req = mapper.billingFormatToOrder(source);
		System.out.println(req);
		
		OrderDeliveryObjectMapper objMapper = Mappers.getMapper(OrderDeliveryObjectMapper.class);
		req = objMapper.billingFormatToOrder(source);
		System.out.println(req);
	}

}
