/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.truebill.model.redis;

import java.util.ArrayList;
import java.util.List;

import th.co.truebill.core.rule.BaseCondition;

/**
 * @author supot
 * @version 1.0
 */
public class ChangeBillingFormat {	
	private Long ban;
	private Long bcban;
	private Long masterBan;
	private Integer cycleCode;
	private Integer cycleMonth;
	private Integer cycleYear;
	private Integer currentCycleCode;
	private Integer currentCycleMonth;
	private Integer currentCycleYear;
	private String billMedia;
	private String reqBillMedia;
	private String memo;
	private String submitter;
	private String orderId;
	private List<BaseCondition> conditions;

	public Long getBan() {
		return ban;
	}

	public void setBan(Long ban) {
		this.ban = ban;
	}

	public Long getBcban() {
		return bcban;
	}

	public void setBcban(Long bcban) {
		this.bcban = bcban;
	}

	public Long getMasterBan() {
		return masterBan;
	}

	public void setMasterBan(Long masterBan) {
		this.masterBan = masterBan;
	}

	public Integer getCycleCode() {
		return cycleCode;
	}

	public void setCycleCode(Integer cycleCode) {
		this.cycleCode = cycleCode;
	}

	public Integer getCycleMonth() {
		return cycleMonth;
	}

	public void setCycleMonth(Integer cycleMonth) {
		this.cycleMonth = cycleMonth;
	}

	public Integer getCycleYear() {
		return cycleYear;
	}

	public void setCycleYear(Integer cycleYear) {
		this.cycleYear = cycleYear;
	}

	public Integer getCurrentCycleCode() {
		return currentCycleCode;
	}

	public void setCurrentCycleCode(Integer currentCycleCode) {
		this.currentCycleCode = currentCycleCode;
	}

	public Integer getCurrentCycleMonth() {
		return currentCycleMonth;
	}

	public void setCurrentCycleMonth(Integer currentCycleMonth) {
		this.currentCycleMonth = currentCycleMonth;
	}

	public Integer getCurrentCycleYear() {
		return currentCycleYear;
	}

	public void setCurrentCycleYear(Integer currentCycleYear) {
		this.currentCycleYear = currentCycleYear;
	}

	public String getBillMedia() {
		return billMedia;
	}

	public void setBillMedia(String billMedia) {
		this.billMedia = billMedia;
	}

	public String getReqBillMedia() {
		return reqBillMedia;
	}

	public void setReqBillMedia(String reqBillMedia) {
		this.reqBillMedia = reqBillMedia;
	}

	public String getMemo() {
		return memo;
	}

	public void setMemo(String memo) {
		this.memo = memo;
	}

	public String getSubmitter() {
		return submitter;
	}

	public void setSubmitter(String submitter) {
		this.submitter = submitter;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public List<BaseCondition> getConditions() {
		if (conditions == null) {
			conditions = new ArrayList<>();
		}
		return conditions;
	}

	public void setConditions(List<BaseCondition> conditions) {
		this.conditions = conditions;
	}

	public ChangeBillingFormat addCondition(BaseCondition condition) {
		if (condition != null) {
			getConditions().add(condition);
		}
		return this;
	}
}
