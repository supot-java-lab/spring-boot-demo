/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.truebill.model.req;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author supot
 * @version 1.0
 */
public class OMXOrderReq implements Serializable {
	private static final long serialVersionUID = 1L;

	@JsonProperty("Order")
	private Order order = new Order();
	@JsonProperty("Customer")
	private Customer customer = new Customer();

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	@Override
	public String toString() {
		return "OMXOrderReq [order=" + order + ", customer=" + customer + "]";
	}

	public static final class Order implements Serializable {
		private static final long serialVersionUID = 1L;
		private String channel;
		private String orderId;
		private String orderType;

		public String getChannel() {
			return channel;
		}

		public void setChannel(String channel) {
			this.channel = channel;
		}

		public String getOrderId() {
			return orderId;
		}

		public void setOrderId(String orderId) {
			this.orderId = orderId;
		}

		public String getOrderType() {
			return orderType;
		}

		public void setOrderType(String orderType) {
			this.orderType = orderType;
		}

		@Override
		public String toString() {
			return "Order [channel=" + channel + ", orderId=" + orderId + ", orderType=" + orderType + "]";
		}

	}

	public static final class Customer implements Serializable {
		private static final long serialVersionUID = 1L;

		private String customerId;
		@JsonProperty("Account")
		private Account acccount = new Account();

		public String getCustomerId() {
			return customerId;
		}

		public void setCustomerId(String customerId) {
			this.customerId = customerId;
		}

		public Account getAcccount() {
			return acccount;
		}

		public void setAcccount(Account acccount) {
			this.acccount = acccount;
		}

		@Override
		public String toString() {
			return "Customer [customerId=" + customerId + ", acccount=" + acccount + "]";
		}

	}

	public static final class Account implements Serializable {
		private static final long serialVersionUID = 1L;

		private String accountId;

		public String getAccountId() {
			return accountId;
		}

		public void setAccountId(String accountId) {
			this.accountId = accountId;
		}

		@Override
		public String toString() {
			return "Account [accountId=" + accountId + "]";
		}

	}
}
