/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.truebill.core.rule;

import java.math.BigDecimal;
import java.util.Map;

import th.co.truebill.util.BeanPropertyUtils;

/**
* @author supot
* @version 1.0
*/
public interface Validation<T, R> {

	public R validate(ValidationContext context, T criteria);
	
	public Map<String, Object> getParams();

	public default Object getValue(String key) {
		if (getParams() == null || key == null || key.isEmpty()) {
			return null;
		}
		return getParams().get(key);
	}

	public default String getString(String key) {
		Object value = getValue(key);
		if (value == null) {
			return null;
		}		
		return value.toString();
	}
	
	public default int getInt(String key) {
		Integer intValue = getInteger(key);
		return (intValue != null ? intValue.intValue() : 0);
	}
	
	public default Integer getInteger(String key) {
		BigDecimal value = getBigDecimal(key);
		return (value != null ? value.intValue() : null);
	}
	
	public default Long getLong(String key) {
		BigDecimal value = getBigDecimal(key);
		return (value != null ? value.longValue() : null);
	}
	
	public default BigDecimal getBigDecimal(String key) {
		Object value = getValue(key);
		if (value == null) {
			return null;
		}
		if (value instanceof BigDecimal) {
			return (BigDecimal) value;
		} else if (value instanceof Number) {
			return BigDecimal.valueOf(((Number) value).doubleValue());
		}
		try {
			return new BigDecimal(value.toString());
		} catch (NumberFormatException ex) {
			// Ignore
		}

		return null;
	}
	
	public default boolean getBoolean(String key) {
		Object value = getValue(key);
		if (value == null) {
			return false;
		}
		if (value instanceof Boolean) {
			return (Boolean) value;
		}
		return ("true".equalsIgnoreCase(value.toString()));
	}
	
	public default void convertPropertyToParameter(T criteria, String... properties) {
		if (criteria == null || getParams() == null || properties == null || properties.length == 0) {
			return;
		}

		for (String property : properties) {
			if (property == null || property.isEmpty()) {
				continue;
			}
			Object value = BeanPropertyUtils.getPropertyValue(criteria, property);			
			getParams().put(property, value);
		}
	}
}
