/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.truebill.core.rule;

/**
* @author supot
* @version 1.0
*/
public enum ConditionNames {
	NO_PAYMENT("noPaymentSameBCContion"),
	ACTIVE_BAN("activeBanConditon");
	
	private String beanName;

	private ConditionNames(String beanName) {
		this.beanName = beanName;
	}

	public String getBeanName() {
		return beanName;
	}
}
