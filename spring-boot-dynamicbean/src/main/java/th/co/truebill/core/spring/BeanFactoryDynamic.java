/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.truebill.core.spring;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author supot
 * @version 1.0
 */

@Component
public class BeanFactoryDynamic {
	private static Logger logger = LoggerFactory.getLogger(BeanFactoryDynamic.class);
	private final BeanFactory beanFactory;

	@Autowired
	public BeanFactoryDynamic(BeanFactory beanFactory) {
		this.beanFactory = beanFactory;
	}
	
	public <T> T getBean(String name, Class<T> type) {
		T bean = null;
		try {
			bean = beanFactory.getBean(name, type);
		} catch (BeansException ex) {
			String error = String.format("getBean by name %s error", name);
			logger.error(error, ex);
		}

		return bean;
	}
}
