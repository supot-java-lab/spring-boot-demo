/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.truebill.core.rule;

import java.util.HashMap;
import java.util.Map;

/**
* @author supot
* @version 1.0
*/
public class ValidationContext {
	private Map<String, Object> cacheValues;

	public Map<String, Object> getCacheValues() {
		if (cacheValues == null) {
			cacheValues = new HashMap<>();
		}
		return cacheValues;
	}
	
	public void addToCaches(String key, Object value) {
		if (key != null && !key.isEmpty() && value != null) {
			getCacheValues().put(key, value);
		}
	}
	
	@SuppressWarnings("unchecked")
	public <T> T getValue(String key) {
		if (cacheValues == null || cacheValues.isEmpty()) {
			return null;
		}

		Object value = cacheValues.get(key);
		if (value == null) {
			return null;
		}
		return (T) cacheValues.get(key);
	}
	
	public int getCacheSize() {
		return (cacheValues != null ? cacheValues.size() : 0);
	}
	
	public void clear() {
		if (cacheValues != null) {
			cacheValues.clear();
		}
	}
}
