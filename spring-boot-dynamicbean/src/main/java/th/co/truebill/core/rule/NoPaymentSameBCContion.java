/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.truebill.core.rule;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

/**
* @author supot
* @version 1.0
*/

@Component
public class NoPaymentSameBCContion implements ValidationBoolean<Object> {
	private Map<String, Object> params = new HashMap<>();
	
	@Override
	public Boolean validate(ValidationContext context, Object criteria) {
		try {
			convertPropertyToParameter(criteria, "ban", "bcban", "currentCycleCode");			
			Long ban = getLong("ban");
			int bcban = getInt("bcban");
			
			//Call API
		} catch (Exception ex) {

		}
		
		return Boolean.TRUE;
	}

	@Override
	public Map<String, Object> getParams() {
		return params;
	}

}
