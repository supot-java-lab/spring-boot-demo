/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.truebill.core.rule;

/**
* @author supot
* @version 1.0
*/
public interface ValidationBoolean<T> extends Validation<T, Boolean> {

}
