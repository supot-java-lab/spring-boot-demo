/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.truebill.core.rule;

import java.util.Map;

/**
* @author supot
* @version 1.0
*/
public class BaseCondition implements Conditions {
	private ConditionNames name;
	private Map<String, Object> params;
	
	@Override
	public ConditionNames getName() {
		return name;
	}

	@Override
	public void setName(ConditionNames name) {
		this.name = name;
	}

	@Override
	public Map<String, Object> getParams() {
		return params;
	}

	@Override
	public void setParams(Map<String, Object> params) {
		this.params = params;
	}
	
}
