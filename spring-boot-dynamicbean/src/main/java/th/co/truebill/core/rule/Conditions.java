/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.truebill.core.rule;

import java.util.HashMap;
import java.util.Map;

/**
* @author supot
* @version 1.0
*/
public interface Conditions {

	public ConditionNames getName();
	
	public void setName(ConditionNames name);
	
	public Map<String, Object> getParams();

	public void setParams(Map<String, Object> params);
	
	public default Conditions addParameter(String name, Object value) {
		if (name != null && !name.isEmpty()) {
			if (getParams() == null) {
				setParams(new HashMap<>());
			}
			getParams().put(name, value);
		}
		return this;
	}

	public default Object getValue(String key) {
		if (getParams() == null || key == null || key.isEmpty()) {
			return null;
		}
		return getParams().get(key);
	}

	public default String getString(String key) {
		Object value = getValue(key);
		if (value == null) {
			return null;
		}		
		return value.toString();
	}
	
	public default int getInt(String key) {
		Integer intValue = getInteger(key);
		return (intValue != null ? intValue.intValue() : 0);
	}
	
	public default Integer getInteger(String key) {
		Object value = getValue(key);
		if (value == null) {
			return null;
		}
		if (value instanceof Number) {
			return ((Number) value).intValue();
		}			
		try {
			return Integer.valueOf(value.toString());
		} catch (NumberFormatException ex) {
			//Ignore
		}
		
		return null;
	}
	
	public default boolean getBoolean(String key) {
		Object value = getValue(key);
		if (value == null) {
			return false;
		}
		if (value instanceof Boolean) {
			return (Boolean) value;
		}
		return ("true".equalsIgnoreCase(value.toString()));
	}
	
	public default String getBeanName() {
		if (getName() == null) {
			return null;
		}
		return getName().getBeanName();
	}
}
