/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.truebill.core.rule;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.springframework.beans.BeanUtils;

import th.co.truebill.util.BeanPropertyUtils;
import th.co.truebill.util.Exceptions;
import th.co.truebill.util.ReflectionUtils;

/**
 * A nested setter.
 *
 * @author DoubleF1re
 * @author V.Ladynev
 */
public final class NestedSetter {
    private final Method[] getMethods;
    private final Method[] setMethods;
    private final Method method;
    private final String propertyName;
    
    private NestedSetter(Class<?> clazz, Method[] getMethods, Method[] setMethods, Method method,
            String propertyName) {
        this.method = method;
        this.propertyName = propertyName;
        this.getMethods = getMethods;
        this.setMethods = setMethods;
    }

    public void set(Object target, Object value) {
        try {
            invokeSet(target, value);
        } catch (Exception ex) {
            checkForPrimitive(value);
            String errorMessage = String.format(
                    "Setter information: expected type: %s, actual type: %s.",
                    method.getParameterTypes()[0].getName(),
                    value == null ? null : value.getClass().getName());
            throw Exceptions.toRuntimeException(errorMessage, ex);
        }
    }

	private void checkForPrimitive(Object value) {
		if (value == null && method.getParameterTypes()[0].isPrimitive()) {
			throw Exceptions.toRuntimeException("Value is null, but property type is primitive.");
		}
	}

	private void invokeSet(Object target, Object value) {
        try {
            Object tmpTarget = target;
            for (int i = 0; i < getMethods.length; i++) {
                Object tmpTarget2 = getMethods[i].invoke(tmpTarget);
                if (tmpTarget2 == null) {
                    tmpTarget2 = BeanUtils.instantiateClass(getMethods[i].getReturnType());
                    setMethods[i].invoke(tmpTarget, tmpTarget2);
                }
                tmpTarget = tmpTarget2;
            }
            
			method.invoke(tmpTarget, value);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
			throw Exceptions.toRuntimeException(ex);
		}
    }

	public Class<?> getPropertyType() {
		if (method == null) {
			return null;
		}

		Class<?>[] paramTypes = method.getParameterTypes();
		if (paramTypes == null || paramTypes.length == 0) {
			return null;
		}
		return paramTypes[0];
	}
    
	public String getName() {
		return propertyName;
	}
	
	/**
     * Create a setter for a nested property.
     */
    public static NestedSetter create(Class<?> theClass, String propertyName) {
        return getSetterOrNull(theClass, propertyName);
    }

    private static NestedSetter getSetterOrNull(Class<?> theClass, String propertyName) {
        if (theClass == Object.class || theClass == null || propertyName == null) {
            return null;
        }

        String[] propertyParts = ReflectionUtils.getPropertyParts(propertyName);

        int nestedCount = propertyParts.length;

        Method[] getMethods = new Method[nestedCount - 1];
        Method[] setMethods = new Method[nestedCount - 1];

        Class<?> currentClass = theClass;
        for (int i = 0; i < nestedCount - 1; i++) {
        	String property = BeanPropertyUtils.toPropertyName(propertyParts[i]);
            Method getter = ReflectionUtils.getClassGetter(currentClass, property);
            if (getter == null) {
            	return null;
            }

            getMethods[i] = getter;
            setMethods[i] = ReflectionUtils.getClassSetter(currentClass, property, getter);
            currentClass = getMethods[i].getReturnType();	
        }

        String property = BeanPropertyUtils.toPropertyName(propertyParts[nestedCount - 1]);
        Method method = setterMethod(currentClass, property);
        if (method != null) {
            ReflectionUtils.makePublic(method);
            return new NestedSetter(theClass, getMethods, setMethods, method, BeanPropertyUtils.toNestedPropertyName(propertyName));
        }

        NestedSetter setter = getSetterOrNull(theClass.getSuperclass(), propertyName);
        if (setter == null) {
            Class<?>[] interfaces = theClass.getInterfaces();
            for (int i = 0; setter == null && i < interfaces.length; i++) {
                setter = getSetterOrNull(interfaces[i], propertyName);
            }
        }

        return setter;
    }

    private static Method setterMethod(Class<?> theClass, String propertyName) {
        Method getter = ReflectionUtils.getClassGetter(theClass, propertyName);
        return ReflectionUtils.getClassSetter(theClass, propertyName, getter);
    }
}
