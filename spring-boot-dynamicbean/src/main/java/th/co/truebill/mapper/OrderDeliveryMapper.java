/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.truebill.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import th.co.truebill.model.redis.ChangeBillingFormat;
import th.co.truebill.model.req.OMXOrderReq;

/**
* @author supot
* @version 1.0
*/

@Mapper
public interface OrderDeliveryMapper {

	@Mapping(source = "orderId", target = "order.orderId")
	@Mapping(source = "ban", target = "customer.acccount.accountId")
	@Mapping(source = "billMedia", target = "order.orderType")
	@Mapping(source = "reqBillMedia", target = "order.channel")
	OMXOrderReq billingFormatToOrder(ChangeBillingFormat source);
}
