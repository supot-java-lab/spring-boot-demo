/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.truebill.order.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import th.co.truebill.BaseService;
import th.co.truebill.core.rule.BaseCondition;
import th.co.truebill.core.rule.Conditions;
import th.co.truebill.core.rule.ValidationBoolean;
import th.co.truebill.core.rule.ValidationContext;
import th.co.truebill.core.spring.BeanFactoryDynamic;
import th.co.truebill.util.Validators;

/**
* @author supot
* @version 1.0
*/
@Component
public class OrderDeliveryValidator extends BaseService {
	
	@Autowired
	private BeanFactoryDynamic beanFactory;

	@SuppressWarnings("unchecked")
	public <T> boolean validateSuccess(T model, List<BaseCondition> conditions) {
		if (Validators.isNull(model) || Validators.isEmpty(conditions)) {
			return true;
		}

		ValidationContext contxt = new ValidationContext();
		for (Conditions condition : conditions) {
			ValidationBoolean<T> validation = beanFactory.getBean(condition.getBeanName(), ValidationBoolean.class);
			if (Validators.isNotNull(validation)) {
				Boolean result = validation.validate(contxt, model);
				logger.info("Validation rule : {} -> {}", condition.getName(), result);
				if (result != null && !result) {
					return result;
				}
			}
		}

		return Boolean.TRUE;
	}
}
