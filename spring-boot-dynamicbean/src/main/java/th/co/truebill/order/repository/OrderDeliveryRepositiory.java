/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.truebill.order.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import th.co.truebill.core.rule.BaseCondition;
import th.co.truebill.core.rule.ConditionNames;
import th.co.truebill.model.redis.ChangeBillingFormat;

/**
* @author supot
* @version 1.0
*/

@Component
public class OrderDeliveryRepositiory {

	public List<ChangeBillingFormat> getChangeBillMedias() {
		List<ChangeBillingFormat> results = new ArrayList<>();
		for (long i = 1; i <= 2; i++) {
			ChangeBillingFormat model = new ChangeBillingFormat();
			model.setBan(i*i);
			model.setBillMedia("S");
			model.setReqBillMedia("PS");
			model.setCurrentCycleCode(1);
			model.setCurrentCycleYear(2020);
			
			BaseCondition condition = new BaseCondition();
			condition.setName(ConditionNames.ACTIVE_BAN);
			condition.addParameter("days", 40);
			condition.addParameter("name", "Supot");			
			model.addCondition(condition);
			
			condition = new BaseCondition();
			condition.setName(ConditionNames.NO_PAYMENT);
			condition.addParameter("days", 40);		
			model.addCondition(condition);
			
			results.add(model);
		}
		return results;
	}
}
