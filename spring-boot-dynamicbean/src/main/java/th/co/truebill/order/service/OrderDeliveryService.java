/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.truebill.order.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import th.co.truebill.BaseService;
import th.co.truebill.model.redis.ChangeBillingFormat;
import th.co.truebill.order.repository.OrderDeliveryRepositiory;
import th.co.truebill.util.Validators;

/**
* @author supot
* @version 1.0
*/

@Component
public class OrderDeliveryService extends BaseService {

	@Autowired
	private OrderDeliveryRepositiory repositiory;	
	@Autowired
	private OrderDeliveryValidator validation;
	
	public void processOrder() {
		try {
			logger.info("Step 1. Starting process change bill media");
			List<ChangeBillingFormat> results = repositiory.getChangeBillMedias();
			if (Validators.isEmpty(results)) {
				logger.warn("Empty bill media data for change");
				return;
			}
			
			for (ChangeBillingFormat billMedia : results) {
				boolean success = validation.validateSuccess(billMedia, billMedia.getConditions());
				if (!success) {
					logger.warn("Validate rule of BAN : {} not valid", billMedia.getBan());
					continue;
				}
				
				logger.info("Next step Process");
				//Next step & process
			}
		} catch (Exception ex) {
			logger.error("processOrder exception", ex);
		}
	}
}
