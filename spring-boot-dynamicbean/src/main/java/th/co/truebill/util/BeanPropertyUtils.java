/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.truebill.util;

import org.apache.commons.beanutils.PropertyUtils;

/**
* @author supot
* @version 1.0
*/
public final class BeanPropertyUtils {
	private BeanPropertyUtils() {
		
	}
	
	public static Object getPropertyValue(Object bean, String property) {
		Object obj = null;
		try {
			obj = PropertyUtils.getNestedProperty(bean, property);
		} catch (Exception ex) {
			//Skip
		}
		
		return obj;
	}
	
	public static String toPropertyName(String name) {
		if (Validators.isEmpty(name)) {
			return null;
		}

		name = name.replaceAll("\\s+", "_");
		if (isValidProperty(name)) {
			return name;
		}

		StringBuilder result = new StringBuilder();
		boolean nextIsUpper = false;
		if (name.length() > 1 && name.substring(1, 2).equals("_")) {
			result.append(name.substring(0, 1).toUpperCase());
		} else {
			result.append(name.substring(0, 1).toLowerCase());
		}

		for (int i = 1; i < name.length(); i++) {
			String s = name.substring(i, i + 1);
			if (s.equals("_")) {
				nextIsUpper = true;
			} else {
				if (nextIsUpper) {
					result.append(s.toUpperCase());
					nextIsUpper = false;
				} else {
					result.append(s.toLowerCase());
				}
			}
		}
		return result.toString();
	}

	public static String toNestedPropertyName(String name) {
		if (Validators.isEmpty(name)) {
			return null;
		}
		
		String[] pros = name.split("\\.");
		StringBuilder sb = new StringBuilder();
		boolean first = true;
		for (String str : pros) {
			str = toPropertyName(str);
			if (Validators.isEmpty(str)) {
				continue;
			}
			
			if (first) {
				sb.append(str);
				first = false;
			} else {
				sb.append(".").append(str);
			}
		}
	
		return sb.toString();
	}

	private static boolean isValidProperty(String value) {
		if (value.contains("_")) {
			return false;
		}

		char[] chs = value.toCharArray();
		if (isLowerCase(chs)) {
			return true;
		}

		return isCamelCase(chs);
	}

	private static boolean isLowerCase(char[] chs) {
		for (char ch : chs) {
			if (!Character.isLowerCase(ch) && !Character.isDigit(ch)) {
				return false;
			}
		}

		return true;
	}

	private static boolean isCamelCase(char[] chs) {
		for (int i = 0; i < chs.length; i++) {
			char ch = chs[i];
			if (i == 0 && (Character.isUpperCase(ch) || Character.isDigit(ch))) {
				return false;
			}
		}

		return true;
	}
}
 