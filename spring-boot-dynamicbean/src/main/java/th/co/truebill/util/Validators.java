/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.truebill.util;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

/**
 * Utility class for Validation data
 * @author supot
 * @version 1.0
 */
public final class Validators {
	
	private Validators() {
	}

	/**
	 * Validate value is null
	 * @param value The value to validate
	 * @return
	 */
	public static boolean isNull(Object value) {
		return (null == value);
	}

	/**
	 * Validate value is not null
	 * @param value The value to validate
	 * @return
	 */
	public static boolean isNotNull(Object value) {
		return !isNull(value);
	}

	/**
	 * Validate value is empty [null also count to empty]
	 * @param value The value to validate
	 * @return
	 */
	public static boolean isEmpty(Object value) {
		if (isNull(value)) {
			return true;
		}

		if (value instanceof String) {
			return ((String) value).trim().isEmpty();
		} else if (value instanceof Collection<?>) {
			return ((Collection<?>) value).isEmpty();
		} else if (value instanceof Iterator<?>) {
			return !((Iterator<?>) value).hasNext();
		} else if (value instanceof Map<?, ?>) {
			return ((Map<?, ?>) value).isEmpty();
		} else if (value.getClass().isArray()) {
			return Array.getLength(value) == 0;
		}
		return value.toString().isEmpty();
	}

	/**
	 * Validate value is not empty [null also count to empty]
	 * @param value The value to validate.
	 * @return
	 */
	public static boolean isNotEmpty(Object value) {
		return !isEmpty(value);
	}

	/**
	 * Validate some of the objects is null.
	 * @param values The multiple values to validate
	 * @return
	 */
	public static boolean isNullOne(Object... values) {
		if (isEmpty(values)) {
			return true;
		}

		for (Object obj : values) {
			if (isNull(obj)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Validate all of the objects is null.
	 * @param values The multiple values to validate
	 * @return
	 */
	public static boolean isNullAll(Object... values) {
		if (isEmpty(values)) {
			return true;
		}

		for (Object obj : values) {
			if (isNotNull(obj)) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Validate all of the objects is not null.
	 * @param values The multiple values to validate
	 * @return 
	 */
	public static boolean isNotNullAll(Object... values) {
		return !isNullOne(values);
	}
	
	/**
	 * Validate some of the objects is empty [null also count to empty].
	 * @param values The multiple values to validate
	 * @return
	 */
	public static boolean isEmptyOne(Object... values) {
		if (isEmpty(values)) {
			return true;
		}

		for (Object obj : values) {
			if (isEmpty(obj)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Validate all of the objects is empty [null also count to empty].
	 * @param values The multiple values to validate
	 * @return
	 */
	public static boolean isEmptyAll(Object... values) {
		if (isEmpty(values)) {
			return true;
		}

		for (Object obj : values) {
			if (isNotEmpty(obj)) {
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Validate all of the objects is not empty [null also count to empty].
	 * @param values The multiple values to validate
	 * @return
	 */
	public static boolean isNotEmptyAll(Object... values) {
		return !isEmptyOne(values);
	}

	/**
	 * Validate value is equals to zero
	 * @param value The number to validate
	 * @return
	 */
	public static boolean isZero(Number value) {
		if (isNull(value)) {
			return false;
		}

		BigDecimal checkVal = toBigDecimal(value);
		return (checkVal.compareTo(BigDecimal.ZERO) == 0);
	}

	/**
	 * Validate value is not equals to zero
	 * @param value The number to validate
	 * @return
	 */
	public static boolean isNotZero(Number value) {
		return !isZero(value);
	}

	/**
	 * Validate value is less than zero
	 * @param value The number to validate
	 * @return
	 */
	public static boolean isLessZero(Number value) {
		if (isNull(value)) {
			return false;
		}

		BigDecimal checkVal = toBigDecimal(value);
		return (checkVal.compareTo(BigDecimal.ZERO) < 0);
	}

	/**
	 * Validate value is less than or equals zero
	 * @param value The number to validate
	 * @return
	 */
	public static boolean isLessEqZero(Number value) {
		if (isNull(value)) {
			return false;
		}

		BigDecimal checkVal = toBigDecimal(value);
		return (checkVal.compareTo(BigDecimal.ZERO) <= 0);
	}
	
	/**
	 * Validate value is more than to zero
	 * @param value The number to validate
	 * @return
	 */
	public static boolean isMoreZero(Number value) {
		if (isNull(value)) {
			return false;
		}

		BigDecimal checkVal = toBigDecimal(value);
		return (checkVal.compareTo(BigDecimal.ZERO) > 0);
	}

	/**
	 * Validate value is more than or equals zero
	 * @param value The number to validate
	 * @return
	 */
	public static boolean isMoreEqZero(Number value) {
		if (isNull(value)) {
			return false;
		}

		BigDecimal checkVal = toBigDecimal(value);
		return (checkVal.compareTo(BigDecimal.ZERO) >= 0);
	}

	/**
	 * Validate value is Collection type
	 * @param value The value to validate
	 * @return
	 */
	public static boolean isCollection(Object value) {
		return (value instanceof Collection<?>);
	}

	/**
	 * Validate value is Array type
	 * @param value The value to validate
	 * @return
	 */
	public static boolean isArray(Object value) {
		return (value != null && value.getClass().isArray());
	}
	
	/**
	 * Validate value is String instance
	 * @param value The value to validate
	 * @return
	 */
	public static boolean isString(Object value) {
		return (value instanceof String);
	}
	
	/**
	 * Validate value is String class
	 * @param clazz The class to validate
	 * @return
	 */
	public static boolean isClassString(Class<?> clazz) {
		return (clazz != null && clazz == String.class);
	}
	
	private static BigDecimal toBigDecimal(Number value) {
		if (value instanceof BigDecimal) {
			return (BigDecimal) value;
		} else if (value instanceof Integer || value instanceof Long || value instanceof BigInteger
				|| value instanceof Short || value instanceof Byte) {
			return BigDecimal.valueOf(value.longValue());
		} else if (value instanceof Float) {
			return BigDecimal.valueOf(value.floatValue());
		}
		
		return BigDecimal.valueOf(value.doubleValue());
	}
}
