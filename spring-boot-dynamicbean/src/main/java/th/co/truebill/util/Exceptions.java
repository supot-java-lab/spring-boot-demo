/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.truebill.util;

/**
* @author Supot Saelao
* @version 1.0
*/
public final class Exceptions {
	private Exceptions() {}
	
    public static RuntimeException toRuntimeException(Throwable throwable) {
        return throwable instanceof RuntimeException ? (RuntimeException) throwable
                : new RuntimeException(throwable);
    }

    public static RuntimeException toRuntimeException(String message, Throwable throwable) {
        return message == null ? toRuntimeException(throwable)
                : new RuntimeException(message, throwable);
    }
    
    public static RuntimeException toRuntimeException(String message) {
        return new RuntimeException(message);
    }
   
}
