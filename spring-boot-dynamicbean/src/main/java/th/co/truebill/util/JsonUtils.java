/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.truebill.util;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.CollectionType;
import com.fasterxml.jackson.databind.type.MapType;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import th.co.truebill.exception.JsonException;

/**
 *
 * @author supot
 * @version 1.0
 */
public final class JsonUtils {
	private static final String FORMAT_DT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	
	private static final ObjectMapper mapper;
	
	private JsonUtils() {}
	
	static {
		mapper = new ObjectMapper();
		mapperConfig(mapper);
	}

	/**
	 * Serialize object value to JSON data format
	 * @param obj The object to serialize.
	 * @return Serialize value as string
	 * @throws JsonException
	 */
	public static String json(Object obj) throws JsonException {
		return json(obj, false);
	}
	
	/**
	 * Serialize object value to JSON data format
	 * @param obj The object to convert.
	 * @param prettyOutput Write out pretty readable.
	 * @return Serialize value as string
	 * @throws JsonException
	 */
	public static String json(Object obj, boolean prettyOutput) throws JsonException {
		String jsonData = null;
		try {
			if (prettyOutput) {
				jsonData = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(obj);
			} else {
				jsonData = mapper.writeValueAsString(obj);
			}
		} catch (JsonProcessingException ex) {
			throw new JsonException(ex);
		}

		return jsonData;
	}

	/**
	 * Deserialize JSON data format to Object class.
	 * @param <T> The type of class for deserialize.
	 * @param json JSON data format.
	 * @param clazzz The class for deserialize.
	 * @return Object class
	 * @throws JsonException
	 */
	public static <T> T model(String json, Class<T> clazzz) throws JsonException {
		if (Validators.isEmpty(json)) {
			return null;
		}
		return model(json.getBytes(), clazzz);
	}
	
	/**
	 * Deserialize JSON data format to Object class.<br/>
	 * In case need full generic type information 
	 * @param <T> The type of class for deserialize.
	 * @param json JSON data format.
	 * @param type JavaType of deserialize.
	 * @return Object class
	 * @throws JsonException
	 */
	public static <T> T model(String json, JavaType type) throws JsonException {
		if (Validators.isEmptyOne(json, type)) {
			return null;
		}
		return model(json.getBytes(), type);
	}

	/**
	 * Deserialize JSON data format to List of model
	 * @param <T> The type of class for deserialize.
	 * @param json JSON data format must be array.
	 * @param clazzz The class for deserialize.
	 * @return Object class
	 * @throws JsonException
	 */
	public static <T> List<T> models(String json, Class<T> clazzz) throws JsonException {
		if (Validators.isEmpty(json)) {
			return Collections.emptyList();
		}		
		return models(json.getBytes(), clazzz);
	}
	
	/**
	 * Serialize object value to JSON data format
	 * @param obj The object to serialize.
	 * @return Serialize value as byte[] arrays
	 * @throws JsonException
	 */
	public static byte[] jsonAsBytes(Object obj) throws JsonException {
		byte[] jsonDatas = null;
		try {
			jsonDatas = mapper.writeValueAsBytes(obj);
		} catch (JsonProcessingException ex) {
			throw new JsonException(ex);
		}

		return jsonDatas;
	}

	/**
	 * Deserialize JSON data format to Object class.
	 * @param <T> The type of class for deserialize.
	 * @param jsonDatas JSON byte[] data format.
	 * @param clazzz The class for deserialize.
	 * @return Object class
	 * @throws JsonException
	 */
	public static <T> T model(byte[] jsonDatas, Class<T> clazzz) throws JsonException {
		try {
			if (Validators.isEmpty(jsonDatas)) {
				return null;
			}

			return mapper.readValue(jsonDatas, clazzz);
		} catch (IOException ex) {
			throw new JsonException(ex);
		}
	}
	
	/**
	 * Deserialize JSON data format to Object class.<br/>
	 * In case need full generic type information 
	 * @param <T> The type of class for deserialize.
	 * @param jsonDatas JSON byte[] data format.
	 * @param type JavaType of deserialize.
	 * @return Object class
	 * @throws JsonException
	 */
	public static <T> T model(byte[] jsonDatas, JavaType type) throws JsonException {
		try {
			if (Validators.isEmpty(jsonDatas)) {
				return null;
			}

			return mapper.readValue(jsonDatas, type);
		} catch (IOException ex) {
			throw new JsonException(ex);
		}
	}
	
	/**
	 * Deserialize JSON data format to List of model.
	 * @param <T> The type of class for deserialize.
	 * @param jsonDatas JSON byte[] data format.
	 * @param clazzz The class for deserialize.
	 * @return Object class
	 * @throws JsonException
	 */
	public static <T> List<T> models(byte[] jsonDatas, Class<T> clazzz) throws JsonException {
		try {
			if (Validators.isEmpty(jsonDatas)) {
				return Collections.emptyList();
			}

			JavaType javaType = collectionType(List.class, clazzz);
			return mapper.readValue(jsonDatas, javaType);
		} catch (IOException ex) {
			throw new JsonException(ex);
		}
	}
	
	/**
	 * Deserialize JSON data format to Map class.
	 * @param json json JSON data format.
	 * @return Deserialize result of Map.
	 * @throws JsonException
	 */
	public static Map<String, Object> map(String json) throws JsonException {
		if (Validators.isEmpty(json)) {
			return Collections.emptyMap();
		}
		return map(json.getBytes());
	}
	
	/**
	 * Deserialize JSON data format to Map class.
	 * @param jsonDatas JSON byte[] data format.
	 * @return Deserialize result of Map.
	 * @throws JsonException
	 */
	@SuppressWarnings("unchecked")
	public static Map<String, Object> map(byte[] jsonDatas) throws JsonException {
		Map<String, Object> maps = JsonUtils.model(jsonDatas, Map.class);
		if (Validators.isNull(maps)) {
			maps = new HashMap<>();
		}

		return maps;
	}
	
	/**
	 * Deserialize JSON data format to Map class.
	 * @param <K> Map key type
	 * @param <V> Map value type
	 * @param json JSON data format.
	 * @param keyClass Map key class
	 * @param valueClass Map value class
	 * @return Deserialize result of Map.
	 * @throws JsonException
	 */
	public static <K, V> Map<K, V> map(String json, Class<K> keyClass, Class<V> valueClass) throws JsonException {
		if (Validators.isEmpty(json) || Validators.isNullOne(keyClass, valueClass)) {
			return Collections.emptyMap();
		}		
		return map(json.getBytes(), keyClass, valueClass);
	}
	
	/**
	 * Deserialize JSON data format to Map class.
	 * @param <K> Map key type
	 * @param <V> Map value type
	 * @param jsonDatas JSON byte[] data format.
	 * @param keyClass Map key class
	 * @param valueClass Map value class
	 * @return Deserialize result of Map.
	 * @throws JsonException
	 */
	public static <K, V> Map<K, V> map(byte[] jsonDatas, Class<K> keyClass, Class<V> valueClass) throws JsonException {
		try {
			if (Validators.isEmpty(jsonDatas) 
					|| Validators.isNullOne(keyClass, valueClass)) {
				return Collections.emptyMap();
			}

			JavaType javaType = mapType(Map.class, keyClass, valueClass);
			return mapper.readValue(jsonDatas, javaType);
		} catch (IOException ex) {
			throw new JsonException(ex);
		}
	}
	
	/**
	 * Create CollectionType {@link CollectionType} instance.
	 * @param collClass The Collection class type
	 * @param elementClazz The element of collection class type
	 * @return {@link CollectionType}
	 */	
	@SuppressWarnings("rawtypes")
	public static JavaType collectionType(Class<? extends Collection> collClass, Class<?> elementClazz) {
		return mapper.getTypeFactory().constructCollectionType(collClass, elementClazz);
	}
	
	/**
	 * Create MapType {@link MapType} instance.
	 * @param mapClass The class of map type
	 * @param keyClass The class of map key
	 * @param valueClass The class of map value
	 * @return {@link MapType}
	 */
    @SuppressWarnings("rawtypes")
	public static JavaType mapType(Class<? extends Map> mapClass,
            Class<?> keyClass, Class<?> valueClass) {
    	return mapper.getTypeFactory().constructMapType(mapClass, keyClass, valueClass);
    }
    
    private static void mapperConfig(ObjectMapper mapper) {
    	mapper.setSerializationInclusion(Include.NON_EMPTY);
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.setDateFormat(new SimpleDateFormat(FORMAT_DT));	
		mapper.registerModule(new JavaTimeModule());
		
		// Only serialization class attributes or member
		// ignore all get/set and isMethod
		mapper.setVisibility(mapper.getVisibilityChecker()
				.withFieldVisibility(Visibility.ANY)
				.withGetterVisibility(Visibility.NONE)
				.withSetterVisibility(Visibility.NONE)
				.withCreatorVisibility(Visibility.NONE)
				.withIsGetterVisibility(Visibility.NONE));
	}
}
