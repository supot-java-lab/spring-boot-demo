/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.truebill;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
* @author supot
* @version 1.0
*/
public abstract class BaseService {
	protected Logger logger = LoggerFactory.getLogger(getClass());
}
