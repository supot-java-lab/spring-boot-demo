/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.truebill.exception;

/**
 * 
 * @author supot
 * @version 1.0
 */
public class JsonException extends Exception {
	private static final long serialVersionUID = 1L;

	public JsonException() {
		super();
	}

	public JsonException(String message) {
		super(message);
	}

	public JsonException(String message, Throwable cause) {
		super(message, cause);
	}

	public JsonException(Throwable cause) {
		super(cause);
	}
}
