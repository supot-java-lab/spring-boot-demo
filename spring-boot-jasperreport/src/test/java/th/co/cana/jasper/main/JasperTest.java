package th.co.cana.jasper.main;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.github.javafaker.Faker;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import th.co.cana.jasper.model.Employee;
import th.co.cana.jasper.utils.Constants;

public class JasperTest {

	public static void main(String[] args) throws JRException {
		createPdfReport(findAll());
	}

	// Method to create the pdf file using the employee list datasource.
	private static void createPdfReport(final List<Employee> employees) throws JRException {
		// Fetching the .jrxml file from the resources folder.
		final InputStream stream = JasperTest.class.getResourceAsStream("/report.jrxml");

		// Compile the Jasper report from .jrxml to .japser
		final JasperReport report = JasperCompileManager.compileReport(stream);

		// Fetching the employees from the data source.
		final JRBeanCollectionDataSource source = new JRBeanCollectionDataSource(employees);

		// Adding the additional parameters to the pdf.
		final Map<String, Object> parameters = new HashMap<>();
		parameters.put("createdBy", "javacodegeek.com");

		// Filling the report with the employee data and additional parameters information.
		final JasperPrint print = JasperFillManager.fillReport(report, parameters, source);

		// Users can change as per their project requrirements or can take it as request input requirement.
		// For simplicity, this tutorial will automatically place the file under the "c:" drive.
		// If users want to download the pdf file on the browser, then they need to use the "Content-Disposition" technique.
		final String filePath = Constants.getReportDir();
		// Export the report to a PDF file.
		JasperExportManager.exportReportToPdfFile(print, filePath + "/Employee_report.pdf");
	}
	
	private static List<Employee> findAll() {
		final Faker faker = new Faker();
		final Random random = new Random();
		final List<Employee> employees = new ArrayList<>();
		// Creating a list of employees using the "faker" object.
		for(int count=0; count<21; count++) {
			employees.add(new Employee(random.nextInt(30 + 1), faker.name().fullName(), 
					faker.job().title(), faker.job().field()));
		}
		return employees;
	}
}
