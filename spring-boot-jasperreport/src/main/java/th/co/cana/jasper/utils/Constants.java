/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.jasper.utils;

import java.util.Locale;

import th.co.cana.utils.FileUtils;
import th.co.cana.utils.Utils;

/**
* @author supot
* @version 1.0
*/
public class Constants {
	private Constants() {
	}
	
	public static final Locale US = Locale.US;
	public static final Locale TH = new Locale("th", "TH");
	
	public static final String LANG_TH = "TH";
	public static final String LANG_US = "US";
	
	public static final String Y = "Y";
	public static final String N = "N";
	public static final String UTF8 = "UTF-8";
	public static final String TEXT_EXT = ".txt";
	
    public static final class API {
    	private API() {}
    	public static final String RESOURCE_PREFIX 		= "/resources";
    	public static final String V1_PREFIX 			= "/v1";
    	
    	private static final String[] JOB_PACKAGES = {"th.co.truecorp.billpresent.controller.job"};

    	private static final String[] EXLUDE_PACKAGES = { "th.co.truecorp.billpresent.controller.job", 
    			"th.co.truecorp.billpresent.controller.provider"
    	};
    	private static final String[] AP_PACKAGES = { "th.co.truecorp.billpresent.controller"
    	};

		public static String[] jobPackage() {
			return JOB_PACKAGES;
		}
		
		public static String[] appPackage() {
			return AP_PACKAGES;
		}
		
		public static String[] excludePackage() {
			return EXLUDE_PACKAGES;
		}
    }
    
	public static String getReportDir() {
		return getWorkDir() + "/reports";
	}
	
	public static String getWorkDir() {
		String workDir = System.getProperty("user.dir");
		if ("/".equals(workDir)) {
			workDir = "/app";
		}
		return FileUtils.convertPathToUnix(workDir);
	}
	
	public static String getJasperDir() {
		return getWorkDir() + "/reports/jasper";
	}
	
	public static String getJasperImageDir() {
		return getWorkDir() + "/reports/jasper/images";
	}
	
	public static String getTempDir() {
		return getWorkDir() + "/reports/temp/" + Utils.getUUID();
	}
}
