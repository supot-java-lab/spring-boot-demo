package th.co.cana.jasper.service;

import java.util.List;

import th.co.cana.jasper.model.Employee;

public interface EmployeeService {

	List<Employee> findAll();
}
