package th.co.cana.product.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import th.co.cana.product.model.CustomerModel;
import th.co.cana.product.service.CustomerService;

import javax.validation.Valid;
import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */

@Tag(name = "Customer", description = "Customer Service API")
@RequiredArgsConstructor
@Slf4j
@RestController
@RequestMapping("/v1/customers")
public class CustomerCtrl {

    private final CustomerService customerService;

    @GetMapping
    public ResponseEntity<List<CustomerModel>> getCustomers() {
        log.info("Staring call get customer API ");
        return ResponseEntity.ok(customerService.createCustomers());
    }

    @PostMapping
    public ResponseEntity<CustomerModel> save(@Valid @RequestBody CustomerModel model) {
        log.info("Staring call save customer API ");
        return ResponseEntity.ok(model);
    }
}
