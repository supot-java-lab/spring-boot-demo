package th.co.cana.gateway.config;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.security.SecurityScheme;
import lombok.extern.slf4j.Slf4j;
import org.springdoc.core.SwaggerUiConfigParameters;
import org.springframework.boot.CommandLineRunner;
import org.springframework.cloud.gateway.route.RouteDefinition;
import org.springframework.cloud.gateway.route.RouteDefinitionLocator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Objects;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Configuration
@Slf4j
public class OpenAPIConfig {
    private static final String API_URL = "http://localhost:8080";
    @Bean
    public OpenAPI gatewayApi() {
        return new OpenAPI()
                .info(new Info().title("Gateway Service API")
                        .description("Gateway service for management API")
                        .version("V1.0")
                        .license(new License().name("Apache 2.0").url("https://springdoc.org")))
                .components(new Components()
                        .addSecuritySchemes("bearer-key",
                                new SecurityScheme().type(SecurityScheme.Type.HTTP).scheme("bearer").bearerFormat("JWT")));
    }

    @Bean
    public CommandLineRunner openApiGroups(
            RouteDefinitionLocator locator,
            SwaggerUiConfigParameters swaggerUiParameters) {
        log.info("UiRootPath: {}", swaggerUiParameters.getUiRootPath());
        log.info("Url: {}", swaggerUiParameters.getUrl());
        log.info("ConfigUrl: {}", swaggerUiParameters.getConfigUrl());
        log.info("UiRootPath: {}", swaggerUiParameters.getUiRootPath());

        swaggerUiParameters.setUrl(API_URL);
        swaggerUiParameters.setConfigUrl(API_URL);
        log.info("Url: {}", swaggerUiParameters.getUrl());
        log.info("ConfigUrl: {}", swaggerUiParameters.getConfigUrl());

        return args -> Objects.requireNonNull(locator
                        .getRouteDefinitions().collectList().block())
                .stream()
                .map(RouteDefinition::getId)
                .forEach(swaggerUiParameters::addGroup);
    }
}
