package th.co.cana.product.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import th.co.cana.product.model.CustomerModel;
import th.co.cana.product.model.OrderModel;
import th.co.cana.product.model.ProductModel;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */
@RequiredArgsConstructor
@Service
@Slf4j
public class ProductService {
    private static final String[] PRODUCTS = {"SALTY SNACK "
            , "SKIN CARE GOLD BOND CREPE CORRECTOR AGE DEFENSE LOTION "
            , "SOFT DRINK PEPSI MANGO "
            , "SPIRITS SPIRITS TANQUERAY SEVILLA ORANGE "
            , "SUGAR CONFECTIONS NERDS Gummy Clusters "
            , "SUSTAINABLE HOME ESSENTIALS DIAL® CONCENTRATED REFILLS "
            , "VITAMINS NATURE'S BOUNTY JELLY BEAN VITAMINS "
            , "WELLNESS LIVE BETTER APPLE CIDER VINEGAR GUMMIES"};

    private final RestTemplate restTemplate;

    public List<ProductModel> createProducts() {
        List<ProductModel> products = new ArrayList<>();
        for (int i = 0; i < PRODUCTS.length; i++) {
            ProductModel product = new ProductModel();
            product.setId((long) i);
            product.setName(PRODUCTS[i]);
            product.setCode("P100" + i);
            product.setPrice(BigDecimal.valueOf(i + 1).add(BigDecimal.valueOf(i * 2.1)));
            products.add(product);
        }

        return products;
    }

    public List<OrderModel> createOrders() {
        List<OrderModel> orders = new ArrayList<>();
        for (int i = 1; i <= 2; i++) {
            OrderModel order = new OrderModel();
            order.setOrderId((long) i);
            order.setOrderDate(LocalDateTime.now());
            order.setProducts(createProducts());
            order.setCustomer(this.createCustomer());
            orders.add(order);
        }

        return orders;
    }

    public List<CustomerModel> getCustomers() {
        String url = "http://api-gateway-service/v1/customers";
        log.info("Call web service API : {}", url);
        ResponseEntity<CustomerModel[]> response = restTemplate.getForEntity(url, CustomerModel[].class);
        CustomerModel[] customerItems = response.getBody();
        if (customerItems == null || customerItems.length == 0) {
            return Collections.emptyList();
        }
        return Arrays.asList(customerItems);
    }

    private CustomerModel createCustomer() {
        CustomerModel model = new CustomerModel();
        model.setCustomerId(1L);
        model.setFirstName("Supot");
        model.setLastName("Sealao");
        model.setBirthDate(LocalDate.of(1982, 2, 17));
        model.setCreditAmt(BigDecimal.valueOf(1500000));

        return model;
    }
}
