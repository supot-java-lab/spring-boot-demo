package th.co.cana.product.model;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
public class OrderModel implements Serializable {
    private Long orderId;
    private LocalDateTime orderDate;
    private CustomerModel customer;
    private List<ProductModel> products;
}
