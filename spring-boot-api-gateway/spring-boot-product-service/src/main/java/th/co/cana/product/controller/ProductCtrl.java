package th.co.cana.product.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import th.co.cana.product.model.CustomerModel;
import th.co.cana.product.model.OrderModel;
import th.co.cana.product.model.ProductModel;
import th.co.cana.product.service.ProductService;

import javax.validation.Valid;
import java.util.List;

/**
 * @author supot.jdev
 * @version 1.0
 */

@Tag(name = "Product", description = "Product Service API")
@RequiredArgsConstructor
@RestController
@RequestMapping("/v1/products")
@Slf4j
public class ProductCtrl {

    private final ProductService productService;
    @GetMapping
    public ResponseEntity<List<ProductModel>> getProducts() {
        log.info("Staring call get products API ");
        return ResponseEntity.ok(productService.createProducts());
    }

    @PostMapping
    public ResponseEntity<ProductModel> save(@Valid @RequestBody ProductModel model) {
        log.info("Staring call save products API ");
        return ResponseEntity.ok(model);
    }

    @GetMapping("/orders")
    public ResponseEntity<List<OrderModel>> getOrders() {
        log.info("Staring call get orders API ");
        return ResponseEntity.ok(productService.createOrders());
    }

    @GetMapping("/customers")
    public ResponseEntity<List<CustomerModel>> getCustomers() {
        log.info("Staring call get customer API ");
        return ResponseEntity.ok(productService.getCustomers());
    }
}
