package th.co.cana.product.model;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
public class ProductModel implements Serializable {
    @NotNull
    private Long id;
    @NotBlank
    private String code;
    @NotBlank
    private String name;
    private BigDecimal price;
}
