/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.datajdbc.model.jdbc;

import java.io.Serializable;

import org.springframework.data.annotation.Transient;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
* @author supot
* @version 1.0
*/

@JsonIgnoreProperties(value = { "mode" })
public class JdbcModel implements Serializable {
	private static final long serialVersionUID = 1L;

	@Transient
	private RowModes mode;

	public RowModes getMode() {
		return mode;
	}

	public void setMode(RowModes mode) {
		this.mode = mode;
	}
	
}
