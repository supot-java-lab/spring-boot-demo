/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.datajdbc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
* @author supot
* @version 1.0
*/

@SpringBootApplication
public class DataJdbcApplication {

	public static void main(String[] args) {
		SpringApplication.run(DataJdbcApplication.class, args);
	}

}
