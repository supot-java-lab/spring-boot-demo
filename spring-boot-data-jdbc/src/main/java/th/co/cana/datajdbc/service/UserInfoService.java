/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.datajdbc.service;

import java.util.List;
import java.util.Optional;

import th.co.cana.datajdbc.model.jdbc.UserInfo;

/**
* @author supot
* @version 1.0
*/
public interface UserInfoService {

	UserInfo save(UserInfo model);
	
	void update(UserInfo model);
	
	void delete(UserInfo model);
	
	Optional<UserInfo> getUser(Long id);
	
	List<UserInfo> getAll();
	
	List<UserInfo> search(String name);
}
