/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.datajdbc.model.jdbc;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.MappedCollection;
import org.springframework.data.relational.core.mapping.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
* @author supot
* @version 1.0
*/

@Data
@EqualsAndHashCode(callSuper=false)
@Table("CUSTOMER")
public class Customer extends JdbcModel implements Persistable<String> {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column("CUST_ID")
	private String custId;
	
	@Column("FIRST_NAME")
	private String firstName;
	
	@Column("LAST_NAME")
	private String lastName;
	
	@Column("BIRTH_DATE")
	private LocalDate birthDate;
	
	@Column("REGISTER_DATE")
	private Date registerDate;
	
	@Column("CREDIT_AMT")
	private BigDecimal creditAmt;
	
	@Column("DEBIT_AMT")
	private BigDecimal debitAmt;

	@Column("CREATED_BY")
	private String createdBy;
	
	@Column("CREATED_DT")
	private LocalDateTime createdDt;
	
	@Column("UPDATED_BY")
	private String updatedBy;
	
	@Column("UPDATED_DT")
	private LocalDateTime updatedDt;

	@MappedCollection(idColumn = "CUST_ID")
	private Set<CustomerOrder> orders;
	
	public void addOrder(CustomerOrder order) {
		if (order == null) {
			return;
		}
		if (orders == null) {
			orders = new HashSet<>();
		}
		orders.add(order);
	}
	
	public void resetOrder() {
		if (orders != null) {
			for (CustomerOrder bean : orders) {
				bean.setOrderId(null);
			}
		}
	}
	
	@Override
	public String getId() {
		return custId;
	}

	@Override
	public boolean isNew() {
		return (getMode() != null && getMode() == RowModes.NEW);
	}

}
