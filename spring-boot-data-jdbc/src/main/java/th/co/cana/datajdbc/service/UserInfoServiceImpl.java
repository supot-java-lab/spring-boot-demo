/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.datajdbc.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import th.co.cana.datajdbc.model.jdbc.UserInfo;
import th.co.cana.datajdbc.repository.UserInfoRepository;

/**
* @author supot
* @version 1.0
*/

@Service
@Transactional
public class UserInfoServiceImpl implements UserInfoService {

	@Autowired
	private UserInfoRepository userInfoRepository;
	
	@Override
	public UserInfo save(UserInfo model) {
		return userInfoRepository.save(model);
	}

	@Override
	public void update(UserInfo model) {
		userInfoRepository.save(model);
	}

	@Override
	public void delete(UserInfo model) {
		userInfoRepository.delete(model);
	}

	@Override
	public Optional<UserInfo> getUser(Long id) {
		return userInfoRepository.findById(id);
	}

	@Override
	public List<UserInfo> getAll() {
		return (List<UserInfo>) userInfoRepository.findAll();
	}

	@Override
	public List<UserInfo> search(String name) {
		return userInfoRepository.search(name);
	}

}
