/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.datajdbc.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import th.co.cana.datajdbc.criteria.Criteria;
import th.co.cana.datajdbc.criteria.IndexParameter;
import th.co.cana.datajdbc.criteria.NameParameter;
import th.co.cana.datajdbc.criteria.Parameter;
import th.co.cana.datajdbc.spring.Transformers;
import th.co.cana.utils.JdbcUtils;
import th.co.cana.utils.Validators;
import th.co.cana.utils.Values;

/**
 * @author supot
 * @version 1.0
 */
public class JdbcDao {
	protected Logger logger = LoggerFactory.getLogger(getClass());
	
	private JdbcTemplate jdbcTemplate;
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
		if (jdbcTemplate != null) {
			this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(jdbcTemplate);
		}
	}
	
	public NamedParameterJdbcTemplate getNamedParameterJdbcTemplate() {
		return namedParameterJdbcTemplate;
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}
	
	/*++++++++++++++++++ SQL -> List Java Bean ++++++++++++++++++ */
	protected <T> List<T> queryToList(String sql, Parameter params, Class<T> clazz) {
		logSqlStatement(sql, params, clazz);
		if (params instanceof NameParameter) {
			return getNamedParameterJdbcTemplate().query(sql, params.toSqlParameter(), Transformers.toBean(clazz));
		} else {
			return getJdbcTemplate().query(sql, toArrays(params), Transformers.toBean(clazz));
		}
	}
	
	/*++++++++++++++++++ SQL -> Java Bean ++++++++++++++++++ */
	protected <T> T queryToBean(String sql, Class<T> clazz) {
		return queryToBean(sql, new IndexParameter(0), clazz);
	}
	
	protected <T> T queryToBean(String sql, Parameter params, Class<T> clazz) {
		logSqlStatement(sql, params, clazz);
		try {
			if (params instanceof NameParameter) {
				return getNamedParameterJdbcTemplate().queryForObject(sql, params.toSqlParameter(),
						Transformers.toBean(clazz));
			} else {
				return getJdbcTemplate().queryForObject(sql, toArrays(params),
						Transformers.toBean(clazz));
			}
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	
	/*++++++++++++++++++ SQL -> Object ++++++++++++++++++ */
	protected <T> T queryToObject(String sql, Class<T> type) {
		return queryToObject(sql, new IndexParameter(0), type);
	}
	
	protected <T> T queryToObject(String sql, Parameter params, Class<T> type) {
		logSqlStatement(sql, params, type);
		try {
			if (params instanceof NameParameter) {
				return getNamedParameterJdbcTemplate().queryForObject(sql, params.toSqlParameter(), type);
			} else {
				return getJdbcTemplate().queryForObject(sql, toArrays(params), type);
			}
		} catch (EmptyResultDataAccessException ex) {
			return null;
		}
	}
	
	/*++++++++++++++++++ SQL -> Custom Extractor ++++++++++++++++++ */
	protected <T> T query(String sql, ResultSetExtractor<T> rse) {
		return query(sql, new IndexParameter(0), rse);
	}
	
	protected <T> T query(String sql, Parameter params, ResultSetExtractor<T> rse) {
		logSqlStatement(sql, params);
		if (params instanceof NameParameter) {
			return getNamedParameterJdbcTemplate().query(sql, params.toSqlParameter(), rse);
		} else {
			return getJdbcTemplate().query(sql, toArrays(params), rse);
		}
	}
	
	/*++++++++++++++++++ PAGGING ++++++++++++++++++ */
	protected <T> List<T> queryForPaging(String sql, Criteria criteria, Class<T> clazz) {
		return queryForPaging(sql, new IndexParameter(), criteria, clazz);
	}

	protected <T> List<T> queryForPaging(String sql, Parameter params, 
			Criteria criteria, Class<T> clazz) {
		
		StringBuilder pageSql = new StringBuilder();
		if (isMySql()) {
			pageSql.append("SELECT * FROM (").append(sql);
			pageSql.append(") TB");
		} else if (isMSSql()) {
			pageSql.append("SELECT * FROM (").append(sql);
			pageSql.append(" ) TB");
			if (Validators.isEmpty(criteria.getOrderByColumn())) {
				pageSql.append(" ORDER BY 1");
			} else {
				pageSql.append(" ORDER BY ").append(criteria.getOrderByColumn());
			}
		} else {
			pageSql.append("SELECT TB2.* FROM (");
			pageSql.append("SELECT TB1.*, ROWNUM AS ROW_NUM FROM (");
			pageSql.append(sql);
			pageSql.append(") TB1");
			pageSql.append(") TB2");
		}
		setPagingOption(pageSql, params, criteria);

		return queryToList(pageSql.toString(), params, clazz);
	}

	protected Long countForPaging(String sql, Parameter params) {
		StringBuilder countSql = new StringBuilder();
		countSql.append("SELECT COUNT(*) AS TOTAL FROM (");
		countSql.append(sql);
		countSql.append(") TB");

		Number value = queryToNumber(countSql.toString(), params);
		if (Validators.isNull(value)) {
			return 0L;
		}

		return value.longValue();
	}
	
	private void setPagingOption(StringBuilder sql, Parameter params, Criteria paging) {
		if (Validators.isNull(paging) || paging.isNullPaging()) {
			return;
		}

		if (isMySql()) {
			this.setMySqlPaging(sql, params, paging);
		} else if (isMSSql()) {
			this.setMSSqlPaging(sql, params, paging);
		} else {
			this.setOraclePaging(sql, params, paging);
		}
	}
	
	private void setMySqlPaging(StringBuilder sql, Parameter params, Criteria paging) {
		if (params instanceof NameParameter) {
			NameParameter name = (NameParameter) params;
			sql.append(" LIMIT :P_ROW_START, :P_ROW_TOTAL");
			name.add("P_ROW_START", paging.getMySqlOffset());
			name.add("P_ROW_TOTAL", paging.getPageSize());
		} else {
			IndexParameter inx = (IndexParameter) params;
			sql.append(" LIMIT ?, ?");
			inx.add(paging.getMySqlOffset());
			inx.add(paging.getPageSize());
		}
	}
	
	private void setMSSqlPaging(StringBuilder sql, Parameter params, Criteria paging) {
		if (params instanceof NameParameter) {
			NameParameter name = (NameParameter) params;
			sql.append(" OFFSET :P_ROW_START ROWS FETCH NEXT :P_ROW_TOTAL ROWS ONLY");
			name.add("P_ROW_START", paging.getMsSqlOffset());
			name.add("P_ROW_TOTAL", paging.getPageSize());
		} else {
			IndexParameter inx = (IndexParameter) params;
			sql.append(" OFFSET ? ROWS FETCH NEXT ? ROWS ONLY");
			inx.add(paging.getMsSqlOffset());
			inx.add(paging.getPageSize());
		}
	}
	
	private void setOraclePaging(StringBuilder sql, Parameter params, Criteria paging) {
		if (params instanceof NameParameter) {
			NameParameter name = (NameParameter) params;
			sql.append(" WHERE ROW_NUM BETWEEN :P_XR_ST AND :P_XR_END");
			name.add("P_XR_ST", paging.getOracleRowStart());
			name.add("P_XR_END", paging.getOracleRowEnd());
		} else {
			IndexParameter inx = (IndexParameter) params;
			sql.append(" WHERE ROW_NUM BETWEEN ? AND ?");
			inx.add(paging.getOracleRowStart());
			inx.add(paging.getOracleRowEnd());
		}
	}
	
	/*++++++++++++++++++ SQL -> Number ++++++++++++++++++ */
	public Number queryToNumber(String sql) {
		return queryToObject(sql, new IndexParameter(0), Number.class);
	}
	
	public Number queryToNumber(String sql, Parameter params) {
		return queryToObject(sql, params, Number.class);
	}
	
	protected void closeResource(Connection conn, Statement stmt, ResultSet rs) {
		closeResource(rs);
		closeResource(stmt);
		closeResource(conn);
	}
	
	protected void closeResource(Statement stmt, ResultSet rs) {
		closeResource(rs);
		closeResource(stmt);
	}
			
	protected void closeResource(AutoCloseable obj) {
		try {
			if (obj != null) {
				obj.close();
			}
		} catch (Exception ex) {
			logger.error(ex.getMessage());
		}
	}
	
	public Connection getConnection() {
		try {
			return getJdbcTemplate().getDataSource().getConnection();
		} catch (SQLException ex) {
			logger.error("getConnection", ex);
		}
		
		return null;
	}
	
	/*++++++++++++++++++ Execute update/insert/delete ++++++++++++++++++ */
	public int execute(String sql) {
		return execute(sql, new IndexParameter(0));
	}
	
	public int execute(String sql, Parameter params) {
		logSqlStatement(sql, params);
		if (params instanceof NameParameter) {
			return getNamedParameterJdbcTemplate().update(sql, params.toSqlParameter());
		} else {
			return getJdbcTemplate().update(sql, toArrays(params));
		}
	}
	
	public int execute(String sql, Object ... params) {
		logSqlStatement(sql, params);
		if (Validators.isEmpty(params)) {
			return getJdbcTemplate().update(sql);
		} else {
			return getJdbcTemplate().update(sql, params);
		}
	}
	
	public int deleteAll(final String table) {
		if (Validators.isNull(table)) {
			return 0;
		}
		
		String sql = "DELETE FROM " + table;
		return execute(sql);
	}
	
	public String sqlFullLike(String value) {
		return JdbcUtils.sqlFullLike(value);
	}
	
	public String sqlStartLike(String value) {
		return JdbcUtils.sqlStartLike(value);
	}
	
	public String sqlEndLike(String value) {
		return JdbcUtils.sqlEndLike(value);
	}
	
	public String createNumberWhereIn(Collection<Number> items) {
		if (Validators.isEmpty(items)) {
			return Values.EMPTY;
		}
		
		StringBuilder sql = new StringBuilder(256);
		boolean first = true;
		for (Number obj : items) {
			if (Validators.isNull(obj)) {
				continue;
			}
			
			if (first) {
				sql.append(obj);
				first = false;
			} else {
				sql.append(",");
				sql.append(obj);
			}
		}
		
		return sql.toString();
	}
	
	public String createNumberWhereIn(Number ... items) {
		if (Validators.isEmpty(items)) {
			return Values.EMPTY;
		}
		
		StringBuilder sql = new StringBuilder(256);
		boolean first = true;
		for (Number obj : items) {
			if (Validators.isNull(obj)) {
				continue;
			}
			
			if (first) {
				sql.append(obj);
				first = false;
			} else {
				sql.append(",");
				sql.append(obj);

			}
		}
		
		return sql.toString();
	}
	
	public String createWhereIn(String ... items) {
		if (Validators.isEmpty(items)) {
			return Values.EMPTY;
		}
		
		StringBuilder sql = new StringBuilder(256);
		boolean first = true;
		for (String obj : items) {
			if (Validators.isNull(obj)) {
				continue;
			}
			
			if (first) {
				sql.append("'");
				sql.append(obj);
				sql.append("'");
				first = false;
			} else {
				sql.append(",");
				sql.append("'");
				sql.append(obj);
				sql.append("'");
			}
		}
		
		return sql.toString();
	}
	
	public String createWhereIn(List<?> items) {
		if (Validators.isEmpty(items)) {
			return Values.EMPTY;
		}
		
		StringBuilder sql = new StringBuilder(256);
		boolean first = true;
		for (Object obj : items) {
			if (Validators.isNull(obj)) {
				continue;
			}
			
			if (first) {
				sql.append("'");
				sql.append(String.valueOf(obj));
				sql.append("'");
				first = false;
			} else {
				sql.append(",");
				sql.append("'");
				sql.append(String.valueOf(obj));
				sql.append("'");
			}
		}
		
		return sql.toString();
	}
	
	public String createWhereIn(Collection<?> items, IndexParameter params) {
		if (Validators.isEmpty(items)) {
			return Values.EMPTY;
		}
		
		StringBuilder sql = new StringBuilder(256);
		boolean first = true;
		for (Object obj : items) {
			if (first) {
				sql.append("?");
				first = false;
			} else {
				sql.append(", ?");
			}
			params.add(obj);
		}
		
		return sql.toString();
	}
	
	public String createWhereIn(Collection<?> items, NameParameter params) {
		return createWhereIn(items, params, "IN");
	}
	
	public String createWhereIn(Collection<?> items, NameParameter params, String prefix) {
		if (Validators.isEmpty(items)) {
			return Values.EMPTY;
		}
		prefix = (Validators.isEmpty(prefix) ? "X" : prefix);

		StringBuilder sql = new StringBuilder(256);
		int inx = 1;
		for (Object obj : items) {
			String paramName = "P_" + prefix + "_PARAM_" + inx;
			if (inx == 1) {
				sql.append(":").append(paramName);
			} else {
				sql.append(", ").append(":").append(paramName);
			}
			params.add(paramName, obj);
			inx++;
		}

		return sql.toString();
	}

	public String createWhereIn(Object[] items, NameParameter params) {
		if (Validators.isEmpty(items)) {
			return Values.EMPTY;
		}

		StringBuilder sql = new StringBuilder(256);
		int inx = 1;
		for (Object obj : items) {
			String paramName = "P_IN_PARAM_" + inx;
			if (inx == 1) {
				sql.append(":" + paramName);
			} else {
				sql.append(", ");
				sql.append(":" + paramName);
			}
			params.add(paramName, obj);
			inx++;
		}

		return sql.toString();
	}
	
	private Object[] toArrays(Parameter params) {
		if (Validators.isNull(params)) {
			return new Object[] {};
		}
		return params.toArrayParameter();
	}
	
	private boolean isMySql() {
		try {
			Connection conn = getJdbcTemplate().getDataSource().getConnection();
			return JdbcUtils.isMySql(conn);
		} catch (SQLException ex) {
			logger.error("isMySql : {}", ex.getMessage());
		}
		
		return false;
	}
	
	private boolean isMSSql() {
		try {
			Connection conn = getJdbcTemplate().getDataSource().getConnection();
			return JdbcUtils.isMsSql(conn);
		} catch (SQLException ex) {
			logger.error("isMySql : {}", ex.getMessage());
		}
		
		return false;
	}
	
	/*++++++++++++++++++ SQL statement log ++++++++++++++++++ */
	private void logSqlStatement(String sql, Parameter params) {
		logSqlStatement(sql, params, null);
	}
	
	private void logSqlStatement(String sql, Parameter params, Class<?> clazz) {
		if (logger.isDebugEnabled()) {
			logger.debug("SQL Statement => {}", sql);
			logger.debug("SQL Parameter => {}", params);
			if (clazz != null) {
				logger.debug("Binding Class => {}", clazz.getName());
			}
		}
	}

	private void logSqlStatement(String sql, Object ... params) {
		if (logger.isDebugEnabled()) {
			logger.debug("SQL Statement => {}", sql);
			logger.debug("SQL Parameter => {}", Arrays.toString(params));
		}
	}
}
