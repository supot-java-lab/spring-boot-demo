/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.datajdbc.criteria;

import java.sql.Types;

/**
 * @author Supot Saelao
 * @version 1.0
 */
public enum ParamTypes {
	VARCHAR(Types.VARCHAR),
	CHAR(Types.CHAR),
	NUMERIC(Types.NUMERIC),
	DECIMAL(Types.DECIMAL),
	INTEGER(Types.INTEGER),
	DATE(Types.DATE),
	TIMESTAMP(Types.TIMESTAMP),
	BLOB(Types.BLOB),
	CLOB(Types.CLOB),
	NULL(Types.NULL),
	OTHER(Types.OTHER),
	CURSOR(-10);
	
	private int value;
	
	private ParamTypes(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}
	
	public static ParamTypes getSqlTypes(int value) {
		for (ParamTypes type : ParamTypes.values()) {
			if (value == type.getValue()) {
				return type;
			}
		}

		return ParamTypes.OTHER;
	}
}