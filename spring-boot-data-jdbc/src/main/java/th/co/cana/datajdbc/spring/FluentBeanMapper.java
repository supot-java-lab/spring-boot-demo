/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.datajdbc.spring;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.util.Assert;

import th.co.cana.utils.ClassUtils;
import th.co.cana.utils.Convertors;
import th.co.cana.utils.DateFormats;
import th.co.cana.utils.JdbcUtils;
import th.co.cana.utils.JdbcUtils.ColumnInfo;

/**
* @author Supot Saelao
* @version 1.0
*/
public class FluentBeanMapper<T> implements RowMapper<T> {
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	private Class<T> classz;
	private Map<String, NestedSetter> setters;
	private List<ColumnInfo> columns;
	
	public FluentBeanMapper(Class<T> classz) {
		this.classz = classz;
	}
	
	@Override
	public T mapRow(ResultSet rs, int rowNumber) throws SQLException {
		Assert.state(this.classz != null, "Mapped class was not specified");		
		this.createCachedSetters(rs);
		
		T result = BeanUtils.instantiateClass(classz);
		for (ColumnInfo col : columns) {
			NestedSetter setter = setters.get(col.getColumn());			
			Object value = JdbcUtils.getResultSetValue(rs, col.getColumn(), col.getType());
			if (setter == null || setter.getPropertyType() == null || value == null) {
				continue;
			}
			
			Class<?> propertyType = setter.getPropertyType();
			Class<?> resultType = value.getClass();
			
			if (logger.isDebugEnabled() && rowNumber == 0) {
				logger.debug("Mapping column '{}' to property '{}' of type {} from type {}", col.getColumn()
						, setter.getName(), propertyType, resultType);
			}
			
			if (propertyType.equals(resultType)) {
				setter.set(result, value);
			} else {
				if (ClassUtils.isString(propertyType)) {
					setter.set(result, value.toString());
				} else {
					setter.set(result, convertByType(propertyType, value));
				}
			}
		}

		return result;
	}
	
	private void createCachedSetters(ResultSet rs) throws SQLException {
		if (columns == null || setters == null) {
			ResultSetMetaData rsmd = rs.getMetaData();
			columns = JdbcUtils.getColumInfo(rsmd);			
			if (setters == null) {
				setters = createSetters(classz);
			}
		}
	}
	
	private Map<String, NestedSetter> createSetters(Class<?> resultClass) {
		Map<String, NestedSetter> result = new HashMap<>(columns.size());
		for (ColumnInfo col : columns) {
			NestedSetter setter = NestedSetter.create(resultClass, col.getColumn());
			if (setter != null) {
				result.put(col.getColumn(), setter);
			}
		}
		
		return result;
	}
	
    private Object convertByType(Class<?> propertyType, Object value) {
		if (propertyType.equals(BigDecimal.class)) {
			return Convertors.toBigDecimal(value);
		} else if (propertyType.equals(BigInteger.class)) {
			return Convertors.toBigInteger(value);
		} else if (propertyType.equals(Long.class)) {
			return Convertors.toLong(value);
		} else if (propertyType.equals(Integer.class)) {
			return Convertors.toInteger(value);
		} else if (propertyType.equals(Double.class)) {
			return Convertors.toDouble(value);
		} else if (propertyType.equals(Float.class)) {
			return Convertors.toFloat(value);
		} else if (propertyType.equals(Short.class)) {
			return Convertors.toShort(value);
		} else if (propertyType.equals(LocalDate.class)) {
			return DateFormats.localDate(value);
		} else if (propertyType.equals(LocalDateTime.class)) {
			return DateFormats.localDateTime(value);
		} else if (propertyType.equals(LocalTime.class)) {
			return DateFormats.time(value);
		} else if (propertyType.equals(Date.class)) {
			return DateFormats.date(value);
		} else {
			if (value instanceof Clob) {
				return JdbcUtils.readClob((Clob) value);
			} else if (value instanceof Blob) {
				return JdbcUtils.toByte((Blob) value);
			}

			return value;
		}
	}
}
