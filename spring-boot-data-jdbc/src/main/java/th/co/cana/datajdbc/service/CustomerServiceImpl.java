/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.datajdbc.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import th.co.cana.datajdbc.model.jdbc.Customer;
import th.co.cana.datajdbc.model.jdbc.RowModes;
import th.co.cana.datajdbc.repository.CustomerRepository;

/**
* @author supot
* @version 1.0
*/

@Service
@Transactional
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerRepository customerRepository;
	
	@Override
	public Customer save(Customer model) {
		model.setMode(RowModes.NEW);
		model.resetOrder();
		return customerRepository.save(model);
	}

	@Override
	public void update(Customer model) {
		model.setMode(null);
		model.resetOrder();
		customerRepository.save(model);
	}

	@Override
	public void delete(Customer model) {
		customerRepository.delete(model);
	}

	@Override
	public Optional<Customer> getCustomer(String id) {
		return customerRepository.findById(id);
	}

	@Override
	public List<Customer> getAll() {
		return (List<Customer>) customerRepository.findAll();
	}

}
