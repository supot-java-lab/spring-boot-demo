/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.datajdbc.criteria;

import java.io.Serializable;

/**
 * @author supot
 * @version 1.0
 */
public class Criteria implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer page;
	private Integer pageSize;
	private String orderByColumn;
	
	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public String getOrderByColumn() {
		return orderByColumn;
	}

	public void setOrderByColumn(String orderByColumn) {
		this.orderByColumn = orderByColumn;
	}

	public int getMySqlOffset() {
		return getRowStart();
	}
	
	public int getMsSqlOffset() {
		return getMySqlOffset();
	}
	
	public Integer getOracleRowStart() {
		return getRowStart() + 1;
	}
	
	public Integer getOracleRowEnd() {	
		return getRowStart() + pageSize.intValue();
	}
	
	public boolean isNullPaging() {
		return (pageSize == null) || (page == null);
	}
	
	public boolean isNotNullPaging() {
		return !isNullPaging();
	}
	
	private int getRowStart() {
		if (page.intValue() <= 0) {
			page = 1;
		}
		return (page.intValue() - 1) * pageSize;
	}
}
