/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.datajdbc.service;

import java.util.List;
import java.util.Optional;

import th.co.cana.datajdbc.model.jdbc.Customer;

/**
* @author supot
* @version 1.0
*/
public interface CustomerService {
	Customer save(Customer model);
	
	void update(Customer model);
	
	void delete(Customer model);
	
	Optional<Customer> getCustomer(String id);
	
	List<Customer> getAll();
}
