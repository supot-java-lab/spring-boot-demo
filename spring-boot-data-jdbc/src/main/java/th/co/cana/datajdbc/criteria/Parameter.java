/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.datajdbc.criteria;

import java.util.Map;

import org.springframework.jdbc.core.namedparam.SqlParameterSource;

/**
* @author Supot Saelao
* @version 1.0
*/
public interface Parameter {

	void clearParameters();
	
	Map<String, Object> toMapParameter();
	
	SqlParameterSource toSqlParameter();
	
	Object[] toArrayParameter();
}
