/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.datajdbc.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import th.co.cana.datajdbc.model.jdbc.Customer;

/**
* @author supot
* @version 1.0
*/

@Repository
public interface CustomerRepository extends CrudRepository<Customer, String> {

}
