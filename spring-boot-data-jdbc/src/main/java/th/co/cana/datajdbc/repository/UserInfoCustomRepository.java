/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.datajdbc.repository;

import java.util.List;

import th.co.cana.datajdbc.model.jdbc.UserInfo;

/**
* @author supot
* @version 1.0
*/
public interface UserInfoCustomRepository {

	List<UserInfo> search(String name);
}
