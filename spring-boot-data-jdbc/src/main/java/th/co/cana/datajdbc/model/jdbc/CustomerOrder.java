/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.datajdbc.model.jdbc;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
* @author supot
* @version 1.0
*/

@Data
@EqualsAndHashCode(callSuper = false)
@Table("CUSTOMER_ORDER")
public class CustomerOrder extends JdbcModel {
	private static final long serialVersionUID = 1L;
	
    @Id
    @Column("ORDER_ID")
    private Long orderId;
    
	@Column("CUST_ID")
	private String custId;
    
    @Column("PRODUCT_NAME")
    private String productName;
    
    @Column("QTY")
    private BigDecimal qty;
    
    @Column("PRICE")
    private BigDecimal price;
    
    @Column("DISCOUNT")
    private BigDecimal discount;
    
	@Column("REMARK")
	private String remark;
	
	@Column("CREATED_BY")
	private String createdBy;
	
	@Column("CREATED_DT")
	private LocalDateTime createdDt;
}
