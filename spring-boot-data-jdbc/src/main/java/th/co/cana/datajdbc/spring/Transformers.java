/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.datajdbc.spring;

import org.springframework.jdbc.core.RowMapper;

/**
 * JDBC Transformer utility class for manage transformer result
 * @author Supot Saelao
 * @version 1.0
 */
public final class Transformers {

	private Transformers() {
	}
	
	/**
	 * Transformer query result to POJO (Java Bean)
	 * @param clazz The target class for transformer
	 * @return
	 */
	public static <T> RowMapper<T> toBean(Class<T> clazz) {
		return new FluentBeanMapper<>(clazz);
	}
	
}
