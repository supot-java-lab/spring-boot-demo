/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.datajdbc.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import th.co.cana.datajdbc.model.jdbc.Customer;
import th.co.cana.datajdbc.service.CustomerService;
import th.co.cana.utils.Validators;

/**
* @author supot
* @version 1.0
*/

@RestController
@RequestMapping("/v1/customers")
public class CustomerCtrl extends Controller {

	@Autowired
	private CustomerService customerServiceImpl;

	@GetMapping
	public List<Customer> getCustomers() {
		return customerServiceImpl.getAll();
	}

	@GetMapping("/{id}")
	public ResponseEntity<Customer> getCustomer(@PathVariable String id) {
		Optional<Customer> customer = customerServiceImpl.getCustomer(id);
		if (!customer.isPresent()) {
			return ResponseEntity.badRequest().build();
		}
		return ResponseEntity.ok().body(customer.get());
	}

	@PostMapping()
	public ResponseEntity<Customer> create(@RequestBody Customer body) {
		logger.info("{}", body);		
		Customer customer = customerServiceImpl.save(body);
		if (Validators.isNotNull(customer)) {
			customer.setMode(null);
		}
		return ResponseEntity.status(HttpStatus.CREATED).body(customer);
	}
}
