/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.datajdbc.model.jdbc;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import lombok.Data;
import lombok.ToString;

/**
 * @author supot
 * @version 1.0
 */

@Data
@ToString(exclude = {"password"})
@Table("USER_INFO")
public class UserInfo implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column("USER_ID")
	private Long userId;
	
	@Column("USER_NAME")
	private String userName;
	
	@Column("PASSWORD")
	private String password;
	
	@Column("FIRST_NAME")
	private String firstName;
	
	@Column("LAST_NAME")
	private String lastName;
	
	@Column("REGISTER_DT")
	private LocalDate registerDt;
	
	@Column("REGISTER_TIME")
	private LocalTime registerTime;
	
	@Column("CREATED_BY")
	private String createdBy;
	
	@Column("CREATED_DT")
	private LocalDateTime createdDt;
	
	@Column("UPDATED_BY")
	private String updatedBy;
	
	@Column("UPDATED_DT")
	private LocalDateTime updatedDt;

	public String getFullName() {
		StringBuilder sb = new StringBuilder();
		if (firstName != null) {
			sb.append(firstName);
		}
		if (lastName != null) {
			if (firstName != null) {
				sb.append(" ").append(lastName);
			} else {
				sb.append(lastName);
			}
		}

		return sb.toString();
	}
}
