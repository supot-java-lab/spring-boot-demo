/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.datajdbc.model.jdbc;

import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Column;

/**
* @author supot
* @version 1.0
*/
public class JdbcModelId extends JdbcModel implements Persistable<String> {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column("ID")
	private String id;
	
	@Override
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public boolean isNew() {
		return (getMode() != null && getMode() == RowModes.NEW);
	}
}
