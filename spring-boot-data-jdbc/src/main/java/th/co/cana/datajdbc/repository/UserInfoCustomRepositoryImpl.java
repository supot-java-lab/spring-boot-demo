/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.datajdbc.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import th.co.cana.datajdbc.criteria.NameParameter;
import th.co.cana.datajdbc.dao.JdbcDao;
import th.co.cana.datajdbc.model.jdbc.UserInfo;

/**
* @author supot
* @version 1.0
*/

@Repository
public class UserInfoCustomRepositoryImpl extends JdbcDao implements UserInfoCustomRepository {

	@Autowired
	public void setJdbcActiveTemplate(JdbcTemplate jdbcTemplate) {
		setJdbcTemplate(jdbcTemplate);
	}
	
	@Override
	public List<UserInfo> search(String name) {
		StringBuilder sql = new StringBuilder();
		NameParameter params = new NameParameter(1);
		sql.append("SELECT USER_ID")
			.append(", USER_NAME")
			.append(", FIRST_NAME")
			.append(", LAST_NAME")
			.append(", REGISTER_DT")
			.append(", REGISTER_TIME")
			.append(", CREATED_DT");
		sql.append(" FROM USER_INFO");
		sql.append(" WHERE FIRST_NAME LIKE :P_NAME OR LAST_NAME LIKE :P_NAME");
		
		params.add("P_NAME", sqlFullLike(name));
		
		return queryToList(sql.toString(), params, UserInfo.class);
	}

}
