/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.datajdbc;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import th.co.cana.datajdbc.model.jdbc.Customer;
import th.co.cana.datajdbc.model.jdbc.CustomerOrder;
import th.co.cana.datajdbc.service.CustomerService;
import th.co.cana.utils.DateUtils;
import th.co.cana.utils.Utils;

/**
* @author supot
* @version 1.0
*/
class TestCustomerService extends BaseUnitTest {

	@Autowired
	private CustomerService customerServiceImpl;
	
	@Test
	//@Disabled
	void test() {
		logger.info("1. Insert record");
		Customer model = new Customer();
		model.setCustId(Utils.getUUID());
		model.setFirstName("Supot");
		model.setLastName("Saelalo");
		model.setBirthDate(LocalDate.of(1982, 2, 17));
		model.setRegisterDate(DateUtils.getDate());
		model.setCreditAmt(BigDecimal.valueOf(250000D));
		model.setDebitAmt(BigDecimal.ZERO);
		model.setCreatedBy("ADMIN");
		model.setCreatedDt(LocalDateTime.now());
		
		CustomerOrder order = new CustomerOrder();
		order.setCreatedBy("ADMIN");
		order.setCreatedDt(LocalDateTime.now());
		order.setPrice(BigDecimal.valueOf(250.45D));
		order.setQty(BigDecimal.valueOf(12.3D));
		order.setDiscount(BigDecimal.ZERO);
		order.setProductName("Iphone demo 1");
		order.setRemark("Test");		
		model.addOrder(order);
		
		order = new CustomerOrder();
		order.setCreatedBy("ADMIN");
		order.setCreatedDt(LocalDateTime.now());
		order.setPrice(BigDecimal.valueOf(250.45D));
		order.setQty(BigDecimal.valueOf(12.3D));
		order.setDiscount(BigDecimal.ZERO);
		order.setProductName("Iphone demo 2");
		order.setRemark("Test");
		model.addOrder(order);
		
		customerServiceImpl.save(model);		
		assertNotNull(model.getCustId());
		
		logger.info("2. Update record");
		order = new CustomerOrder();
		order.setCreatedBy("ADMIN");
		order.setCreatedDt(LocalDateTime.now());
		order.setPrice(BigDecimal.valueOf(250.45D));
		order.setQty(BigDecimal.valueOf(12.3D));
		order.setDiscount(BigDecimal.ZERO);
		order.setProductName("Iphone demo 3");
		order.setRemark("Test");
		model.addOrder(order);
		
		customerServiceImpl.update(model);
		
		logger.info("3. Get record");
		Optional<Customer> opCust = customerServiceImpl.getCustomer(model.getCustId());
		if (opCust.isPresent()) {
			model = opCust.get();
			System.out.println(model);
		}
		
		logger.info("4. Get all record");
		List<Customer> items = customerServiceImpl.getAll();
		for (Customer bean : items) {
			System.out.println(model);
		}
	}
	
	@Test
	@Disabled
	void testUpdate() {
		logger.info("1. Update record");
		Customer model = new Customer();
		model.setCustId("52ABC8DF-C628-4715-9506-1A40BCBC6054");
		model.setFirstName("Supot1");
		model.setLastName("Saelalo1");
		model.setBirthDate(LocalDate.of(1982, 2, 17));
		model.setRegisterDate(DateUtils.getDate());
		model.setCreditAmt(BigDecimal.valueOf(250000D));
		model.setDebitAmt(BigDecimal.ZERO);
		model.setCreatedBy("ADMIN");
		model.setCreatedDt(LocalDateTime.now());
		
		customerServiceImpl.update(model);
		
		assertNotNull(model.getCustId());
	}
}
