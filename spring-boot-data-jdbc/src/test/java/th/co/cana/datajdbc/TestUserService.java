/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.datajdbc;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import th.co.cana.datajdbc.model.jdbc.UserInfo;
import th.co.cana.datajdbc.service.UserInfoService;

/**
* @author supot
* @version 1.0
*/
class TestUserService extends BaseUnitTest {
	
	@Autowired
	private UserInfoService userInfoServiceImpl;

	@Test
	void testUserInfo() {
		UserInfo model = new UserInfo();
		model.setUserName("supot");
		model.setPassword("password");
		model.setFirstName("Supot");
		model.setLastName("Saelao");
		model.setRegisterDt(LocalDate.now());
		model.setRegisterTime(LocalTime.now());
		model.setCreatedBy("ADMIN");
		model.setCreatedDt(LocalDateTime.now());
		
		logger.info("1. Insert new user Info");
		userInfoServiceImpl.save(model);
		
		assertNotNull(model.getUserId());
		
		logger.info("2. Get user Info");
		Optional<UserInfo> user = userInfoServiceImpl.getUser(model.getUserId());
		assertEquals(user.get().getUserId(), model.getUserId());
		
		logger.info("3. Get all user Info");
		List<UserInfo> users = userInfoServiceImpl.getAll();
		assertTrue(users != null && !users.isEmpty());
		
		for (UserInfo bean : users) {
			logger.info("{}", bean);
		}
		
		logger.info("4. Search user by name");
		users = userInfoServiceImpl.search("Supot");
		assertTrue(users != null && !users.isEmpty());		
		for (UserInfo bean : users) {
			logger.info("{}", bean);
		}
	}

}
