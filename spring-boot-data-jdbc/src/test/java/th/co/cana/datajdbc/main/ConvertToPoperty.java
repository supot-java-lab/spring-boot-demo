/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/
package th.co.cana.datajdbc.main;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import th.co.cana.utils.IOUtils;
import th.co.cana.utils.Validators;

/**
 * @author Supot Saelao
 * @version 1.0
 */
public class ConvertToPoperty {
	private static final Map<String, String> JAVA_TYPES;
	static {
		JAVA_TYPES = new HashMap<>(15);
		JAVA_TYPES.put("VARCHAR2", "String");
		JAVA_TYPES.put("NVARCHAR2", "String");
		JAVA_TYPES.put("VARCHAR", "String");
		JAVA_TYPES.put("CHAR", "String");
		JAVA_TYPES.put("NCHAR", "String");
		JAVA_TYPES.put("LONG", "String");
		JAVA_TYPES.put("CLOB", "String");
		JAVA_TYPES.put("NCLOB", "String");
		JAVA_TYPES.put("NUMBER", "BigDecimal");
		JAVA_TYPES.put("DATE", "Date");
		JAVA_TYPES.put("TIMESTAMP", "Date");
		JAVA_TYPES.put("BLOB", "byte[]");
	}
	
	public static void main(String[] args) throws IOException {
		List<String> datas = readFile("src/test/resources/property.txt");
		genProperty(datas);
	}
	
	public static void genProperty(List<String> datas) throws IOException {
        for (String str : datas) {
        	String[] pros = str.split("[|]");
            String pro = convert(pros[0]);
			if (pros.length > 1) {
            	System.out.println("private " +toJavaType(pros[1]) +" "+ pro + ";");
            } else {
            	System.out.println("private String " + pro + ";");
            }
        }	    
	}
	
    public static void setProperty(List<String> datas) throws IOException {
        for (String str : datas) {
            String pro = convertToSet(str);
            System.out.println("report.set" + pro + "(rs.getString(\"" + str + "\"));");
        }
    }
	   
	private static String convert(String val) {
		StringBuilder sb = new StringBuilder(56);
		String[] datas = val.toLowerCase().split("_");
		
		for (int r = 0, rSize = datas.length; r < rSize; r++) {
			if(r == 0) {
				sb.append(datas[r]);
			} else {
				char[] chars = datas[r].toCharArray();
				for (int i = 0, size = chars.length; i < size; i++) {
					if (i == 0) {
						sb.append(String.valueOf(chars[i]).toUpperCase());
					} else {
						sb.append(chars[i]);
					}
				}
			}
		}
		
		return sb.toString();
	}
	
    private static String convertToSet(String val) {
        StringBuilder sb = new StringBuilder(56);
        String[] datas = val.toLowerCase().split("_");

        for (int r = 0, rSize = datas.length; r < rSize; r++) {
            char[] chars = datas[r].toCharArray();
            for (int i = 0, size = chars.length; i < size; i++) {
                if (i == 0) {
                    sb.append(String.valueOf(chars[i]).toUpperCase());
                } else {
                    sb.append(chars[i]);
                }
            }
        }

        return sb.toString();
    }
	   
	private static List<String> readFile(String file) throws IOException {
		return IOUtils.readLines(new FileInputStream(file));
	}
	
	private static String toJavaType(String dbType) {
		String type = JAVA_TYPES.get(dbType);
		if (Validators.isEmpty(type)) {
			return "String";
		}

		return type;
	}
}

