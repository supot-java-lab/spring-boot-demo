/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by thaihealth.or.th All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.datajdbc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

/**
* @author supot
* @version 1.0
*/

@SpringBootTest(classes = DataJdbcApplication.class)
public class BaseUnitTest {
	protected Logger logger = LoggerFactory.getLogger(getClass());

}
