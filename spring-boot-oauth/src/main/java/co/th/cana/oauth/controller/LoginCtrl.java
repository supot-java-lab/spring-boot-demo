/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package co.th.cana.oauth.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.annotation.RegisteredOAuth2AuthorizedClient;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author supot
 * @version 1.0
 */

@RestController
public class LoginCtrl extends BaseController {
	
	@GetMapping("/")
	public ResponseEntity<OAuth2User> index(@RegisteredOAuth2AuthorizedClient OAuth2AuthorizedClient authorizedClient,
			@AuthenticationPrincipal OAuth2User oauth2User) {
		
		logger.info("getPrincipalName : {}", authorizedClient.getPrincipalName());
		logger.info("getRegistrationId : {}", authorizedClient.getClientRegistration().getRegistrationId());
		logger.info("getClientName : {}", authorizedClient.getClientRegistration().getClientName());
		logger.info("getAccessToken : {}", authorizedClient.getAccessToken().getTokenValue());
		logger.info("getRefreshToken : {}", authorizedClient.getRefreshToken());
		
		logger.info("Response : {}", oauth2User);
		
		return ResponseEntity.ok(oauth2User);
	}
}
