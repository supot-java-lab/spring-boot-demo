/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package co.th.cana.oauth.utils;

import java.util.Set;

/**
* @author supot
* @version 1.0
*/
public final class Auth2Utils {
	public static final String FACEBOOK 	= "facebook";
	public static final String GOOGLE 		= "google";
	public static final String GITHUB 		= "github";
	public static final String LINE 		= "line";
	
	private Auth2Utils() {}
	
	public static boolean isGoogle(Set<String> providers) {
		if (providers == null || providers.isEmpty()) {
			return false;
		}
		
		for (String provider : providers) {
			if (GOOGLE.equalsIgnoreCase(provider)) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean isFacebook(Set<String> providers) {
		if (providers == null || providers.isEmpty()) {
			return false;
		}
		
		for (String provider : providers) {
			if (FACEBOOK.equalsIgnoreCase(provider)) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean isGithub(Set<String> providers) {
		if (providers == null || providers.isEmpty()) {
			return false;
		}
		
		for (String provider : providers) {
			if (GITHUB.equalsIgnoreCase(provider)) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean isLine(Set<String> providers) {
		if (providers == null || providers.isEmpty()) {
			return false;
		}
		
		for (String provider : providers) {
			if (LINE.equalsIgnoreCase(provider)) {
				return true;
			}
		}
		return false;
	}
}
