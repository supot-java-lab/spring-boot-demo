/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package co.th.cana.oauth.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.oauth2.client.CommonOAuth2Provider;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.ClientRegistration.Builder;
import org.springframework.security.oauth2.client.registration.ClientRegistrationRepository;
import org.springframework.security.oauth2.client.registration.InMemoryClientRegistrationRepository;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.security.oauth2.core.ClientAuthenticationMethod;

import co.th.cana.oauth.property.CustomOAuthProperty;
import co.th.cana.oauth.utils.Auth2Utils;
import th.co.cana.utils.Validators;

/**
* @author supot
* @version 1.0
*/

@Configuration
public class SecurityOauth2LoginConfig {

	@Value("${oauth2.provider}")
	private Set<String> providers;
	
	@Bean
	public ClientRegistrationRepository clientRegistrationRepository() {
		List<ClientRegistration> registrations = new ArrayList<>(5);
		if (Auth2Utils.isGoogle(providers)) {
			registrations.add(googleRegistration());
		}
		
		if (Auth2Utils.isFacebook(providers)) {
			registrations.add(facebookRegistration());
		}
		
		if (Auth2Utils.isGithub(providers)) {
			registrations.add(githubRegistration());
		}
		
		if (Auth2Utils.isLine(providers)) {
			registrations.add(lineRegistration());
		}
		
		return new InMemoryClientRegistrationRepository(registrations);
	}
	
	@Bean
	@ConfigurationProperties(prefix = "oauth2.google")
	public CustomOAuthProperty google() {
		return new CustomOAuthProperty();
	}
	
	@Bean
	@ConfigurationProperties(prefix = "oauth2.facebook")
	public CustomOAuthProperty facebook() {
		return new CustomOAuthProperty();
	}
	
	@Bean
	@ConfigurationProperties(prefix = "oauth2.github")
	public CustomOAuthProperty github() {
		return new CustomOAuthProperty();
	}
	
	@Bean
	@ConfigurationProperties(prefix = "oauth2.line")
	public CustomOAuthProperty line() {
		return new CustomOAuthProperty();
	}
	
	private ClientRegistration lineRegistration() {
		Builder builder = ClientRegistration.withRegistrationId("line")
				.clientId(line().getClientId())
				.clientSecret(line().getClientSecret())
				.redirectUriTemplate(line().getRedirectUriTemplate())
				.scope(line().getScope())
				.clientAuthenticationMethod(ClientAuthenticationMethod.BASIC)
				.authorizationGrantType(AuthorizationGrantType.AUTHORIZATION_CODE)
				.authorizationUri(line().getAuthorizationUri())
				.tokenUri(line().getTokenUri())
				.userInfoUri(line().getUserInfoUri())
				.userNameAttributeName(line().getUserNameAttribute())
				.clientName(line().getClientName());
		
		if (Validators.isNotEmpty(line().getScope())) {
			builder.scope(line().getScope());
		} else {
			builder.scope("profile");
		}
		
		return builder.build();
	}

	private ClientRegistration googleRegistration() {
		Builder builder = CommonOAuth2Provider.GOOGLE.getBuilder("google");
		builder.clientId(google().getClientId())
			.clientSecret(google().getClientSecret());
		
		if (Validators.isNotEmpty(google().getScope())) {
			builder.scope(google().getScope());
		}
		return builder.build();
	}
	
	private ClientRegistration facebookRegistration() {
		Builder builder = CommonOAuth2Provider.FACEBOOK.getBuilder("facebook");
		builder.clientId(facebook().getClientId())
			.clientSecret(facebook().getClientSecret());
		
		if (Validators.isNotEmpty(facebook().getScope())) {
			builder.scope(facebook().getScope());
		}
		return builder.build();
	}
	
	private ClientRegistration githubRegistration() {
		Builder builder = CommonOAuth2Provider.GITHUB.getBuilder("github");
		builder.clientId(github().getClientId())
			.clientSecret(github().getClientSecret());
		
		if (Validators.isNotEmpty(github().getScope())) {
			builder.scope(github().getScope());
		}
		
		return builder.build();
	}
}
