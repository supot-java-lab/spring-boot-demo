/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package co.th.cana.oauth.property;

import org.springframework.boot.autoconfigure.security.oauth2.client.OAuth2ClientProperties.Registration;

/**
 * @author supot
 * @version 1.0
 */
public class CustomOAuthProperty extends Registration {
	private String authorizationUri;
	private String tokenUri;
	private String userInfoUri;
	private String userInfoAuthenticationMethod;
	private String userNameAttribute;
	private String jwkSetUri;
	private String issuerUri;
	private String redirectUriTemplate;
	
	public String getAuthorizationUri() {
		return authorizationUri;
	}

	public void setAuthorizationUri(String authorizationUri) {
		this.authorizationUri = authorizationUri;
	}

	public String getTokenUri() {
		return tokenUri;
	}

	public void setTokenUri(String tokenUri) {
		this.tokenUri = tokenUri;
	}

	public String getUserInfoUri() {
		return userInfoUri;
	}

	public void setUserInfoUri(String userInfoUri) {
		this.userInfoUri = userInfoUri;
	}

	public String getUserInfoAuthenticationMethod() {
		return userInfoAuthenticationMethod;
	}

	public void setUserInfoAuthenticationMethod(String userInfoAuthenticationMethod) {
		this.userInfoAuthenticationMethod = userInfoAuthenticationMethod;
	}

	public String getUserNameAttribute() {
		return userNameAttribute;
	}

	public void setUserNameAttribute(String userNameAttribute) {
		this.userNameAttribute = userNameAttribute;
	}

	public String getJwkSetUri() {
		return jwkSetUri;
	}

	public void setJwkSetUri(String jwkSetUri) {
		this.jwkSetUri = jwkSetUri;
	}

	public String getIssuerUri() {
		return issuerUri;
	}

	public void setIssuerUri(String issuerUri) {
		this.issuerUri = issuerUri;
	}

	public String getRedirectUriTemplate() {
		return redirectUriTemplate;
	}

	public void setRedirectUriTemplate(String redirectUriTemplate) {
		this.redirectUriTemplate = redirectUriTemplate;
	}

}
