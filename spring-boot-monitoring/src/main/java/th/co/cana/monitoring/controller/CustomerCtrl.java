/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.monitoring.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.micrometer.core.instrument.Metrics;
import th.co.cana.monitoring.model.CustomerModel;
import th.co.cana.monitoring.utils.ServiceUtils;
import th.co.cana.utils.Validators;

/**
* @author supot
* @version 1.0
*/
@RestController
@RequestMapping("/v1/customers")
public class CustomerCtrl extends Controller {

	@GetMapping
	public List<CustomerModel> getCustomers() {
		return ServiceUtils.getCustomers();
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<CustomerModel> getCustomer(@PathVariable("id") String id) {
		CustomerModel cust = ServiceUtils.getCustomer(id);
		if (Validators.isNull(cust)) {
			Metrics.counter("customer", "customerId", id, "state", "404");
			logger.warn("Customer id: {} not found", id);
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(cust);
	}
}
