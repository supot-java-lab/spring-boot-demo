/*
* -----------------------------------------------------------------------------------
* Copyright © 2020 by Cana enterprise co,.Ltd. All rights reserved.
* -----------------------------------------------------------------------------------
*/

package th.co.cana.monitoring.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import th.co.cana.monitoring.model.UserModel;
import th.co.cana.monitoring.utils.ServiceUtils;
import th.co.cana.utils.Validators;

/**
 * @author supot
 * @version 1.0
 */

@RestController
@RequestMapping("/v1/users")
public class UserCtrl extends Controller {

	@GetMapping
	public List<UserModel> getUsers() {
		return ServiceUtils.getUsers();
	}

	@GetMapping(value = "/{id}")
	public ResponseEntity<UserModel> getUserById(@PathVariable("id") String id) {
		UserModel user = ServiceUtils.getUser(id);
		if (Validators.isNull(user)) {
			logger.warn("User id: {} not found", id);
			return ResponseEntity.notFound().build();
		}

		return ResponseEntity.ok(user);
	}
}
