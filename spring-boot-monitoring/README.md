# Spring boot Actuator 
This is Spring Boot Actuators to monitor our metrics using InfluxDB

### The service end-point of actuator
- GET : /autoconfig Auto-configuration report
- GET: /configprops Configuration properties 
- GET: /beans Bean context and relationship
- GET: /dump Snapshot thread activity
- GET: /env Environment properties
- GET: /env/{name} Environment properties by name
- GET: /health Reports of health metrics application
- GET: /info Custom application information
- GET: /mappings URI paths mapped controllers
- GET: /metrics Memory usage and HTTP request counters
- GET: /metrics/{name} Metric by name
- GET: /trace Basic trace information

#### Some of end-point disabled because Sensitive by default
Disabled by add properties value

	management.security.enabled=false
or
	
	management.endpoints.web.exposure.include=health,prometheus,info,metrics,threaddump,heapdump
	