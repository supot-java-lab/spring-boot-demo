package th.co.cana.framework.config;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author supot.jdev
 * @version 1.0
 */

@Configuration
public class OpenApiDocumentConfig {

    @Bean
    public OpenAPI customerApi() {
        return new OpenAPI().info(createInfo()).components(components());
    }

    private Info createInfo() {
        return new Info().title("Demo Service API")
                .description("Demo service API for management customer information")
                .version("V1.0")
                .license(new License().name("Apache 2.0").url("https://springdoc.org"));
    }

    private Components components() {
        return new Components().addSecuritySchemes("bearer-key", new SecurityScheme()
                        .type(SecurityScheme.Type.HTTP)
                        .scheme("bearer").bearerFormat("JWT"));
    }
}
