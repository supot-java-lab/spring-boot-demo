package th.co.cana.framework.models;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

/**
 * @author supot.jdev
 * @version 1.0
 */
@Data
public class Customer {
    private Long customerId;
    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;
    private LocalDate birthDate;
    private BigDecimal creditAmt;
}
